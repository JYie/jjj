package com.triplej.service.restful;

import com.google.common.collect.Sets;

import javax.ws.rs.core.Application;
import java.util.Set;

/**
 * Created by kami on 2014. 7. 25..
 */
public class ServiceRegister extends Application {

    @Override
    public Set<Class<?>> getClasses(){
        Set<Class<?>> services = Sets.newHashSet();
        services.add(AccountActionService.class);
        return services;
    }
}
