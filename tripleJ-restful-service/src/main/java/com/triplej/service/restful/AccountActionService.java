package com.triplej.service.restful;

import com.google.common.collect.Maps;
import com.tripleJ.InitializeException;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;
import com.tripleJ.ds.account.Account;
import com.tripleJ.ds.account.AlreadyExistException;
import com.tripleJ.service.AccountAction;
import com.tripleJ.service.ServiceNotAvailableException;
import com.tripleJ.service.UserException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;

/**
 * Created by kami on 2014. 7. 25..
 */
@Path("/1.0/account_action/")
public class AccountActionService {

    private static AccountAction accountAction;
    private static ObjectMapper jsonEngine = new ObjectMapper();

    private static boolean initFlag = false;

    static{
        try {
            accountAction = new AccountAction();
            initFlag = true;
        } catch (InitializeException e) {

        }
    }

    @POST
    @Path(value="/{uid}")
    public Response registerAccount(@Context HttpServletRequest request,
                                    @PathParam(value = "uid") String uid,
                                    @QueryParam(value = "name") String name,
                                    @QueryParam(value = "provider") String provider,
                                    @QueryParam(value = "email") String email) {
        if(!initFlag)
            return Response.status(500).entity("Service is not established").build();
        try {
            Account account = new Account(uid, name, provider, email, null);
            String accountId = accountAction.registerAccount(uid, account);
            Map<String, String> map = Maps.newHashMap();
            map.put("accountId", accountId);
            return Response.status(200).type(MediaType.APPLICATION_JSON)
                    .entity(jsonEngine.writeValueAsString(map)).build();
        } catch (ServiceNotAvailableException | IOException sie) {
            return Response.status(500).build();
        } catch (MissingRequiredException e) {
            return Response.status(400).build();
        } catch (UserException e) {
            return Response.status(400).build();
        } catch (AlreadyExistException e) {
            return Response.status(400).build();
        }
    }
}
