package com.tripleJ.ds;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import java.net.UnknownHostException;
import java.util.Set;

/**
 * Created by kami on 2014. 7. 18..
 */
public class MongoSetupForTest {

    private static String url = "127.0.0.1";
    private static int port = 27017;

    private static DB db;

    public static void setupBeforeClass() throws UnknownHostException {

        ServerAddress address = new ServerAddress(url,port);

        MongoClient client = new MongoClient(address);
        db = client.getDB("tripleJ_test");
        Set<String> colls = db.getCollectionNames();

        if(null != colls && colls.size() > 0)
            db.dropDatabase();

        MongoProvider.registerDB(db);
    }

    public static void tearDownAfterClass(){
        db.dropDatabase();
    }

}
