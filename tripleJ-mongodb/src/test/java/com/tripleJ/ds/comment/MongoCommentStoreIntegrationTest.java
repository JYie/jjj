package com.tripleJ.ds.comment;

import com.google.common.collect.Sets;
import com.tripleJ.ds.MongoSetupForTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class MongoCommentStoreIntegrationTest {

    private static MongoCommentStore mcs;

    static{
        System.out.println("----- Start MongoCommentStoreIntegrationTest -----");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        MongoSetupForTest.setupBeforeClass();
        mcs = new MongoCommentStore();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        MongoSetupForTest.tearDownAfterClass();
    }

    @Test
    public void testSimpleCycle() throws Exception {

        String commentId1 = "testForAdding1";
        String author1 = "author1";
        String listener1 = "listener1";
        Comment comment1 = new Comment("Hi1");

        String commentId2 = "testForAdding2";
        Comment comment2 = new Comment("Hello");
        String author2 = "author2";

        String silentComment = "testForNarrowComment";
        Comment comment3 = new Comment("silent hi");

        Date created1 = Calendar.getInstance().getTime();

        mcs.addComment(commentId1, author1, listener1, comment1, created1, Scope.WIDE);
        mcs.addComment(silentComment, author1, listener1, comment3, created1, Scope.NARROW);

        Date created2 = Calendar.getInstance().getTime();
        mcs.addComment(commentId2, author2, listener1, comment2, created2, Scope.WIDE);

        System.out.printf("succeeds to add a comment having id : %s, %s, %s \n", commentId1, silentComment, comment2);

        System.out.println("isExists test1");
        assertTrue(mcs.isExist(commentId1) && mcs.isExist(silentComment) && mcs.isExist(commentId2));

        System.out.println("Recover test");
        List<CommentInformation> list = mcs.recoverByList(commentId1);
        System.out.println("expected size : 1");
        System.out.println("actual size : " + list.size());

        assertTrue(null != list && list.size() == 1);

        CommentInformation ci_recovered = list.get(0);
        System.out.println("expected id : " + commentId1);
        System.out.println("actual id : " + ci_recovered.getIdentifier());
        assertTrue(null != ci_recovered.getIdentifier() && ci_recovered.getIdentifier().compareTo(commentId1) == 0);

        System.out.println("expected author : " + author1);
        System.out.println("actual author : " + ci_recovered.getAuthor());
        assertTrue(null != ci_recovered.getAuthor() && ci_recovered.getAuthor().compareTo(author1) == 0);

        System.out.println("expected listener : " + listener1);
        System.out.println("actual listener : " + ci_recovered.getListener());
        assertTrue(null != ci_recovered.getListener() && ci_recovered.getListener().compareTo(listener1) == 0);

        String author_recovered = mcs.getAuthor(commentId1);
        System.out.println("expected author : " + author1);
        System.out.println("actual author : " + author_recovered);
        assertTrue(null != author_recovered && author_recovered.compareTo(author1) == 0);

        String listener_recovered = mcs.getListener(commentId1);
        System.out.println("expected listener : " + listener1);
        System.out.println("actual listener : " + listener_recovered);
        assertTrue(null != listener_recovered && listener_recovered.compareTo(listener1) == 0);

        Date created_recovered = mcs.getCreated(commentId1);
        System.out.println("expected created date : " + created1);
        System.out.println("actual created date : " + created_recovered);
        assertTrue(null != created_recovered && created_recovered.compareTo(created1) == 0);

        Date lastAccess_recovered = mcs.getLastAccessed(commentId1);
        System.out.println("expected lastAccess date : " + created1);
        System.out.println("actual lastAccess date : " + lastAccess_recovered);
        assertTrue(null != lastAccess_recovered && lastAccess_recovered.compareTo(created1) == 0);

        int actualSizeByAuthor = mcs.getSizeofWideCommentsByAuthor(author1);
        System.out.println("expected size of comments by author : 1");
        System.out.println("actual size of comments by author : " + actualSizeByAuthor);
        assertTrue(1 == actualSizeByAuthor);

        int actualSizeByAuthor1_2 = mcs.getSizeofWideCommentsByAuthor(new String[]{author1, author2});
        System.out.println("expected size of comments by author : 2");
        System.out.println("actual size of comments by author : " + actualSizeByAuthor1_2);
        assertTrue(2 == actualSizeByAuthor1_2);

        int actualSizeByListener = mcs.getSizeofWideCommentsByListener(listener1);
        System.out.println("expected size of comments by listener : 2");
        System.out.println("actual size of comments by listener : " + actualSizeByListener);
        assertTrue(2 == actualSizeByListener);

        List<String> comments = mcs.listWideCommentsByAuthor(0, 2, author1);
        assertTrue(null != comments && comments.size() == 1);
        String commentId_recovered = comments.get(0);
        System.out.println("expected comment Id : " + commentId1);
        System.out.println("actual comment Id : " + commentId_recovered);
        assertTrue(null != commentId_recovered && commentId_recovered.compareTo(commentId1) == 0);

        List<String> comments2 = mcs.listWideCommentsByListener(0, 1, listener1);
        assertTrue(null != comments2 && comments2.size() == 1);
        String commentId2_recovered = comments2.get(0);
        System.out.println("expected comment Id : " + commentId2);
        System.out.println("actual comment Id : " + commentId2_recovered);
        assertTrue(null != commentId2_recovered && commentId2_recovered.compareTo(commentId2) == 0);

        List<String> silents = mcs.listNarrowCommentsByPair(0, 1, author1, listener1);
        assertTrue(null != silents && silents.size() == 1);
        String silent = silents.get(0);
        System.out.println("expected comment Id : " + silentComment);
        System.out.println("actual comment Id : " + silent);
        assertTrue(null != silent && silent.compareTo(silentComment) == 0);

        mcs.removeComment(commentId2);

        System.out.println("isExists test2");
        assertTrue(mcs.isExist(commentId1) && mcs.isExist(silentComment) && !mcs.isExist(commentId2));

        List<CommentInformation> empty = mcs.recoverByList(commentId2);
        assertTrue(null != empty && empty.size() == 1);
        System.out.println("expected size : 1");
        System.out.println("actual : " + empty);

        Set<CommentInformation> emptySet = mcs.recoverBySet(commentId2);
        assertTrue(null == emptySet || emptySet.size() == 0);
        System.out.println("expected size : 0");
        System.out.println("actual : " + emptySet);

        actualSizeByAuthor1_2 = mcs.getSizeofWideCommentsByAuthor(new String[]{author1, author2});
        System.out.println("expected size of comments by author : 1");
        System.out.println("actual size of comments by author : " + actualSizeByAuthor1_2);
        assertTrue(1 == actualSizeByAuthor1_2);

        actualSizeByListener = mcs.getSizeofWideCommentsByListener(listener1);
        System.out.println("expected size of comments by listener : 1");
        System.out.println("actual size of comments by listener : " + actualSizeByListener);
        assertTrue(1 == actualSizeByListener);

        Comment updated = new Comment("Hihihi");
        Date updatedTime = Calendar.getInstance().getTime();
        mcs.updateComment(commentId1, updated, updatedTime);

        list = mcs.recoverByList(commentId1);
        System.out.println("expected size : 1");
        System.out.println("actual size : " + list.size());

        assertTrue(null != list && list.size() == 1);

        ci_recovered = list.get(0);
        System.out.println(ci_recovered);
        assertTrue(null != ci_recovered.getComment() && null != ci_recovered.getComment().getContent());
        assertTrue(ci_recovered.getComment().getContent().compareTo(updated.getContent()) == 0);

        lastAccess_recovered = mcs.getLastAccessed(commentId1);
        System.out.println("expected lastAccess date : " + updatedTime);
        System.out.println("actual lastAccess date : " + lastAccess_recovered);
        assertTrue(null != lastAccess_recovered && lastAccess_recovered.compareTo(updatedTime) == 0);
    }

    @Test
    public void testTwokindsofRecovers() throws Exception{
        System.out.println("Test recoverBySet and recoverByList concretely");
        String author = "authorForRecovering";
        String listener = "listenerForRecovering";
        Comment comment = new Comment("recoverBySet and recoverByList test");

        for(int i=0;i<10;i++){
            String commentId = new StringBuffer("commentForRecovering").append(i).toString();
            mcs.addComment(commentId, author, listener, comment, null, Scope.WIDE);
            Thread.sleep(10);
        }

        String[] requestIds = new String[]{"commentForRecovering1", "commentForRecovering2", "commentForRecovering11", "commentForRecovering3"};
        List<CommentInformation> recovered2 = mcs.recoverByList(requestIds);
        assertTrue(null != recovered2 && recovered2.size() == 4);
        System.out.println("expected the recovered list size : 4");
        System.out.println("actual the recovered list size : " + recovered2.size());
        assertTrue(recovered2.get(0).getIdentifier().compareTo("commentForRecovering1") == 0);
        assertTrue(recovered2.get(1).getIdentifier().compareTo("commentForRecovering2") == 0);
        CommentInformation ci3 = recovered2.get(2);
        assertTrue(null == ci3);
        System.out.println("expected Recovered list's 3rd element : null");
        System.out.println("actual Recovered list's 3rd element  : " + ci3);
        assertTrue(recovered2.get(3).getIdentifier().compareTo("commentForRecovering3") == 0);

        Set<CommentInformation> recovered1 = mcs.recoverBySet(requestIds);
        assertTrue(null != recovered1 && recovered1.size() == 3);
        System.out.println("expected the recovered set size : 3");
        System.out.println("actual the recovered set size : " + recovered1.size());
        Set<CommentInformation> expected = Sets.newHashSet();
        expected.add(new CommentInformation(author, listener, "commentForRecovering1", comment, Scope.WIDE));
        expected.add(new CommentInformation(author, listener, "commentForRecovering2", comment, Scope.WIDE));
        expected.add(new CommentInformation(author, listener, "commentForRecovering3", comment, Scope.WIDE));

        assertTrue(Sets.difference(expected, recovered1).isEmpty());
    }


    @Test
    public void testOffsetAndCount() throws Exception{
        System.out.println("Test offset and count functionality");
        String author = "author";
        String listener = "listener";
        Comment comment = new Comment("offset and count test");

        for(int i=0;i<100;i++) {
            String commentId = new StringBuffer("comment").append(i).toString();
            mcs.addComment(commentId, author, listener, comment, Calendar.getInstance().getTime(), Scope.WIDE);
            Thread.sleep(10);
        }

        List<String> onlyRecent = mcs.listWideCommentsByAuthor(0, 1, author);
        assertTrue(null != onlyRecent);
        System.out.println("expected size : 1");
        System.out.println("actual size : " + onlyRecent.size());
        assertTrue(1 == onlyRecent.size());
        String recent = onlyRecent.get(0);
        String expectedRecent = "comment99";
        System.out.println("expected recent thing : comment99");
        System.out.println("actual recent thing : " + recent);
        assertTrue(expectedRecent.compareTo(recent) == 0);

        List<String> tenTo20 = mcs.listWideCommentsByAuthor(10, 10, author);
        assertTrue(null != tenTo20);
        System.out.println("expected size : 10");
        System.out.println("actual size : " + tenTo20.size());
        assertTrue(10 == tenTo20.size());
        String tenTh = tenTo20.get(0);
        String expected10th = "comment89";
        System.out.println("expected 10th comment : comment89");
        System.out.println("actual 10th comment : " + tenTh);
        assertTrue(expected10th.compareTo(tenTh) == 0);
        String twentieth = tenTo20.get(9);
        String expected20th = "comment80";
        System.out.println("expected 20th comment : comment80");
        System.out.println("actual 20th comment : " + twentieth);
        assertTrue(expected20th.compareTo(twentieth) == 0);

        List<String> require10but5 = mcs.listWideCommentsByAuthor(95, 10, author);
        assertTrue(null != require10but5);
        System.out.println("expected size : 5");
        System.out.println("actual size : " + require10but5.size());
        assertTrue(5 == require10but5.size());
        String start = require10but5.get(0);
        String end = require10but5.get(4);
        String expectedStart = "comment4";
        String expectedEnd = "comment0";
        System.out.println("expected 95th comment : comment4");
        System.out.println("actual 95th comment : " + expectedStart);
        assertTrue(expectedStart.compareTo(start) == 0);
        System.out.println("expected 100th comment : comment0");
        System.out.println("actual 100th comment : " + expectedEnd);
        assertTrue(expectedEnd.compareTo(end) == 0);
    }
}