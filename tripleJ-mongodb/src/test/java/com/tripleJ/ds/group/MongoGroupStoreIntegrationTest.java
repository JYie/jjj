package com.tripleJ.ds.group;

import com.google.common.collect.Sets;
import com.tripleJ.ds.MongoSetupForTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class MongoGroupStoreIntegrationTest {

    private static MongoGroupStore mgs;

    static{
        System.out.println("----- Start MongoGroupStoreIntegrationTest -----");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        MongoSetupForTest.setupBeforeClass();
        mgs = new MongoGroupStore();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        MongoSetupForTest.tearDownAfterClass();
    }

    @Test
    public void testSimpleCycle() throws Exception{
        String creator = "kami";
        String groupId1 = "groupIdForTesting";
        String groupName = "cbsh";
        String intro = "This is a group for testing";
        Group group1 = new Group(creator, groupName, intro, null);

        mgs.createGroup(groupId1, group1);

        System.out.printf("succeeds to create group %s\n", groupId1);

        String member1 = "ksk";
        String member2 = "jkt";

        mgs.join(groupId1, member1);
        mgs.join(groupId1, member2);

        System.out.printf("succeeds to join %s, %s to %s \n", member1, member2, groupId1);

        assertTrue(mgs.isMember(groupId1, member2));
        assertTrue(mgs.isMember(groupId1, member1));

        Set<String> kskGroup = mgs.getGroups(member1);
        System.out.println("expected member1's group size : 1");
        System.out.println("actual member1's group size : " + kskGroup.size());
        assertTrue(null != kskGroup & kskGroup.size() == 1);
        String[] kskGroupArray = kskGroup.toArray(new String[]{});
        assertTrue(null != kskGroupArray[0] && kskGroupArray[0].compareTo(groupId1) == 0);

        Set<String> cbshMembers = mgs.getMembers(groupId1);
        System.out.println("expected group 1's member size : 2");
        System.out.println("actual group's member size : " + cbshMembers.size());
        assertTrue(null != cbshMembers && cbshMembers.size() == 2);

        Set<String> expectedCbshMembers = Sets.newHashSet();
        expectedCbshMembers.add(member1);
        expectedCbshMembers.add(member2);
        assertTrue(Sets.difference(expectedCbshMembers, cbshMembers).isEmpty());

        mgs.dropOut(groupId1, member1);

        System.out.printf("succeeds to drop out %s from %s\n", member1, groupId1);
        cbshMembers = mgs.getMembers(groupId1);
        System.out.println("expected group 1's member size after dropping 1 member : a");
        System.out.println("actual group's member size after dropping 1 member : " + cbshMembers.size());

        assertTrue(null != cbshMembers && cbshMembers.size() == 1);

        Set<String> expectedCbshMembersAfterRemoving = Sets.newHashSet();
        expectedCbshMembersAfterRemoving.add(member2);
        assertTrue(Sets.difference(expectedCbshMembersAfterRemoving, cbshMembers).isEmpty());

        kskGroup = mgs.getGroups(member1);
        assertTrue(null == kskGroup || kskGroup.size() == 0);

        assertTrue(mgs.isMember(groupId1, member2));
        assertTrue(!mgs.isMember(groupId1, member1));

        String groupId2 = "groupId2ForTesting";
        String groupName2 = "cbsh8th";
        String intro2 = "This is a second group for testing";
        Group group2 = new Group(creator, groupName2, intro2, null);

        mgs.createGroup(groupId2, group2);
        System.out.printf("succeeds to create group %s\n", groupId2);

        assertTrue(mgs.isExist(groupId1) && mgs.isExist(groupId2));

        List<GroupInformation> list = mgs.recoverByList(new String[]{groupId1, groupId2});
        assertTrue(null != list && list.size() == 2);
        GroupInformation groupInformation1 = list.get(0);
        assertTrue(null != groupInformation1);
        String recoveredId1 = groupInformation1.getIdentifier();
        assertTrue(null != recoveredId1 && recoveredId1.compareTo(groupId1) == 0);
        GroupInformation groupInformation2 = list.get(1);
        assertTrue(null != groupInformation2);
        String recoveredId2 = groupInformation2.getIdentifier();
        assertTrue(null != recoveredId2 && recoveredId2.compareTo(groupId2) == 0);

        Set<GroupInformation> set = mgs.recoverBySet(new String[]{groupId1, groupId1, groupId2});
        assertTrue(null != set && set.size() == 2);

        Set<GroupInformation> expectedSet = Sets.newHashSet();
        expectedSet.add(new GroupInformation(groupId1, group1));
        expectedSet.add(new GroupInformation(groupId2, group2));
        assertTrue(Sets.difference(expectedSet, set).isEmpty());

        mgs.removeGroup(groupId2);
        System.out.printf("succeeds to remove group %s\n", groupId2);

        assertTrue(mgs.isExist(groupId1) && !mgs.isExist(groupId2));
    }

}