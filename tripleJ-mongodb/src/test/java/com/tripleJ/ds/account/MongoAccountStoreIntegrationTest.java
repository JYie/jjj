package com.tripleJ.ds.account;

import com.google.common.collect.Sets;
import com.tripleJ.ds.MongoSetupForTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class MongoAccountStoreIntegrationTest {
    private static MongoAccountStore mas;

    static{
        System.out.println("----- Start MongoAccountStoreIntegrationTest -----");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        MongoSetupForTest.setupBeforeClass();
        mas = new MongoAccountStore();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        MongoSetupForTest.tearDownAfterClass();
    }

    @Test
    public void testSimpleCycle() throws Exception{
        String uid1 = "account1";
        String name1 = "jjj";
        String provider1 = "google.com";
        String email1 = "jj@nt.co.kr";
        Account account1 = new Account(uid1, name1, provider1, email1, null);
        String accountid1 = "accountid1";

        String uid2 = "account2";
        String name2 = "qqq";
        String provider2 = "facebook.com";
        String email2 = "qq@nt.co.kr";
        Account account2 = new Account(uid2, name2, provider2, email2, null);
        String accountid2 = "accountid2";

        mas.registerAccount(accountid1, account1);
        mas.registerAccount(accountid2, account2);

        System.out.printf("succeeds to register %s, %s \n", account1, account2);

        assertTrue(mas.isRegisteredId(accountid1) && mas.isRegisteredId(accountid2));

        String[] accountid12 = new String[]{accountid1, accountid2};
        List<AccountInformation> recovered12 = mas.recoverByList(accountid12);
        assertTrue(null != recovered12 && recovered12.size() == 2);

        AccountInformation ai1 = recovered12.get(0);
        AccountInformation ai2 = recovered12.get(1);
        assertTrue(null != ai1.getAccountId() && ai1.getAccountId().compareTo(accountid1) == 0);
        assertTrue(null != ai2.getAccountId() && ai2.getAccountId().compareTo(accountid2) == 0);

        Set<AccountInformation> recoveredSet = mas.recoverBySet(accountid12);
        assertTrue(null != recoveredSet);
        Set<AccountInformation> expectedSet = Sets.newHashSet();
        expectedSet.add(new AccountInformation(accountid1, account1));
        expectedSet.add(new AccountInformation(accountid2, account2));
        assertTrue(Sets.difference(expectedSet, recoveredSet).isEmpty());

        mas.unregisterAccount(accountid2);
        System.out.printf("succeeds to unregister %s\n", account2);

        assertTrue(!mas.isRegisteredId(accountid2));

        recovered12 = mas.recoverByList(accountid12);
        assertTrue(null != recovered12 && recovered12.size() == 2);

        ai1 = recovered12.get(0);
        ai2 = recovered12.get(1);
        assertTrue(null != ai1.getAccountId() && ai1.getAccountId().compareTo(accountid1) == 0);
        assertTrue(null == ai2);

        recoveredSet = mas.recoverBySet(accountid12);
        assertTrue(null != recoveredSet);
        expectedSet = Sets.newHashSet();
        expectedSet.add(new AccountInformation(accountid1, account1));
        assertTrue(Sets.difference(expectedSet, recoveredSet).isEmpty());


    }

}