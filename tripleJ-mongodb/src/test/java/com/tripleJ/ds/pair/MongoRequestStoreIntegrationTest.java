package com.tripleJ.ds.pair;

import com.google.common.collect.Sets;
import com.tripleJ.ds.MongoSetupForTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class MongoRequestStoreIntegrationTest {

    private static MongoRequestStore mrs;

    static{
        System.out.println("----- Start MongoRequestStoreIntegrationTest -----");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        MongoSetupForTest.setupBeforeClass();
        mrs = new MongoRequestStore();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        MongoSetupForTest.tearDownAfterClass();
    }

    @Test
    public void testSimpleCycle() throws Exception {

        String requestId1 = "testForAdding1";
        String requestor1 = "requestor1";
        String responder1 = "responder1";
        Request request1 = new Request(RequestType.AddRelation, "Hi");

        String requestId2 = "testForAdding2";
        Request request2 = new Request(RequestType.AddRelation, "Hello");
        String requestor2 = "requestor2";

        Date created1 = Calendar.getInstance().getTime();

        mrs.registerRequest(requestId1, requestor1, responder1, request1, created1);
        System.out.println("succeeds to add a request having id : " + requestId1);

        Date created2 = Calendar.getInstance().getTime();
        mrs.registerRequest(requestId2, requestor2, responder1, request2, created2);

        System.out.println("succeeds to add a request having id : " + requestId2);

        assertTrue(mrs.isExist(requestId1) && mrs.isExist(requestId2));

        System.out.println("Recover test");
        List<RequestInformation> list = mrs.recoverByList(requestId1);
        System.out.println("expected size : 1");
        System.out.println("actual size : " + list.size());

        assertTrue(null != list && list.size() == 1);

        RequestInformation ri_recovered = list.get(0);
        System.out.println("expected id : " + requestId1);
        System.out.println("actual id : " + ri_recovered.getIdentifier());
        assertTrue(null != ri_recovered.getIdentifier() && ri_recovered.getIdentifier().compareTo(requestId1) == 0);

        System.out.println("expected requestor : " + requestor1);
        System.out.println("actual requestor : " + ri_recovered.getRequestor());
        assertTrue(null != ri_recovered.getRequestor() && ri_recovered.getRequestor().compareTo(requestor1) == 0);

        System.out.println("expected responder : " + responder1);
        System.out.println("actual responder : " + ri_recovered.getResponder());
        assertTrue(null != ri_recovered.getResponder() && ri_recovered.getResponder().compareTo(responder1) == 0);

        Set<RequestInformation> set1 = mrs.recoverBySet(new String[]{requestId1});
        Set<RequestInformation> set2 = mrs.recoverBySet(new String[]{requestId1, requestId1});

        assertTrue(null != set1 && null != set2);
        System.out.println("Set1 size : " + set1.size());
        System.out.println("Set2 size : " + set2.size());
        assertTrue(set1.size() == 1 && set2.size() == 1);
        for(RequestInformation ri: set1){
            System.out.println(ri);
        }
        for(RequestInformation ri: set2){
            System.out.println(ri);
        }
        assertTrue(set1.containsAll(set2));

        String requestor_recovered = mrs.getRequestor(requestId1);
        System.out.println("expected requestor : " + requestor1);
        System.out.println("actual requestor : " + requestor_recovered);
        assertTrue(null != requestor_recovered && requestor_recovered.compareTo(requestor1) == 0);

        String responder_recovered = mrs.getResponder(requestId1);
        System.out.println("expected responder : " + responder1);
        System.out.println("actual responder : " + responder_recovered);
        assertTrue(null != responder_recovered && responder_recovered.compareTo(responder1) == 0);

        Date created_recovered = mrs.getCreated(requestId1);
        System.out.println("expected created date : " + created1);
        System.out.println("actual created date : " + created_recovered);
        assertTrue(null != created_recovered && created_recovered.compareTo(created1) == 0);

        Date lastAccess_recovered = mrs.getLastAccessed(requestId1);
        System.out.println("expected lastAccess date : " + created1);
        System.out.println("actual lastAccess date : " + lastAccess_recovered);
        assertTrue(null != lastAccess_recovered && lastAccess_recovered.compareTo(created1) == 0);

        int actualSizeByRequestor = mrs.getSizeOfRequestsByRequestor(requestor1);
        System.out.println("expected size of requests by requestor : 1");
        System.out.println("actual size of requests by requestor : " + actualSizeByRequestor);
        assertTrue(1 == actualSizeByRequestor);

        int actualSizeByRequestor1_2 = mrs.getSizeOfRequestsByRequestor(new String[]{requestor1, requestor2});
        System.out.println("expected size of requests by requestor : 2");
        System.out.println("actual size of requests by requestor : " + actualSizeByRequestor1_2);
        assertTrue(2 == actualSizeByRequestor1_2);

        int actualSizeByResponder = mrs.getSizeOfRequestsByResponder(responder1);
        System.out.println("expected size of requests by responder : 2");
        System.out.println("actual size of requests by responder : " + actualSizeByResponder);
        assertTrue(2 == actualSizeByResponder);

        List<String> requests2 = mrs.listRequestsByResponder(0, 1, responder1);
        assertTrue(null != requests2 && requests2.size() == 1);
        String requestId2_recovered = requests2.get(0);
        System.out.println("expected request Id : " + requestId2);
        System.out.println("actual request Id : " + requestId2_recovered);
        assertTrue(null != requestId2_recovered && requestId2_recovered.compareTo(requestId2) == 0);

        Date updatedTime = Calendar.getInstance().getTime();
        mrs.updateStatus(requestId2, RequestStatus.ACCEPTED, updatedTime);

        lastAccess_recovered = mrs.getLastAccessed(requestId2);
        System.out.println("expected lastAccess date : " + updatedTime);
        System.out.println("actual lastAccess date : " + lastAccess_recovered);
        assertTrue(null != lastAccess_recovered && lastAccess_recovered.compareTo(updatedTime) == 0);
    }

    @Test
    public void testTwokindsofRecovers() throws Exception{
        System.out.println("Test recoverBySet and recoverByList concretely");
        String requestor = "requestorForRecovering";
        String responder = "responderForRecovering";
        Request request = new Request(RequestType.AddRelation, "recoverBySet and recoverByList test");

        for(int i=0;i<10;i++){
            String requestId = new StringBuffer("requestForRecovering").append(i).toString();
            mrs.registerRequest(requestId, requestor, responder, request, Calendar.getInstance().getTime());
            Thread.sleep(10);
        }

        String[] requestIds = new String[]{"requestForRecovering1", "requestForRecovering2", "requestForRecovering11", "requestForRecovering3"};
        List<RequestInformation> recovered2 = mrs.recoverByList(requestIds);
        assertTrue(null != recovered2 && recovered2.size() == 4);
        System.out.println("expected the recovered list size : 4");
        System.out.println("actual the recovered list size : " + recovered2.size());
        assertTrue(recovered2.get(0).getIdentifier().compareTo("requestForRecovering1") == 0);
        assertTrue(recovered2.get(1).getIdentifier().compareTo("requestForRecovering2") == 0);
        RequestInformation ri3 = recovered2.get(2);
        assertTrue(null == ri3);
        System.out.println("expected Recovered list's 3rd element : null");
        System.out.println("actual Recovered list's 3rd element  : " + ri3);
        assertTrue(recovered2.get(3).getIdentifier().compareTo("requestForRecovering3") == 0);

        Set<RequestInformation> recovered1 = mrs.recoverBySet(requestIds);
        assertTrue(null != recovered1 && recovered1.size() == 3);
        System.out.println("expected the recovered set size : 3");
        System.out.println("actual the recovered set size : " + recovered1.size());
        Set<RequestInformation> expected = Sets.newHashSet();
        expected.add(new RequestInformation(requestor, responder, "requestForRecovering1", RequestStatus.CREATED, request, null));
        expected.add(new RequestInformation(requestor, responder, "requestForRecovering2", RequestStatus.CREATED, request, null));
        expected.add(new RequestInformation(requestor, responder, "requestForRecovering3", RequestStatus.CREATED, request, null));

        assertTrue(Sets.difference(expected, recovered1).isEmpty());
    }

    @Test
    public void testOffsetAndCount() throws Exception{
        System.out.println("Test offset and count functionality");
        String requestor = "author";
        String responder = "responder";
        Request request = new Request(RequestType.AddRelation, "offset and count test");

        for(int i=0;i<100;i++) {
            String requestId = new StringBuffer("request").append(i).toString();
            mrs.registerRequest(requestId, requestor, responder, request, Calendar.getInstance().getTime());
            Thread.sleep(10);
        }

        List<String> onlyRecent = mrs.listRequestsByRequestor(0, 1, requestor);
        assertTrue(null != onlyRecent);
        System.out.println("expected size : 1");
        System.out.println("actual size : " + onlyRecent.size());
        assertTrue(1 == onlyRecent.size());
        String recent = onlyRecent.get(0);
        String expectedRecent = "request99";
        System.out.println("expected recent thing : request99");
        System.out.println("actual recent thing : " + recent);
        assertTrue(expectedRecent.compareTo(recent) == 0);

        List<String> tenTo20 = mrs.listRequestsByRequestor(10, 10, requestor);
        assertTrue(null != tenTo20);
        System.out.println("expected size : 10");
        System.out.println("actual size : " + tenTo20.size());
        assertTrue(10 == tenTo20.size());
        String tenTh = tenTo20.get(0);
        String expected10th = "request89";
        System.out.println("expected 10th request : comment89");
        System.out.println("actual 10th request : " + tenTh);
        assertTrue(expected10th.compareTo(tenTh) == 0);
        String twentieth = tenTo20.get(9);
        String expected20th = "request80";
        System.out.println("expected 20th request : comment80");
        System.out.println("actual 20th request : " + twentieth);
        assertTrue(expected20th.compareTo(twentieth) == 0);

        List<String> require10but5 = mrs.listRequestsByRequestor(95, 10, requestor);
        assertTrue(null != require10but5);
        System.out.println("expected size : 5");
        System.out.println("actual size : " + require10but5.size());
        assertTrue(5 == require10but5.size());
        String start = require10but5.get(0);
        String end = require10but5.get(4);
        String expectedStart = "request4";
        String expectedEnd = "request0";
        System.out.println("expected 95th request : comment4");
        System.out.println("actual 95th request : " + expectedStart);
        assertTrue(expectedStart.compareTo(start) == 0);
        System.out.println("expected 100th request : comment0");
        System.out.println("actual 100th request : " + expectedEnd);
        assertTrue(expectedEnd.compareTo(end) == 0);
    }
}
