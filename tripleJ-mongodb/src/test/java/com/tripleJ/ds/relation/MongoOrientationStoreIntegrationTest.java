package com.tripleJ.ds.relation;

import com.tripleJ.TripleJType;
import com.tripleJ.ds.MongoSetupForTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class MongoOrientationStoreIntegrationTest {

    private static MongoOrientationStore mos;

    static{
        System.out.println("----- Start MongoOrientationStoreIntegrationTest -----");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        MongoSetupForTest.setupBeforeClass();
        mos = new MongoOrientationStore();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        MongoSetupForTest.tearDownAfterClass();
    }

    @Test
    public void testSimpleCycleForOrientation() throws Exception {
        String commentRoot = "commentRoot";
        String commentChild1 = "commentChild1";
        String commentChild2 = "commentChild2";
        String commentChild1_1 = "commentChild1_1";

        String comment3 = "comment3";
        String comment3_1 = "comment3_1";

        mos.addOrientation(TripleJType.Comment, commentRoot, commentChild1);
        mos.addOrientation(TripleJType.Comment, commentRoot, commentChild2);
        mos.addOrientation(TripleJType.Comment, commentChild1, commentChild1_1);
        mos.addOrientation(TripleJType.Comment, comment3, comment3_1);

        System.out.println("succeeds to add orientations of comments type");

        Set<Knowing> inNeighbors = mos.getInNeighbors(TripleJType.Comment, commentChild1);
        Set<Knowing> outNeighbors = mos.getOutNeighbors(TripleJType.Comment, commentChild1);

        System.out.println("expected inNeighbor Size : 1");
        System.out.println("actual inNeighbor Size : " + inNeighbors.size());
        assertTrue(null != inNeighbors && inNeighbors.size() == 1);
        Knowing[] inNeighborArray = inNeighbors.toArray(new Knowing[inNeighbors.size()]);
        Knowing inNeighbor = inNeighborArray[0];
        assertTrue(inNeighbor.isOriented());

        System.out.println("expected inNeighbor Source : " + commentRoot);
        System.out.println("actual inNeighbor Source : " + inNeighbor.getSource());
        System.out.println("expected inNeighbor Destination : " + commentChild1);
        System.out.println("actual inNeighbor Destination : " + inNeighbor.getDestination());
        assertTrue(null != inNeighbor.getSource() && inNeighbor.getSource().compareTo(commentRoot) == 0);
        assertTrue(null != inNeighbor.getDestination() && inNeighbor.getDestination().compareTo(commentChild1) == 0);

        System.out.println("expected outNeighbor Size : 1");
        System.out.println("actual outNeighbor Size : " + outNeighbors.size());
        assertTrue(null != outNeighbors && outNeighbors.size() == 1);
        Knowing[] outNeighborArray = outNeighbors.toArray(new Knowing[outNeighbors.size()]);
        Knowing outNeighbor = outNeighborArray[0];
        assertTrue(outNeighbor.isOriented());
        System.out.println("expected outNeighbor Source : " + commentChild1);
        System.out.println("actual outNeighbor Source : " + outNeighbor.getSource());
        System.out.println("expected outNeighbor Destination : " + commentChild1_1);
        System.out.println("actual outNeighbor Destination : " + outNeighbor.getDestination());
        assertTrue(null != outNeighbor.getSource() && outNeighbor.getSource().compareTo(commentChild1) == 0);
        assertTrue(null != outNeighbor.getDestination() && outNeighbor.getDestination().compareTo(commentChild1_1) == 0);

        Set<Knowing> inNeighbors2 = mos.getInNeighbors(TripleJType.Comment, new String[]{commentChild1, comment3_1});
        System.out.println("expected inNeighbor Size : " + 2);
        System.out.println("actual inNeighbor Size : " + inNeighbors2.size());
        assertTrue(null != inNeighbors2 && inNeighbors2.size() == 2);
        for(Knowing knowing:inNeighbors2)
            assertTrue(knowing.isOriented());

        Set<Knowing> outNeighbors2 = mos.getOutNeighbors(TripleJType.Comment, new String[]{commentChild1, comment3});
        System.out.println("expected outNeighbor Size : " + 2);
        System.out.println("actual outNeighbor Size : " + outNeighbors2.size());
        assertTrue(null != outNeighbors2 && outNeighbors2.size() == 2);
        for(Knowing knowing:outNeighbors2)
            assertTrue(knowing.isOriented());

        mos.removeOrientation(TripleJType.Comment, comment3, comment3_1);

        inNeighbors2 = mos.getInNeighbors(TripleJType.Comment, new String[]{commentChild1, comment3_1});
        System.out.println("expected inNeighbor Size after removing: " + 1);
        System.out.println("actual inNeighbor Size after removing: " + inNeighbors2.size());
        assertTrue(null != inNeighbors2 && inNeighbors2.size() == 1);

        outNeighbors2 = mos.getOutNeighbors(TripleJType.Comment, new String[]{commentChild1, comment3});
        System.out.println("expected outNeighbor Size after removing : " + 1);
        System.out.println("actual outNeighbor Size after removing : " + outNeighbors2.size());
        assertTrue(null != outNeighbors2 && outNeighbors2.size() == 1);
    }

    @Test
    public void testSimpleCycleForRelation() throws Exception {
        String account1 = "account1";
        String account2 = "account2";
        String account3 = "account3";
        String account4 = "account4";
        String account5 = "account5";

        mos.addRelation(TripleJType.Account, account1, account2);
        mos.addRelation(TripleJType.Account, account3, account2);
        mos.addRelation(TripleJType.Account, account4, account5);

        Set<Knowing> relations = mos.getRelations(TripleJType.Account, account2);
        assertTrue(null != relations && relations.size() == 2);

        for(Knowing knowing:relations){
            assertTrue(!knowing.isOriented());
            assertTrue(knowing.getSource().compareTo(account2) == 0 || knowing.getDestination().compareTo(account2) == 0);
        }

        Set<Knowing> relations2 = mos.getRelations(TripleJType.Account, new String[]{account2, account4});
        assertTrue(null != relations2 && relations2.size() == 3);

        mos.removeRelation(TripleJType.Account, account4, account5);
        relations2 = mos.getRelations(TripleJType.Account, new String[]{account2, account4});
        assertTrue(null != relations2 && relations2.size() == 2);

        for(Knowing knowing:relations2){
            assertTrue(!knowing.isOriented());
            assertTrue(knowing.getSource().compareTo(account2) == 0 || knowing.getDestination().compareTo(account2) == 0);
        }
    }
}
