package com.tripleJ.ds;

import com.mongodb.DB;

/**
 * Created by kami on 2014. 7. 25..
 */
public class MongoProvider {

    private static DB database;

    public static void registerDB(DB db){
        database = db;
    }

    public static DB getDB(){
        return database;
    }
}
