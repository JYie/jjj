package com.tripleJ.ds.account;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mongodb.*;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

import com.tripleJ.InitializeException;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;
import com.tripleJ.ds.MongoProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created by kami on 2014. 7. 14..
 */
public class MongoAccountStore implements AccountStore {

    private static final String AccountCollection = "TripleJ_Account_Information";

    private static final String ID = "ID";
    private static final String ACCOUNTINFO = "ACCOUNTINFO";
    private static final String UID = "UID";
    private static final String NAME = "NAME";
    private static final String PROVIDER = "PROVIDER";
    private static final String IMAGE_ID = "IMAGE_ID";
    private static final String EMAIL = "EMAIL";

    private final DB db;
    private final DBCollection accountCollection;

    protected MongoAccountStore(DB db) throws InitializeException {
        if(null == db)
            throw new InitializeException("DB must not be null");
        this.db = db;
        this.accountCollection = db.getCollection(AccountCollection);
    }

    public MongoAccountStore() throws InitializeException{
       this.db = MongoProvider.getDB();
        if(null == this.db)
            throw new InitializeException("DB must not be null");
        this.accountCollection = this.db.getCollection(AccountCollection);
    }

    @Override
    public void registerAccount(String accountId, Account account) throws StoreException {

        DBObject object = new BasicDBObject();
        object.put(ID, accountId);

        object.put(ACCOUNTINFO, convertAccount(account));

        try{
            accountCollection.insert(object, WriteConcern.SAFE);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public void unregisterAccount(String accountId) throws StoreException {
        DBObject object = new BasicDBObject();
        object.put(ID, accountId);
        if(isExist(accountId)) {
            try{
                accountCollection.remove(object);
            }catch(com.mongodb.MongoException me){
                throw new StoreException(me.getMessage());
            }
        }

    }

    private boolean isExist(String accountId) throws StoreException{
        DBObject search = new BasicDBObject().append(ID, accountId);
        try{
            DBObject result = accountCollection.findOne(search);
            if(null != result)
                return true;
            else
                return false;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public void modifyAccount(String accountId, Account account) throws StoreException {
        DBObject old = new BasicDBObject().append(ID, accountId);
        DBObject new_ = new BasicDBObject().append("$set", new BasicDBObject().append(ACCOUNTINFO, convertAccount(account)));
        if(isExist(accountId)) {
            try {
                this.accountCollection.update(old, new_);
            }catch(com.mongodb.MongoException me){
                throw new StoreException(me.getMessage());
            }
        }
    }

    @Override
    public boolean isRegisteredId(String accountId) throws StoreException {
        return isExist(accountId);
    }

    @Override
    public List<AccountInformation> recoverByList(String... accountIds) throws StoreException {
        List<AccountInformation> recovered = Lists.newLinkedList();
        for(int i=0;i<accountIds.length;i++){
            String accountId = accountIds[i];
            DBObject search = new BasicDBObject().append(ID, accountId);
            try{
                DBObject single = this.accountCollection.findOne(search);
                if(null == single) {
                    recovered.add(null);
                    continue;
                }
                AccountInformation accountInformation = convertObject(single);
                recovered.add(accountInformation);
            }catch(com.mongodb.MongoException me){
                throw new StoreException(me.getMessage());
            }
        }
        return recovered;
    }

    @Override
    public Set<AccountInformation> recoverBySet(String... accountIds) throws StoreException {
        Set<AccountInformation> recovered = Sets.newHashSet();

        List<String> condition = Lists.newLinkedList();
        for(String accountId:accountIds)
            condition.add(accountId);
        DBObject in = new BasicDBObject("$in", condition);
        DBObject query = new BasicDBObject(ID, in);

        try{
            DBCursor cursor = this.accountCollection.find(query);
            if(null == cursor)
                return recovered;

            while(cursor.hasNext()){
                DBObject object = cursor.next();
                AccountInformation accountInformation = convertObject(object);
                System.out.println(object);
                System.out.println(accountInformation);
                recovered.add(accountInformation);
            }
            return recovered;
        }catch(com.mongodb.MongoException me) {
            throw new StoreException(me.getMessage());
        }
    }

    private DBObject convertAccount(final Account account){
        DBObject accountObject = new BasicDBObject();
        String uid = account.getUid();
        if(null != uid)
            accountObject.put(UID, uid);

        String name = account.getName();
        if(null != name)
            accountObject.put(NAME, name);

        String provider = account.getProvider();
        if(null != provider)
            accountObject.put(PROVIDER, provider);

        String email = account.getEmail();
        if(null != email)
            accountObject.put(EMAIL, email);

        byte[] image = account.getImage();
        if(image != null && image.length > 0){
            GridFS gridFS = new GridFS(db);
            GridFSInputFile gridFSInputFile = gridFS.createFile(image);
            gridFSInputFile.save();
            Object imageId = gridFSInputFile.getId();
            accountObject.put(IMAGE_ID, imageId);
        }
        return accountObject;
    }

    private AccountInformation convertObject(final DBObject object){
        String accountId = (String)object.get(ID);

        Account account;

        DBObject accountObject = (DBObject)object.get(ACCOUNTINFO);
        if(null == accountObject)
            account = null;

        String uid = (String)accountObject.get(UID);
        String name = (String)accountObject.get(NAME);
        String provider = (String)accountObject.get(PROVIDER);
        String email = (String)accountObject.get(EMAIL);

        String imageId = (String)accountObject.get(IMAGE_ID);

        ByteArrayOutputStream bao = null;
        if(null != imageId){
            GridFS gridFS = new GridFS(db);
            GridFSDBFile out = gridFS.findOne( new BasicDBObject( "_id" , imageId ) );
            try {
                bao = new ByteArrayOutputStream();
                out.writeTo(bao);
            } catch (IOException e) {

            }
        }

        try{
            if(null != bao)
                account = new Account(uid, name, provider, email, bao.toByteArray());
            else
                account = new Account(uid, name, provider, email, null);
        }catch (MissingRequiredException mre){
            return null;
        }
        AccountInformation accountInformation = new AccountInformation(accountId, account);

        return accountInformation;
    }
}
