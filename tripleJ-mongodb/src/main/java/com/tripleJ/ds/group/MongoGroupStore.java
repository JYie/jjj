package com.tripleJ.ds.group;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mongodb.*;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.tripleJ.InitializeException;
import com.tripleJ.StoreException;
import com.tripleJ.ds.MongoProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Created by kami on 2014. 7. 14..
 */
public class MongoGroupStore implements GroupStore {

    private static final String GroupCollection = "TripleJ_Group_Information";
    private static final String GroupMemberCollection = "TripleJ_GM_Information";
    private static final String MemberGroupCollection = "TripleJ_MG_Information";

    private static final String DELIMITER4CREATORS = ",";

    private static final String GROUPID = "GROUPID";
    private static final String ACCOUNTID = "ACCOUNTID";

    private static final String GROUP = "GROUP";

    private static final String MEMBERS = "MEMBERS";
    private static final String GROUPS = "GROUPS";

    private static final String CREATORS = "CREATORS";
    private static final String NAME = "NAME";
    private static final String IMAGE_ID = "IMAGE_ID";
    private static final String INTRO = "INTRO";

    private final DB db;
    private final DBCollection groupCollection;
    private final DBCollection gmCollection;
    private final DBCollection mgCollection;

    protected MongoGroupStore(DB db) throws InitializeException {
        if(null == db)
            throw new InitializeException("DB must not be null");
        this.db = db;
        this.groupCollection = db.getCollection(GroupCollection);
        this.gmCollection = db.getCollection(GroupMemberCollection);
        this.mgCollection = db.getCollection(MemberGroupCollection);
    }

    public MongoGroupStore() throws InitializeException{
        this.db = MongoProvider.getDB();
        if(null == this.db)
            throw new InitializeException("DB must not be null");
        this.groupCollection = this.db.getCollection(GroupCollection);
        this.gmCollection = this.db.getCollection(GroupMemberCollection);
        this.mgCollection = this.db.getCollection(MemberGroupCollection);
    }

    @Override
    public void createGroup(String groupId, Group group) throws StoreException {
        DBObject object = new BasicDBObject();
        object.put(GROUPID, groupId);
        object.put(GROUP, convertGroup(group));

        try{
            this.groupCollection.insert(object, WriteConcern.SAFE);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public void removeGroup(String groupId) throws StoreException {
        DBObject object = new BasicDBObject();
        object.put(GROUPID, groupId);

        try{
            this.groupCollection.remove(object);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }


    @Override
    public void updateGroup(String groupId, Group group) throws StoreException {
        DBObject old = new BasicDBObject().append(GROUPID, groupId);
        DBObject new_ = new BasicDBObject().append("$set", new BasicDBObject().append(GROUP, convertGroup(group)));

        try{
            this.groupCollection.update(old, new_);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public void join(String groupId, String accountId) throws StoreException {
        putGMrelation(groupId, accountId);
        putMGrelation(groupId, accountId);
    }

    private void putGMrelation(String groupId, String accountId) throws StoreException{
        try{
            DBObject forCurrent = new BasicDBObject(GROUPID, groupId);
            DBObject current = this.gmCollection.findOne(forCurrent);
            if(null == current){
                BasicDBList newList = new BasicDBList();
                newList.add(new BasicDBObject(ACCOUNTID, accountId));
                DBObject init = new BasicDBObject();
                init.put(GROUPID, groupId);
                init.put(MEMBERS, newList);
                this.gmCollection.insert(init, WriteConcern.SAFE);
                return;
            }

            BasicDBList origin = (BasicDBList)current.get(MEMBERS);
            if(null == origin)
                origin = new BasicDBList();

            DBObject new_ = new BasicDBObject(ACCOUNTID,accountId);
            origin.add(new_);

            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(MEMBERS, origin));;
            this.gmCollection.update(forCurrent, updated);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private void putMGrelation(String groupId, String accountId) throws StoreException{
        try{
            DBObject forCurrent = new BasicDBObject(ACCOUNTID, accountId);
            DBObject current = this.mgCollection.findOne(forCurrent);
            if(null == current){
                BasicDBList newList = new BasicDBList();
                newList.add(new BasicDBObject(GROUPID, groupId));
                DBObject init = new BasicDBObject();
                init.put(ACCOUNTID, accountId);
                init.put(GROUPS, newList);
                this.mgCollection.insert(init, WriteConcern.SAFE);
                return;
            }

            BasicDBList origin = (BasicDBList)current.get(GROUPS);
            if(null == origin)
                origin = new BasicDBList();

            DBObject new_ = new BasicDBObject(GROUPID, groupId);
            origin.add(new_);

            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(GROUPS, origin));;
            this.mgCollection.update(forCurrent, updated);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public void dropOut(String groupId, String accountId) throws StoreException {
        removeGMrelation(groupId, accountId);
        removeMGrelation(groupId, accountId);
    }

    @Override
    public boolean isExist(String groupId) throws StoreException {
        DBObject search = new BasicDBObject().append(GROUPID, groupId);
        try{
            DBObject single = this.groupCollection.findOne(search);
            if(null != single)
                return true;
            return false;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private void removeGMrelation(String groupId, String accountId) throws StoreException{
        try{
            DBObject forCurrent = new BasicDBObject(GROUPID, groupId);
            DBObject current = this.gmCollection.findOne(forCurrent);
            if(null == current)
                return;

            BasicDBList origin = (BasicDBList)current.get(MEMBERS);
            if(null == origin)
                return;

            DBObject beRemoved = new BasicDBObject(ACCOUNTID,accountId);
            origin.remove(beRemoved);
            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(MEMBERS, origin));;
            this.gmCollection.update(forCurrent, updated);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private void removeMGrelation(String groupId, String accountId) throws StoreException{
        try{
            DBObject forCurrent = new BasicDBObject(ACCOUNTID, accountId);
            DBObject current = this.mgCollection.findOne(forCurrent);
            if(null == current)
                return;

            BasicDBList origin = (BasicDBList)current.get(GROUPS);
            if(null == origin)
                return;

            DBObject beRemoved = new BasicDBObject(GROUPID, groupId);
            origin.remove(beRemoved);
            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(GROUPS, origin));;
            this.mgCollection.update(forCurrent, updated);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public boolean isMember(String groupId, String accountId) throws StoreException {
        try{
            DBObject search = new BasicDBObject(ACCOUNTID, accountId);
            DBObject current = this.mgCollection.findOne(search);
            if(null == current)
                return false;

            BasicDBList currentGroups = (BasicDBList)current.get(GROUPS);
            if(null == currentGroups)
                return false;

            int size = currentGroups.size();
            for(int i=0;i<size;i++){
                BasicDBObject group = (BasicDBObject)currentGroups.get(i);
                String groupId_ = (String)group.get(GROUPID);
                if(groupId_.compareTo(groupId) == 0)
                    return true;
            }
            return false;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Set<String> getMembers(String groupId) throws StoreException {
        try{
            DBObject search = new BasicDBObject(GROUPID, groupId);
            DBObject currentMembers = this.gmCollection.findOne(search);
            if(null == currentMembers)
                return Sets.newHashSet();

            BasicDBList members = (BasicDBList)currentMembers.get(MEMBERS);
            if(null == members || members.isEmpty())
                return Sets.newHashSet();

            Set<String> result = Sets.newHashSet();
            int size = members.size();
            for(int i=0;i<size;i++){
                BasicDBObject member = (BasicDBObject)members.get(i);
                result.add((String)member.get(ACCOUNTID));
            }
            return result;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<GroupInformation> recoverByList(String... groupIds) throws StoreException {
        try{
            List<GroupInformation> recovered = Lists.newLinkedList();
            if(null == groupIds || groupIds.length == 0)
                return recovered;
            for(int i=0;i<groupIds.length;i++){
                String groupId = groupIds[i];
                DBObject search = new BasicDBObject().append(GROUPID, groupId);
                DBObject single = this.groupCollection.findOne(search);
                if(null == single) {
                    recovered.add(null);
                    continue;
                }

                GroupInformation groupInformation = convertObject(single);
                recovered.add(groupInformation);
            }
            return recovered;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Set<GroupInformation> recoverBySet(String... groupIds) throws StoreException {
        try{
            Set<GroupInformation> recovered = Sets.newHashSet();

            List<String> condition = Lists.newLinkedList();
            for(String commentId:groupIds)
                condition.add(commentId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(GROUPID, in);
            DBCursor cursor = this.groupCollection.find(query);

            if(null != cursor){
                while(cursor.hasNext()){
                    DBObject object = cursor.next();
                    GroupInformation groupInformation = convertObject(object);
                    recovered.add(groupInformation);
                }
            }
            return recovered;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public Set<String> getGroups(String accountId) throws StoreException {
        try{
            DBObject search = new BasicDBObject(ACCOUNTID, accountId);
            DBObject currentGroups = this.mgCollection.findOne(search);
            if(null == currentGroups)
                return Sets.newHashSet();

            BasicDBList groups = (BasicDBList)currentGroups.get(GROUPS);
            if(null == groups || groups.isEmpty())
                return Sets.newHashSet();

            Set<String> result = Sets.newHashSet();
            int size = groups.size();
            for(int i=0;i<size;i++){
                BasicDBObject group = (BasicDBObject)groups.get(i);
                result.add((String)group.get(GROUPID));
            }
            return result;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private DBObject convertGroup(final Group group){
        DBObject groupObject = new BasicDBObject();
        String name = group.getName();
        groupObject.put(NAME, name);
        String intro = group.getIntroduction();
        groupObject.put(INTRO, intro);
        Set<String> creators = group.getCreators();
        if(null != creators){
            boolean isFirst = true;
            StringBuffer creatorStr = new StringBuffer();
            for(String creator:creators){
                if(!isFirst)
                    creatorStr.append(DELIMITER4CREATORS);
                creatorStr.append(creator);
                isFirst = false;
            }
            groupObject.put(CREATORS, creatorStr.toString());
        }

        byte[] image = group.getImage();
        if(image != null && image.length > 0){
            GridFS gridFS = new GridFS(db);
            GridFSInputFile gridFSInputFile = gridFS.createFile(image);
            gridFSInputFile.save();
            Object imageId = gridFSInputFile.getId();
            groupObject.put(IMAGE_ID, imageId);
        }

        return groupObject;
    }

    private GroupInformation convertObject(final DBObject object) {
        String groupId = (String)object.get(GROUPID);

        Group group;

        DBObject groupObject = (DBObject)object.get(GROUP);
        if(null == groupObject)
            return null;

        String name = (String)groupObject.get(NAME);
        String intro = (String)groupObject.get(INTRO);
        String imageId = (String)groupObject.get(IMAGE_ID);

        ByteArrayOutputStream bao = null;
        if(null != imageId){
            GridFS gridFS = new GridFS(db);
            GridFSDBFile out = gridFS.findOne( new BasicDBObject( "_id" , imageId ) );
            try {
                bao = new ByteArrayOutputStream();
                out.writeTo(bao);
            } catch (IOException e) {

            }
        }

        String creatorStr = (String)groupObject.get(CREATORS);
        Set<String> creators = convertCreator(creatorStr);
        if(null != bao)
            group = new Group(null ,name, intro, bao.toByteArray());
        else
            group = new Group(null, name, intro, null);

        if(null != creators && creators.size() > 0){
            for(String creator: creators)
                group.putCreator(creator);
        }

        GroupInformation groupInformation = new GroupInformation(groupId, group);

        return groupInformation;
    }

    private Set<String> convertCreator(String creatorStr){
        Set<String> creatorSet = Sets.newHashSet();
        if(null == creatorStr)
            return creatorSet;
        StringTokenizer st = new StringTokenizer(creatorStr, DELIMITER4CREATORS);
        while(st.hasMoreTokens())
            creatorSet.add(st.nextToken());
        return creatorSet;
    }
}
