package com.tripleJ.ds.pair;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mongodb.*;
import com.tripleJ.InitializeException;

import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;
import com.tripleJ.ds.MongoProvider;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Created by kami on 2014. 7. 14..
 */
public class MongoRequestStore implements RequestStore {

    private static final String RequestCollection = "TripleJ_Request_Information";

    private static final String ID = "ID";
    private static final String REQUESTOR = "REQUESTOR";
    private static final String RESPONDER = "RESPONDER";
    private static final String REQUEST = "REQUEST";
    private static final String TYPE = "TYPE";
    private static final String STATUS = "STATUS";
    private static final String COMMENT = "COMMENT";
    private static final String INIT = "INIT";
    private static final String LAST = "LAST";


    private final DB db;
    private final DBCollection requestCollection;

    protected MongoRequestStore(DB db) throws InitializeException {
        if(null == db)
            throw new InitializeException("DB must not be null");
        this.db = db;
        this.requestCollection = db.getCollection(RequestCollection);
    }

    public MongoRequestStore() throws InitializeException{
        this.db = MongoProvider.getDB();
        if(null == this.db)
            throw new InitializeException("DB must not be null");
        this.requestCollection = db.getCollection(RequestCollection);
    }

    @Override
    public void registerRequest(String requestId, String requstorId, String responderId, Request request, Date date) throws StoreException {
        try{
            DBObject object = new BasicDBObject();
            object.put(ID, requestId);
            object.put(REQUESTOR, requstorId);
            object.put(RESPONDER, responderId);
            object.put(REQUEST, convertRequest(request));
            if(null != date) {
                object.put(INIT, date);
                object.put(LAST, date);
            }
            object.put(STATUS, RequestStatus.CREATED.name());
            this.requestCollection.insert(object, WriteConcern.SAFE);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public void updateRequest(String requestId, Request request, Date current) throws StoreException {
        try{
            DBObject old = new BasicDBObject().append(ID, requestId);
            DBObject new_ = new BasicDBObject().append("$set", new BasicDBObject().append(REQUEST, convertRequest(request)).append(LAST, current));
            this.requestCollection.update(old, new_);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public void updateStatus(String requestId, RequestStatus status, Date current) throws StoreException {
        try{
            DBObject old = new BasicDBObject().append(ID, requestId);
            DBObject new_ = new BasicDBObject().append("$set", new BasicDBObject().append(STATUS, status.name()).append(LAST, current));
            this.requestCollection.update(old, new_);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public boolean isExist(String requestId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, requestId);
            DBObject single = this.requestCollection.findOne(search);
            if(null != single)
                return true;
            return false;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public RequestStatus getStatus(String requestId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, requestId);
            DBObject single = this.requestCollection.findOne(search);
            if(null == single)
                return null;
            String status = (String)single.get(STATUS);
            if(null == status)
                return null;

            return convertStatus(status);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<RequestInformation> recoverByList(String... requestIds) throws StoreException {
        try{
            List<RequestInformation> recovered = Lists.newLinkedList();
            for(int i=0;i<requestIds.length;i++){
                String requestId = requestIds[i];
                DBObject search = new BasicDBObject().append(ID, requestId);
                DBObject single = this.requestCollection.findOne(search);
                if(null == single) {
                    recovered.add(null);
                    continue;
                }
                RequestInformation requestInformation = convertObject(single);
                recovered.add(requestInformation);
            }
            return recovered;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Set<RequestInformation> recoverBySet(String... requestIds) throws StoreException {
        try{
            Set<RequestInformation> recovered = Sets.newHashSet();

            List<String> condition = Lists.newLinkedList();
            for(String requestId:requestIds)
                condition.add(requestId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(ID, in);
            DBCursor cursor = this.requestCollection.find(query);

            if(null == cursor)
                return recovered;

            while(cursor.hasNext()){
                DBObject object = cursor.next();
                RequestInformation requestInformation = convertObject(object);
                if(null != requestInformation)
                    recovered.add(requestInformation);
            }

            return recovered;

        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public String getRequestor(String requestId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, requestId);
            DBObject single = this.requestCollection.findOne(search);
            if(null == single)
                return null;
            return (String)single.get(REQUESTOR);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public String getResponder(String requestId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, requestId);
            DBObject single = this.requestCollection.findOne(search);
            if(null == single)
                return null;
            return (String)single.get(RESPONDER);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Date getLastAccessed(String requestId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, requestId);
            DBObject single = this.requestCollection.findOne(search);
            if(null != single)
                return (Date)single.get(LAST);
            return null;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Date getCreated(String requestId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, requestId);
            DBObject single = this.requestCollection.findOne(search);
            if(null != single)
                return (Date)single.get(INIT);
            return null;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public int getSizeOfRequestsByRequestor(String... requestorIds) throws StoreException {
        try{
            if(requestorIds.length < 1)
                return 0;
            List<String> condition = Lists.newLinkedList();
            for(String requestorId:requestorIds)
                condition.add(requestorId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(REQUESTOR, in);
            DBCursor cursor = this.requestCollection.find(query);
            return cursor.count();
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<String> listRequestsByRequestor(int offset, int count, String... requestorIds) throws StoreException {
        try{
            List<String> result = Lists.newLinkedList();
            if(!checkOffsetCountCondition(offset, count))
                return result;

            List<String> condition = Lists.newLinkedList();
            for(String requestor:requestorIds)
                condition.add(requestor);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(REQUESTOR, in);
            DBObject sortOption = new BasicDBObject(LAST, -1);
            DBCursor cursor;
            if(offset > 0)
                cursor = this.requestCollection.find(query).sort(sortOption).skip(offset).limit(count);
            else
                cursor = this.requestCollection.find(query).sort(sortOption).limit(count);

            while(cursor.hasNext())
                result.add((String)cursor.next().get(ID));
            return result;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }

    }

    @Override
    public int getSizeOfRequestsByResponder(String... responderIds) throws StoreException {
        try{
            if(responderIds.length < 1)
                return 0;
            List<String> condition = Lists.newLinkedList();
            for(String responderId:responderIds)
                condition.add(responderId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(RESPONDER, in);
            DBCursor cursor = this.requestCollection.find(query);
            return cursor.count();
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<String> listRequestsByResponder(int offset, int count, String... responderIds) throws StoreException {
        try{
            List<String> result = Lists.newLinkedList();
            if(!checkOffsetCountCondition(offset, count))
                return result;

            List<String> condition = Lists.newLinkedList();
            for(String responderId:responderIds)
                condition.add(responderId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(RESPONDER, in);
            DBObject sortOption = new BasicDBObject(LAST, -1);
            DBCursor cursor;
            if(offset > 0)
                cursor = this.requestCollection.find(query).sort(sortOption).skip(offset).limit(count);
            else
                cursor = this.requestCollection.find(query).sort(sortOption).limit(count);

            while(cursor.hasNext())
                result.add((String)cursor.next().get(ID));
            return result;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private DBObject convertRequest(final Request request){
        DBObject requestObject = new BasicDBObject();
        String comment = request.getComment();
        if(comment != null)
            requestObject.put(COMMENT, comment);
        requestObject.put(TYPE, request.getRequestType().name());

        return requestObject;
    }

    private RequestInformation convertObject(final DBObject object) {
        String requestId = (String)object.get(ID);
        String requestorId = (String)object.get(REQUESTOR);
        String responderId = (String)object.get(RESPONDER);

        String status = (String)object.get(STATUS);
        RequestStatus requestStatus = convertStatus(status);

        DBObject requestObject = (DBObject)object.get(REQUEST);
        if(null == requestObject)
            return null;

        String type = (String)requestObject.get(TYPE);
        RequestType requestType = convertType(type);
        String comment = (String)requestObject.get(COMMENT);
        try{
            Request request = new Request(requestType, comment);
            Date date = (Date)requestObject.get(LAST);

            RequestInformation requestInformation = new RequestInformation(requestorId, responderId, requestId, requestStatus, request, date);

            return requestInformation;
        }catch(MissingRequiredException me){
            return null;
        }

    }

    private RequestStatus convertStatus(String status){
        if(null == status || status.isEmpty())
            return null;
        if(status.compareToIgnoreCase(RequestStatus.CREATED.name()) == 0)
            return RequestStatus.CREATED;
        if(status.compareToIgnoreCase(RequestStatus.NOTIFIED.name()) == 0)
            return RequestStatus.NOTIFIED;
        if(status.compareToIgnoreCase(RequestStatus.CANCLED.name()) == 0)
            return RequestStatus.CANCLED;
        if(status.compareToIgnoreCase(RequestStatus.ACCEPTED.name()) == 0)
            return RequestStatus.ACCEPTED;
        if(status.compareToIgnoreCase(RequestStatus.DECLINED.name()) == 0)
            return RequestStatus.DECLINED;
        return null;
    }

    private RequestType convertType(String type){
        if(null == type || type.isEmpty())
            return null;

        if(type.compareToIgnoreCase(RequestType.AddRelation.name()) == 0)
            return RequestType.AddRelation;
        if(type.compareToIgnoreCase(RequestType.JoinGroup.name()) == 0)
            return RequestType.JoinGroup;
        return null;
    }

    private boolean checkOffsetCountCondition(int offset, int count) throws ArrayIndexOutOfBoundsException{
        if(offset < 0)
            throw new ArrayIndexOutOfBoundsException("offset must be greater than or equal to 0");
        if(count <= 0)
            return false;
        return true;
    }
}
