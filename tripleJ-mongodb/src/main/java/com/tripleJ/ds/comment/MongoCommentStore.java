package com.tripleJ.ds.comment;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mongodb.*;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.tripleJ.InitializeException;
import com.tripleJ.Location;
import com.tripleJ.StoreException;
import com.tripleJ.ds.MongoProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by kami on 2014. 7. 14..
 */
public class MongoCommentStore implements CommentStore {

    private static final String WideCommentCollection = "TripleJ_Wide_Comment_Information";
    private static final String NarrowCommentCollection = "TripleJ_Narrow_Comment_Information";

    private static final String ID = "ID";
    private static final String AUTHOR = "AUTHOR";
    private static final String LISTENER = "LISTENER";
    private static final String COMMENT = "COMMENT";
    private static final String INIT = "INIT";
    private static final String LAST = "LAST";
    private static final String SCOPE = "SCOPE";
    private static final String CONTENT = "CONTENT";
    private static final String IMAGE_ID = "IMAGE_ID";
    private static final String LOCATION = "LOCATION";

    private final DB db;
    private final DBCollection wideCollection;
    private final DBCollection narrowCollection;

    protected MongoCommentStore(DB db) throws InitializeException {
        if(null == db)
            throw new InitializeException("DB must not be null");
        this.db = db;
        this.wideCollection = db.getCollection(WideCommentCollection);
        this.narrowCollection = db.getCollection(NarrowCommentCollection);
    }

    public MongoCommentStore() throws InitializeException{
        this.db = MongoProvider.getDB();
        if(null == this.db)
            throw new InitializeException("DB must not be null");
        this.wideCollection = this.db.getCollection(WideCommentCollection);
        this.narrowCollection = this.db.getCollection(NarrowCommentCollection);
    }

    @Override
    public void addComment(String commentId, String authorId, String listenerId, Comment comment, Date date, Scope scope) throws StoreException {
        DBObject object = new BasicDBObject();
        object.put(ID, commentId);
        object.put(AUTHOR, authorId);
        object.put(LISTENER, listenerId);
        object.put(COMMENT, convertComment(comment));
        if(null != date) {
            object.put(INIT, date);
            object.put(LAST, date);
        }
        if(null == scope)
            object.put(SCOPE, Scope.getDefault().name());
        else
            object.put(SCOPE, scope.name());

        DBCollection dbCollection = null;
        switch (scope){
            case WIDE:
                dbCollection = this.wideCollection;
                break;
            case NARROW:
                dbCollection = this.narrowCollection;
                break;
        }
        if(null == dbCollection)
            throw new StoreException("Unknown scope");

        try{
            dbCollection.insert(object, WriteConcern.SAFE);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private boolean isExistOnWideCollection(String commentId) throws StoreException{
        try{
            DBObject search = new BasicDBObject().append(ID, commentId);
            DBObject result = wideCollection.findOne(search);
            if(null != result)
                return true;
            else
                return false;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private boolean isExistOnNarrowCollection(String commentId){
        DBObject search = new BasicDBObject().append(ID, commentId);
        DBObject result = narrowCollection.findOne(search);
        if(null != result)
            return true;
        else
            return false;
    }

    @Override
    public void removeComment(String commentId) throws StoreException {
        try{
            DBObject object = new BasicDBObject();
            object.put(ID, commentId);

            if(isExistOnWideCollection(commentId)) {
                this.wideCollection.remove(object);
                return;
            }
            if(isExistOnNarrowCollection(commentId)){
                this.narrowCollection.remove(object);
                return;
            }
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public void updateComment(String commentId, Comment comment, Date current) throws StoreException {
        try{
            DBObject old = new BasicDBObject().append(ID, commentId);
            DBObject new_ = new BasicDBObject().append("$set", new BasicDBObject().append(COMMENT, convertComment(comment)).append(LAST, current));
            if(isExistOnWideCollection(commentId)) {
                this.wideCollection.update(old, new_);
                return;
            }
            if(isExistOnNarrowCollection(commentId)){
                this.narrowCollection.update(old, new_);
                return;
            }
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public boolean isExist(String commentId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, commentId);
            DBObject single = wideCollection.findOne(search);
            if(null != single)
                return true;
            single = narrowCollection.findOne(search);
            if(null != single)
                return true;
            return false;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<CommentInformation> recoverByList(String... commentIds) throws StoreException {
        try{
            List<CommentInformation> recovered = Lists.newLinkedList();
            for(int i=0;i<commentIds.length;i++){
                String commentId = commentIds[i];
                DBObject search = new BasicDBObject().append(ID, commentId);
                DBObject single = null;
                boolean isWide = false;
                if(isExistOnWideCollection(commentId)) {
                    single = this.wideCollection.findOne(search);
                    isWide = true;
                }
                if(isExistOnNarrowCollection(commentId))
                    single = this.narrowCollection.findOne(search);
                if(null == single) {
                    recovered.add(null);
                    continue;
                }

                CommentInformation commentInformation = convertObject(single, isWide);
                recovered.add(commentInformation);
            }
            return recovered;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Set<CommentInformation> recoverBySet(String... commentIds) throws StoreException {
        try{
            Set<CommentInformation> recovered = Sets.newHashSet();

            List<String> condition = Lists.newLinkedList();
            for(String commentId:commentIds)
                condition.add(commentId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(ID, in);
            DBCursor cursorForWide = this.wideCollection.find(query);
            DBCursor cursorForNarrow = this.narrowCollection.find(query);

            if(null != cursorForWide){
                while(cursorForWide.hasNext()){
                    DBObject object = cursorForWide.next();
                    CommentInformation commentInformation = convertObject(object, true);
                    recovered.add(commentInformation);
                }
            }

            if(null != cursorForNarrow){
                while(cursorForNarrow.hasNext()){
                    DBObject object = cursorForNarrow.next();
                    CommentInformation commentInformation = convertObject(object, false);
                    recovered.add(commentInformation);
                }
            }
            return recovered;

        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public String getAuthor(String commentId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, commentId);
            DBObject single = wideCollection.findOne(search);
            if(null != single)
                return (String)single.get(AUTHOR);
            single = narrowCollection.findOne(search);
            if(null != single)
                return (String)single.get(AUTHOR);
            return null;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public String getListener(String commentId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, commentId);
            DBObject single = wideCollection.findOne(search);
            if(null != single)
                return (String)single.get(LISTENER);
            single = narrowCollection.findOne(search);
            if(null != single)
                return (String)single.get(LISTENER);
            return null;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Date getLastAccessed(String commentId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, commentId);
            DBObject single = wideCollection.findOne(search);
            if(null != single)
                return (Date)single.get(LAST);
            single = narrowCollection.findOne(search);
            if(null != single)
                return (Date)single.get(LAST);
            return null;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Date getCreated(String commentId) throws StoreException {
        try{
            DBObject search = new BasicDBObject().append(ID, commentId);
            DBObject single = wideCollection.findOne(search);
            if(null != single)
                return (Date)single.get(INIT);
            single = narrowCollection.findOne(search);
            if(null != single)
                return (Date)single.get(INIT);
            return null;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<String> listWideCommentsByAuthor(int offset, int count, String... authorIds) throws StoreException {
        return listCommentsByAuthor(this.wideCollection, offset, count, authorIds);
    }

    @Override
    public List<String> listNarrowCommentsByAuthor(int offset, int count, String... authorIds) throws StoreException {
        return listCommentsByAuthor(this.narrowCollection, offset, count, authorIds);
    }

    private List<String> listCommentsByAuthor(DBCollection collection, int offset, int count, String... authorIds){
        List<String> result = Lists.newLinkedList();
        if(!checkOffsetCountCondition(offset, count))
            return result;

        List<String> condition = Lists.newLinkedList();
        for(String authorId:authorIds)
            condition.add(authorId);
        DBObject in = new BasicDBObject("$in", condition);
        DBObject query = new BasicDBObject(AUTHOR, in);
        DBObject sortOption = new BasicDBObject(LAST, -1);
        DBCursor cursor;
        if(offset > 0)
            cursor = collection.find(query).sort(sortOption).skip(offset).limit(count);
        else
            cursor = collection.find(query).sort(sortOption).limit(count);

        while(cursor.hasNext())
            result.add((String)cursor.next().get(ID));
        return result;
    }

    @Override
    public int getSizeofWideCommentsByAuthor(String... authorIds) throws StoreException {
        return getSizeofCommentsByAuthor(this.wideCollection, authorIds);
    }

    @Override
    public int getSizeofNarrowCommentsByAuthor(String... authorIds) throws StoreException {
        return getSizeofCommentsByAuthor(this.narrowCollection, authorIds);
    }

    private int getSizeofCommentsByAuthor(DBCollection collection, String... authorIds) throws StoreException{
        try{
            if(authorIds.length < 1)
                return 0;
            List<String> condition = Lists.newLinkedList();
            for(String authorId:authorIds)
                condition.add(authorId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(AUTHOR, in);
            DBCursor cursor = collection.find(query);
            return cursor.count();

        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<String> listWideCommentsByListener(int offset, int count, String... listenerIds) throws StoreException {
        return listCommentsByListener(this.wideCollection, offset, count, listenerIds);
    }

    @Override
    public List<String> listNarrowCommentsByListener(int offset, int count, String... listenerIds) throws StoreException {
        return listCommentsByListener(this.narrowCollection, offset, count, listenerIds);
    }

    private List<String> listCommentsByListener(DBCollection collection, int offset, int count, String... listenerIds) throws StoreException{
        try{
            List<String> result = Lists.newLinkedList();
            if(!checkOffsetCountCondition(offset, count))
                return result;
            List<String> condition = Lists.newLinkedList();
            for(String authorId:listenerIds)
                condition.add(authorId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(LISTENER, in);
            DBObject sortOption = new BasicDBObject(LAST, -1);
            DBCursor cursor;
            if(offset > 0)
                cursor = collection.find(query).sort(sortOption).skip(offset).limit(count);
            else
                cursor = collection.find(query).sort(sortOption).limit(count);

            while(cursor.hasNext())
                result.add((String)cursor.next().get(ID));

            return result;
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public int getSizeofWideCommentsByListener(String... listenerIds) throws StoreException {
        return getSizeofCommentsByListener(this.wideCollection, listenerIds);
    }

    @Override
    public int getSizeofNarrowCommentsByListener(String... listenerIds) throws StoreException {
        return getSizeofCommentsByListener(this.narrowCollection, listenerIds);
    }

    private int getSizeofCommentsByListener(DBCollection collection, String... listenerIds) throws StoreException{
        try{
            List<String> condition = Lists.newLinkedList();
            for(String authorId:listenerIds)
                condition.add(authorId);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(LISTENER, in);

            DBCursor cursor = collection.find(query);
            return cursor.size();
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public List<String> listWideCommentsByPair(int offset, int count, String author, String listener) {
        return listCommentsByPair(this.wideCollection, offset, count, author, listener);
    }

    @Override
    public List<String> listNarrowCommentsByPair(int offset, int count, String author, String listener) {
        return listCommentsByPair(this.narrowCollection, offset, count, author, listener);
    }

    private List<String> listCommentsByPair(DBCollection collection, int offset, int count, String author, String listener){
        List<String> result = Lists.newLinkedList();
        if(!checkOffsetCountCondition(offset, count))
            return result;

        DBObject query =  new BasicDBObject(AUTHOR, author).append(LISTENER, listener);
        DBObject sortOption = new BasicDBObject(LAST, -1);
        DBCursor cursor;
        if(offset > 0)
            cursor = collection.find(query).sort(sortOption).skip(offset).limit(count);
        else
            cursor = collection.find(query).sort(sortOption).limit(count);

        while(cursor.hasNext())
            result.add((String)cursor.next().get(ID));
        return result;
    }


    private DBObject convertComment(final Comment comment){
        DBObject commentObject = new BasicDBObject();
        String content = comment.getContent();
        if(content != null)
            commentObject.put(CONTENT, content);

        byte[] image = comment.getImage();
        if(image != null && image.length > 0){
            GridFS gridFS = new GridFS(db);
            GridFSInputFile gridFSInputFile = gridFS.createFile(image);
            gridFSInputFile.save();
            Object imageId = gridFSInputFile.getId();
            commentObject.put(IMAGE_ID, imageId);
        }
        Location location = comment.getLocation();
        if(null != location)
            commentObject.put(LOCATION, location);

        return commentObject;
    }

    private CommentInformation convertObject(final DBObject object, boolean scopeFlag){
        String commentId = (String)object.get(ID);
        String authorId = (String)object.get(AUTHOR);
        String listenerId = (String)object.get(LISTENER);

        Comment comment;

        DBObject commentObject = (DBObject)object.get(COMMENT);
        if(null == commentObject)
            comment = null;

        String content = (String)commentObject.get(CONTENT);
        String locationform = (String)commentObject.get(LOCATION);
        Location location = null;
        try {
            location = new Location(locationform);
        } catch (InitializeException e) {

        }
        String imageId = (String)commentObject.get(IMAGE_ID);

        ByteArrayOutputStream bao = null;
        if(null != imageId){
            GridFS gridFS = new GridFS(db);
            GridFSDBFile out = gridFS.findOne( new BasicDBObject( "_id" , imageId ) );
            try {
                bao = new ByteArrayOutputStream();
                out.writeTo(bao);
            } catch (IOException e) {

            }
        }

        Scope scope;
        if(scopeFlag)
            scope = Scope.WIDE;
        else
            scope = Scope.NARROW;

        if(null != bao)
            comment = new Comment(content, bao.toByteArray(), location);
        else
            comment = new Comment(content, null, location);
        CommentInformation commentInformation = new CommentInformation(authorId, listenerId, commentId, comment, scope);

        return commentInformation;
    }

    private boolean checkOffsetCountCondition(int offset, int count) throws ArrayIndexOutOfBoundsException{
        if(offset < 0)
            throw new ArrayIndexOutOfBoundsException("offset must be greater than or equal to 0");
        if(count <= 0)
            return false;
        return true;
    }
}

