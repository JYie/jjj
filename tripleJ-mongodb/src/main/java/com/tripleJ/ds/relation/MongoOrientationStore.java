package com.tripleJ.ds.relation;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mongodb.*;
import com.tripleJ.InitializeException;
import com.tripleJ.StoreException;
import com.tripleJ.TripleJType;
import com.tripleJ.ds.MongoProvider;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by kami on 2014. 7. 14..
 */
public class MongoOrientationStore implements OrientationStore {

    private static final Map<TripleJType, String> relation_collection_name_map;
    private static final Map<TripleJType, String> orientation_forward_collection_name_map;
    private static final Map<TripleJType, String> orientation_backward_collection_name_map;

    private static final String KEY_COLLUM = "Key";
    private static final String VALUE_LIST_COLLUM = "ValueList";
    private static final String VALUE_COLUM = "Value";

    static{
        relation_collection_name_map = ImmutableMap.of(TripleJType.Account, "TRIPLEJ_ACCOUNT_RELATION");
        orientation_forward_collection_name_map = ImmutableMap.of(TripleJType.Comment, "TRIPLEJ_COMMENT_FORWARD", TripleJType.Follows, "TRIPLEJ_FOLLOW_FORWARD" );
        orientation_backward_collection_name_map = ImmutableMap.of(TripleJType.Comment, "TRIPLEJ_COMMENT_BACKWARD", TripleJType.Follows, "TRIPLEJ_FOLLOW_BACKWARD");
    }

    private final DB db;

    protected MongoOrientationStore(DB db) throws InitializeException {
        if(null == db)
            throw new InitializeException("DB must not be null");
        this.db = db;
    }

    public MongoOrientationStore() throws InitializeException{
        this.db = MongoProvider.getDB();
        if(null == this.db)
            throw new InitializeException("DB must not be null");
    }

    @Override
    public void addOrientation(final GraphType graphType, final String source, final String destination) throws StoreException {
        TripleJType type = convertType(graphType);

        String collectionForward = orientation_forward_collection_name_map.get(type);
        String collectionBackward = orientation_backward_collection_name_map.get(type);

        if(null == collectionForward || null == collectionBackward)
            throw new StoreException("Unsupported type");

        putOrientationInfoForward(collectionForward, source, destination);
        putOrientationInfoBackward(collectionBackward, source, destination);
    }

    void putOrientationInfoForward(final String collectionForward, final String source, final String dest) throws StoreException{
        try{
            DBCollection forward = db.getCollection(collectionForward);
            putKnowings(forward, source, dest);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    void putOrientationInfoBackward(final String collectionBackward, final String source, final String dest) throws StoreException {
        try{
            DBCollection backward = db.getCollection(collectionBackward);
            putKnowings(backward, dest, source);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }


    void putKnowings(final DBCollection collection, final String key, final String value) throws StoreException {
        try{
            DBObject forCurrent = new BasicDBObject(KEY_COLLUM, key);
            DBObject current = collection.findOne(forCurrent);
            if(null == current){
                BasicDBList newList = new BasicDBList();
                newList.add(new BasicDBObject(VALUE_COLUM, value));
                DBObject init = new BasicDBObject();
                init.put(KEY_COLLUM, key);
                init.put(VALUE_LIST_COLLUM, newList);
                collection.insert(init, WriteConcern.SAFE);
                return;
            }

            BasicDBList origin = (BasicDBList)current.get(VALUE_LIST_COLLUM);
            if(null == origin)
                origin = new BasicDBList();

            DBObject new_ = new BasicDBObject(VALUE_COLUM, value);
            origin.add(new_);

            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(VALUE_LIST_COLLUM, origin));;
            collection.update(forCurrent, updated);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public void removeOrientation(final GraphType graphType, final String source, final String destination) throws StoreException {
        TripleJType type = convertType(graphType);

        String collectionForward = orientation_forward_collection_name_map.get(type);
        String collectionBackward = orientation_backward_collection_name_map.get(type);

        if(null == collectionForward || null == collectionBackward)
            throw new StoreException("Unsupported type");

        removeOrientationInfoForward(collectionForward, source, destination);
        removeOrientationInfoBackward(collectionBackward, source, destination);
    }

    void removeKnowing(final DBCollection collection, final String key, final String value) throws StoreException {
        try{
            DBObject forCurrent = new BasicDBObject(KEY_COLLUM, key);
            DBObject current = collection.findOne(forCurrent);
            if(null == current)
                return;

            BasicDBList origin = (BasicDBList)current.get(VALUE_LIST_COLLUM);
            if(null == origin)
                return;

            DBObject beRemoved = new BasicDBObject(VALUE_COLUM, value);
            origin.remove(beRemoved);

            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(VALUE_LIST_COLLUM, origin));;
            collection.update(forCurrent, updated);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    void removeOrientationInfoForward(final String collectionForward, final String source, final String dest) throws StoreException {
        DBCollection forward = db.getCollection(collectionForward);
        removeKnowing(forward, source, dest);
    }

    void removeOrientationInfoBackward(final String collectionBackward, final String source, final String dest) throws StoreException {
        DBCollection backward = db.getCollection(collectionBackward);
        removeKnowing(backward, dest, source);
    }

    @Override
    public void addRelation(final GraphType graphType, final String vert1, final String vert2) throws StoreException {
        TripleJType type = convertType(graphType);

        String collectionRelation = relation_collection_name_map.get(type);

        if(null == collectionRelation)
            throw new StoreException("Unsupported type");

        putRelationInfo(collectionRelation, vert1, vert2);
        putRelationInfo(collectionRelation, vert2, vert1);
    }


    void putRelationInfo(final String collectionRelation, final String node1, final String node2) throws StoreException {
        DBCollection relation = db.getCollection(collectionRelation);
        putKnowings(relation, node1, node2);
    }


    @Override
    public void removeRelation(final GraphType graphType, final String vert1, final String vert2) throws StoreException {
        TripleJType type = convertType(graphType);

        String collectionRelation = relation_collection_name_map.get(type);

        if(null == collectionRelation)
            throw new StoreException("Unsupported type");

        removeRelationInfo(collectionRelation, vert1, vert2);
        removeRelationInfo(collectionRelation, vert2, vert1);
    }

    void removeRelationInfo(final String collectionRelation, final String node1, final String node2) throws StoreException {
        DBCollection relation = db.getCollection(collectionRelation);
        removeKnowing(relation, node1, node2);
    }

    @Override
    public Set<Knowing> getOutNeighbors(final GraphType graphType, final String... sources) throws StoreException {
        try{
            TripleJType type = convertType(graphType);

            String collectionForward = orientation_forward_collection_name_map.get(type);
            DBCollection forward = db.getCollection(collectionForward);

            List<String> condition = Lists.newLinkedList();
            for(String source:sources)
                condition.add(source);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(KEY_COLLUM, in);

            return pickKnowings(forward, query, true, true);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Set<Knowing> getInNeighbors(final GraphType graphType, final String... destinations) throws StoreException {
        try{
            TripleJType type = convertType(graphType);

            String collectionBackward = orientation_backward_collection_name_map.get(type);
            DBCollection backward = db.getCollection(collectionBackward);

            List<String> condition = Lists.newLinkedList();
            for(String destination:destinations)
                condition.add(destination);

            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(KEY_COLLUM, in);

            return pickKnowings(backward, query, true, false);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    @Override
    public Set<Knowing> getRelations(final GraphType graphType, final String... vertices) throws StoreException {
        try{
            TripleJType type = convertType(graphType);

            String collectionRelation = relation_collection_name_map.get(type);
            DBCollection relation = db.getCollection(collectionRelation);

            List<String> condition = Lists.newLinkedList();
            for(String vertex:vertices)
                condition.add(vertex);
            DBObject in = new BasicDBObject("$in", condition);
            DBObject query = new BasicDBObject(KEY_COLLUM, in);

            return pickKnowings(relation, query, false, false);
        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }

    private TripleJType convertType(final GraphType graphType) throws StoreException {
        TripleJType type;
        if(graphType instanceof TripleJType)
            type = (TripleJType)graphType;
        else
            throw new StoreException("Unsupported type");
        return type;
    }

    private Set<Knowing> pickKnowings(final DBCollection collection, final DBObject query, final boolean oriented, final boolean direction) throws StoreException {
        try{
            DBCursor cursor = collection.find(query);
            if(null == cursor)
                return Sets.newHashSet();

            Set<Knowing> result = Sets.newHashSet();
            while(cursor.hasNext()){
                DBObject object = cursor.next();

                String keyObj = (String)object.get(KEY_COLLUM);
                BasicDBList valuesObj =  (BasicDBList)object.get(VALUE_LIST_COLLUM);
                if(null == keyObj ||null == valuesObj)
                    continue;

                int size = valuesObj.size();
                for(int i=0;i<size;i++){
                    BasicDBObject knowingObj = (BasicDBObject)valuesObj.get(i);
                    String value = (String)knowingObj.get(VALUE_COLUM);
                    Knowing knowing = new Knowing(keyObj, value, oriented, 1, direction);
                    result.add(knowing);
                }
            }
            return result;

        }catch(com.mongodb.MongoException me){
            throw new StoreException(me.getMessage());
        }
    }
}
