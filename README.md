TripleJ
=======

## Introduction

TripleJ is an opensource platform for social network service. It is made up with sub components. Each sub component has own folder and has individual build system. 

## Hierarchy
	TripleJ/
		tripleJ-api/
		tripleJ-mongodb/
		tripleJ-simple-web/

### pom.xml
	Almost sub-project use maven as its build environment, so those have their own build files named pom.xml. It is a typical build script in maven(http://maven.apache.org) build environment.

### INSTALL
	Some sub-project has INSTALL document. It explains how you install this project in your project in more detail.

## Compile

Just run following command to build this project.

	$ mvn compile

## Package

Just run following command to package this project.

	$ mvn package

## Test

Just run following command to do unit tests of this project.
Almost sub projects are unit testable. 

	$ mvn test

## Install

Just run following command to install this project in your maven-local-repository. 

	$ mvn install

## Report

Just run following command to report test-results. 
The reporting file named surefire-report.html would be located in target/site.

	$ mvn surefire-report:report

## Clean

Just run following command to clean this project.

	$ mvn clean

