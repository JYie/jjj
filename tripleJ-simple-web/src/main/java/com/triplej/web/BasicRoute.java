package com.triplej.web;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Created by kami on 2014. 8. 6..
 */
public abstract class BasicRoute implements Route {

    protected String errorUrl;
    protected String error;
    protected String url;

    abstract boolean doActual(Request request, Response response) ;

    @Override
    public Object handle(Request request, Response response) {

        boolean result = doActual(request, response);
        if(result && null != url && !url.isEmpty())
            response.redirect(url);
        else{
            if(null == errorUrl || errorUrl.isEmpty())
                errorUrl = Urls.errorUrl;
            if(null == url || url.isEmpty())
                error = "url to handle this request is not fixed";

            StringBuffer sb = new StringBuffer(errorUrl);
            sb.append("?");
            sb.append(Params.errorReason);
            sb.append("=");
            if (null != error)
                sb.append(error);
            else
                sb.append("Unknown");
            response.redirect(sb.toString());
        }
        return null;
    }
}
