package com.triplej.web;

import com.tripleJ.*;
import com.tripleJ.service.RequestAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 * Created by kami on 2014. 8. 4..
 */
public class HandleRespondRequest extends BasicRoute {

    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleRespondRequest.class);
    }


    @Override
    boolean doActual(Request request, Response response) {
        String handler = LogOn.getCurrentId(request);
        String action = request.queryMap("action").value();
        String requestId = request.queryMap("requestId").value();

        try{
            RequestAction requestAction = new RequestAction();
            if(action.compareToIgnoreCase("accept") == 0){
                requestAction.accept(handler, requestId);
            }else if(action.compareToIgnoreCase("decline") == 0){
                requestAction.decline(handler, requestId);
            }
            super.url = Urls.listRequestsUrl;
            return true;
        }catch (UnauthorizedException uae) {
            super.errorUrl = Urls.authFailUrl;
            super.error = uae.getMessage();
            logger.error(error);
            return false;
        }catch (TripleJException e) {
            super.errorUrl = Urls.errorUrl;
            super.error = e.getMessage();
            logger.error(error);
            return false;
        }
    }
}
