package com.triplej.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tripleJ.*;
import com.tripleJ.ds.account.AccountInformation;
import com.tripleJ.ds.comment.Comment;
import com.tripleJ.ds.comment.CommentInformation;
import com.tripleJ.ds.group.Group;
import com.tripleJ.ds.group.GroupInformation;
import com.tripleJ.ds.pair.RequestInformation;
import com.tripleJ.ds.pair.RequestStatus;
import com.tripleJ.ds.pair.RequestType;
import com.tripleJ.ds.relation.RootedTree;
import com.tripleJ.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.*;
import spark.template.mustache.MustacheTemplateEngine;

import java.util.*;

import static spark.Spark.*;
import static spark.SparkBase.staticFileLocation;

/**
 * This class includes a main function to ignite spark.
 * Created by kami on 2014. 7. 30..
 */
public class SparkMain {

    private static final Set<String> urlsexclude;
    private static final Set<String> transparentUrls;

    private static final Filter signupFilter;

    static Logger logger;

    static{
        logger = LoggerFactory.getLogger(SparkMain.class);
        Set<String> set = Sets.newHashSet();
        set.add(Urls.signUpUrl);
        set.add(Urls.handleSignupUrl);
        set.add(Urls.styleUrl);
        set.add(Urls.signOutUrl);

        Set<String> trans = Sets.newHashSet();
        trans.add(Urls.signOutUrl);

        urlsexclude = Collections.unmodifiableSet(set);
        transparentUrls = Collections.unmodifiableSet(trans);

        signupFilter = signupFilter();
    }


    public static void main(String[] args) {
        try {
            Initializer.loadConfiguration();
            Initializer.applyStoreConfiguration();
        } catch (IllegalConfigurationException e) {
            logger.error("can not find valid configuration file( maybe named triplej-web-service.conf)");
            return;
        }

        staticFileLocation("/views");

        before(signupFilter);

        exception(Exception.class, (e, request, response) -> {
            e.printStackTrace();
            String message = e.getMessage();
            if(null != message)
                logger.error(e.getMessage());
            else
                logger.error(e.getClass().getName());

            response.status(404);
            response.body("Unpredictable Exception occured \n " + e.getClass().getName() + " : " + e.getMessage());
        });

        get(Urls.handleCommentUrl, new HandleComment());
        get(Urls.handleReCommentUrl, new HandleReComment());
        get(Urls.handleRequestFriendUrl, new HandleReqeustFriend());
        get(Urls.handleRequestJoinGroupUrl, new HandleRequestJoinGroup());
        get(Urls.handleCreateGroupUrl, new HandleCreateGroup());
        get(Urls.handleRespondRequestUrl, new HandleRespondRequest());
        get(Urls.handleSignupUrl, new HandleSignIn());

        get(Urls.signUpUrl,  (request, response) -> generateSignUpView(request), new MustacheTemplateEngine());
        get(Urls.signOutUrl, (request, response) -> signout(request), new MustacheTemplateEngine());

        get(Urls.createGroupUrl,  (request, response) -> generateCreateGroupView(request), new MustacheTemplateEngine());

        get(Urls.addCommentUrl, (request, response) -> new ModelAndView(pickCurrentUser(request), Templates.commentAddTemplate), new MustacheTemplateEngine());

        get(Urls.requestFriendUrl,  (request, response) -> generateRequestFriendView(request), new MustacheTemplateEngine());
        get(Urls.requestJoinGroupUrl,  (request, response) -> generateRequestJoinGroupView(request), new MustacheTemplateEngine());

        get(Urls.authFailUrl, (request, response) -> pickReasonForAuthFail(request, response), new MustacheTemplateEngine());

        get(Urls.loginSuccess, (request, response) -> signSuccess(request), new MustacheTemplateEngine());
        get(Urls.errorUrl, (request, response) -> pickReasonForError(request, response), new MustacheTemplateEngine());

        get(Urls.listFriendsUrl,  (request, response) -> showFriends(request, response), new MustacheTemplateEngine());
        get(Urls.listRequestsUrl,  (request, response) -> showRequests(request, response), new MustacheTemplateEngine());
        get(Urls.listCommentsUrl,  (request, response) -> showComments(request, response), new MustacheTemplateEngine());
        get(Urls.listGroupsUrl,  (request, response) -> showGroups(request, response), new MustacheTemplateEngine());
    }

    private static ModelAndView generateRequestFriendView(Request request){
        Map<String, String> map = pickCurrentUser(request);
        String title = "Request Friend";
        String handleUrl = Urls.handleRequestFriendUrl;
        String paramName = Params.friendID;
        map.put("title", title);
        map.put("handleUrl", handleUrl);
        map.put("paramName", paramName);
        return new ModelAndView(map, Templates.requestTemplate);
    }

    private static ModelAndView generateRequestJoinGroupView(Request request){
        Map<String, String> map = pickCurrentUser(request);
        String title = "Request to join group";
        String handleUrl = Urls.handleRequestJoinGroupUrl;
        String paramName = Params.groupId;
        map.put("title", title);
        map.put("handleUrl", handleUrl);
        map.put("paramName", paramName);
        return new ModelAndView(map, Templates.requestTemplate);
    }

    private static ModelAndView generateSignUpView(Request request){
        Map<String, String> map = Maps.newHashMap();
        map.put("handleUrl", Urls.handleSignupUrl);
        map.put("origin", request.queryMap("origin").value());
        return new ModelAndView(map, Templates.signUpTemplate);
    }

    private static ModelAndView generateCreateGroupView(Request request){
        Map<String, String> map = Maps.newHashMap();
        map.put("handleUrl", Urls.handleCreateGroupUrl);
        return new ModelAndView(map, Templates.createGroupTemplate);
    }


    private static ModelAndView signout(Request request){
        LogOn.logout(request);
        Map<String, String> map = Maps.newHashMap();
        map.put("handleUrl", Urls.handleSignupUrl);
        return new ModelAndView(map, Templates.signUpTemplate);
    }

    private static ModelAndView signSuccess(Request request){
        String uid = LogOn.getCurrentId(request);
        String provider = request.queryMap("provider").value();
        Map<String, String> map = Maps.newHashMap();
        map.put("name", uid);
        if(null != provider)
            map.put("provider", provider);
        return new ModelAndView(map, Templates.successTemplate);
    }

    private static Filter signupFilter(){
        Filter filter = (request, response) -> {
            String goal = request.pathInfo();
            logger.info("target url = " + goal);
            if(!urlsexclude.contains(goal) && !LogOn.isActive(request)) {
                logger.info(goal + " is filtered");
                if(transparentUrls.contains(goal))
                    response.redirect(Urls.signUpUrl);
                else{
                    StringBuffer withOrigin = new StringBuffer(Urls.signUpUrl);
                    withOrigin.append("?origin=");
                    withOrigin.append(goal);
                    logger.info(withOrigin.toString());
                    response.redirect(withOrigin.toString());
                }
            }
        };
        return filter;
    }

    static ModelAndView showFriends(Request request, Response response){
        String currentId = LogOn.getCurrentId(request);

        try{
            AccountView accountView = new AccountView();
            Set<AccountInformation> friendInfos = accountView.listFriends(currentId);

            Map map = Maps.newHashMap();
            map.put("id", currentId);

            if(null == friendInfos || friendInfos.isEmpty())
                return new ModelAndView(map, Templates.listFriendsTemplate);

            List<Map<String, String>> results = Lists.newLinkedList();
            Iterator<AccountInformation> iterator = friendInfos.iterator();
            while(iterator.hasNext()){
                AccountInformation friendInfo = iterator.next();

                String friendId = friendInfo.getAccountId();
                Map<String, String> info = Maps.newHashMap();
                    info.put("id", friendId);
                    results.add(info);
            }
            map.put("friends", results);
            return new ModelAndView(map, Templates.listFriendsTemplate);
        } catch(TripleJException te){
            Map map = Maps.newHashMap();
            map.put("reason", te.getMessage());
            return new ModelAndView(map, Templates.errorTemplate);
        }
    }

    static ModelAndView showGroups(Request request, Response response) {
        String currentUser = LogOn.getCurrentId(request);
        try {
            GroupView groupView = new GroupView();
            Set<GroupInformation> groups = groupView.listGroups(currentUser);

            Map map = Maps.newHashMap();
            map.put("id", currentUser);

            if(null == groups || groups.isEmpty())
                return new ModelAndView(map, Templates.listGroupsTemplate);

            List<Map<String, String>> results = Lists.newLinkedList();
            Iterator<GroupInformation> iterator = groups.iterator();
            while(iterator.hasNext()){
                GroupInformation groupInformation = iterator.next();
                Group group = groupInformation.getGroup();
                Map<String, String> info = Maps.newHashMap();
                info.put("id", groupInformation.getIdentifier());
                info.put("name", group.getName());
                info.put("introduction", group.getIntroduction());
                info.put("creators", group.getCreators().toString());
                results.add(info);
            }
            map.put("groups", results);
            return new ModelAndView(map, Templates.listGroupsTemplate);
        } catch (InitializeException e) {
            e.printStackTrace();
        } catch (ServiceNotAvailableException e) {
            e.printStackTrace();
        } catch (UserException e) {
            e.printStackTrace();
        }
        return null;
    }


    static ModelAndView showComments(Request request, Response response){
        String currentUser = LogOn.getCurrentId(request);
        try{
            PostingView postingView = new PostingView();
            List<RootedTree<CommentInformation>> timelines = postingView.listTimelines(currentUser, 0, 10);

            Map map = Maps.newHashMap();
            map.put("id", currentUser);
            if(null == timelines || timelines.isEmpty())
                return new ModelAndView(map, Templates.listCommentsTemplate);

            List<Map<String, String>> results = Lists.newLinkedList();
            for(int i=0;i<timelines.size();i++){
                RootedTree<CommentInformation> rootedTree = timelines.get(i);
                CommentInformation root = rootedTree.getRoot();
                String rootId = root.getIdentifier();
                Comment comment = root.getComment();
                String content = comment.getContent();
                String author = root.getAuthor();
                String listener = root.getListener();
                boolean hasListener = null != listener && !listener.isEmpty();
                int size = rootedTree.getMembers().size() - 1;

                Map<String, String> info = Maps.newHashMap();
                info.put("commentId", rootId);
                info.put("comment", content);
                info.put("author", author);
                info.put("listener", listener);
                if(hasListener)
                    info.put("hasListener", "true");
                else
                    info.put("hasListener", "false");

                if(size  > 0){
                    info.put("hasReplies", "true");
                    info.put("rSize", new Integer(size).toString());
                }

                results.add(info);
            }
            map.put("comments", results);
            return new ModelAndView(map, Templates.listCommentsTemplate);
        } catch(TripleJException te){
            Map map = Maps.newHashMap();
            map.put("reason", te.getMessage());
            return new ModelAndView(map, Templates.errorTemplate);
        }
    }

    static ModelAndView showRequests(Request request, Response response){
        String currentUser = LogOn.getCurrentId(request);

        try {
            RequestView requestView = new RequestView();
            List<RequestInformation> lists = requestView.listRequests(currentUser, 0 ,10);
            List<RequestInformation> listsForGroup = requestView.listGroupRequest(currentUser, 0 ,10);
            if(null != listsForGroup && !listsForGroup.isEmpty())
                lists.addAll(listsForGroup);

            Map map = Maps.newHashMap();
            map.put("id", currentUser);
            if(null == lists || lists.isEmpty())
                return new ModelAndView(map, Templates.listRequestTemplate);

            List<Map<String, String>> results = Lists.newLinkedList();
            for(int i=0;i<lists.size();i++){
                RequestInformation ri = lists.get(i);
                String requestor = ri.getRequestor();
                String responder = ri.getResponder();
                RequestStatus status = ri.getCurrentStatus();
                com.tripleJ.ds.pair.Request req = ri.getRequest();
                String message = req.getComment();
                RequestType type = req.getRequestType();
                Map<String, String> info = Maps.newHashMap();
                info.put("type", type.name());
                info.put("requestor", requestor);
                info.put("responder", responder);
                info.put("status", status.name());
                info.put("message", message);
                if(hasPermissionToHandleRequest(ri, currentUser)) {
                    info.put("needResponse", "true");
                    String id = ri.getIdentifier();
                    StringBuffer url = new StringBuffer(Urls.handleRespondRequestUrl);
                    url.append("?requestId="+id);
                    url.append("&action=");
                    info.put("acceptUrl", new StringBuffer(url).append("accept").toString());
                    info.put("declineUrl", new StringBuffer(url).append("decline").toString());
                }

                results.add(info);
            }
            map.put("requests", results);
            return new ModelAndView(map, Templates.listRequestTemplate);
        }catch(TripleJException te){
            Map map = Maps.newHashMap();
            map.put("reason", te.getMessage());
            return new ModelAndView(map, Templates.errorTemplate);
        }
    }

    static boolean hasPermissionToHandleRequest(RequestInformation requestInformation, String currentUser){
        RequestStatus status = requestInformation.getCurrentStatus();
        if(status.getValue() > RequestStatus.NOTIFIED.getValue())
            return false;

        String requestor = requestInformation.getRequestor();
        String responder = requestInformation.getResponder();

        com.tripleJ.ds.pair.Request req = requestInformation.getRequest();
        RequestType type = req.getRequestType();

        if(type == RequestType.AddRelation)
            return (responder.compareTo(currentUser) == 0);
        if(type == RequestType.JoinGroup){
            return (requestor.compareTo(currentUser) != 0);
        }
        return false;
    }

    static ModelAndView pickReasonForAuthFail(Request request, Response response){
        String reason = request.queryMap(Params.errorReason).value();
        if(null == reason)
            reason = "Unknown";

        Map<String,String> map = Maps.newHashMap();
        map.put("reason", reason);
        return new ModelAndView(map, Templates.authFailTemplate);
    }

    static ModelAndView pickReasonForError(Request request, Response response){
        String reason = request.queryMap(Params.errorReason).value();
        if(null == reason)
            reason = "Unknown";

        Map<String,String> map = Maps.newHashMap();
        map.put("reason", reason);
        return new ModelAndView(map, Templates.errorTemplate);
    }


    static Map<String, String> pickCurrentUser(Request request){
        Map<String, String> map = Maps.newHashMap();
        String id = LogOn.getCurrentId(request);
        if(null == id)
            map.put("id", "Who?");
        else
            map.put("id", id);
        return map;
    }
}
