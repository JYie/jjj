package com.triplej.web;

import com.tripleJ.*;
import com.tripleJ.ds.comment.Comment;
import com.tripleJ.service.CommentAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;


/**
 * Created by kami on 2014. 7. 31..
 */
public class HandleComment extends BasicRoute {

    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleComment.class);
    }

    @Override
    boolean doActual(Request request, Response response) {
        String author = LogOn.getCurrentId(request);
        String listener = request.queryMap(Params.listener).value();
        String comment = request.queryMap(Params.comment).value();
        Comment comment1 = new Comment(comment);

        try {
            CommentAction commentAction = new CommentAction();
            commentAction.addCommentToAccount(author, listener, comment1);
            super.url = Urls.listCommentsUrl;
            return true;
        }catch (UnauthorizedException uae) {
            super.errorUrl = Urls.authFailUrl;
            super.error = uae.getMessage();
            logger.error(error);
            return false;
        } catch (TripleJException te) {
            super.errorUrl = Urls.errorUrl;
            super.error = te.getMessage();
            logger.error(error);
            return false;
        }
    }
}
