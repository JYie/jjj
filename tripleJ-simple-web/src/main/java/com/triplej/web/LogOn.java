package com.triplej.web;

import spark.Request;
import spark.Session;

/**
 * Created by kami on 2014. 7. 31..
 */
public class LogOn {

    private static final String LogInFlag = "LogInFlag";
    private static final String ID = "ID";

    public static boolean isActive(Request request){
        Session session = request.session();
        if(null == session.attribute(LogInFlag))
            return false;
        else
            return session.attribute(LogInFlag);
    }

    public static String getCurrentId(Request request){
        Session session = request.session();
        return session.attribute(ID);
    }


    public static void login(Request request, String uid){
        Session session = request.session();
        session.attribute(LogInFlag, true);
        session.attribute(ID, uid);
    }

    public static void logout(Request request){
        Session session = request.session();
        session.invalidate();
    }
}
