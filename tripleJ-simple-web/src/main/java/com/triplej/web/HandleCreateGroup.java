package com.triplej.web;

import com.tripleJ.TripleJException;
import com.tripleJ.ds.group.Group;
import com.tripleJ.service.GroupAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 * Created by kami on 2014. 8. 6..
 */
public class HandleCreateGroup extends BasicRoute {
    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleCreateGroup.class);
    }

    @Override
    boolean doActual(Request request, Response response) {
        String creator = LogOn.getCurrentId(request);
        String groupName = request.queryMap(Params.groupId).value();
        String intro = request.queryMap(Params.introduction).value();

        try {
            Group group = new Group(creator, groupName, intro, null);
            GroupAction groupAction = new GroupAction();
            groupAction.createGroup(groupName, group);
            super.url = Urls.listGroupsUrl;
            return true;
        } catch (TripleJException e) {
            super.error = e.getMessage();
            logger.error(error);
            super.errorUrl = Urls.errorUrl;
            return false;
        }
    }
}
