package com.triplej.web;

/**
 * Created by kami on 2014. 8. 1..
 */
public class Templates {

    static final String signUpTemplate = "signup.mustache";
    static final String createGroupTemplate = "createGroup.mustache";
    static final String commentAddTemplate = "addComment.mustache";
    static final String requestTemplate = "request.mustache";

    static final String listRequestTemplate = "listRequests.mustache";
    static final String listCommentsTemplate = "listComments.mustache";
    static final String listFriendsTemplate = "listFriends.mustache";
    static final String listGroupsTemplate = "listGroups.mustache";

    static final String authFailTemplate = "authFail.mustache";
    static final String errorTemplate = "error.mustache";
    static final String successTemplate = "success.mustache";

}
