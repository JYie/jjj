package com.triplej.web;

import com.tripleJ.*;
import com.tripleJ.ds.comment.Comment;
import com.tripleJ.ds.relation.TreeException;
import com.tripleJ.service.CommentAction;
import com.tripleJ.service.UnrecognizableException;
import com.tripleJ.unique.InvalidIdException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Created by kami on 2014. 8. 4..
 */
public class HandleReComment extends BasicRoute {

    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleReComment.class);
    }

    @Override
    boolean doActual(Request request, Response response) {
        String author = LogOn.getCurrentId(request);
        String pCommentId = request.queryMap(Params.commentId).value();
        String reComment = request.queryMap(Params.comment).value();

        Comment comment1 = new Comment(reComment);

        try {
            CommentAction commentAction = new CommentAction();
            commentAction.addReply(author, pCommentId, comment1);
            super.url = Urls.listCommentsUrl;
            return true;
        }catch (UnauthorizedException uae) {
            super.errorUrl = Urls.authFailUrl;
            super.error = uae.getMessage();
            logger.error(error);
            return false;
        }catch (TripleJException ie) {
            super.errorUrl = Urls.errorUrl;
            error = ie.getMessage();
            logger.error(error);
            return false;
        }
    }
}
