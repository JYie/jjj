package com.triplej.web;

import com.google.common.collect.Maps;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.tripleJ.InitializeException;
import com.tripleJ.ds.MongoProvider;
import com.tripleJ.service.Provider;

import java.io.IOException;
import java.io.InputStream;
import java.lang.instrument.UnmodifiableClassException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Properties;

/**
 * Initializer is the class to set configurations for Spark Service.
 * It assumes that we use the configuration file named "configuration/triplej-web-service.conf"
 * Created by kami on 2014. 7. 30..
 */
public class Initializer {

    private static Properties configuration;
    private static final String defaultDB = "TripleJ_Simple_Web";
    static void loadConfiguration() throws IllegalConfigurationException {
        configuration = new Properties();
        InputStream in = Initializer.class.getClassLoader().getResourceAsStream(
                "configuration/triplej-web-service.conf");
        if (in == null) {
            // throw new IllegalConfigurationException(
            // "Could not find service.conf");
            throw new IllegalConfigurationException(
                    "Could not find triplej-web-service.conf");

        } else {
            try {
                configuration.load(in);
            } catch (IOException ioe) {
                throw new IllegalConfigurationException(
                        "triplej-web-service.conf is not analyzed");
            }
        }
    }

    static void applyStoreConfiguration() throws IllegalConfigurationException{
        String storeType = configuration.getProperty("storeType");
        if(null == storeType || storeType.isEmpty())
            applyDefaulltStore();

        if(storeType.compareToIgnoreCase("mongo") == 0 || storeType.compareToIgnoreCase("mongodb") == 0)
            applyMongoStoreConfiguration();

        if(storeType.compareToIgnoreCase("rdb") == 0)
            applyRDBStoreConfiguration();
    }

    static void applyDefaulltStore() throws IllegalConfigurationException{
        applyMongoStoreConfiguration();
    }

    static void applyMongoStoreConfiguration() throws IllegalConfigurationException{
        String mongoUrl = configuration.getProperty("mongo.url");
        String mongoPort = configuration.getProperty("mongo.port");

        String url;
        if(null == mongoUrl || mongoUrl.isEmpty())
            url = "127.0.0.1";
        else
            url = mongoUrl;

        int port;
        if(null == mongoPort || mongoPort.isEmpty())
            port = 27017;
        else {
            try{
                port = Integer.parseInt(mongoPort);
            }catch(NumberFormatException nfe){
                throw new IllegalConfigurationException("mongo.port has to have a number type");
            }
        }

        String dbName = configuration.getProperty("mongo.db");
        if(null == dbName || dbName.isEmpty())
            dbName = defaultDB;

        ServerAddress address = null;
        try {
            address = new ServerAddress(url,port);
        } catch (UnknownHostException e) {
            throw new IllegalConfigurationException("Host information is not valid or server is not running");
        }

        MongoClient client = new MongoClient(address);
        DB db = client.getDB(dbName);

        MongoProvider.registerDB(db);

        Map<Provider.StoreKey, String> stores = Maps.newHashMap();
        stores.put(Provider.StoreKey.Account, "com.tripleJ.ds.account.MongoAccountStore");
        stores.put(Provider.StoreKey.Preference, "com.tripleJ.ds.account.MongoPreferenceStore");
        stores.put(Provider.StoreKey.Comment, "com.tripleJ.ds.comment.MongoCommentStore");
        stores.put(Provider.StoreKey.Group, "com.tripleJ.ds.group.MongoGroupStore");
//        stores.put(Provider.StoreKey.Capacity, "com.tripleJ.ds.group.MongoCapacityStore"); Uses a defaultCapacityStore
        stores.put(Provider.StoreKey.Request, "com.tripleJ.ds.pair.MongoRequestStore");
        stores.put(Provider.StoreKey.Orientation, "com.tripleJ.ds.relation.MongoOrientationStore");
        try {
            Provider.registerStores(stores);
            System.out.println("success to register stores");
        } catch (InitializeException ie) {
            ie.printStackTrace();
            assert(false);
        } catch (UnmodifiableClassException ue) {
            ue.printStackTrace();
            assert(false);
        }
    }

    static void applyRDBStoreConfiguration(){

    }
}
