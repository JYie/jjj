package com.triplej.web;

/**
 * Created by kami on 2014. 8. 1..
 */
public class Urls {

    static final String styleUrl = "/stylesheet.css";

    static final String signUpUrl = "/signup";
    static final String handleSignupUrl = "/handleSignup";
    static final String handleCreateGroupUrl = "/handleCreateGroup";
    static final String signOutUrl = "/signout";
    static final String authFailUrl = "/authFail";
    static final String createGroupUrl = "/createGroup";
    static final String addCommentUrl = "/addComment";
    static final String handleCommentUrl = "/handleComment";
    static final String handleReCommentUrl = "/handleReComment";
    static final String handleRequestFriendUrl = "/handleRequestFriend";
    static final String handleRequestJoinGroupUrl = "/hanldeRequestJoinGroup";
    static final String handleRespondRequestUrl = "/handleRespondRequest";

    static final String requestFriendUrl = "/requestFriend";
    static final String requestJoinGroupUrl = "/requestJoinGroup";
    static final String listFriendsUrl = "/listFriends";
    static final String listRequestsUrl = "/listRequests";
    static final String listCommentsUrl = "/listComments";
    static final String listGroupsUrl = "/listGroups";
    static final String loginSuccess = "/success";
    static final String errorUrl = "/error";

}
