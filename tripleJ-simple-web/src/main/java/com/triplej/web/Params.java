package com.triplej.web;

/**
 * Created by kami on 2014. 8. 4..
 */
public class Params {

    static final String errorReason = "errorReason";

    static final String accountId = "accountId";
    static final String groupId = "groupId";
    static final String introduction = "introduction";
    static final String friendID = "friendId";
    static final String commentId = "commentId";
    static final String comment = "comment";
    static final String listener = "listener";
}
