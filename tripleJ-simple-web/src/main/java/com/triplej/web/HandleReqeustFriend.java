package com.triplej.web;

import com.tripleJ.*;
import com.tripleJ.ds.pair.InappropriateStatusException;
import com.tripleJ.service.AccountAction;
import com.tripleJ.service.UnrecognizableException;
import com.tripleJ.unique.InvalidIdException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Created by kami on 2014. 7. 31..
 */
public class HandleReqeustFriend extends BasicRoute{

    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleReqeustFriend.class);
    }

    @Override
    boolean doActual(Request request, Response response) {
        String requestor = LogOn.getCurrentId(request);
        String responder = request.queryMap(Params.friendID).value();
        try {
            AccountAction accountAction = new AccountAction();
            accountAction.requestToAddRelation(requestor, responder);
            super.url = Urls.listRequestsUrl;
            return true;
        }catch (UnauthorizedException uae) {
            super.errorUrl = Urls.authFailUrl;
            super.error = uae.getMessage();
            logger.error(error);
            return false;
        }catch (TripleJException e) {
            super.errorUrl = Urls.errorUrl;
            super.error = e.getMessage();
            logger.error(error);
            return false;
        }
    }
}
