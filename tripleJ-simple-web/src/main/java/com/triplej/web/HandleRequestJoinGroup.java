package com.triplej.web;

import com.tripleJ.TripleJException;
import com.tripleJ.service.GroupAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 * Created by kami on 2014. 8. 6..
 */
public class HandleRequestJoinGroup extends BasicRoute {
    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleRequestJoinGroup.class);
    }

    @Override
    boolean doActual(Request request, Response response) {
        String requestor = LogOn.getCurrentId(request);
        String groupId = request.queryMap(Params.groupId).value();
        try {
            GroupAction groupAction = new GroupAction();
            groupAction.registerRequestToJoin(requestor, groupId);
            logger.info(requestor + " requests to join group " + groupId);
            super.url = Urls.listRequestsUrl;
            return true;
        } catch (TripleJException e) {
            super.error = e.getMessage();
            logger.error(error);
            super.errorUrl = Urls.errorUrl;
            return false;
        }
    }
}
