package com.triplej.web;

import com.tripleJ.TripleJException;
import com.tripleJ.ds.account.Account;
import com.tripleJ.ds.account.AlreadyExistException;
import com.tripleJ.service.AccountAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Created by kami on 2014. 8. 1..
 */
public class HandleSignIn implements Route {

    private static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HandleSignIn.class);
    }

    @Override
    public Object handle(Request request, Response response) {
        String origin = request.queryMap("origin").value();
        String name = request.queryMap("name").value();
        String email = request.queryMap("email").value();
        int pointAt = email.indexOf('@');
        String provider;
        if(-1 != pointAt)
            provider = email.substring(email.indexOf('@') + 1);
        else {
            String reason = "email is not valid";
            response.redirect(Urls.errorUrl+"?reason="+reason);
            return null;
        }

        String uid = generateID(provider, name);
        LogOn.login(request, uid);

        Account account = null;
        try {
            account = new Account(name, name, provider, email, null);
            AccountAction accountAction = new AccountAction();
            accountAction.registerAccount(uid, account);
        }catch(AlreadyExistException ae){
            logger.info(uid + "alredy exists");
        }catch (TripleJException e) {
            response.redirect(Urls.errorUrl+"?"+Params.errorReason+"="+e.getMessage());
            return null;
        }
        if(null != origin && !origin.isEmpty() && origin.compareTo("/") != 0) {
            response.redirect(origin);
        }
        else {
            StringBuffer go = new StringBuffer(Urls.loginSuccess);
            if(null != provider && !provider.isEmpty()) {
                go.append("?provider=");
                go.append(provider);
            }
            response.redirect(go.toString());
        }
        return null;
    }

    private String generateID(String provider, String uid){
        StringBuffer sb = new StringBuffer();
        sb.append(provider);
        sb.append(":");
        sb.append(uid);
        return sb.toString();

    }
}
