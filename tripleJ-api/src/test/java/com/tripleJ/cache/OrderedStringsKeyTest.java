package com.tripleJ.cache;

import org.junit.Test;

import static org.junit.Assert.*;

public class OrderedStringsKeyTest {
    @Test
    public void testComparing2(){
        String[] s1 = {"a","b","c"};
        String[] s2 = {"b","a","c"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparing1(){
        String[] s1 = {"a","b","c"};
        String[] s2 = {"a","b","c"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(key1.equals(key2));
        assertTrue(key1.hashCode() == key2.hashCode());
    }

    @Test
    public void testComparing3(){
        String[] s1 = {"a","bc"};
        String[] s2 = {"a","b","c"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparing4(){
        String[] s1 = {"ab","cd","e"};
        String[] s2 = {"abc","d","e"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparingIgnoringEmpty1(){
        String[] s1 = {"a","b","c"};
        String[] s2 = {"a","b","c", ""};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparingIgnoringEmpty2(){
        String[] s1 = {"a","b", "", "c"};
        String[] s2 = {"", "a","b","c", ""};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparingIngoringDuplication(){
        String[] s1 = {"a","b","c","c"};
        String[] s2 = {"a","b","c"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparingIgoringNull1(){
        String[] s1 = {"a","b", null,"c"};
        String[] s2 = {"a","b","c"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testComparingIgnoringNull2(){
        String[] s1 = {"a","b","c"};
        String[] s2 = {"a",null,"b","c"};

        OrderedStringsKey key1 = new OrderedStringsKey(s1);
        OrderedStringsKey key2 = new OrderedStringsKey(s2);

        assertTrue(!key1.equals(key2));
        assertTrue(key1.hashCode() != key2.hashCode());
    }

    @Test
    public void testIsAffecting(){
        String[] s1 = {"a","b","c"};
        OrderedStringsKey key1 = new OrderedStringsKey(s1);

        String effect = "a";
        String no_effect1 = "d";
        String no_effect2 = "ab";
        String no_effect3 = null;
        boolean result1 = key1.isAffected(effect);
        boolean result2 = key1.isAffected(no_effect1);
        boolean result3 = key1.isAffected(no_effect2);
        boolean result4 = key1.isAffected(no_effect3);

        assertTrue(result1);
        assertTrue(!result2);
        assertTrue(!result3);
        assertTrue(!result4);
    }
}
