package com.tripleJ.unique;

import com.google.common.collect.Maps;
import com.tripleJ.TripleJType;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class IDGeneratorTest {

    @BeforeClass
    public static void setUpBefore(){
        IDGenerator.DEV_VERSION = false;
    }

    @Test
    public void testSkipping() throws Exception{
        Map<String, String> resources1 = Maps.newHashMap();
        resources1.put("id", "vinetree");
        resources1.put("length", "12");
        resources1.put("9","value");

        String generated1 = IDGenerator.generateId(TripleJType.Account, resources1);

        Map<String, String> resources2 = Maps.newHashMap();
        resources2.put("id", "vinetree");

        String generated2 = IDGenerator.generateId(TripleJType.Account, resources2);

        assertTrue(generated1.compareTo(generated2) == 0);
    }

    @Test
    public void testCheckNormal() throws Exception {
        Map<String, String> resources = Maps.newHashMap();
        resources.put("id", "vinetree");

        String generated = IDGenerator.generateId(TripleJType.Account, resources);
        System.out.println(generated);
        try{
            new IDGenerator(generated).checkValidity();
            assertTrue(true);
        }catch(InvalidIdException ide){
            fail();
        }
    }

    @Test(expected = InvalidIdException.class)
    public void testCheckInvalidCase1() throws Exception{
        Map<String, String> resources = Maps.newHashMap();
        resources.put("id", "vinetree");

        String generated = IDGenerator.generateId(TripleJType.Account, resources);
        String forged1 = generated + "1";
        new IDGenerator(forged1).checkValidity();
    }

    @Test(expected = InvalidIdException.class)
    public void testCheckInvalidCase2() throws Exception{
        Map<String, String> resources = Maps.newHashMap();
        resources.put("id", "vinetree");

        String generated = IDGenerator.generateId(TripleJType.Account, resources);

        String forged2 = generated.substring(1);
        new IDGenerator(forged2).checkValidity();

        String forged3 = generated.substring(0, generated.length() - 2);
        new IDGenerator(forged3).checkValidity();
    }

    @Test(expected = InvalidIdException.class)
    public void testCheckInvalidCase3() throws Exception{
        Map<String, String> resources = Maps.newHashMap();
        resources.put("id", "vinetree");

        String generated = IDGenerator.generateId(TripleJType.Account, resources);

        String forged3 = generated.substring(0, generated.length() - 2);
        new IDGenerator(forged3).checkValidity();
    }
}