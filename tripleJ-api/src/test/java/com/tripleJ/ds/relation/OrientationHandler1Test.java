package com.tripleJ.ds.relation;

import java.util.HashSet;
import java.util.Set;


import com.tripleJ.*;
import com.tripleJ.unique.InvalidIdException;
import org.junit.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Sets;

public class OrientationHandler1Test {

    protected static OrientationHandler orientationHandler;

    @BeforeClass
    public static void setUpBeforeClass() {

        try {
            orientationHandler = new OrientationHandler(new RelationHandlerForBasicTest());
        } catch (InitializeException ne) {
            assert (false);
        }
        System.out.println("----- Start OrientedRelationHandler1Test -----");
    }

    @Test
    public void testFindingNeighbors() {
         /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        try {
            Set<String> NeoOutNeighbors = orientationHandler.getOutNeighbors(TripleJType.Follows, "Neo");
            Set<String> CypherInNeighbors = orientationHandler.getInNeighbors(TripleJType.Follows,"Cypher");
            Set<String> CyperNeighbors = orientationHandler.getOutNeighbors(TripleJType.Follows, "Cypher");
            Assert.assertTrue(NeoOutNeighbors.size() == 2
                    && NeoOutNeighbors.contains("Trinity")
                    && NeoOutNeighbors.contains("Cypher"));
            Assert.assertTrue(CypherInNeighbors.size() == 1
                    && CypherInNeighbors.contains("Neo"));
            Assert.assertTrue(CyperNeighbors == null || CyperNeighbors.size() == 0);
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    public void testGettingDepth() {
        orientationHandler.setMaxDepth(5);
        Assert.assertTrue(3 == orientationHandler.getDepthFromSource());
        Assert.assertTrue(2 == orientationHandler.getDepthFromDestination());

        orientationHandler.setMaxDepth(4);
        Assert.assertTrue(2 == orientationHandler.getDepthFromSource());
        Assert.assertTrue(2 == orientationHandler.getDepthFromDestination());
    }

    @Test(expected = TreeException.class)
    public void testBlockingToGenerateCycle1() throws TreeException {
        String source = "Trinity";
        String destination = "Morpheus";

        try {
            orientationHandler.addOrientation(TripleJType.Comment, source, destination); //To check the tree characteristic, we used TripleType.Comment which has a tree characteristic
        } catch (MissingRequiredException e) {
            Assert.fail();
        } catch (StoreException e) {
            Assert.fail();
        } catch (InvalidIdException e) {
            Assert.fail();
        } catch (IncompatibleException e) {
            Assert.fail();
        }
    }

    @Test
    public void testGettingIntersectionRecursivelyAsDirection1() {
		 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String source = "Morpheus";
        String destination = "Agent Smith";
        Set<String> srcs = new HashSet<String>();
        srcs.add(source);
        Set<String> dests = new HashSet<String>();
        dests.add(destination);
        try {
            int expected = orientationHandler.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests);
            int actual = 3;

            System.out.println("testGettingIntersectionRecursivelyAsDirection1");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + actual);

            Assert.assertTrue(actual == expected);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testGettingIntersectionRecursivelyAsDirection2() {
		 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String source = "Trinity";
        String destination = "Morpheus";
        Set<String> srcs = new HashSet<String>();
        srcs.add(source);
        Set<String> dests = new HashSet<String>();
        dests.add(destination);
        try {
            int expected = orientationHandler.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests);
            int actual = -1;

            System.out.println("testGettingIntersectionRecursivelyAsDirection2");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + actual);

            Assert.assertTrue(actual == expected);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testGettingIntersectionRecursively() {
		 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String source = "Agent Smith";
        String destination = "Morpheus";
        Set<String> srcs = new HashSet<String>();
        srcs.add(source);
        Set<String> dests = new HashSet<String>();
        dests.add(destination);
        try {
            int expected = orientationHandler.findIntersectionPointRecursively(TripleJType.Follows, 0, srcs, dests);
            int actual = 3;

            System.out.println("testGettingIntersectionRecursively");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + actual);

            Assert.assertTrue(actual == expected);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingPathByOutDirection() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String[] srcs = {"Neo"};
        try {

            Set<String> actual = orientationHandler.getNextNeighborsByOutDirection(TripleJType.Follows, srcs);
            String[] expected = {"Cypher", "Trinity"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByOutDirection");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }

    }

    @Test
    public void testExpandingPathByInDirection() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String[] dests = {"Neo", "Trinity"};

        try {
            Set<String> actual = orientationHandler.getNextNeighborsByInDirection(TripleJType.Follows, dests);
            String[] expected = {"Morpheus", "Trinity", "Neo"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByInDirection");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

            Assert.assertTrue(expected_.containsAll(actual));
            Assert.assertTrue(actual.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingPathByBothDirection() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String[] srcs = {"Neo", "Trinity"};
        try {

            Set<String> actual = orientationHandler.getNextNeighborsByBothDirection(TripleJType.Follows, srcs);
            String[] expected = {"Morpheus", "Trinity", "Neo", "Cypher", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByBothDirection");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

            Assert.assertTrue(expected_.containsAll(actual));
            Assert.assertTrue(actual.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }

    }

    @Test
    public void testExpandingPathByIndirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String[] dests = {"Agent Smith"};
        int max = 4;
        orientationHandler.setMaxDepth(max);

        try {

            Set<String> actual = orientationHandler.expandByInDirectionRecursively(TripleJType.Follows, max - 1, Sets.newHashSet(dests));
            String[] expected = {"Morpheus", "Trinity", "Neo", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByIndirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

            Assert.assertTrue(expected_.containsAll(actual));
            Assert.assertTrue(actual.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingWKPathByIndirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */


        final int max = 4;
        final int distance = 3;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.expandByInDirectionRecursively(TripleJType.Follows, distance, 0, "Agent Smith", null);
            if(null == actual)
                Assert.fail();

            Set<String> actual_ = actual.getWings();
            String[] expected = {"Trinity", "Neo", "Morpheus", "Agent Smith"};
//            Knowing[] expected = {new Knowing("Agent Smith", "Neo", true, 2, false), new Knowing("Agent Smith", "Trinity", true, 1, false),
//                    new Knowing("Agent Smith", "Morpheus", true, 3, false),
//                    new Knowing("Agent Smith", "Agent Smith", true, 0, false)};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathWKByIndirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testGoingWKPathByIndirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */

        Knowing[] destinations = {new Knowing("Agent Smith", "Agent Smith", true, 0, false)};
        final int max = 4;
        final int distance = 3;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.goByInDirectionRecursively(TripleJType.Follows, distance, 0, "Agent Smith", null);
            if(null == actual)
                Assert.fail();

            Set<String> actual_ = actual.getWings();
//            Knowing[] expected = {new Knowing("Agent Smith", "Trinity", true, 3, false),
//                    new Knowing("Agent Smith", "Morpheus", true, 3, false)};
            String[] expected = {"Morpheus", "Trinity"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testGoingPathWKByIndirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingPathByOutdirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String[] source = {"Neo"};
        final int max = 4;
        final int distance = 3;
        orientationHandler.setMaxDepth(max);

        try {
            Set<String> actual = orientationHandler.expandByOutDirectionRecursively(TripleJType.Follows, distance, Sets.newHashSet(source));
            String[] expected = {"Cypher", "Trinity", "Neo", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByOutdirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

            Assert.assertTrue(expected_.containsAll(actual));
            Assert.assertTrue(actual.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingPathWKByOutdirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */
        final int max = 4;
        final int distance = 3;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.expandByOutDirectionRecursively(TripleJType.Follows, distance, 0, "Neo", null);
            Set<String> actual_ = actual.getWings();
            String[] expected = {"Cypher", "Trinity", "Neo", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByOutdirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testGoingPathWKByOutdirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */
        final int max = 4;
        final int distance = 2;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.goByOutDirectionRecursively(TripleJType.Follows, distance, 0, "Neo", null);
            Set<String> actual_ = actual.getWings();
            String[] expected = {"Neo", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testGoingPathByOutdirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingPathByBothdirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^                      
         *     v     \                     
         * (Cypher) (Morpheus)   
         */
        String[] sources = {"Trinity"};
        int max = 3;
        orientationHandler.setMaxDepth(max);
        try {
            Set<String> actual = orientationHandler.expandByBothDirectionRecursively(TripleJType.Follows, max - 1, Sets.newHashSet(sources));
            String[] expected = {"Cypher", "Trinity", "Neo", "Morpheus", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByBothdirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

            Assert.assertTrue(expected_.containsAll(actual));
            Assert.assertTrue(actual.containsAll(expected_));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingPathByRelationRecursively() {
	 	 /*
         *    (Neo) ---- (Trinity) ---- (Agent Smith)
         *     |     \
         *     |     \
         * (Cypher) (Morpheus)
         */
        String[] sources = {"Trinity"};
        int max = 3;
        orientationHandler.setMaxDepth(max);
        try {
            Set<String> actual = orientationHandler.expandByBothDirectionRecursively(TripleJType.Account, max - 1, Sets.newHashSet(sources));
            String[] expected = {"Cypher", "Trinity", "Neo", "Morpheus", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByRelationRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual);

            Assert.assertTrue(expected_.containsAll(actual));
            Assert.assertTrue(actual.containsAll(expected_));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingWKPathByBothdirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */

        final int max = 4;
        final int distance = 3;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.expandByBothDirectionRecursively(TripleJType.Follows, distance, 0, "Agent Smith", null);
            Set<String> actual_ = actual.getWings();
            String[] expected = {"Cypher", "Trinity", "Neo", "Agent Smith", "Morpheus"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingPathByBothdirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testExpandingWKPathByRelationRecursively() {
	 	 /*
         *    (Neo) ---- (Trinity) ---- (Agent Smith)
         *     |     \
         *     |     \
         * (Cypher) (Morpheus)
         */

        final int max = 4;
        final int distance = 3;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.expandByBothDirectionRecursively(TripleJType.Account, distance, 0, "Agent Smith", null);
            Set<String> actual_ = actual.getWings();
            String[] expected = {"Cypher", "Trinity", "Neo", "Agent Smith", "Morpheus"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testExpandingWKPathByRelationRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testGoingWKPathByBothdirectionRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */

        final int max = 4;
        final int distance = 2;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.goByBothDirectionRecursively(TripleJType.Follows, distance, 0, "Agent Smith", null);
            Set<String> actual_ = actual.getWings();
            String[] expected = {"Neo", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testGoingPathByBothdirectionRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testGoingWKPathByRelationRecursively() {
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */

        final int max = 4;
        final int distance = 2;
        orientationHandler.setMaxDepth(max);

        try {
            Knowings actual = orientationHandler.goByBothDirectionRecursively(TripleJType.Follows, distance, 0, "Agent Smith", null);
            Set<String> actual_ = actual.getWings();
            String[] expected = {"Neo", "Agent Smith"};
            Set<String> expected_ = Sets.newHashSet(expected);

            System.out.println("testGoingWKPathByRelationRecursively");
            System.out.println("expected : " + expected_);
            System.out.println("actual : " + actual_);

            Assert.assertTrue(expected_.containsAll(actual_));
            Assert.assertTrue(actual_.containsAll(expected_));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testHavingDirectedPath(){
	 	 /*
         *    (Neo) <----> (Trinity) ----> (Agent Smith)
         *     |    ^
         *     v     \
         * (Cypher) (Morpheus)
         */

        final int max = 4;
        orientationHandler.setMaxDepth(max);

        try {

            boolean actual1 = orientationHandler.hasDirectPath(TripleJType.Follows, "Morpheus" , "Agent Smith");
            boolean actual2 = orientationHandler.hasDirectPath(TripleJType.Follows, "Agent Smith", "Morpheus" );

            System.out.println("testHavingDirectedPath from Morpheus to Agent Smith");
            System.out.println("expected : " + true);
            System.out.println("actual : " + actual1);

            System.out.println("testHavingDirectedPath from Agent Smith to Morpheus");
            System.out.println("expected : " + false);
            System.out.println("actual : " + actual2);

            Assert.assertTrue(actual1);
            Assert.assertTrue(!actual2);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testHavingPath(){
  	 	 /*
         *    (Neo) ---- (Trinity) ---- (Agent Smith)
         *     |    \
         *     |     \
         * (Cypher) (Morpheus)
         */

        final int max = 4;
        orientationHandler.setMaxDepth(max);

        try {
            boolean actual = orientationHandler.hasPath(TripleJType.Account, "Agent Smith", "Morpheus" );

            System.out.println("testHavingPath between Morpheus and Agent Smith");
            System.out.println("expected : " + true);
            System.out.println("actual : " + actual);

            Assert.assertTrue(actual);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }

    @Test
    public void testHavingPathUnreached(){
  	 	 /*
         *    (Neo) ---- (Trinity) ---- (Agent Smith)
         *     |    \
         *     |     \
         * (Cypher) (Morpheus)
         */

        final int max = 2;
        orientationHandler.setMaxDepth(max);

        try {
            boolean actual = orientationHandler.hasPath(TripleJType.Account, "Agent Smith", "Morpheus" );

            System.out.println("testHavingPathUnreached between Morpheus and Agent Smith");
            System.out.println("expected : " + false);
            System.out.println("actual : " + actual);

            Assert.assertTrue(!actual);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail();
        }
    }


    public static class RelationHandlerForBasicTest implements OrientationStore {

        @Override
        public void addOrientation(GraphType graphType, String source, String dest) {
            // TODO Auto-generated method stub
            return;
        }

        @Override
        public void removeOrientation(GraphType graphType, String source, String dest) {
            // TODO Auto-generated method stub
            return;
        }

        @Override
        public void addRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
            return;
        }

        @Override
        public void removeRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
            return;
        }

        @Override
        public Set<Knowing> getOutNeighbors(GraphType graphType, String... sources) {
            // TODO Auto-generated method stub
	 	 	/*
	        *    (Neo) <----> (Trinity) ----> (Agent Smith)
	        *     |    ^                      
	        *     v     \                     
	        * (Cypher) (Morpheus)   
	        */
            if (null == sources)
                return null;

            Set<Knowing> result = Sets.newHashSet();
            for (String source : sources) {
                if (source.compareTo("Neo") == 0) {
                    result.add(new Knowing("Neo","Trinity",1));
                    result.add(new Knowing("Nwo","Cypher",1));
                } else if (source.compareTo("Trinity") == 0) {
                    result.add(new Knowing("Trinity","Neo",1));
                    result.add(new Knowing("Trinity","Agent Smith",1));
                } else if (source.compareTo("Morpheus") == 0)
                    result.add(new Knowing("Morpheus","Neo",1));
            }
            return result;
        }

        @Override
        public Set<Knowing> getInNeighbors(GraphType graphType, String... destinations) {
            // TODO Auto-generated method stub
 	 			/*
		        *    (Neo) <----> (Trinity) ----> (Agent Smith)
		        *     |    ^                      
		        *     v     \                     
		        * (Cypher) (Morpheus)   
		        */
            if (null == destinations)
                return null;

            Set<Knowing> result = Sets.newHashSet();
            for (String dest : destinations) {
                if (dest.compareTo("Trinity") == 0)
                    result.add(new Knowing("Neo","Trinity",1));
                else if(dest.compareTo("Cypher") == 0)
                    result.add(new Knowing("Neo","Cypher",1));
                else if (dest.compareTo("Agent Smith") == 0)
                    result.add(new Knowing("Trinity","Agent Smith",1));
                else if (dest.compareTo("Neo") == 0) {
                    result.add(new Knowing("Trinity","Neo",1));
                    result.add(new Knowing("Morpheus","Neo",1));
                }
            }
            return result;
        }

        @Override
        public Set<Knowing> getRelations(GraphType graphType, String... vertices) throws StoreException {
  	 	 	/*
	        *    (Neo) ---- (Trinity) ---- (Agent Smith)
	        *     |    \
	        *     |     \
	        * (Cypher) (Morpheus)
	        */

            Set<Knowing> result = Sets.newHashSet();
            for(String vertex: vertices){
                if(vertex.compareTo("Neo") == 0){
                    result.add(new Knowing("Neo","Cypher", false, 1));
                    result.add(new Knowing("Neo","Trinity", false, 1));
                    result.add(new Knowing("Neo","Morpheus", false, 1));
                }else if(vertex.compareTo("Cypher") == 0){
                    result.add(new Knowing("Cypher","Neo", false, 1));
                }else if(vertex.compareTo("Morpheus") == 0){
                    result.add(new Knowing("Morpheus","Neo", false, 1));
                }else if(vertex.compareTo("Trinity") == 0){
                    result.add(new Knowing("Trinity","Neo", false, 1));
                    result.add(new Knowing("Trinity","Agent Smith", false, 1));
                }else if(vertex.compareTo("Agent Smith") == 0){
                    result.add(new Knowing("Agent Smith", "Trinity", false, 1));
                }
            }
            return result;
        }
    }

}
