package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import com.tripleJ.DuplicateIdException;
import com.tripleJ.MissingRequiredException;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class TreeTest {

    private static RootedTree<String> tree;

    private static String root;
    private static String child1;
    private static Set<String> depth1;
    private static Set<String> depth2_1;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        root = "root";
        child1 = "child1";

        depth1 = Sets.newHashSet();
        depth2_1 = Sets.newHashSet();

        depth1.add("child1");
        depth1.add("child2");

        depth2_1.add("child2_1");
        depth2_1.add("child2_2");

        tree = new RootedTree(root);
        tree.addChildren(root, depth1.toArray(new String[]{}));
        tree.addChildren(child1, depth2_1.toArray(new String[]{}));
    }

    @Test
    public void testEquals() throws Exception{
        RootedTree<String> toBecompared1 = new RootedTree("root");

        toBecompared1.addChildren("root", "child2");
        toBecompared1.addChildren("root", "child1");

        toBecompared1.addChildren("child1", "child2_1");
        toBecompared1.addChildren("child1", "child2_2");

        assertTrue(tree.equals(toBecompared1));
        assertTrue(tree.hashCode() == toBecompared1.hashCode());

        RootedTree<String> toBecompared2 = new RootedTree("root");
        toBecompared2.addChildren("root", "child2");
        toBecompared2.addChildren("root", "child1");

        toBecompared2.addChildren("child2", "child2_1");
        toBecompared2.addChildren("child1", "child2_2");

        assertTrue(!tree.equals(toBecompared2));
        assertTrue(tree.hashCode() != toBecompared2.hashCode());

        RootedTree<String> toBecompared3 = new RootedTree("root");
        toBecompared3.addChildren("root", "child2");
        toBecompared3.addChildren("root", "child1");

        toBecompared3.addChildren("child1", "child2_1");

        assertTrue(!tree.equals(toBecompared3));
        assertTrue(tree.hashCode() != toBecompared3.hashCode());

    }

    @Test
    public void testGettingRoot() throws Exception{
        String root = tree.getRoot();
        if(null == root)
            fail();
        String expected = "root";
        assertTrue(expected.compareTo(root) == 0);
    }

    @Test
    public void testGettingSingleLeaf() throws Exception{
        RootedTree<String> single = new RootedTree("root");
        Set<String> actual = single.getLeaves();

        Set<String> expected = Sets.newHashSet();
        expected.add("root");

        assertTrue(Sets.difference(expected, actual).isEmpty());
    }

    @Test
    public void testGettingLeaves() throws Exception{
        Set<String> leaves = tree.getLeaves();
        if(null == leaves)
            fail();
        int expectedSize = 3;
        assertTrue(leaves.size() == expectedSize);

        Set<String> expectedLeaves = Sets.newHashSet();
        expectedLeaves.add("child2");
        expectedLeaves.add("child2_1");
        expectedLeaves.add("child2_2");

        Sets.SetView<String> differences = Sets.difference(leaves, expectedLeaves);
        assertTrue(differences.isEmpty());
    }

    @Test
    public void testGetChildren() throws Exception {
        Set<String> actual1 = tree.getChildren(root);
        if(null == actual1 || actual1.size() == 0)
            assertTrue(false);
        Sets.SetView<String> diff1 = Sets.difference(actual1, depth1);
        assertTrue(diff1.isEmpty());

        Set<String> actual2 = tree.getChildren(child1);
        if(null == actual2 || actual2.size() == 0)
            assertTrue(false);
        Sets.SetView<String> diff2 = Sets.difference(actual2, depth2_1);
        assertTrue(diff2.isEmpty());
    }


    @Test
    public void testHasChildren() throws Exception {
        assertTrue(tree.hasChildren(root));
        assertTrue(tree.hasChildren(child1));
        assertTrue(!tree.hasChildren("child2_2"));
        assertTrue(!tree.hasChildren("child2_1"));
    }

    @Test(expected = DuplicateIdException.class)
    public void testAddChildrenInvokingDupException() throws DuplicateIdException, OrphanException, MissingRequiredException {
        tree.addChildren(root, child1);
    }

    @Test(expected = DuplicateIdException.class)
    public void testAddChildren1()throws DuplicateIdException, OrphanException, MissingRequiredException {
        tree.addChildren( "child2_2" , root);
    }

    @Test(expected = OrphanException.class)
    public void testAddChildren2() throws DuplicateIdException, OrphanException, MissingRequiredException{
        tree.addChildren("child2_3", "child3_1" , root);
    }
}
