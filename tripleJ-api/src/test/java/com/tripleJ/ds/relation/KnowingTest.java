package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;


/**
 * @author : kami
 * Date: 13. 7. 30.
 * Time: 오후 4:48
 * To change this template use File | Settings | File Templates.
 */
public class KnowingTest {

    @Test
    public void testEqual1(){
        Knowing k1 = new Knowing("aa", "bb", 2);
        Knowing k2 = new Knowing("aa", "bb", 2);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }

    @Test
    public void testEqual2(){
        Knowing k1 = new Knowing("aa", "bb", 1);
        Knowing k2 = new Knowing("aa", "bb", 2);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }

    @Test
    public void testEqual3(){
        Knowing k1 = new Knowing("aa", "bb", 2);
        Knowing k2 = new Knowing("aa", "cc", 2);

        Assert.assertTrue((!k1.equals(k2)));
        Assert.assertTrue(k1.hashCode() != k2.hashCode());
    }

    @Test
    public void testEqual4(){
        Knowing k1 = new Knowing("aa", "bb", 2);
        Knowing k2 = new Knowing("cc", "bb", 2);

        Assert.assertTrue(!(k1.equals(k2)));
        Assert.assertTrue(k1.hashCode() != k2.hashCode());
    }

    @Test
    public void testEqual5(){
        Knowing k1 = new Knowing("aa", "bb", true, 2, true);
        Knowing k2 = new Knowing("aa", "bb", 2);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }

    @Test
    public void testEqual6(){
        Knowing k1 = new Knowing("aa", "bb", true, 2, true);
        Knowing k2 = new Knowing("aa", "bb", true, 2, false);

        Assert.assertTrue(!(k1.equals(k2)));
        Assert.assertTrue(k1.hashCode() != k2.hashCode());
    }

    @Test
    public void testEqual6_1(){
        Knowing k1 = new Knowing("bb", "aa", true, 2, true);
        Knowing k2 = new Knowing("aa", "bb", true, 2, false);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }


    @Test
    public void testEqual7(){
        Knowing k1 = new Knowing("aa", "bb", true, 4, false);
        Knowing k2 = new Knowing("aa", "bb", true, 2, false);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }

    @Test
    public void testEqual8(){
        Knowing k1 = new Knowing("aa", "bb", false, 2);
        Knowing k2 = new Knowing("aa", "bb", false, 2);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }

    @Test
    public void testEqual9(){
        Knowing k1 = new Knowing("aa", "bb", false, 4);
        Knowing k2 = new Knowing("aa", "bb", false, 2);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }


    @Test
    public void testEqual10(){
        Knowing k1 = new Knowing("aa", "bb", true, 2);
        Knowing k2 = new Knowing("aa", "bb", false, 2);

        Assert.assertTrue(!(k1.equals(k2)));
        Assert.assertTrue(k1.hashCode() != k2.hashCode());
    }

    @Test
    public void testEqaul11(){
        Knowing k1 = new Knowing("aa", "bb", false, 2, true);
        Knowing k2 = new Knowing("aa", "bb", false, 2, false);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }


    @Test
    public void testEqaul12(){
        Knowing k1 = new Knowing("aa", "bb", false, 2, true);
        Knowing k2 = new Knowing("aa", "bb", false, 4, false);

        Assert.assertTrue(k1.equals(k2));
        Assert.assertTrue(k1.hashCode() == k2.hashCode());
    }

    @Test
    public void testGettingSourceDestination(){
        Knowing k1 = new Knowing("aa", "bb", true, 2, true);
        Knowing k2 = new Knowing("aa", "bb", true, 2, false);

        Assert.assertTrue(null != k1.getSource() && k1.getSource().compareTo("aa") == 0);
        Assert.assertTrue(null != k1.getDestination() && k1.getDestination().compareTo("bb") == 0);

        Assert.assertTrue(null != k2.getSource() && k2.getSource().compareTo("bb") == 0);
        Assert.assertTrue(null != k2.getDestination() && k2.getDestination().compareTo("aa") == 0);
    }

    @Test
    public void testAddingToSet1(){
        Knowing k1 = new Knowing("aa", "bb", 2);
        Knowing k2 = new Knowing("aa", "cc" , 2);
        Set<Knowing> set = Sets.newHashSet(k1);
        Assert.assertTrue(set.add(k2));
    }

    @Test
    public void testAddingToSet2(){
        Knowing k1 = new Knowing("aa", "bb", 2);
        Knowing k2 = new Knowing("aa", "bb" , 3);
        Set<Knowing> set = Sets.newHashSet(k1);
        Assert.assertTrue(!set.add(k2));
    }

    @Test
    public void testAddingToSet3(){
        Knowing k1 = new Knowing("aa", "bb",false, 2);
        Knowing k2 = new Knowing("aa", "bb",false, 3);
        Set<Knowing> set = Sets.newHashSet(k1);
        Assert.assertTrue(!set.add(k2));
    }
}
