package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

/**
 * @author : kami
 * Date: 13. 7. 30.
 * Time: 오후 6:02
 * To change this template use File | Settings | File Templates.
 */
public class KnowingsTest {

    private Knowings knowings;

    @Before
    public void setUp(){
        this.knowings = new Knowings("aa");
    }

    @Test
    public void testAddKnowing0(){
        Assert.assertTrue(!this.knowings.addKnowing(null, 1));
        Assert.assertTrue(!this.knowings.addKnowing("dd",-1));
    }

    @Test
    public void testAddKnowing1(){
        Knowing knowing = new Knowing("aa", "bb", 2);
        Assert.assertTrue(this.knowings.addKnowing(knowing));
    }


//    @Test(expected = UnsupportedOperationException.class)
//    public void testImmutable1() throws UnsupportedOperationException{
//        Knowing knowing = new Knowing("aa", "bb", 2);
//        Set<Knowing> immutables = this.knowings.getKnowings();
//        immutables.add(knowing);
//    }

    @Test
    public void testAddKnowing2(){
        Knowing knowing = new Knowing("cc", "bb", 2);
        Assert.assertTrue(!this.knowings.addKnowing(knowing));
    }
//
//    @Test(expected = UnsupportedOperationException.class)
//    public void testImmutable2() throws  UnsupportedOperationException{
//        Knowing knowing = new Knowing("aa", "bb", 2);
//        this.knowings.addKnowing(knowing);
//        Set<Knowing> immutables = this.knowings.getKnowings();
//        immutables.remove(knowing);
//    }

    @Test
    public void testAddKnowing3(){
        Assert.assertTrue(this.knowings.addKnowing("dd",2));
        Assert.assertTrue(!this.knowings.addKnowing("dd", 3));
        Assert.assertTrue(!this.knowings.addKnowing(new Knowing("aa","dd",3)));
    }

    @Test
    public void testHanging1(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", true, 2, true);
        dests.addKnowing("stop", true, 2, false);

        Knowing[] expected = {new Knowing("srcs", "stop", true, 2, true)};
        Set<Knowing> expected_ = Sets.newHashSet(expected);
        Set<Knowing> actual = Knowings.getHookedKnowingsFront(srcs, dests);
        if(null == actual)
            Assert.fail();
        Assert.assertTrue(expected_.containsAll(actual));
        Assert.assertTrue(actual.containsAll(expected_));
    }


    @Test
    public void testHanging1_1(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", true, 2, true);
        dests.addKnowing("stop", true, 2, false);

        Knowing[] expected = {new Knowing("dests", "stop", true, 2, false)};
        Set<Knowing> expected_ = Sets.newHashSet(expected);
        Set<Knowing> actual = Knowings.getHookedKnowingsRear(srcs, dests);
        if(null == actual)
            Assert.fail();
        Assert.assertTrue(expected_.containsAll(actual));
        Assert.assertTrue(actual.containsAll(expected_));
    }

    @Test
    public void testHanging2(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", true, 2, true);
        dests.addKnowing("stop", true, 2, true);

        Set<Knowing> actual = Knowings.getHookedKnowingsFront(srcs, dests);

        Assert.assertTrue(null == actual || actual.size() == 0);
    }


    @Test
    public void testHanging2_1(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", true, 2, true);
        dests.addKnowing("stop", true, 2, true);

        Set<Knowing> actual = Knowings.getHookedKnowingsRear(srcs, dests);

        Assert.assertTrue(null == actual || actual.size() == 0);
    }

    @Test
    public void testHanging3(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", true, 2, true);
        dests.addKnowing("stop", false, 2);

        Set<Knowing> actual = Knowings.getHookedKnowingsFront(srcs, dests);

        Assert.assertTrue(null == actual || actual.size() == 0);
    }

    @Test
    public void testHanging3_1(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", true, 2, true);
        dests.addKnowing("stop", false, 2);

        Set<Knowing> actual = Knowings.getHookedKnowingsRear(srcs, dests);

        Assert.assertTrue(null == actual || actual.size() == 0);
    }

    @Test
    public void testHanging4(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", false, 2);
        dests.addKnowing("stop", false, 2);

        Knowing[] expected = {new Knowing("srcs", "stop", false, 2)};
        Set<Knowing> expected_ = Sets.newHashSet(expected);
        Set<Knowing> actual = Knowings.getHookedKnowingsFront(srcs, dests);

        if(null == actual)
            Assert.fail();
        Assert.assertTrue(expected_.containsAll(actual));
        Assert.assertTrue(actual.containsAll(expected_));
    }

    @Test
    public void testHanging4_1(){
        Knowings srcs = new Knowings("srcs");
        Knowings dests = new Knowings("dests");

        srcs.addKnowing("stop", false, 2);
        dests.addKnowing("stop", false, 2);

        Knowing[] expected = {new Knowing("dests", "stop", false, 2)};
        Set<Knowing> expected_ = Sets.newHashSet(expected);
        Set<Knowing> actual = Knowings.getHookedKnowingsRear(srcs, dests);

        if(null == actual)
            Assert.fail();
        Assert.assertTrue(expected_.containsAll(actual));
        Assert.assertTrue(actual.containsAll(expected_));
    }
}
