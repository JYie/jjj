package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import com.tripleJ.*;
import com.tripleJ.unique.InvalidIdException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


import java.util.Set;

/**
 * Created by kami on 2014. 7. 4..
 */
public class OrientationHandlerForTreeTest {

    protected static OrientationHandler orientationHandler;

    @BeforeClass
    public static void setUpBeforeClass() {

        try {
            orientationHandler = new OrientationHandler(new StoreForTreeTest());
        } catch (InitializeException ne) {
            assert (false);
        }
        System.out.println("----- Start OrientationHandlerForTreeTest -----");
    }

    @Test(expected = IncompatibleException.class)
    public void testForFuctionBannedForTree1() throws IncompatibleException{
        try {
            orientationHandler.addRelation(TripleJType.Comment, "Root", "Child1");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        } catch (TreeException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = IncompatibleException.class)
    public void testForFuctionBannedForTree2() throws IncompatibleException{
        try {
            orientationHandler.removeRelation(TripleJType.Comment, "Root", "Child1");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
    }


    @Test(expected = TreeException.class)
    public void testForUniqueParent1() throws TreeException{
        try {
            orientationHandler.addOrientation(TripleJType.Comment, "Root1", "Child1");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        }
    }

    @Test
    public void testForUniqueParent2(){
        try {
            orientationHandler.addOrientation(TripleJType.Comment, "SuperRoot", "Root");
            assertTrue(true);
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        } catch (TreeException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        }
    }

    @Test(expected = TreeException.class)
    public void testForAvoidingUnintendedRoot() throws TreeException{
        try {
            orientationHandler.removeOrientation(TripleJType.Comment, "Root", "Child1");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        }
    }

    @Test
    public void testForNormalRemoving() {
        try {
            orientationHandler.removeOrientation(TripleJType.Comment, "Child1", "Child11");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        } catch (TreeException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        }
    }

    @Test(expected =  TreeException.class)
    public void testForAvoidingToGenerateCycle() throws TreeException{
        try {
            orientationHandler.addOrientation(TripleJType.Comment, "Child11", "Root");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        }
    }

    @Test(expected = IncompatibleException.class)
    public void testForGettingParent1() throws IncompatibleException {
        try {
            orientationHandler.getParent(TripleJType.Account, "Child1");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
    }

    @Test
    public void testForGettingParent2(){
        String actual = null;
        try {
            actual = orientationHandler.getParent(TripleJType.Comment, "Child1");
        } catch (MissingRequiredException e) {
            e.printStackTrace();
        } catch (StoreException e) {
            e.printStackTrace();
        } catch (IncompatibleException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
        if(null == actual)
            fail();
        assertTrue(actual.compareTo("Root") == 0);
    }

    @Test
    public void testForGettingRoot1(){
        String actual = null;
        try {
            actual = orientationHandler.getRoot(TripleJType.Comment, "Child11");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
        String expected = "Root";
        assertTrue(actual.compareTo(expected) == 0);
    }

    @Test
    public void testForGettingRoot2(){
        String actual = null;
        try {
            actual = orientationHandler.getRoot(TripleJType.Comment, "Root");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
        String expected = "Root";
        assertTrue(actual.compareTo(expected) == 0);
    }

    @Test(expected = IncompatibleException.class)
    public void testForGettingRoot3() throws IncompatibleException {
        try {
            orientationHandler.getRoot(TripleJType.Account, "Child1");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
    }

    @Test
    public void testForGettingRoot4(){
        String actual = null;
        try {
            actual = orientationHandler.getRoot(TripleJType.Comment, "Child3");
        } catch (MissingRequiredException e) {
            fail();
        } catch (StoreException e) {
            fail();
        } catch (IncompatibleException e) {
            fail();
        } catch (InvalidIdException e) {
            fail();
        }
        String expected = "Child3";

        assertTrue(actual.compareTo(expected) == 0);
    }
    /*
    *    (Root) ----> (Child1) ----> (Child11)
    *     |
    *     v
	* (Child2) -----> (Child21)
	*/
    private static class StoreForTreeTest implements OrientationStore {
        @Override
        public void addOrientation(GraphType graphType, String source, String dest) throws StoreException {
            return;
        }

        @Override
        public void removeOrientation(GraphType graphType, String source, String dest) throws StoreException {
            return;
        }

        @Override
        public void addRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
            return;
        }

        @Override
        public void removeRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
            return;
        }

        @Override
        public Set<Knowing> getOutNeighbors(GraphType graphType, String... sources) throws StoreException {
            if (null == sources)
                return null;

            Set<Knowing> result = Sets.newHashSet();
            for (String source : sources) {
                if (source.compareTo("Root") == 0) {
                    result.add(new Knowing("Root", "Child1", 1));
                    result.add(new Knowing("Root", "Child2", 1));
                } else if (source.compareTo("Child1") == 0) {
                    result.add(new Knowing("Child1", "Child11", 1));
                } else if (source.compareTo("Child2") == 0)
                    result.add(new Knowing("Child2", "Child21", 1));
            }
            return result;
        }

        @Override
        public Set<Knowing> getInNeighbors(GraphType graphType, String... destinations) throws StoreException {
            if (null == destinations)
                return null;

            Set<Knowing> result = Sets.newHashSet();
            for (String destination : destinations) {
                if (destination.compareTo("Child1") == 0){
                    result.add(new Knowing("Root", "Child1", 1));
                }else if(destination.compareTo("Child2") == 0) {
                    result.add(new Knowing("Root", "Child2", 1));
                }else if (destination.compareTo("Child11") == 0) {
                    result.add(new Knowing("Child1", "Child11", 1));
                } else if (destination.compareTo("Child21") == 0)
                    result.add(new Knowing("Child2", "Child21", 1));
            }
            return result;
        }

        @Override
        public Set<Knowing> getRelations(GraphType graphType, String... vertices) throws StoreException {
            return null;
        }
    }
}
