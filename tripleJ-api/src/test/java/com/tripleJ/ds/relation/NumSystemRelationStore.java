package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import com.tripleJ.StoreException;

import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public abstract class NumSystemRelationStore implements OrientationStore {

	public static final int defaultMax = 30000;
	
	Map<String, Set<String>> outNeighborsMap;
	Map<String, Set<String>> inNeighborsMap;
	
	int max;

	public NumSystemRelationStore(){
		this.max = defaultMax;
	}
	
	public NumSystemRelationStore(int max){
		this.max = max;
	}
	
	public void initialize(){
		inNeighborsMap = new Hashtable<String, Set<String>>();
		outNeighborsMap = new Hashtable<String, Set<String>>();
		pushInNeighbors();
		pushOutNeighbors();
	}

	abstract void pushInNeighbors();

	abstract void pushOutNeighbors();

	// private static int calExponential(int current, int num, int divisor){
	// if(num % divisor == 0)
	// return calExponential(++current, num/divisor, divisor);
	// else
	// return current;
	// }

	// public static void main(String[] ag){
	//
	// Set<String> test = outNeighborsMap.get(new Integer(10).toString());
	// if(null == outNeighborsMap){
	// System.out.println("null");
	// return;
	// }
	// for(String str:test){
	// System.out.println(str);
	// }
	//
	// System.out.println("------------------");
	//
	// Set<String> test2 = inNeighborsMap.get(new Integer(400).toString());
	// if(null == inNeighborsMap){
	// System.out.println("null");
	// return;
	// }
	//
	// for(String str:test2){
	// System.out.println(str);
	// }
	// }
	//


	@Override
	public void addOrientation(GraphType graphType, String source, String dest) {
		return;
	}

	@Override
	public void removeOrientation(GraphType graphType, String source, String dest) {
		return;
	}

	@Override
	public Set<Knowing> getOutNeighbors(GraphType graphType, String... sources) {

		if (null == sources)
			return null;

		Set<Knowing> result = Sets.newHashSet();
		for (String source : sources) {
			Set<String> outNeighbors = outNeighborsMap.get(source);
			if (null != outNeighbors){
                for(String outNeighbor:outNeighbors)
                    result.add(new Knowing(source, outNeighbor,1));
            }
		}
		return result;
	}

	@Override
	public Set<Knowing> getInNeighbors(GraphType graphType, String... destinations) {

		if (null == destinations)
			return null;

		Set<Knowing> result = Sets.newHashSet();
        for (String destination : destinations) {
			Set<String> inNeighbors = inNeighborsMap.get(destination);
			if (null != inNeighbors){
                for(String inNeighbor:inNeighbors)
                    result.add(new Knowing(inNeighbor,destination,1));
            }
		}
		return result;
	}

    @Override
    public void addRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
        return;
    }

    @Override
    public void removeRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
        return;
    }

    @Override
    public Set<Knowing> getRelations(GraphType graphType, String... vertices) throws StoreException {
        Set<Knowing> result = Sets.newHashSet();

        Set<String> neighbors = Sets.newHashSet();
        for(String vertex: vertices){
            Set<String> inNeighbors = inNeighborsMap.get(vertex);
            Set<String> outNeighbors = outNeighborsMap.get(vertex);

            if(null != outNeighbors)
                neighbors.addAll(outNeighbors);
            if(null != inNeighbors)
                neighbors.addAll(inNeighbors);

            for(String neighbor: neighbors){
                result.add(new Knowing(vertex, neighbor, false, 1));
            }
        }
        return result;
    }
}
