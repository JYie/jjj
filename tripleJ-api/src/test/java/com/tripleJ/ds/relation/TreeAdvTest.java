package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import com.tripleJ.TripleJException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
/**
 * Created by kami on 2014. 7. 10..
 */
public class TreeAdvTest {

    private RootedTree<String> tree1;
    private RootedTree<String> tree2;

    @Before
    public void setUp(){
        tree1 = new RootedTree("root");
        tree2 = new RootedTree("root");
        try {
            tree1.addChildren("root", "child2");
            tree1.addChildren("root", "child1");
            tree1.addChildren("child1", "child2_2");
            tree1.addChildren("child1", "child2_1");

            tree2.addChildren("root", "child2");
            tree2.addChildren("root", "child1");
            tree2.addChildren("child2", "child2_1");
            tree2.addChildren("child2", "child2_2");
        } catch (TripleJException e) {
            fail();
        }
    }

    @Test
    public void testConstructorFromImage1(){
        int image[] = tree1.makeFootprint();
        List<String> elements = tree1.getMembers();

        try {
            RootedTree<String> rootedTree = new RootedTree(elements, image);
            String actualRoot = rootedTree.getRoot();
            assertTrue(null != actualRoot);
            assertTrue(actualRoot.compareTo(tree1.getRoot()) == 0);

            List<String> members = rootedTree.getMembers();
            assertTrue(null != members);
            assertTrue(members.size() == tree1.getMembers().size());

            Set<String> leaves = rootedTree.getLeaves();
            assertTrue(null != leaves);
            assertTrue(leaves.size() == tree1.getLeaves().size());

            assertTrue(Sets.difference(tree1.getLeaves(), leaves).isEmpty());

            Set<String> actualLevel1 = rootedTree.getChildren("root");
            assertTrue(Sets.difference(actualLevel1, tree1.getChildren("root")).isEmpty());

            Set<String> actualLevel2 = rootedTree.getChildren("child1");
            assertTrue(Sets.difference(actualLevel2, tree1.getChildren("child1")).isEmpty());
        } catch (TripleJException e) {
            fail();
        }
    }

    @Test
    public void testConstructorFromImage2(){
        int image[] = tree2.makeFootprint();
        List<String> elements = tree2.getMembers();

        try {
            RootedTree<String> rootedTree = new RootedTree(elements, image);
            String actualRoot = rootedTree.getRoot();
            assertTrue(null != actualRoot);
            assertTrue(actualRoot.compareTo(tree2.getRoot()) == 0);

            List<String> members = rootedTree.getMembers();
            assertTrue(null != members);
            assertTrue(members.size() == tree2.getMembers().size());

            Set<String> leaves = rootedTree.getLeaves();
            assertTrue(null != leaves);
            assertTrue(leaves.size() == tree2.getLeaves().size());

            assertTrue(Sets.difference(tree2.getLeaves(), leaves).isEmpty());

            Set<String> actualLevel1 = rootedTree.getChildren("root");
            assertTrue(Sets.difference(actualLevel1, tree2.getChildren("root")).isEmpty());

            Set<String> actualLevel2 = rootedTree.getChildren("child2");
            assertTrue(Sets.difference(actualLevel2, tree2.getChildren("child2")).isEmpty());
        } catch (TripleJException e) {
            fail();
        }
    }

    @Test
    public void testPrintImage1(){

        int[] actualImage = tree1.makeFootprint();
        int[] expectedImage = new int[]{2,2,0,0,0};

        assertTrue(Arrays.equals(actualImage, expectedImage));
    }

    @Test
    public void testPrintImage2(){
        int[] actualImage = tree2.makeFootprint();
        int[] expectedImage = new int[]{2,0,2,0,0};

        assertTrue(Arrays.equals(actualImage, expectedImage));
    }
}

