package com.tripleJ.ds.relation;

import java.util.List;
import java.util.Set;

import com.tripleJ.*;
import com.tripleJ.unique.InvalidIdException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Sets;


public class OrientationHandler4Test {
	protected static OrientationHandler relationHandler;
	protected static PrimeFactorRelationStore primeFactorRelStore;

	@BeforeClass
	public static void setUpBeforeClass() {

		primeFactorRelStore = new PrimeFactorRelationStore(30100);
		primeFactorRelStore.initialize();
		try {
			relationHandler = new OrientationHandler(primeFactorRelStore);
		} catch (InitializeException ne) {
			assert (false);
		}
		System.out.println("----- Start OrientedRelationHandler4Test -----");
	}

	@Before
	public void setUpBefore(){
		relationHandler.setMaxDepth(6);
	}
	
	@Test
	public void testFindingNeighbors() {

		try {
			Set<String> TenOutNeighbors = relationHandler.getOutNeighbors(TripleJType.Follows, "10");
			Set<String> TwentyInNeighbors = relationHandler
					.getInNeighbors(null, "20");

			Assert.assertTrue(TenOutNeighbors.contains("20")
					&& TenOutNeighbors.contains("30"));
			Assert.assertTrue(TwentyInNeighbors.contains("10"));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingDepth() {
		relationHandler.setMaxDepth(5);
		Assert.assertTrue(3 == relationHandler.getDepthFromSource());
		Assert.assertTrue(2 == relationHandler.getDepthFromDestination());

		relationHandler.setMaxDepth(4);
		Assert.assertTrue(2 == relationHandler.getDepthFromSource());
		Assert.assertTrue(2 == relationHandler.getDepthFromDestination());
	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection0() {
		relationHandler.setMaxDepth(4);
		String source = "2";
		String destination = "4";
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);
		try {
			Assert.assertTrue(1 == relationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}
	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection1() {

		relationHandler.setMaxDepth(10);
		String source = "2";
		String destination = "1024";
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);
		try {
			Assert.assertTrue(9 == relationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}
	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection2() {
		relationHandler.setMaxDepth(6);
		String source = "2";
		String destination = new Integer(2 * 2 * 3 * 3 * 5 * 7 * 11).toString();
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);

		int expected = 6;
		int result;
		try {
			result = relationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests);
			Assert.assertTrue(expected == result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection3() {
		relationHandler.setMaxDepth(5);
		String source = "2";
		String destination = new Integer(2 * 2 * 3 * 3 * 5 * 7 * 11).toString();
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);

		int expected = -1;
		int result;
		try {
			result = relationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests);
			Assert.assertTrue(expected == result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection4() {

		String source = "3";
		String destination = "25";
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);
		try {
			Assert.assertTrue(-1 == relationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Follows, 0, srcs, dests));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}
	}

	@Test
	public void testExpandingPathByInDirection() {

		String[] dests = { "15", "25" };
		Set<String> seconds;
		try {
			seconds = relationHandler.getNextNeighborsByInDirection(TripleJType.Follows, dests);
			Assert.assertTrue(null != seconds && 2 == seconds.size());
			Assert.assertTrue(seconds.contains("3") && seconds.contains("5"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

	}

	@Test
	public void testExpandingPathByOutDirection1() {
		String src = "27";
		Set<String> seconds;
		try {
			seconds = relationHandler.getNextNeighborsByOutDirection(TripleJType.Follows, src);
			List<Integer> primes = primeFactorRelStore.primes;
			int pSize = primes.size();
			int expected = 0;
			for (int i = 0; i < pSize; i++) {
				int prime = primes.get(i);
				int source = new Integer(src).intValue();
				if (source * prime > primeFactorRelStore.max) {
					expected = i;
					break;
				}
			}
			Assert.assertTrue(null != seconds && expected == seconds.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

	}

	@Test
	public void testGettingNeigborsGoingToDest() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<String> result = relationHandler
					.getSrcOutNeighborsLinkingToDest(TripleJType.Follows, src, dest);
			String[] expected = { new Integer(2 * 3).toString(),
					new Integer(2 * 5).toString(),
					new Integer(2 * 7).toString(),
					new Integer(2 * 11).toString() };
			Set<String> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingNeigborsGoingToDest");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsGoingToDest1() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<Knowing> actual = relationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Follows, 2, false, src, dest);
            Knowing[] expected = {new Knowing(src, new Integer(2*3*5).toString(), true, 2, true)
                                 , new Knowing(src, new Integer(2*3*7).toString(), true, 2, true)
                                 , new Knowing(src, new Integer(2*3*11).toString(), true, 2, true)
                                 , new Knowing(src, new Integer(2*5*7).toString(), true, 2, true)
                                 , new Knowing(src, new Integer(2*5*11).toString(), true, 2, true)
                                 , new Knowing(src, new Integer(2*7*11).toString(), true, 2, true)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);
			
			System.out.println("testGettingKnowingsGoingToDest1");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + actual);
			
			Assert.assertTrue(expected_.containsAll(actual));
			Assert.assertTrue(actual.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsGoingToDest2() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<Knowing> result = relationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Follows, 3, false, src, dest);

            Knowing[] expected = {new Knowing(src, new Integer(2*3*5*7).toString(), true, 3, true)
                    , new Knowing(src, new Integer(2*3*5*11).toString(), true, 3, true)
                    , new Knowing(src, new Integer(2*3*7*11).toString(), true, 3, true)
                    , new Knowing(src, new Integer(2*5*7*11).toString(), true, 3, true)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsGoingToDest2");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test(expected = BoundaryException.class)
	public void testGettingKnowingsGoingToDest3() throws BoundaryException {
		String src = "2";
		String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();
		relationHandler.setMaxDepth(6);
		try {
			relationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Follows, 7, false, src, dest);
		} catch (MissingRequiredException e) {
			// TODO Auto-generated catch block
			Assert.fail();
		} catch (StoreException e) {
            Assert.fail();
        } catch (InvalidIdException e) {
            Assert.fail();
        } catch (IncompatibleException e) {
            Assert.fail();
        }
    }

	@Test(expected = BoundaryException.class)
	public void testGettingRelationsGoingToDest4() throws BoundaryException {
		String src = "2";
		String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();
		try {
			relationHandler.getSrcOutKnowingsLinkingToDest(TripleJType.Follows, -1, false, src,
                    dest);
		} catch (MissingRequiredException e) {
			// TODO Auto-generated catch block
			Assert.fail();
		} catch (StoreException se) {
			Assert.fail();
		} catch (InvalidIdException e) {
            Assert.fail();
        } catch (IncompatibleException e) {
            Assert.fail();
        }
    }

	@Test
	public void testGettingKnowingsGoingToDest5() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<Knowing> result = relationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Follows, 2, true, src, dest);

            Knowing[] expected = {new Knowing(src, new Integer(2*3).toString(), true, 1, true)
                    , new Knowing(src, new Integer(2*5).toString(), true, 1, true)
                    , new Knowing(src, new Integer(2*7).toString(), true, 1, true)
                    , new Knowing(src, new Integer(2*11).toString(), true, 1, true)
                    , new Knowing(src, new Integer(2*3*5).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*3*7).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*3*11).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*5*7).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*5*11).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*7*11).toString(), true, 2, true)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsGoingToDest5");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);

			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingNeighborsComingFromSrc() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<String> result = relationHandler
					.getDestInNeighborsLinkingFromSrc(TripleJType.Follows, src, dest);
			String[] expected = { new Integer(2 * 3 * 5 * 7).toString(),
					new Integer(2 * 3 * 5 * 11).toString(),
					new Integer(2 * 3 * 7 * 11).toString(),
					new Integer(2 * 5 * 7 * 11).toString() };
			Set<String> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingNeighborsComingFromSrc");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsComingFromSrc1() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<Knowing> result = relationHandler
					.getDestInKnowingsLinkingFromSrc(TripleJType.Follows, 2, false, src, dest);

            Knowing[] expected = {new Knowing(dest, new Integer(2*3*5).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*3*7).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*3*11).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*5*7).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*5*11).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*7*11).toString(), true, 2, false)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsComingFromSrc1");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsComingFromSrc2() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<Knowing> result = relationHandler
					.getDestInKnowingsLinkingFromSrc(TripleJType.Follows, 3, false, src, dest);

            Knowing[] expected = {new Knowing(dest, new Integer(2*3).toString(), true, 3, false)
                    , new Knowing(dest, new Integer(2*5).toString(), true, 3, false)
                    , new Knowing(dest, new Integer(2*7).toString(), true, 3, false)
                    , new Knowing(dest, new Integer(2*11).toString(), true, 3, false)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsComingFromSrc2");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsComingFromSrc3() {
		try {
			String src = "2";
			String dest = new Integer(2 * 3 * 5 * 7 * 11).toString();

			Set<Knowing> result = relationHandler
					.getDestInKnowingsLinkingFromSrc(TripleJType.Follows, 2, true, src, dest);

            Knowing[] expected = {new Knowing(dest, new Integer(2*3*5).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*3*7).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*3*11).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*5*7).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*5*11).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*7*11).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*3*5*7).toString(), true, 1, false)
                    , new Knowing(dest, new Integer(2*3*5*11).toString(), true, 1, false)
                    , new Knowing(dest, new Integer(2*3*7*11).toString(), true, 1, false)
                    , new Knowing(dest, new Integer(2*5*7*11).toString(), true, 1, false)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);
			System.out.println("testGettingKnowingsComingFromSrc3");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestDirectPathLength0() {

		try {
			relationHandler.setMaxDepth(4);
			String src = "2";
			String dest = new Integer(2 * 2).toString();

            int result = relationHandler.getShortestDirectPathLength(TripleJType.Follows, src, dest);
			int expected = 1;

            System.out.println("testGettingShortestDirectPathLength0");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + result);

            Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestDirectPathLength1() {

		try {
			relationHandler.setMaxDepth(6);
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 5 * 7 * 11).toString();

			int result = relationHandler.getShortestDirectPathLength(TripleJType.Follows, src, dest);
			int expected = 5;

            System.out.println("testGettingShortestDirectPathLength1");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + result);

            Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestDirectPathLength2() {
		try {
			String src = new Integer(2 * 2 * 3 * 5 * 7 * 11).toString();
			String dest = new Integer(3 * 5).toString();
			int result = relationHandler.getShortestDirectPathLength(TripleJType.Follows, src, dest);
			int expected = -1;

            System.out.println("testGettingShortestDirectPathLength2");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + result);

            Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestPathLength() {

		try {
			String src = new Integer(2 * 2 * 3 * 5 * 7 * 11).toString();
			String dest = new Integer(3 * 5).toString();
            relationHandler.setMaxDepth(6);
			int result = relationHandler.getShortestPathLength(TripleJType.Account, src, dest);
			int expected = 4;

            System.out.println("testGettingShortestPathLength");
            System.out.println("expected : " + expected);
            System.out.println("actual : " + result);

            Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
