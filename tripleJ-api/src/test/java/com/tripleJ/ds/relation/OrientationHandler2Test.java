package com.tripleJ.ds.relation;

import java.util.Set;


import com.google.common.collect.Sets;
import com.tripleJ.*;
import com.tripleJ.unique.InvalidIdException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public  class OrientationHandler2Test {

    protected static OrientationHandler orientationHandler;

    @BeforeClass
    public static void setUpBeforeClass() {

        try {
            orientationHandler = new OrientationHandler(new RelationHandlerForAdvTest());
        } catch (InitializeException ne) {
            assert (false);
        }
        System.out.println("----- Start OrientedRelationHandler2Test -----");
    }

	@Test
	public void testGettingOutNeigborsGoingToDest(){
	 	/*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{
        	Assert.assertTrue(2 == orientationHandler.getSrcOutNeighborsLinkingToDest(TripleJType.Follows, "Neo", "Agent Smith").size());
        	
        	Assert.assertTrue(1 == orientationHandler.getSrcOutNeighborsLinkingToDest(TripleJType.Follows, "Neo", "Morpheus").size());
        	Assert.assertTrue(1 == orientationHandler.getSrcOutNeighborsLinkingToDest(TripleJType.Follows, "Neo", "Cypher").size());
        	Assert.assertTrue(0 == orientationHandler.getSrcOutNeighborsLinkingToDest(TripleJType.Follows, "Trinity", "Cypher").size());
        	
        }catch(Exception e){
        		assert(false);
        }				
	}
	
	@Test
	public void testGettingInNeighborsComingFromDest(){
	 	/*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{
        	Assert.assertTrue(2 == orientationHandler.getDestInNeighborsLinkingFromSrc(TripleJType.Follows, "Neo", "Agent Smith").size());
        	Assert.assertTrue(1 == orientationHandler.getDestInNeighborsLinkingFromSrc(TripleJType.Follows, "Trinity", "Agent Smith").size());
        	Assert.assertTrue(1 == orientationHandler.getDestInNeighborsLinkingFromSrc(TripleJType.Follows, "Neo", "Cypher").size());
        	Assert.assertTrue(2 == orientationHandler.getDestInNeighborsLinkingFromSrc(TripleJType.Follows, "Morpheus", "Agent Smith").size());
        	Assert.assertTrue(0 == orientationHandler.getDestInNeighborsLinkingFromSrc(TripleJType.Follows, "Trinity", "Cypher").size());
        
        }catch(Exception e){
        	Assert.fail();
        }						
	}

	@Test
	public void testGettingSrcNeighborsLinkingToDest(){
	 	/*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{
        	Set<String> tNeighbors = orientationHandler.getSrcBothNeighborsLinkingToDest(TripleJType.Follows, "Trinity", "Cypher");
        	Assert.assertTrue(3 == tNeighbors.size());
        	Assert.assertTrue(tNeighbors.contains("Neo") &&
        					tNeighbors.contains("Morpheus") &&
        					tNeighbors.contains("Agent Smith"));
        
        }catch(Exception e){
        	Assert.fail();
        }						
	}

	@Test
	public void testGettingDestNeighborsLinkingFromSrc(){
		/*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{
        	Set<String> tNeighbors = orientationHandler.getDestBothNeighborsLinkingFromSrc(TripleJType.Follows, "Cypher", "Trinity");
        	Assert.assertTrue(3 == tNeighbors.size());
        	Assert.assertTrue(tNeighbors.contains("Neo") &&
        					tNeighbors.contains("Morpheus") &&
        					tNeighbors.contains("Agent Smith"));
        
        }catch(Exception e){
        	Assert.fail();
        }
	}
	
	@Test
	public void testGettingShortestDirectPathLength(){
	 	/*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{        	
        	Assert.assertTrue(2 == orientationHandler.getShortestDirectPathLength(TripleJType.Follows, "Neo", "Agent Smith"));
        	Assert.assertTrue(-1 == orientationHandler.getShortestDirectPathLength(TripleJType.Follows, "Trinity", "Neo"));
        	Assert.assertTrue(1 == orientationHandler.getShortestDirectPathLength(TripleJType.Follows, "Neo", "Trinity"));
        	Assert.assertTrue(-1 == orientationHandler.getShortestDirectPathLength(TripleJType.Follows, "Cypher","Neo"));
        }catch(Exception e){
        	Assert.fail();
        }
	}
	
	@Test
	public void testGettingShortestPathLength(){
	 	/*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{        	
        	Assert.assertTrue(2 == orientationHandler.getShortestPathLength(TripleJType.Follows, "Neo", "Agent Smith"));
        	Assert.assertTrue(1 == orientationHandler.getShortestPathLength(TripleJType.Follows, "Trinity", "Neo"));

        	Assert.assertTrue(2 == orientationHandler.getShortestPathLength(TripleJType.Follows, "Cypher", "Neo"));
        }catch(Exception e){
        	Assert.fail();
        }
	}

    @Test
    public void testIsRoot(){
        /*
         *  (Neo) ----> (Trinity) ----> (Agent Smith)
         *     \       ^                ^
         *      v     /                /
         *    (Morpheus) ---->  (Cypher)
         */
        try{
            Assert.assertTrue(orientationHandler.isRoot(TripleJType.Follows, "Neo"));
            Assert.assertTrue(!orientationHandler.isRoot(TripleJType.Follows, "Trinity"));
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void testGettingRoot(){
        try {
            String actual = orientationHandler.getRoot(TripleJType.Follows, "Neo");
            String expected = "Neo";
            System.out.println("expected root from Neo = " + expected);
            System.out.println("actual root from Neo = " + actual);
            Assert.assertTrue(null != actual);
            Assert.assertTrue(actual.compareTo(expected) == 0);

            String actual2 = orientationHandler.getRoot(TripleJType.Follows, "Cypher");
            System.out.println("expected root from Cypher = " + expected);
            System.out.println("actual root from Cypher = " + actual2);

            Assert.assertTrue(null != actual2);
            Assert.assertTrue(actual2.compareTo(expected) == 0);

        } catch (TripleJException e) {
            Assert.fail();
        }

    }

    @Test(expected = IncompatibleException.class)
    public void testIsRootNotavailableForUndirectedGraph() throws IncompatibleException {
        try {
            orientationHandler.isRoot(TripleJType.Account, "Neo");
        } catch (MissingRequiredException e) {
            Assert.fail();
        } catch (StoreException e) {
            Assert.fail();
        } catch (InvalidIdException e) {
            Assert.fail();
        }
    }

    @Test(expected = IncompatibleException.class)
    public void testGettingRootNotavailableForUndirectedGraph() throws IncompatibleException {
        try {
            orientationHandler.getRoot(TripleJType.Account, "Neo");
        } catch (MissingRequiredException e) {
            Assert.fail();
        } catch (StoreException e) {
            Assert.fail();
        } catch (InvalidIdException e) {
            Assert.fail();
        }
    }

    public static class RelationHandlerForAdvTest implements OrientationStore {

		@Override
		public void addOrientation(GraphType graphType, String source, String dest) {
			// TODO Auto-generated method stub
			return;
		}

		@Override
		public void removeOrientation(GraphType graphType, String source, String dest) {
			// TODO Auto-generated method stub
			return;
		}

        @Override
        public void addRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
            return;
        }

        @Override
        public void removeRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
            return;
        }

        @Override
		public Set<Knowing> getOutNeighbors(GraphType graphType, String... sources) {
			// TODO Auto-generated method stub
			/*
	         *  (Neo) ----> (Trinity) ----> (Agent Smith)
	         *     \       ^                ^
	         *      v     /                /
	         *    (Morpheus) ---->  (Cypher)
	         */
			if(null == sources)
				return null;

			Set<Knowing> result = Sets.newHashSet();
			for(String source : sources){
				if(source.compareTo("Neo") == 0){
					result.add(new Knowing("Neo","Trinity",1));
                    result.add(new Knowing("Neo","Morpheus",1));
				}else if(source.compareTo("Trinity") == 0){
                    result.add(new Knowing("Trinity","Agent Smith",1));
				}else if(source.compareTo("Morpheus") == 0){
                    result.add(new Knowing("Morpheus","Trinity",1));
                    result.add(new Knowing("Morpheus","Cypher",1));
				}else if(source.compareTo("Cypher") == 0){
                    result.add(new Knowing("Cypher","Agent Smith",1));
				}
			}

			return result;
		}

		@Override
		public Set<Knowing> getInNeighbors(GraphType graphType, String... destinations) {
			// TODO Auto-generated method stub
		    /*
	         *  (Neo) ----> (Trinity) ----> (Agent Smith)
	         *     \       ^                ^
	         *      v     /                /
	         *    (Morpheus) ---->  (Cypher)
	         */

			if(null == destinations)
				return null;

			Set<Knowing> result = Sets.newHashSet();
			for(String dest : destinations){
				if(dest.compareTo("Trinity") == 0){
                    result.add(new Knowing("Neo","Trinity",1));
                    result.add(new Knowing("Morpheus","Trinity",1));
				}else if(dest.compareTo("Morpheus") == 0){
                    result.add(new Knowing("Neo","Morpheus",1));
				}else if(dest.compareTo("Cypher") == 0){
                    result.add(new Knowing("Morpheus","Cypher",1));
                }else if(dest.compareTo("Agent Smith") == 0){
                    result.add(new Knowing("Cypher","Agent Smith",1));
                    result.add(new Knowing("Trinity","Agent Smith",1));
				}				
			}
			return result;
		}

        @Override
        public Set<Knowing> getRelations(GraphType graphType, String... vertices) throws StoreException {
            Set<Knowing> result = Sets.newHashSet();
            for(String vertex: vertices){
                if(vertex.compareTo("Neo") == 0){
                    result.add(new Knowing("Neo","Trinity", false, 1));
                    result.add(new Knowing("Neo","Morpheus", false, 1));
                }else if(vertex.compareTo("Cypher") == 0){
                    result.add(new Knowing("Cypher","Morpheus", false, 1));
                    result.add(new Knowing("Cypher","Agent Smith", false, 1));
                }else if(vertex.compareTo("Morpheus") == 0){
                    result.add(new Knowing("Morpheus","Neo", false, 1));
                    result.add(new Knowing("Morpheus","Trinity", false, 1));
                    result.add(new Knowing("Morpheus","Cypher", false, 1));
                }else if(vertex.compareTo("Trinity") == 0){
                    result.add(new Knowing("Trinity","Neo", false, 1));
                    result.add(new Knowing("Trinity","Morpheus", false, 1));
                    result.add(new Knowing("Trinity","Agent Smith", false, 1));
                }else if(vertex.compareTo("Agent Smith") == 0){
                    result.add(new Knowing("Agent Smith", "Trinity", false, 1));
                    result.add(new Knowing("Agent Smith", "Cypher", false, 1));
                }
            }
            return result;
        }
    }
}
