package com.tripleJ.ds.relation;

import java.util.Set;

import com.google.common.collect.Sets;

public class FixedNumRelationStore extends NumSystemRelationStore {

	public static final int[] defaultFixNums = {2,3,4,5,6,7,8,9,10};
	
	int[] fixedNums;
	
	public FixedNumRelationStore(int max, int[] fixedNums) {
		this.max = max;
		if(null == fixedNums)
			return;
		this.fixedNums = new int[fixedNums.length];
		System.arraycopy(fixedNums, 0, this.fixedNums, 0, fixedNums.length);
	}

	public FixedNumRelationStore(int max) {
		this.max = max;
		this.fixedNums = new int[defaultFixNums.length];
		System.arraycopy(defaultFixNums, 0, this.fixedNums, 0, defaultFixNums.length);
	}
	
	public FixedNumRelationStore(int[] fixedNums) {
		this.max = defaultMax;
		if(null == fixedNums)
			return;
		this.fixedNums = new int[fixedNums.length];
		System.arraycopy(fixedNums, 0, this.fixedNums, 0, fixedNums.length);
	}

	public FixedNumRelationStore() {
		this.max = defaultMax;
		this.fixedNums = new int[defaultFixNums.length];
		System.arraycopy(defaultFixNums, 0, this.fixedNums, 0, defaultFixNums.length);
	}

	@Override
	void pushInNeighbors() {
		for (int i = 4; i < max; i++) {
			Set<String> inNeighbors = Sets.newHashSet();
			for (int divisor:this.fixedNums) {
				if(i % divisor != 0)
					continue;
				int neighbor = i / divisor;
				inNeighbors.add(new Integer(neighbor).toString());
			}
			inNeighborsMap.put(new Integer(i).toString(), inNeighbors);
		}
	}

	@Override
	void pushOutNeighbors() {
		for (int i = 1; i < max; i++) {
			Set<String> multiples = Sets.newHashSet();
			for (int multiple:this.fixedNums) {
				int neighbor = i * multiple;
				if (neighbor < max)
					multiples.add(new Integer(neighbor).toString());
			}
			outNeighborsMap.put(new Integer(i).toString(), multiples);
		}
	}

}
