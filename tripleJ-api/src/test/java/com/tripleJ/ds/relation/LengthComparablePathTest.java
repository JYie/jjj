package com.tripleJ.ds.relation;

import org.junit.Assert;
import org.junit.Test;

public class LengthComparablePathTest {

	public LengthComparablePathTest() {
		// TODO Auto-generated constructor stub
	}

	@Test
	public void testSortable(){
		
		String[] strs1 = {"1111", "2222", "3333"};
		String[] strs2 = {"1111", "2222"};
		
		LengthComparablePath lsp1 = new LengthComparablePath(strs1);
		LengthComparablePath lsp2 = new LengthComparablePath(strs2);
		Assert.assertTrue(lsp1.compareTo(lsp2) > 0);
	}
	
}
