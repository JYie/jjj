package com.tripleJ.ds.relation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

public class PrimeFactorRelationStore extends NumSystemRelationStore {

	List<Integer> primes;
	
	public PrimeFactorRelationStore(int i) {
		super(i);
	}
	
	public PrimeFactorRelationStore(){
		super();
	}
	
	@Override
	void pushInNeighbors() {
		if(null == primes)
			generatePrimes();
		for (int i = 4; i < max; i++) {
			Set<Integer> primes = getPrimeDivisors(i);
			Set<String> inNeighbors = Sets.newHashSet();
			for (Integer prime : primes) {
				int pValue = prime.intValue();
				int neighbor = i / pValue;
				inNeighbors.add(new Integer(neighbor).toString());
			}
			inNeighborsMap.put(new Integer(i).toString(), inNeighbors);
		}
	}
	@Override
	void pushOutNeighbors() {
		if(null == primes)
			generatePrimes();
		int size = primes.size();
		for (int i = 1; i < max; i++) {
			Set<String> multiples = Sets.newHashSet();
			for (int j = 0; j < size; j++) {
				int prime = primes.get(j).intValue();
				int neighbor = i * prime;
				if (neighbor < max)
					multiples.add(new Integer(neighbor).toString());
			}
			outNeighborsMap.put(new Integer(i).toString(), multiples);
		}
	}
	
	private Set<Integer> getPrimeDivisors(int num) {
		if(null == primes)
			generatePrimes();
		Set<Integer> pds = Sets.newHashSet();
		int size = primes.size();
		for (int i = 0; i < size; i++) {
			Integer prime = primes.get(i);
			int pValue = prime.intValue();
			if (pValue > num)
				break;
			if (num % pValue == 0)
				pds.add(pValue);
		}
		return pds;
	}

	void generatePrimes() {
		primes = new ArrayList<Integer> ();
		for (int i = 2; i < max; i++)
			primes.add(new Integer(i));
		for (int i = 2; i < max; i++) {
			if (isPrime(i)) {
				int count = max / i + 1;
				for (int j = 2; j < count; j++)
					primes.remove(new Integer(i * j));
			}
		}
	}
	
	boolean isPrime(int num) {
		return BigInteger.valueOf(num).isProbablePrime(100);
	}

}
