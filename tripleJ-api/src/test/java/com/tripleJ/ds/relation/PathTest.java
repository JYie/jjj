package com.tripleJ.ds.relation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PathTest {

	public PathTest() {
		// TODO Auto-generated constructor stub
	}
	
	@Before
	public void setUp(){
		
	}
	
	@After
	public void tearDown(){
		
	}
	
	@Test
	public void testGetSrc(){
		
	}
	
	@Test
	public void testGetDest(){
		
	}
	
	@Test
	public void testGetPathLength(){
		String sd = "sd";
		Path path2 = new Path(sd);
		Assert.assertTrue(0 == path2.getLength());
		
		String[] srcs = {"a","b","c"};
		Path path = new Path(srcs);
		Assert.assertTrue(2 == path.getLength());
	}
	
	@Test
	public void testEqual1(){
		String[] pathElements1 = {"a", "b", "c"};
		Path path1 = new Path(pathElements1);
		String[] pathElements2 = {"a", "b", "c"};
		Path path2 = new Path(pathElements2);
		Assert.assertTrue(path1.equals(path2));
	}


	@Test
	public void testEqual2(){
		String[] pathElements1 = {"a", "b", "c"};
		Path path1 = new Path(pathElements1);
		String[] pathElements2 = {"a", "b", "c", "d"};
		Path path2 = new Path(pathElements2);
		Assert.assertTrue(!path1.equals(path2));
	}
	

	@Test
	public void testEqual3(){
		String[] pathElements1 = {"a", "b", "c"};
		Path path1 = new Path(pathElements1);
		String[] pathElements2 = {"a", "d", "c"};
		Path path2 = new Path(pathElements2);
		Assert.assertTrue(!path1.equals(path2));
	}
	
	@Test
	public void testEqual4(){
		String[] pathElements1 = {"a", "b", "c", "d"};
		Path path1 = new Path(pathElements1);
		String[] pathElements2 = {"a", "c", "b", "d"};
		Path path2 = new Path(pathElements2);
		Assert.assertTrue(!path1.equals(path2));
	}
}
