package com.tripleJ.ds.relation;

import java.util.Set;

import com.tripleJ.InitializeException;

import com.tripleJ.TripleJType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Sets;

public class OrientationHandler5Test {

	protected static OrientationHandler orientationHandler;
	protected static FixedNumRelationStore fixedNumRelStore;
	
	@BeforeClass
	public static void setUpBeforeClass() {

		int[] fixedMultiples = { 2, 3, 4, 6, 12, 15, 16 };
		fixedNumRelStore = new FixedNumRelationStore(31000, fixedMultiples);
		fixedNumRelStore.initialize();
		try {
			orientationHandler = new OrientationHandler(fixedNumRelStore);
		} catch (InitializeException ne) {
			assert (false);
		}
		System.out.println("----- Start OrientedRelationHandler5Test -----");
	}
	
	@Before
	public void setUpBefore(){
		orientationHandler.setMaxDepth(6);
	}

	
	@Test
	public void testFindingNeighbors() {

		try {
			Set<String> TenOutNeighbors = orientationHandler.getOutNeighbors(TripleJType.Comment, "10");
			Set<String> TwentyInNeighbors = orientationHandler
					.getInNeighbors(null, "20");

			Set<String> expected1 = Sets.newHashSet();
			Set<String> expected2 = Sets.newHashSet();
			for (int multiple : fixedNumRelStore.fixedNums) {
				if (multiple * 10 <= fixedNumRelStore.max)
					expected1.add(new Integer(multiple * 10).toString());
				if (20 % multiple == 0)
					expected2.add(new Integer(20 / multiple).toString());
			}

			Assert.assertTrue(TenOutNeighbors.containsAll(expected1)
					&& expected1.containsAll(TenOutNeighbors));
			Assert.assertTrue(TwentyInNeighbors.containsAll(expected2)
					&& expected2.containsAll(TwentyInNeighbors));

		} catch (Exception e) {
			assert (false);
		}
	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection1() {
		orientationHandler.setMaxDepth(8);
		String source = "2";
		String destination = "1024";
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);
		/**
		 * 16 makes length of this path be 3
		 */
		try {
			Assert.assertTrue(3 == orientationHandler
                    .findIntersectionPointRecursivelyAsDirection(TripleJType.Comment, 0, srcs, dests));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

		String source2 = "1";
		String destination2 = new Integer(3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 4)
				.toString();
		Set<String> srcs2 = Sets.newHashSet(source2);
		Set<String> dests2 = Sets.newHashSet(destination2);

		try {
			Assert.assertTrue(8 == orientationHandler
                    .findIntersectionPointRecursivelyAsDirection(TripleJType.Comment, 0, srcs2,
                            dests2));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}
	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection2() {
		orientationHandler.setMaxDepth(6);
		String source = "2";
		String destination = new Integer(2 * 2 * 3 * 3 * 5).toString();
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);

		int expected = 2;
		int result;
		try {
			result = orientationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Comment, 0, srcs, dests);
			Assert.assertTrue(expected == result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection3() {
		orientationHandler.setMaxDepth(6);
		String source = "2";
		String destination = new Integer(2 * 3 * 3 * 5 * 7).toString();
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);

		int expected = -1;
		int result;
		try {
			result = orientationHandler
					.findIntersectionPointRecursivelyAsDirection(TripleJType.Comment, 0, srcs, dests);
			Assert.assertTrue(expected == result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail();
		}

	}

	@Test
	public void testGettingIntersectionRecursivelyAsDirection4() {
		orientationHandler.setMaxDepth(4);
		String source = "3";
		String destination = new Integer(3 * 3 * 3 * 3 * 3 * 3).toString();
		Set<String> srcs = Sets.newHashSet(source);
		Set<String> dests = Sets.newHashSet(destination);
		try {
			Assert.assertTrue(-1 == orientationHandler
                    .findIntersectionPointRecursivelyAsDirection(TripleJType.Comment, 0, srcs, dests));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGettingShortestDirectPathLength1() {

		try {
			orientationHandler.setMaxDepth(6);
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 3 * 5 * 5).toString();

			int result = orientationHandler.getShortestDirectPathLength(TripleJType.Comment, src, dest);
			int expected = 3;

			Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestDirectPathLength2() {
		try {
			String src = new Integer(2 * 2 * 3 * 5 * 7).toString();
			String dest = new Integer(3 * 5).toString();

			int result = orientationHandler.getShortestDirectPathLength(TripleJType.Comment, src, dest);
			int expected = -1;

			Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestDirectPathLength3() {
		try {
			orientationHandler.setMaxDepth(3);
			String src = new Integer(3).toString();
			String dest = new Integer(3 * 3 * 3 * 3 * 3).toString();

			int result = orientationHandler.getShortestDirectPathLength(TripleJType.Comment, src, dest);
			int expected = -1;

			Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingShortestPathLength() {
		try {
			orientationHandler.setMaxDepth(5);
			String src = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();
			String dest = new Integer(3 * 5).toString();

			int result = orientationHandler.getShortestPathLength(TripleJType.Comment, src, dest);
			int expected = 2;

			Assert.assertTrue(result == expected);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingNeigborsGoingToDest() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<String> result = orientationHandler
					.getSrcOutNeighborsLinkingToDest(TripleJType.Comment, src, dest);
			String[] expected = { new Integer(2 * 2).toString(),
					new Integer(2 * 15).toString()};
			Set<String> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingNeigborsGoingToDest");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			assert (false);
		}
	}

	@Test
	public void testGettingKnowingsGoingToDest1() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<Knowing> result = orientationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Comment, 2, false, src, dest);

            Knowing[] expected = {new Knowing(src, new Integer(2*15*15).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*2*15).toString(), true, 2, true)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);
			
			System.out.println("testGettingKnowingsGoingToDest1");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsGoingToDest2() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<Knowing> result = orientationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Comment, 3, false, src, dest);

            Knowing[] expected = {new Knowing(src, new Integer(2*2*15*15).toString(), true, 3, true)
                    , new Knowing(src, new Integer(2*2*2*15).toString(), true, 3, true)
                    , new Knowing(src, new Integer(2*4*15*15).toString(), true, 3, true)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsGoingToDest2");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsGoingToDest3() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<Knowing> result = orientationHandler
					.getSrcOutKnowingsLinkingToDest(TripleJType.Comment, 2, true, src, dest);

            Knowing[] expected = {new Knowing(src, new Integer(2*2).toString(), true, 1, true)
                    , new Knowing(src, new Integer(2*15).toString(), true, 1, true)
                    , new Knowing(src, new Integer(2*15*15).toString(), true, 2, true)
                    , new Knowing(src, new Integer(2*2*15).toString(), true, 2, true)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);
			
			System.out.println("testGettingKnowingsGoingToDest3");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));
			
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingNeighborsComingFromSrc() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<String> result = orientationHandler
					.getDestInNeighborsLinkingFromSrc(TripleJType.Comment, src, dest);
			String[] expected = { new Integer(2 * 2 * 3 * 5).toString(),
					new Integer(2 * 3 * 3 * 5 * 5).toString()};
			Set<String> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingNeighborsComingFromSrc");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsComingFromSrc1() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<Knowing> result = orientationHandler
					.getDestInKnowingsLinkingFromSrc(TripleJType.Comment, 2, false, src, dest);

            Knowing[] expected = {new Knowing(dest, new Integer(2*3*5).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*2).toString(), true, 2, false)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsComingFromSrc1");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsComingFromSrc2() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<Knowing> result = orientationHandler
					.getDestInKnowingsLinkingFromSrc(TripleJType.Comment, 3, false, src, dest);

            Knowing[] expected = {new Knowing(dest, new Integer(2).toString(), true, 3, false)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);

			System.out.println("testGettingKnowingsComingFromSrc2");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testGettingKnowingsComingFromSrc3() {
		try {
			String src = "2";
			String dest = new Integer(2 * 2 * 3 * 3 * 5 * 5).toString();

			Set<Knowing> result = orientationHandler
					.getDestInKnowingsLinkingFromSrc(TripleJType.Comment, 2, true, src, dest);

            Knowing[] expected = {new Knowing(dest, new Integer(2*2*3*5).toString(), true, 1, false)
                    , new Knowing(dest, new Integer(2*3*3*5*5).toString(), true, 1, false)
                    , new Knowing(dest, new Integer(2*2).toString(), true, 2, false)
                    , new Knowing(dest, new Integer(2*3*5).toString(), true, 2, false)};

			Set<Knowing> expected_ = Sets.newHashSet(expected);
			System.out.println("testGettingKnowingsComingFromSrc3");
			System.out.println("expected : " + expected_);
			System.out.println("actual : " + result);
			Assert.assertTrue(expected_.containsAll(result));
			Assert.assertTrue(result.containsAll(expected_));

		} catch (Exception e) {
			Assert.fail();
		}
	}
}
