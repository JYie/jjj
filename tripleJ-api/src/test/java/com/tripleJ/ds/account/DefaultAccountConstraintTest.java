package com.tripleJ.ds.account;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultAccountConstraintTest {

    private DefaultAccountConstraint defaultAccountConstraint;

    @Before
    public void setUp() throws Exception {
        defaultAccountConstraint = new DefaultAccountConstraint();
        defaultAccountConstraint.init();
    }

    @Test
    public void testCheckReadComment() throws Exception {
        String actual0 = defaultAccountConstraint.checkReadComment(1);
        assertTrue(null == actual0);

        String actual1 = defaultAccountConstraint.checkReadComment(2);
        assertTrue(null != actual1 && !actual1.isEmpty());

        defaultAccountConstraint.giveAuthorization(DefaultAccountConstraint.Keys.FriendsOfFriendsCanReadComment.toString());
        String actual2 = defaultAccountConstraint.checkReadComment(1);
        assertTrue(null == actual2);

        String actual3 = defaultAccountConstraint.checkReadComment(2);
        assertTrue(null == actual3);

        String actual4 = defaultAccountConstraint.checkReadComment(3);
        assertTrue(null != actual4 && !actual4.isEmpty());

        defaultAccountConstraint.removeAuthorization(DefaultAccountConstraint.Keys.FriendsOfFriendsCanReadComment.toString());
        String actual5 = defaultAccountConstraint.checkReadComment(1);
        assertTrue(null == actual5);

        String actual6 = defaultAccountConstraint.checkReadComment(2);
        assertTrue(null != actual6 && !actual6.isEmpty());
    }

    @Test
    public void testCheckWriteComment() throws Exception {
        String actual0 = defaultAccountConstraint.checkWriteComment(1);
        assertTrue(null == actual0);

        String actual1 = defaultAccountConstraint.checkWriteComment(2);
        assertTrue(null != actual1 && !actual1.isEmpty());

    }

    @Test
    public void testCheckWriteReComment() throws Exception {
        String actual0 = defaultAccountConstraint.checkWriteReComment(1);
        assertTrue(null == actual0);

        String actual1 = defaultAccountConstraint.checkWriteReComment(2);
        assertTrue(null != actual1 && !actual1.isEmpty());

        defaultAccountConstraint.giveAuthorization(DefaultAccountConstraint.Keys.FriendsOfFriendsCanWriteReCommentToMe.toString());
        String actual2 = defaultAccountConstraint.checkWriteReComment(1);
        assertTrue(null == actual2);

        String actual3 = defaultAccountConstraint.checkWriteReComment(2);
        assertTrue(null == actual3);

        String actual4 = defaultAccountConstraint.checkWriteReComment(3);
        assertTrue(null != actual4 && !actual4.isEmpty());

        defaultAccountConstraint.removeAuthorization(DefaultAccountConstraint.Keys.FriendsOfFriendsCanWriteReCommentToMe.toString());
        String actual5 = defaultAccountConstraint.checkWriteReComment(1);
        assertTrue(null == actual5);

        String actual6 = defaultAccountConstraint.checkWriteReComment(2);
        assertTrue(null != actual6 && !actual6.isEmpty());
    }

    @Test
    public void testCheckSendMessage() throws Exception {
        String actual0 = defaultAccountConstraint.checkSendMessage(1);
        assertTrue(null == actual0);

        String actual1 = defaultAccountConstraint.checkSendMessage(2);
        assertTrue(null != actual1 && !actual1.isEmpty());

        defaultAccountConstraint.giveAuthorization(DefaultAccountConstraint.Keys.EveryOneCanSendMessageToMe.toString());
        String actual2 = defaultAccountConstraint.checkSendMessage(1);
        assertTrue(null == actual2);

        String actual3 = defaultAccountConstraint.checkSendMessage(2);
        assertTrue(null == actual3);

        String actual4 = defaultAccountConstraint.checkSendMessage(7);
        assertTrue(null == actual4);

        defaultAccountConstraint.removeAuthorization(DefaultAccountConstraint.Keys.EveryOneCanSendMessageToMe.toString());
        String actual5 = defaultAccountConstraint.checkSendMessage(1);
        assertTrue(null == actual5);

        String actual6 = defaultAccountConstraint.checkSendMessage(3);
        assertTrue(null != actual6 && !actual6.isEmpty());
    }

    @Test
    public void testCheckSendInvitation() throws Exception {
        String actual0 = defaultAccountConstraint.checkSendInvitation(1);
        assertTrue(null == actual0);

        String actual1 = defaultAccountConstraint.checkSendInvitation(2);
        assertTrue(null == actual1);

        String actual2 = defaultAccountConstraint.checkSendInvitation(3);
        assertTrue(null != actual2 && !actual2.isEmpty());

        defaultAccountConstraint.giveAuthorization(DefaultAccountConstraint.Keys.EveryOneCanInviteMe.toString());
        String actual3 = defaultAccountConstraint.checkSendInvitation(1);
        assertTrue(null == actual3);

        String actual4 = defaultAccountConstraint.checkSendInvitation(3);
        assertTrue(null == actual4);

        String actual5 = defaultAccountConstraint.checkSendInvitation(6);
        assertTrue(null == actual5);

        defaultAccountConstraint.removeAuthorization(DefaultAccountConstraint.Keys.EveryOneCanInviteMe.toString());
        String actual6 = defaultAccountConstraint.checkSendInvitation(1);
        assertTrue(null == actual6);

        String actual7 = defaultAccountConstraint.checkSendInvitation(2);
        assertTrue(null == actual7);

        String actual8 = defaultAccountConstraint.checkSendInvitation(3);
        assertTrue(null != actual8 && !actual8.isEmpty());
    }

}