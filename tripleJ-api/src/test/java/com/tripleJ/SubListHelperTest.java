package com.tripleJ;

import org.junit.Test;

import static org.junit.Assert.*;

public class SubListHelperTest {

    @Test
    public void testGetToIndex0(){
        int origin = 0;
        int offset = 0;
        int size = 1;

        int expected = -1;

        int actual = SubListHelper.getToIndex(origin, offset, size);

        assertTrue(expected == actual);
    }

    @Test
    public void testGetToIndex1() {
        int origin = 2;
        int offset = 0;
        int size = 10;

        int expected = 2;
        int actual = SubListHelper.getToIndex(origin, offset, size);

        assertTrue(expected == actual);
    }

    @Test
    public void testGetToIndex2(){
        int origin = 5;
        int offset = 3;
        int size = 1;

        int expected = 4;
        int actual = SubListHelper.getToIndex(origin, offset, size);

        assertTrue(expected == actual);
    }

    /**
     * This case says that if offset is already greater than or equal to origin size, it would return -1;
     */
    @Test
    public void testGetToIndex3(){
        int origin = 2;
        int offset = 2;
        int size = 1;

        int expected = -1;
        int actual = SubListHelper.getToIndex(origin, offset, size);

        assertTrue(expected == actual);
    }
}