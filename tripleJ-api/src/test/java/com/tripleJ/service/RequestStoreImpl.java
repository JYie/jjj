package com.tripleJ.service;

import com.tripleJ.StoreException;
import com.tripleJ.ds.pair.Request;
import com.tripleJ.ds.pair.RequestInformation;
import com.tripleJ.ds.pair.RequestStatus;
import com.tripleJ.ds.pair.RequestStore;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by kami on 2014. 8. 8..
 */
public class RequestStoreImpl implements RequestStore {
    @Override
    public void registerRequest(String requestId, String requstorId, String responderId, Request request, Date current) throws StoreException {

    }

    @Override
    public void updateRequest(String requestId, Request request, Date current) throws StoreException {

    }

    @Override
    public void updateStatus(String requestId, RequestStatus status, Date current) throws StoreException {

    }

    @Override
    public boolean isExist(String requestId) throws StoreException {
        return false;
    }

    @Override
    public RequestStatus getStatus(String requestId) throws StoreException {
        return null;
    }

    @Override
    public List<RequestInformation> recoverByList(String... requestId) throws StoreException {
        return null;
    }

    @Override
    public Set<RequestInformation> recoverBySet(String... requestId) throws StoreException {
        return null;
    }

    @Override
    public String getRequestor(String requestId) throws StoreException {
        return null;
    }

    @Override
    public String getResponder(String requestId) throws StoreException {
        return null;
    }

    @Override
    public Date getLastAccessed(String requestId) throws StoreException {
        return null;
    }

    @Override
    public Date getCreated(String requestId) throws StoreException {
        return null;
    }

    @Override
    public int getSizeOfRequestsByRequestor(String... requestorIds) throws StoreException {
        return 0;
    }

    @Override
    public List<String> listRequestsByRequestor(int offset, int count, String... requestorId) throws StoreException {
        return null;
    }

    @Override
    public int getSizeOfRequestsByResponder(String... responderIds) throws StoreException {
        return 0;
    }

    @Override
    public List<String> listRequestsByResponder(int offset, int count, String... responderId) throws StoreException {
        return null;
    }
}
