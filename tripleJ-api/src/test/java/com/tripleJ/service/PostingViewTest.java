package com.tripleJ.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tripleJ.InitializeException;
import com.tripleJ.StoreException;
import com.tripleJ.TripleJType;
import com.tripleJ.ds.account.*;
import com.tripleJ.ds.comment.*;
import com.tripleJ.ds.relation.*;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class PostingViewTest {

    private static PostingView postingView;

    @BeforeClass
    public static void setUpBeforeClass() {
        try {
            AccountHandler accountHandler = new AccountHandler(new AccountStoreImpl(), null);
            CommentHandler commentHandler = new CommentHandler(new CommentStoreImpl());
            OrientationHandler orientationHandler = new OrientationHandler(new OrientationStoreImpl());

            postingView = new PostingView(accountHandler, commentHandler, orientationHandler);
        } catch (InitializeException e) {
            fail();
        }
        System.out.println("----- Start TimelineViewTest -----");
    }

    @Test
    public void testExtractTree() throws Exception {
        List<String> candidate1 = Lists.newLinkedList();
        candidate1.add(TestParams.commentChild1);

        RootedTree<String> extractedTree1 = postingView.extractTree(candidate1);

        String expectedRoot = TestParams.commentRoot;
        String extractedRoot = extractedTree1.getRoot();

        System.out.println("testExtractTree");

        assertTrue(null != extractedRoot);
        assertTrue(expectedRoot.compareTo(extractedRoot) == 0);

        assertTrue(candidate1.isEmpty());

        List<String> candidate2 = Lists.newLinkedList();
        candidate2.add(TestParams.commentChild2);
        candidate2.add(TestParams.commentChild1);
        candidate2.add(TestParams.commentRoot);
        candidate2.add(TestParams.comment3);

        RootedTree<String> extractedTree2 = postingView.extractTree(candidate2);
        String extractedRoot2 = extractedTree2.getRoot();

        assertTrue(null != extractedRoot2);
        assertTrue(expectedRoot.compareTo(extractedRoot2) == 0);

        assertTrue(candidate2.size() == 1);
        assertTrue(candidate2.get(0).compareTo(TestParams.comment3) == 0);
    }

    @Test
    public void testGenerateTree() throws Exception {
        RootedTree<String> actual = postingView.generateTree(TestParams.commentRoot);
        RootedTree<String> expected = new RootedTree(TestParams.commentRoot);
        expected.addChildren(TestParams.commentRoot, TestParams.commentChild1);
        expected.addChildren(TestParams.commentRoot, TestParams.commentChild2);
        expected.addChildren(TestParams.commentChild2, TestParams.commentChild2_1);

        System.out.println("testGenerateTree");
        System.out.println("actual = " + Arrays.toString(actual.makeFootprint()));
        System.out.println("expected = " + Arrays.toString(expected.makeFootprint()));
        assertTrue(actual.equals(expected) && actual.hashCode() == expected.hashCode());

        RootedTree<String> actual2 = postingView.generateTree(TestParams.commentChild2_1);
        System.out.println("actual(from child2_1) = " + Arrays.toString(actual2.makeFootprint()));
        assertTrue(actual2.equals(expected) && actual2.hashCode() == expected.hashCode());

        RootedTree<String> actual3 = postingView.generateTree(TestParams.commentChild2);
        System.out.println("actual(from child2) = " + Arrays.toString(actual3.makeFootprint()));
        assertTrue(actual3.equals(expected) && actual3.hashCode() == expected.hashCode());
    }

    @Test
    public void testExpandTree() throws Exception {
        RootedTree<String> needExpands = new RootedTree(TestParams.commentRoot);
        assertTrue(postingView.expandTree(needExpands));

        Set<String> expected = Sets.newHashSet();
        expected.add(TestParams.commentChild1);
        expected.add(TestParams.commentChild2);
        Set<String> firstLevel = needExpands.getChildren(TestParams.commentRoot);
        System.out.println("testExpandTree");
        assertTrue(Sets.difference(firstLevel, expected).isEmpty());

        RootedTree<String> notNeedExpands = new RootedTree(TestParams.comment3);
        assertTrue(!postingView.expandTree(notNeedExpands));

        Set<String> expected_ = Sets.newHashSet();
        expected_.add(TestParams.comment3);
        assertTrue(Sets.difference(notNeedExpands.getLeaves(), expected_).isEmpty());
    }

    @Test
    public void testTransform() throws Exception {
        System.out.println("testTransform");
        List<RootedTree<String>> list = Lists.newLinkedList();
        RootedTree<String> treeFrom1 = postingView.generateTree(TestParams.commentChild1);
        RootedTree<String> treeFrom3 = postingView.generateTree(TestParams.comment3);
        list.add(treeFrom1);
        list.add(treeFrom3);

        List<RootedTree<CommentInformation>> actual = postingView.transform(list);

        assertTrue(null != actual && actual.size() == list.size());
        for(int i=0;i<list.size();i++){
            RootedTree<String> stringTree = list.get(i);
            RootedTree<CommentInformation> ciTree = actual.get(i);
            String root = stringTree.getRoot();
            CommentInformation root_ = ciTree.getRoot();
            assertTrue(root.compareTo(root_.getIdentifier()) == 0);
            System.out.println("origin tree image = " + Arrays.toString(stringTree.makeFootprint()));
            System.out.println("transformed tree image = " + Arrays.toString(ciTree.makeFootprint()));
            assertTrue(Arrays.equals(stringTree.makeFootprint(), ciTree.makeFootprint()));
            List<String> stringMembers = stringTree.getMembers();
            List<CommentInformation> ciMember = ciTree.getMembers();
            assertTrue(stringMembers.size() == ciMember.size());
            for(int j=0;j<stringMembers.size();j++){
                String str = stringMembers.get(j);
                CommentInformation ci = ciMember.get(j);
                assertTrue(str.compareTo(ci.getIdentifier()) == 0);
            }
        }
    }

    @Test
    public void testListTimelines() throws Exception{
        System.out.println("testListComments");
        RootedTree<String> treeWithComment1Root = postingView.generateTree(TestParams.commentRoot);
        RootedTree<String> treeWithComment3Root = postingView.generateTree(TestParams.comment3);

        List<RootedTree<CommentInformation>> neoTimeline = postingView.listTimelines(TestParams.Neo, 0, 5);
        System.out.println("expected Neo timeline size = 2");
        System.out.println("actual Neo timeline size = " + neoTimeline.size());
        assertTrue(null != neoTimeline && neoTimeline.size() == 2);
        System.out.println("Neo Timelie : It collects a comment which neo writes and a comment which trinity writes to Neo");
        for(RootedTree<CommentInformation> neo : neoTimeline){
            boolean ex1 = Arrays.equals(neo.makeFootprint(), treeWithComment1Root.makeFootprint());
            boolean ex2 = Arrays.equals(neo.makeFootprint(), treeWithComment3Root.makeFootprint());
            System.out.println("Neo : " + Arrays.toString(neo.makeFootprint()));
            assertTrue(ex1 || ex2);
        }

        System.out.println("Trinity Timelie : It collects tree of comments which includes a comment which trinity writes");
        List<RootedTree<CommentInformation>> trinityTimeline = postingView.listTimelines(TestParams.Trinity, 0, 5);
        assertTrue(null != trinityTimeline && trinityTimeline.size() == 2);
        for(RootedTree<CommentInformation> trinity : trinityTimeline){
            boolean ex1 = Arrays.equals(trinity.makeFootprint(), treeWithComment1Root.makeFootprint());
            boolean ex2 = Arrays.equals(trinity.makeFootprint(), treeWithComment3Root.makeFootprint());
            System.out.println("Trinity : " + Arrays.toString(trinity.makeFootprint()));
            assertTrue(ex1 || ex2);
        }

        System.out.println("Cypher Timeline : It collects tree of comments which includes a comment which Neo writes, since Neo is a friend of Cypher");
        List<RootedTree<CommentInformation>> cypherTimeline = postingView.listTimelines(TestParams.Cypher, 0, 5);
        assertTrue(null != cypherTimeline && cypherTimeline.size() == 1);
        RootedTree<CommentInformation> cypher = cypherTimeline.get(0);
        assertTrue(Arrays.equals(cypher.makeFootprint(), treeWithComment1Root.makeFootprint()));
        System.out.println("Cypher : " + Arrays.toString(cypher.makeFootprint()));

        System.out.println("Smith Timeline : It collects tree of comments which includes a comment which Trinity writes, since Trinity is a friend of Smith");
        List<RootedTree<CommentInformation>> smithTimeline = postingView.listTimelines(TestParams.Smith, 0, 5);
        assertTrue(null != smithTimeline && smithTimeline.size() == 2);
        for(RootedTree<CommentInformation> smith : smithTimeline){
            boolean ex1 = Arrays.equals(smith.makeFootprint(), treeWithComment1Root.makeFootprint());
            boolean ex2 = Arrays.equals(smith.makeFootprint(), treeWithComment3Root.makeFootprint());
            System.out.println("Smith : " + Arrays.toString(smith.makeFootprint()));
            assertTrue(ex1 || ex2);
        }
    }
}