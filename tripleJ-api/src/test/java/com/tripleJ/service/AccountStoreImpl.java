package com.tripleJ.service;

import com.tripleJ.StoreException;
import com.tripleJ.ds.account.Account;
import com.tripleJ.ds.account.AccountInformation;
import com.tripleJ.ds.account.AccountStore;

import java.util.List;
import java.util.Set;

/**
 * Created by kami on 2014. 8. 8..
 */
public class AccountStoreImpl implements AccountStore{
    @Override
    public void registerAccount(String accountId, Account account) throws StoreException {

    }

    @Override
    public void unregisterAccount(String accountId) throws StoreException {

    }

    @Override
    public void modifyAccount(String accountId, Account account) throws StoreException {

    }

    @Override
    public boolean isRegisteredId(String accountId) throws StoreException {
        return false;
    }

    @Override
    public List<AccountInformation> recoverByList(String... accountIds) throws StoreException {
        return null;
    }

    @Override
    public Set<AccountInformation> recoverBySet(String... accountIds) throws StoreException {
        return null;
    }
}
