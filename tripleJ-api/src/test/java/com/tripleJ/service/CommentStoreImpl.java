package com.tripleJ.service;

/**
 * Created by kami on 2014. 8. 8..
 */

import com.google.common.collect.Lists;
import com.tripleJ.StoreException;
import com.tripleJ.ds.comment.Comment;
import com.tripleJ.ds.comment.CommentInformation;
import com.tripleJ.ds.comment.CommentStore;
import com.tripleJ.ds.comment.Scope;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Neo : CommentRoot, Comment2_1
 * Cypher : CommentChild1
 * Trinity : CommentChild2
 * Trinity -> Neo : Comment3
 *
 */
public class CommentStoreImpl implements CommentStore {

    @Override
    public void addComment(String commentId, String authorId, String listenerId, Comment comment, Date calendar, Scope scope) throws StoreException {
        return;
    }

    @Override
    public void removeComment(String commentId) throws StoreException {
        return;
    }

    @Override
    public void updateComment(String commentId, Comment comment, Date current) throws StoreException {
        return;
    }

    @Override
    public boolean isExist(String commentId) throws StoreException {
        return false;
    }

    @Override
    public List<CommentInformation> recoverByList(String... commentId) throws StoreException {
        List<CommentInformation> recovered = Lists.newLinkedList();
        for(int i=0;i<commentId.length;i++){
            if(commentId[i].compareToIgnoreCase(TestParams.commentRoot) == 0)
                recovered.add(new CommentInformation(TestParams.Neo, TestParams.commentRoot, new Comment("Hi", null, null)));
            else if(commentId[i].compareToIgnoreCase(TestParams.commentChild1) == 0)
                recovered.add(new CommentInformation(TestParams.Cypher, TestParams.commentChild1, new Comment("Re Hi", null, null)));
            else if(commentId[i].compareToIgnoreCase(TestParams.commentChild2) == 0)
                recovered.add(new CommentInformation(TestParams.Trinity, TestParams.commentChild2, new Comment("How is it going?", null, null)));
            else if(commentId[i].compareToIgnoreCase(TestParams.commentChild2_1) == 0)
                recovered.add(new CommentInformation(TestParams.Neo, TestParams.commentChild2_1, new Comment("It is good?", null, null)));
            else if(commentId[i].compareToIgnoreCase(TestParams.comment3) == 0)
                recovered.add(new CommentInformation(TestParams.Trinity, TestParams.Neo, TestParams.comment3, new Comment("Hi Neo", null, null)));
        }
        return recovered;
    }

    @Override
    public Set<CommentInformation> recoverBySet(String... commentIds) throws StoreException {
        return null;
    }

    @Override
    public String getAuthor(String commentId) throws StoreException {
        if(commentId.compareToIgnoreCase(TestParams.commentRoot) == 0)
            return TestParams.Neo;
        else if(commentId.compareToIgnoreCase(TestParams.commentChild1) == 0)
            return TestParams.Cypher;
        else if(commentId.compareToIgnoreCase(TestParams.commentChild2) == 0)
            return TestParams.Trinity;
        else if(commentId.compareToIgnoreCase(TestParams.commentChild2_1) == 0)
            return TestParams.Neo;
        else if(commentId.compareToIgnoreCase(TestParams.comment3) == 0)
            return TestParams.Trinity;
        else
            return null;
    }

    @Override
    public String getListener(String commentId) throws StoreException {
        if(commentId.compareToIgnoreCase(TestParams.commentRoot) == 0)
            return TestParams.Neo;
        else if(commentId.compareToIgnoreCase(TestParams.commentChild1) == 0)
            return TestParams.Cypher;
        else if(commentId.compareToIgnoreCase(TestParams.commentChild2) == 0)
            return TestParams.Trinity;
        else if(commentId.compareToIgnoreCase(TestParams.commentChild2_1) == 0)
            return TestParams.Neo;
        else if(commentId.compareToIgnoreCase(TestParams.comment3) == 0)
            return TestParams.Neo;
        else
            return null;
    }

    @Override
    public Date getLastAccessed(String commentId) throws StoreException {
        return null;
    }

    @Override
    public Date getCreated(String commentId) throws StoreException {
        return null;
    }

    @Override
    public List<String> listWideCommentsByAuthor(int offset, int count, String... authorIds) throws StoreException {
        List<String> comments = Lists.newLinkedList();
        for(String author:authorIds){
            if(author.compareToIgnoreCase(TestParams.Neo) == 0){
                comments.add(TestParams.commentRoot);
                comments.add(TestParams.commentChild2_1);
            }else if(author.compareToIgnoreCase(TestParams.Cypher) == 0){
                comments.add(TestParams.commentChild1);
            }else if(author.compareToIgnoreCase(TestParams.Trinity) == 0){
                comments.add(TestParams.commentChild2);
                comments.add(TestParams.comment3);
            }
        }
        return comments;
    }

    @Override
    public List<String> listNarrowCommentsByAuthor(int offset, int count, String... authorIds) throws StoreException {
        return null;
    }

    @Override
    public int getSizeofWideCommentsByAuthor(String... authorIds) throws StoreException {
        int total = 0;
        for(String author:authorIds){
            if(author.compareToIgnoreCase(TestParams.Neo) == 0){
                total = total + 2;
            }else if(author.compareToIgnoreCase(TestParams.Cypher) == 0){
                total = total + 1;
            }else if(author.compareToIgnoreCase(TestParams.Trinity) == 0){
                total = total + 2;
            }
        }
        return total;
    }

    @Override
    public int getSizeofNarrowCommentsByAuthor(String... authorIds) throws StoreException {
        return 0;
    }

    @Override
    public List<String> listWideCommentsByListener(int offset, int count, String... listenerIds) throws StoreException {
        List<String> comments = Lists.newLinkedList();
        for(String listener:listenerIds){
            if(listener.compareToIgnoreCase(TestParams.Neo) == 0){
                comments.add(TestParams.commentRoot);
                comments.add(TestParams.commentChild2_1);
                comments.add(TestParams.comment3);
            }else if(listener.compareToIgnoreCase(TestParams.Cypher) == 0){
                comments.add(TestParams.commentChild1);
            }else if(listener.compareToIgnoreCase(TestParams.Trinity) == 0){
                comments.add(TestParams.commentChild2);
            }
        }
        return comments;
    }

    @Override
    public List<String> listNarrowCommentsByListener(int offset, int count, String... listenerIds) throws StoreException {
        return null;
    }

    @Override
    public int getSizeofWideCommentsByListener(String... listenerIds) throws StoreException {
        int total = 0;
        for(String listener:listenerIds){
            if(listener.compareToIgnoreCase(TestParams.Neo) == 0){
                total = total + 3;
            }else if(listener.compareToIgnoreCase(TestParams.Cypher) == 0){
                total = total + 1;
            }else if(listener.compareToIgnoreCase(TestParams.Trinity) == 0){
                total = total + 1;
            }
        }
        return total;
    }

    @Override
    public int getSizeofNarrowCommentsByListener(String... listenerIds) throws StoreException {
        return 0;
    }

    @Override
    public List<String> listWideCommentsByPair(int offset, int count, String author, String listener) {
        List<String> pairs = Lists.newLinkedList();

        if(author.compareToIgnoreCase(TestParams.Trinity) == 0 && listener.compareToIgnoreCase(TestParams.Neo) == 0)
            pairs.add(TestParams.comment3);

        return pairs;
    }

    @Override
    public List<String> listNarrowCommentsByPair(int offset, int count, String author, String listener) {
        return null;
    }
}