package com.tripleJ.service;

import com.google.common.collect.Ordering;
import com.tripleJ.ds.pair.*;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class RequestViewTest {

    @Test
    public void testGetByDateOnly() throws Exception {
        Ordering<RequestInformation> orderingByDateOnly = RequestView.getByDateOnly();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1);
        Date date1 = calendar.getTime();
        calendar.set(2000, 1, 2);
        Date date2 = calendar.getTime();

        Request request = new Request(RequestType.AddRelation, null);

        RequestInformation information1 = new RequestInformation("1", "1", "1", null, request, date1);
        RequestInformation information2 = new RequestInformation("1", "1", "1", null, request, date2);

        assertTrue(orderingByDateOnly.compare(information1, information2) < 0);

        RequestInformation information3 = new RequestInformation("1", "1", "1", RequestStatus.NOTIFIED, request, date1);
        RequestInformation information4 = new RequestInformation("1", "1", "1", RequestStatus.ACCEPTED, request, date1);

        assertTrue(orderingByDateOnly.compare(information3, information4) == 0);

        RequestInformation information5 = new RequestInformation("1", "1", "1", RequestStatus.ACCEPTED, request, date1);
        RequestInformation information6 = new RequestInformation("1", "1", "1" ,RequestStatus.CREATED, request, date2);

        assertTrue(orderingByDateOnly.compare(information5, information6) < 0);

    }

    @Test
    public void testGetByStatusPriority() throws Exception {
        Ordering<RequestInformation> orderingByStatusPriority = RequestView.getByStatusPriority();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1);
        Date date1 = calendar.getTime();
        calendar.set(2000, 1, 2);
        Date date2 = calendar.getTime();

        Request request = new Request(RequestType.AddRelation, null);

        RequestInformation information0 = new RequestInformation("1", "1", "1", RequestStatus.ACCEPTED, request, date1);

        assertTrue(orderingByStatusPriority.compare(information0, information0) == 0);

        RequestInformation information1 = new RequestInformation("1", "1", "1", RequestStatus.ACCEPTED, request, date1);
        RequestInformation information2 = new RequestInformation("1", "1", "1", RequestStatus.DECLINED, request, date2);

        assertTrue(orderingByStatusPriority.compare(information1, information2) < 0);

        RequestInformation information3 = new RequestInformation("1", "1", "1", RequestStatus.NOTIFIED, request, date1);
        RequestInformation information4 = new RequestInformation("1", "1", "1", RequestStatus.ACCEPTED, request, date1);

        assertTrue(orderingByStatusPriority.compare(information3, information4) < 0);

        RequestInformation information5 = new RequestInformation("1", "1", "1", RequestStatus.ACCEPTED, request, date1);
        RequestInformation information6 = new RequestInformation("1", "1", "1", RequestStatus.CREATED, request, date1);

        assertTrue(orderingByStatusPriority.compare(information5, information6) > 0);
    }
}
