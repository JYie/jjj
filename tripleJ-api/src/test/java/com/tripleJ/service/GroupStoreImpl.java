package com.tripleJ.service;

import com.tripleJ.StoreException;
import com.tripleJ.ds.group.Group;
import com.tripleJ.ds.group.GroupInformation;
import com.tripleJ.ds.group.GroupStore;

import java.util.List;
import java.util.Set;

/**
 * Created by kami on 2014. 8. 8..
 */
public class GroupStoreImpl implements GroupStore {
    @Override
    public void createGroup(String groupId, Group group) throws StoreException {

    }

    @Override
    public void removeGroup(String groupId) throws StoreException {

    }

    @Override
    public void updateGroup(String groupId, Group group) throws StoreException {

    }

    @Override
    public void join(String groupId, String accountId) throws StoreException {

    }

    @Override
    public void dropOut(String groupId, String accountId) throws StoreException {

    }

    @Override
    public boolean isExist(String groupId) throws StoreException {
        return false;
    }

    @Override
    public boolean isMember(String groupId, String accountId) throws StoreException {
        return false;
    }

    @Override
    public Set<String> getMembers(String groupId) throws StoreException {
        return null;
    }

    @Override
    public Set<String> getGroups(String accountId) throws StoreException {
        return null;
    }

    @Override
    public List<GroupInformation> recoverByList(String... groupIds) throws StoreException {
        return null;
    }

    @Override
    public Set<GroupInformation> recoverBySet(String... groupIds) throws StoreException {
        return null;
    }
}
