package com.tripleJ.service;

import com.google.common.collect.Sets;
import com.tripleJ.StoreException;
import com.tripleJ.TripleJType;
import com.tripleJ.ds.relation.GraphType;
import com.tripleJ.ds.relation.Knowing;
import com.tripleJ.ds.relation.OrientationStore;

import java.util.Set;

/**
 * Created by kami on 2014. 8. 8..
 */
public class OrientationStoreImpl implements OrientationStore {

    @Override
    public void addOrientation(GraphType graphType, String source, String destination) throws StoreException {
        return;
    }

    @Override
    public void removeOrientation(GraphType graphType, String source, String destination) throws StoreException {
        return;
    }

    @Override
    public void addRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
        return;
    }

    @Override
    public void removeRelation(GraphType graphType, String vert1, String vert2) throws StoreException {
        return;
    }

    @Override
    public Set<Knowing> getOutNeighbors(GraphType graphType, String... sources) throws StoreException {
        TripleJType type = (TripleJType)graphType;
        switch (type){
            case Comment :
                return getCommentOutNeighbors(sources);
            default:
                return null;
        }
    }

    private Set<Knowing> getCommentOutNeighbors(String... sources){
        Set<Knowing> outNeighbors = Sets.newHashSet();
        for(String source: sources){
            if(source.compareToIgnoreCase(TestParams.commentRoot) == 0) {
                outNeighbors.add(new Knowing(TestParams.commentRoot, TestParams.commentChild1, 1));
                outNeighbors.add(new Knowing(TestParams.commentRoot, TestParams.commentChild2, 1));
            }else if(source.compareToIgnoreCase(TestParams.commentChild2) == 0)
                outNeighbors.add(new Knowing(TestParams.commentChild2, TestParams.commentChild2_1, 1));
        }
        return outNeighbors;
    }


    @Override
    public Set<Knowing> getInNeighbors(GraphType graphType, String... destinations) throws StoreException {
        TripleJType type = (TripleJType)graphType;
        switch (type){
            case Comment :
                return getCommentInNeighbors(destinations);
            default:
                return null;
        }
    }

    private Set<Knowing> getCommentInNeighbors(String... destinations){
        Set<Knowing> inNeighbors = Sets.newHashSet();
        for(String destination: destinations){
            if(destination.compareToIgnoreCase(TestParams.commentChild1) == 0)
                inNeighbors.add(new Knowing(TestParams.commentRoot, TestParams.commentChild1, 1));
            else if(destination.compareToIgnoreCase(TestParams.commentChild2) == 0)
                inNeighbors.add(new Knowing(TestParams.commentRoot, TestParams.commentChild2, 1));
            else if(destination.compareToIgnoreCase(TestParams.commentChild2_1) == 0)
                inNeighbors.add(new Knowing(TestParams.commentChild2, TestParams.commentChild2_1, 1));
        }
        return inNeighbors;
    }

    @Override
    public Set<Knowing> getRelations(GraphType graphType, String... vertices) throws StoreException {
        TripleJType type = (TripleJType)graphType;
        switch (type){
            case Account:
                return getAccountRelation(vertices);
            default:
                return null;
        }
    }

    private Set<Knowing> getAccountRelation(String... nodes){
  	 	 	/*
	        *    (Neo) ---- (Trinity) ---- (Agent Smith)
	        *     |    \
	        *     |     \
	        * (Cypher) (Morpheus)
	        */

        Set<Knowing> result = Sets.newHashSet();
        for(String node: nodes){
            if(node.compareTo(TestParams.Neo) == 0){
                result.add(new Knowing(TestParams.Neo, TestParams.Cypher, false, 1));
                result.add(new Knowing(TestParams.Neo, TestParams.Trinity, false, 1));
                result.add(new Knowing(TestParams.Neo, TestParams.Morpheus, false, 1));
            }else if(node.compareTo(TestParams.Cypher) == 0){
                result.add(new Knowing(TestParams.Cypher, TestParams.Neo, false, 1));
            }else if(node.compareTo(TestParams.Morpheus) == 0){
                result.add(new Knowing(TestParams.Morpheus, TestParams.Neo, false, 1));
            }else if(node.compareTo(TestParams.Trinity) == 0){
                result.add(new Knowing(TestParams.Trinity, TestParams.Neo, false, 1));
                result.add(new Knowing(TestParams.Trinity, TestParams.Smith, false, 1));
            }else if(node.compareTo(TestParams.Smith) == 0){
                result.add(new Knowing(TestParams.Smith, TestParams.Trinity, false, 1));
            }
        }
        return result;
    }

}
