package com.tripleJ.service;

/**
 * Created by kami on 2014. 8. 8..
 */
public class TestParams {
    static final String commentRoot = "commentRoot";
    static final String commentChild1 = "commentChild1";
    static final String commentChild2 = "commentChild2";
    static final String commentChild2_1 = "commentChild2_1";
    static final String comment3 = "comment3";

    static final String Neo = "Neo";
    static final String Morpheus = "Morpheus";
    static final String Trinity = "Trinity";
    static final String Cypher = "Cypher";
    static final String Smith = "Smith";

}
