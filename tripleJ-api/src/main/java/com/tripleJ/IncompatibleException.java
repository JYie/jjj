package com.tripleJ;

/**
 * Created by kami on 2014. 7. 7..
 */
public class IncompatibleException extends TripleJException {

    public IncompatibleException(Exception exception) {
        super(exception);
    }

    public IncompatibleException(String arg, Exception exception) {
        super(arg, exception);
    }

    public IncompatibleException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public IncompatibleException(String arg0) {
        super(arg0);
    }

    public IncompatibleException(Throwable arg0) {
        super(arg0);
    }

    public IncompatibleException() {
    }
}
