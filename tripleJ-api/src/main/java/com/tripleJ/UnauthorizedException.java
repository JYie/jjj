package com.tripleJ;

/**
 * Created by kami on 2014. 6. 18..
 */
public class UnauthorizedException extends TripleJException {
    public UnauthorizedException(Exception exception) {
        super(exception);
    }

    public UnauthorizedException(String arg, Exception exception) {
        super(arg, exception);
    }

    public UnauthorizedException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public UnauthorizedException(String arg0) {
        super(arg0);
    }

    public UnauthorizedException(Throwable arg0) {
        super(arg0);
    }

    public UnauthorizedException() {
    }
}
