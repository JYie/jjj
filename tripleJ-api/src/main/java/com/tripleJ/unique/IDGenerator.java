package com.tripleJ.unique;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.tripleJ.TripleJType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by kami on 2014. 6. 19..
 */
public class IDGenerator {

    static boolean DEV_VERSION = true;

    private static final String VERSION = "V1.0.0";
    private static final String PASS = "vinetree";

    private final String id;
    private final String type;
    private final String version;
    private final Map<String, String> resources;
    private final String oneWay;
    private int cursor = 0;

    public IDGenerator(String id) throws InvalidIdException{
        if(DEV_VERSION){
           this.id = id;
           this.type = "TEST";
           this.version = VERSION;
           this.resources = Maps.newHashMap();
            this.oneWay = "";
        }else{
            this.id = id;
            List<String> list = extractContinuously();
            this.version = list.get(0);
            this.type = list.get(1);
            int size = list.size();
            Map<String, String> resources_ = Maps.newHashMap();
            for(int i=2;i<size - 1; i=i+2){
                resources_.put(list.get(i), list.get(i+1));
            }
            resources = Collections.unmodifiableMap(resources_);
            this.oneWay = list.get(list.size() - 1);
        }
    }

//    private static void checkSingleValidity(String id) throws InvalidIdException{
//        if(null == id || id.isEmpty())
//            throw new InvalidIdException("null or empty string can not be a valid id");
//
//        try{
//            List<String> list = Lists.newLinkedList();
//            int length = id.length();
//            int cursor = 0;
//            while(cursor< length)
//                cursor = extract(list, id, cursor);
//
//            String version = list.get(0);
//            String type = list.get(1);
//            int size = list.size();
//            if(size <= 2)
//                throw new InvalidIdException("there is no hash value");
//
//            Map<String, String> resources = Maps.newHashMap();
//            for(int i=2;i<size - 1; i=i+2){
//                resources.put(list.get(i), list.get(i+1));
//            }
//
//            String oneWay = list.get(list.size() - 1);
//            if(null == oneWay || oneWay.isEmpty())
//                throw new InvalidIdException("there is no hash value");
//
//            StringBuffer sb = new StringBuffer();
//
//            sb.append(version.length()).append(version);
//            sb.append(type.length()).append(type);
//
//            String resourceStr = integrate(resources);
//            sb.append(resourceStr);
//
//            String oneWay2 = generateOneWayCode(resourceStr + PASS);
//
//            if(oneWay.compareTo(oneWay2) != 0)
//                throw new InvalidIdException("Hash value is not valid");
//        }catch(NumberFormatException ne){
//            throw new InvalidIdException("Unformatted id");
//        }catch(ArrayIndexOutOfBoundsException aiob){
//            throw new InvalidIdException("Unformatted id");
//        }
//    }
//
//
//    public static void checkValidity(String... ids) throws InvalidIdException{
//        for(String id:ids)
//            checkSingleValidity(id);
//    }

    public void checkValidity() throws InvalidIdException{
        if(DEV_VERSION)
            return;
        StringBuffer sb = new StringBuffer();

        sb.append(getVersion().length()).append(getVersion());
        sb.append(getType().length()).append(getType());

        String resourceStr = integrate(getResources());
        sb.append(resourceStr);

        String oneWay = generateOneWayCode(resourceStr + PASS);
        if(null == oneWay || oneWay.isEmpty())
            throw new InvalidIdException("hash code is not found");
        if(oneWay.compareTo(this.oneWay) != 0)
            throw new InvalidIdException("invalid hash code");
    }

    public static String generateId(TripleJType tripleJType, Map<String, String> resources){
        StringBuffer sb = new StringBuffer();

        sb.append(VERSION.length()).append(VERSION);
        sb.append(tripleJType.getName().length()).append(tripleJType.getName());

        String resourceStr = integrate(resources);
        sb.append(resourceStr);

        String oneWay = generateOneWayCode(resourceStr + PASS);
        sb.append(oneWay.length());
        sb.append(oneWay);
        return sb.toString();
    }
//
//    private static int extract(List<String> result, String id, int cursor){
//        int len = 0;
//        int current = cursor;
//        while(true){
//            char ch = id.charAt(cursor);
//            if(ch > 47 && ch < 58) {
//                cursor++;
//                len ++;
//            }
//            else
//                break;
//        }
//        char[] length = new char[len];
//        for(int i=0;i<len;i++)
//            length[i] = id.charAt(i+current);
//
//        int length_ = Integer.parseInt(new String(length));
//        String value = id.substring(cursor, cursor+length_);
//        result.add(value);
//        return cursor + length_;
//
//    }

    private String extract() throws InvalidIdException{
        try {
            int len = 0;
            int current = cursor;
            while(true){
                char ch = id.charAt(cursor);
                if(ch > 47 && ch < 58) {
                    cursor++;
                    len ++;
                }
                else
                    break;
            }
            char[] length = new char[len];
            for(int i=0;i<len;i++)
                length[i] = id.charAt(i+current);

            int length_ = Integer.parseInt(new String(length));
            String value = id.substring(cursor, cursor + length_);
            cursor = cursor + length_;
            return value;
        }catch(NumberFormatException nfe){
            throw new InvalidIdException("unformatted id");
        }catch(IndexOutOfBoundsException iobe){
            throw new InvalidIdException("unformatted id");
        }
    }

    private List<String> extractContinuously() throws InvalidIdException{
        List<String> list = Lists.newLinkedList();
        int length = this.id.length();
        while(cursor<length)
            list.add(extract());
        return list;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getVersion() {
        return version;
    }

    public Map<String, String> getResources() {
        return resources;
    }

    private static String integrate(Map<String, String> resources){
        if(null == resources || resources.keySet().isEmpty())
            return "";

        List<String> keys = Lists.newLinkedList();
        keys.addAll(resources.keySet());
        Collections.sort(keys, stringOrder);

        StringBuffer integrate = new StringBuffer();

        int size = keys.size();
        ConstraindString cs = new ConstraindString(48,57); //It filters a String which starts with 0~9
        for(int i=0;i<size;i++){
            String key = keys.get(i);
            String value = resources.get(key);
            if(!cs.isConstrained(key) && !cs.isConstrained(value))
                integrate.append(key.length()).append(key).append(value.length()).append(value).toString();
        }

        return integrate.toString();
    }

    /**
     * As of now, we use a MD5 algorithm to generate a hash value.
     * @param integrated
     * @return
     */
    private static String generateOneWayCode(String integrated){
        if(null == integrated || integrated.isEmpty())
            return "";

        String MD5;
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(integrated.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer("o"); //adding 'o' is to prevent for hash code to start with numbers
            for(int i = 0 ; i < byteData.length ; i++){
                sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
            }
            MD5 = sb.toString();
        }catch(NoSuchAlgorithmException e){
            MD5 = null;
        }
        return MD5;
    }

    private static Ordering<String> stringOrder = new Ordering<String>() {
        @Override
        public int compare(String t, String t2) {
            return t.compareTo(t2);
        }
    };

    public static void main(String[] args){
        Map<String, String> resources = Maps.newHashMap();
        resources.put("author", "kami");
        String generated = generateId(TripleJType.Comment, resources);

        System.out.println(generated);
        try{
            IDGenerator idg = new IDGenerator(generated);
            idg.checkValidity();

        }catch (InvalidIdException ie){
            System.out.println("error");
        }
    }
}
