package com.tripleJ.unique;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 6. 30..
 */
public class InvalidIdException extends TripleJException {

    public InvalidIdException(Exception exception) {
        super(exception);
    }

    public InvalidIdException(String arg, Exception exception) {
        super(arg, exception);
    }

    public InvalidIdException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public InvalidIdException(String arg0) {
        super(arg0);
    }

    public InvalidIdException(Throwable arg0) {
        super(arg0);
    }

    public InvalidIdException() {
    }
}
