package com.tripleJ.unique;

/**
 * Created by kami on 2014. 8. 7..
 */
public class UIDPolicy {

    private static final ConstraindString rule = new ConstraindString(48,57);

    public static boolean canUse(String id){
        return !rule.isConstrained(id);
    }
}
