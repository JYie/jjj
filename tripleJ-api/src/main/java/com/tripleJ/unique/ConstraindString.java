package com.tripleJ.unique;

import com.tripleJ.ConstrainedObject;

/**
 * Created by kami on 2014. 7. 18..
 */
public class ConstraindString implements ConstrainedObject<String>{

    private final int from;
    private final int to;

    public ConstraindString(int from, int to){
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean isConstrained(String s) {
        if(null == s)
            return false;
        char ch = s.charAt(0);
        if(ch >= this.from && ch <= this.to)
            return true;
        return false;
    }
}
