package com.tripleJ.unique;

import java.util.Map;

/**
 * Created by kami on 2014. 6. 23..
 */
public interface IdGeneratable {

    /**
     * It has to return map which would be used to generate unique id with extra data in map delivered.
     * @return map of resources which will be used to generate id
     */
    public Map<String, String> getResources();
}
