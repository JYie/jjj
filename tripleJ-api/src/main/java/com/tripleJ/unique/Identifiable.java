package com.tripleJ.unique;

import java.util.Set;

/**
 * Created by kami on 2014. 6. 17..
 */
public interface Identifiable {

    public String getIdentifier();

}
