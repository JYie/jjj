package com.tripleJ;

/**
 * Created by kami on 2014. 6. 16..
 */
public class DuplicateIdException extends TripleJException {

    public DuplicateIdException() {
        super();
    }

    public DuplicateIdException(Exception exception) {
        super(exception);
    }

    public DuplicateIdException(String arg, Exception exception) {
        super(arg, exception);
    }

    public DuplicateIdException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public DuplicateIdException(String arg0) {
        super(arg0);
    }

    public DuplicateIdException(Throwable arg0) {
        super(arg0);
    }

}
