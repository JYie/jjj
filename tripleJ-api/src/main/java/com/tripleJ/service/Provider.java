package com.tripleJ.service;

import com.tripleJ.InitializeException;
import com.tripleJ.ds.account.AccountHandler;
import com.tripleJ.ds.account.AccountStore;
import com.tripleJ.ds.account.PreferenceStore;
import com.tripleJ.ds.comment.CommentHandler;
import com.tripleJ.ds.comment.CommentStore;
import com.tripleJ.ds.group.CapacityStore;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.group.GroupStore;
import com.tripleJ.ds.pair.RequestHandler;
import com.tripleJ.ds.pair.RequestStore;
import com.tripleJ.ds.relation.OrientationHandler;
import com.tripleJ.ds.relation.OrientationStore;

import java.lang.instrument.UnmodifiableClassException;
import java.util.Map;

/**
 *
 * Created by kami on 2014. 7. 25..
 */
public class Provider {

    public static enum StoreKey{
        Account, Preference, Comment, Group, Capacity, Request, Orientation
    }

    private static AccountStore accountStore;
    private static CommentStore commentStore;
    private static PreferenceStore preferenceStore;
    private static GroupStore groupStore;
    private static CapacityStore capacityStore;
    private static RequestStore requestStore;
    private static OrientationStore orientationStore;

    private static AccountHandler accountHandler;
    private static CommentHandler commentHandler;
    private static GroupHandler groupHandler;
    private static RequestHandler requestHandler;
    private static OrientationHandler orientationHandler;

    public static void registerStores(Map<StoreKey, String> storeMap) throws InitializeException, UnmodifiableClassException {
        if(null != accountStore)
            throw new UnmodifiableClassException("Every Store is alread fixed");
        if(null == storeMap)
            throw new InitializeException("storeMap must not be null");

        String accountStoreClass = storeMap.get(StoreKey.Account);
        if(null == accountStoreClass)
            throw new InitializeException("AccountStore must not be null");

        String commentStoreClas = storeMap.get(StoreKey.Comment);
        if(null == commentStoreClas)
            throw new InitializeException("CommentStore must not be null");

        String groupStoreClass = storeMap.get(StoreKey.Group);
        if(null == groupStoreClass)
            throw new InitializeException("GroupStore must not be null");

        String requestStoreClass = storeMap.get(StoreKey.Request);
        if(null == requestStoreClass)
            throw new InitializeException("RequestStore must not be null");

        String orientationStoreClass = storeMap.get(StoreKey.Orientation);
        if(null == orientationStoreClass)
            throw new InitializeException("OrientationStore must not be null");

        String preferenceStoreClass = storeMap.get(StoreKey.Preference);

        String capcityStoreClass = storeMap.get(StoreKey.Capacity);

        try{
            Class asc = Class.forName(accountStoreClass);
            accountStore = (AccountStore)asc.newInstance();

            Class csc = Class.forName(commentStoreClas);
            commentStore = (CommentStore)csc.newInstance();

            Class gsc = Class.forName(groupStoreClass);
            groupStore = (GroupStore)gsc.newInstance();

            Class rsc = Class.forName(requestStoreClass);
            requestStore = (RequestStore)rsc.newInstance();

            Class osc = Class.forName(orientationStoreClass);
            orientationStore = (OrientationStore)osc.newInstance();

        } catch (ClassNotFoundException cnef) {
            throw new InitializeException(cnef.getCause().getMessage());
        } catch (InstantiationException ie) {
            throw new InitializeException(ie.getCause().getMessage());
        } catch (IllegalAccessException iae) {
            throw new InitializeException(iae.getCause().getMessage());
        }

        try{
            if(null != preferenceStoreClass){
                Class psc = Class.forName(preferenceStoreClass);
                preferenceStore = (PreferenceStore)psc.newInstance();
            }

            if(null != capcityStoreClass){
                Class capaSc = Class.forName(capcityStoreClass);
                capacityStore = (CapacityStore)capaSc.newInstance();
            }
        } catch (ClassNotFoundException e) {

        } catch (InstantiationException e) {

        } catch (IllegalAccessException e) {

        }
        setHandler();
    }

    public static AccountHandler getAccountHandler(){
        return accountHandler;
    }

    public static CommentHandler getCommentHandler(){
        return commentHandler;
    }

    public static GroupHandler getGroupHandler(){
        return groupHandler;
    }

    public static RequestHandler getRequestHandler(){
        return requestHandler;
    }

    public static OrientationHandler getOrientationHandler(){
        return orientationHandler;
    }

    private static void setHandler(){
        try {
            accountHandler = new AccountHandler(accountStore, preferenceStore);
            commentHandler = new CommentHandler(commentStore);
            groupHandler = new GroupHandler(groupStore, preferenceStore, capacityStore);
            requestHandler = new RequestHandler(requestStore);
            orientationHandler = new OrientationHandler(orientationStore);
        } catch (InitializeException e) {
            e.printStackTrace();
        }
    }

}
