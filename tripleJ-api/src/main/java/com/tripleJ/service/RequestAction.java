package com.tripleJ.service;

import com.tripleJ.*;
import com.tripleJ.ds.group.Capacity;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.pair.*;
import com.tripleJ.ds.relation.OrientationHandler;
import com.tripleJ.ds.relation.TreeException;
import com.tripleJ.unique.InvalidIdException;

import java.util.Iterator;
import java.util.List;

/**
 * RequestAction class is for the action related to
 * Created by kami on 2014. 6. 30..
 */
public class RequestAction {

    private final RequestHandler requestHandler;
    private final OrientationHandler orientationHandler;
    private final GroupHandler groupHandler;

    RequestAction(RequestHandler requestHandler, OrientationHandler orientationHandler, GroupHandler groupHandler) throws InitializeException{
        if(null == requestHandler || null == orientationHandler || null == groupHandler)
            throw new InitializeException("requestHandler must not be null");
        this.requestHandler = requestHandler;
        this.orientationHandler = orientationHandler;
        this.groupHandler = groupHandler;
    }

    public RequestAction() throws InitializeException{
        this.requestHandler = Provider.getRequestHandler();
        this.orientationHandler = Provider.getOrientationHandler();
        this.groupHandler = Provider.getGroupHandler();

        if(null == this.requestHandler || null == this.orientationHandler || null == this.groupHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     * It actually completes the process that the request indicates by accepting it. If the process completes successfully, the status of the request would be changed to RequestStatus.Accepted.
     * Otherwise, it would be changed to RequestStatus.NOTIFIED.
     * @param handlerId
     * @param requestId
     * @throws UserException
     * @throws ServiceNotAvailableException
     * @throws InappropriateStatusException
     * @throws UnauthorizedException
     */
    public void accept(String handlerId, String requestId) throws UserException, ServiceNotAvailableException, InappropriateStatusException, UnauthorizedException {
        try{
            String actualResponderId = requestHandler.getResponder(requestId);
            if(null == actualResponderId)
                throw new UnrecognizableException("can not find an actual responder");

            List<RequestInformation> requests = requestHandler.recoverByList(requestId);
            if(null == requests || requests.size() == 0)
                throw new UnrecognizableException("can not find Request having id " + requestId);
            assert(requests.size() == 1);

            Iterator<RequestInformation> iterated = requests.iterator();
            RequestInformation requestInformation = null;
            while(iterated.hasNext())
                requestInformation = iterated.next();

            assert(null != requestInformation);

            Request request = requestInformation.getRequest();
            assert(null != request);

            RequestType type = request.getRequestType();
            if(null == type)
                throw new UnrecognizableException("RequestType is not recognizable");

            boolean result;
            String requestor = requestHandler.getRequestor(requestId);
            if(null == requestor)
                throw new UnrecognizableException("Requestor must not be null to make an relation");
            switch(type){
                case AddRelation:
                    checkPermissionToAddRelation(actualResponderId, handlerId);
                    result = orientationHandler.addRelation(TripleJType.Account, requestor, actualResponderId);
                    break;
                case JoinGroup:
                    // In this case, actualResponder means groupId
                    checkPermissionToJoinGroup(actualResponderId, handlerId);
                    result = groupHandler.join(actualResponderId, requestor);
                    break;
                default:
                    throw new UnrecognizableException("Unrecognizable type");
            }

            if(result)
                requestHandler.updateStatus(requestId, RequestStatus.ACCEPTED);
            else
                requestHandler.updateStatus(requestId, RequestStatus.NOTIFIED);
        } catch (IncompatibleException ie) {
            assert(false);
        } catch (MissingRequiredException | InvalidIdException | TreeException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (UnrecognizableException e) {
            e.printStackTrace();
        }
    }

    /**
     It actually completes the process that the request indicates by declining it. If the process completes successfully, the status of the request would be changed to RequestStatus.Declined.
     * Otherwise, it would be changed to RequestStatus.NOTIFIED.
     * @param responderId
     * @param requestId
     * @throws ServiceNotAvailableException
     * @throws UserException
     * @throws UnauthorizedException
     * @throws InappropriateStatusException
     */
    public void decline(String responderId, String requestId) throws ServiceNotAvailableException, UserException, UnauthorizedException, InappropriateStatusException {
        try{
            String actualResponder = requestHandler.getResponder(requestId);
            if(null == actualResponder)
                throw new UnrecognizableException("can not find an actual responder");
            if(actualResponder.compareTo(responderId) != 0)
                throw new UnauthorizedException("responderId does not match with actual responder id");

            requestHandler.updateStatus(requestId, RequestStatus.DECLINED);
        } catch (MissingRequiredException | InvalidIdException | UnrecognizableException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    void checkPermissionToJoinGroup(final String groupId, final String handlerId) throws InvalidIdException, StoreException, MissingRequiredException, UnauthorizedException {
        Capacity capacity = groupHandler.getCapacity(groupId, handlerId);
        if(null == capacity)
            throw new UnauthorizedException(handlerId + "has no authorization to handle this request");
        boolean authorization = capacity.canDo(RequestType.JoinGroup);
        if(!authorization)
            throw new UnauthorizedException(handlerId + "has no authorization to handle this request");
    }

    void checkPermissionToAddRelation(final String actualResponderId, final String handlerId) throws UnauthorizedException {
        if(actualResponderId.compareTo(handlerId) != 0)
            throw new UnauthorizedException("responderId does not match with actual responder id");

    }
}
