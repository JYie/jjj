package com.tripleJ.service;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 8. 5..
 */
public class ServiceNotAvailableException extends TripleJException {

    public ServiceNotAvailableException(Exception exception) {
        super(exception);
    }

    public ServiceNotAvailableException(String arg, Exception exception) {
        super(arg, exception);
    }

    public ServiceNotAvailableException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ServiceNotAvailableException(String arg0) {
        super(arg0);
    }

    public ServiceNotAvailableException(Throwable arg0) {
        super(arg0);
    }

    public ServiceNotAvailableException() {
    }
}
