package com.tripleJ.service;

import com.tripleJ.*;
import com.tripleJ.ds.account.AccountHandler;
import com.tripleJ.ds.account.Preference;
import com.tripleJ.ds.group.DefaultGroupConstraint;
import com.tripleJ.ds.relation.TreeException;
import com.tripleJ.security.AccessConstraint;
import com.tripleJ.ds.account.DefaultAccountConstraint;
import com.tripleJ.ds.comment.Comment;
import com.tripleJ.ds.comment.CommentHandler;
import com.tripleJ.ds.comment.Scope;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.relation.OrientationHandler;
import com.tripleJ.unique.InvalidIdException;

/**
 * Created by kami on 2014. 6. 26..
 */
public class CommentAction {

    private final AccountHandler accountHandler;
    private final CommentHandler commentHandler;
    private final OrientationHandler orientationHandler;
    private final GroupHandler groupHandler;

    /**
     *
     * @param accountHandler
     * @param commentHandler
     * @param orientationHandler
     * @param groupHandler
     * @throws InitializeException
     */
    CommentAction(AccountHandler accountHandler, CommentHandler commentHandler, OrientationHandler orientationHandler, GroupHandler groupHandler) throws InitializeException{
        if(null == accountHandler || null == commentHandler || null == orientationHandler || null == groupHandler)
            throw new InitializeException("Every handler must not be null");
        this.accountHandler = accountHandler;
        this.commentHandler = commentHandler;
        this.orientationHandler = orientationHandler;
        this.groupHandler = groupHandler;
    }

    public CommentAction() throws InitializeException {
        this.accountHandler = Provider.getAccountHandler();
        this.commentHandler = Provider.getCommentHandler();
        this.groupHandler = Provider.getGroupHandler();
        this.orientationHandler = Provider.getOrientationHandler();
        if(null == this.accountHandler || null == this.commentHandler || null == this.groupHandler || null == this.orientationHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     * It registers comment which author writes to listener.
     * @param authorId
     * @param listenerId
     * @param comment
     * @return
     * @throws ServiceNotAvailableException
     * @throws UserException
     * @throws UnauthorizedException
     */
    public String addCommentToAccount(final String authorId, final String listenerId, final Comment comment) throws ServiceNotAvailableException, UserException, UnauthorizedException {
        try{
            int relation = orientationHandler.getShortestPathLength(TripleJType.Account, authorId, listenerId);
            Preference listenerPreference = accountHandler.getPreference(listenerId);
            AccessConstraint constraint;
            if(null == listenerPreference)
                constraint = new DefaultAccountConstraint();
            else
                constraint = listenerPreference.getAccessConstraint();

            constraint.checkPermission(AccessConstraint.ConstrainedActions.WriteComment, relation);

            return commentHandler.addComment(authorId, listenerId, comment, Scope.WIDE);
        } catch (UnrecognizableException ue) {
            throw new UnauthorizedException(ue);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     * It registers comment which author writes to the group.
     * @param authorId
     * @param groupId
     * @param comment
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws UnauthorizedException
     * @throws UnrecognizableException
     * @throws InvalidIdException
     */
    public String addCommentToGroup(final String authorId, final String groupId, final Comment comment) throws UnauthorizedException, UserException, ServiceNotAvailableException {

        try{
            Preference groupPreference = groupHandler.getPreference(groupId);
            int relation;
            boolean isMember = groupHandler.isMember(groupId, authorId);
            if(isMember)
                relation = 1;
            else
                relation = -1;

            AccessConstraint constraint;
            if(null == groupPreference)
                constraint = new DefaultGroupConstraint();
            else
                constraint = groupPreference.getAccessConstraint();

            constraint.checkPermission(AccessConstraint.ConstrainedActions.WriteComment, relation);

            return commentHandler.addComment(authorId, groupId, comment, Scope.WIDE);
        }catch (UnrecognizableException ue) {
            throw new UnauthorizedException(ue);
        }catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        }catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     * It sends message from author to listener
     * @param authorId
     * @param listenerId
     * @param comment
     * @return
     * @throws UnauthorizedException
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public String sendMessage(final String authorId, final String listenerId, final Comment comment) throws UnauthorizedException, UserException, ServiceNotAvailableException {
        try{
            int relation = orientationHandler.getShortestPathLength(TripleJType.Account, authorId, listenerId);
            Preference listenerPreference = accountHandler.getPreference(listenerId);
            AccessConstraint constraint;
            if(null == listenerPreference)
                constraint = new DefaultAccountConstraint();
            else
                constraint = listenerPreference.getAccessConstraint();

            constraint.checkPermission(AccessConstraint.ConstrainedActions.SendMessage, relation);

            return commentHandler.addComment(authorId, listenerId, comment, Scope.NARROW);
        }catch (UnrecognizableException ue) {
            throw new UnauthorizedException(ue);
        }catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        }catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }

    }

    /**
     *
     * @param authorId
     * @param commentId
     * @param comment
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws UnauthorizedException
     * @throws UnrecognizableException
     * @throws InvalidIdException
     * @throws TreeException
     * @throws IncompatibleException
     */
    public void addReply(final String authorId, final String commentId, final Comment comment) throws UnauthorizedException, UserException, ServiceNotAvailableException {
        try{
            String listenerId = commentHandler.getAuthor(commentId);
            if(null == listenerId)
                throw new UnrecognizableException("commentId is not recognizable");
            int relation = orientationHandler.getShortestPathLength(TripleJType.Account, authorId, listenerId);
            Preference listenerPreference = accountHandler.getPreference(listenerId);
            AccessConstraint constraint;
            if(null == listenerPreference)
                constraint = new DefaultAccountConstraint();
            else
                constraint = listenerPreference.getAccessConstraint();

            constraint.checkPermission(AccessConstraint.ConstrainedActions.WriteReComment, relation);

            String childCommentId = commentHandler.addComment(authorId, listenerId, comment, Scope.WIDE);
            orientationHandler.addOrientation(TripleJType.Comment, commentId, childCommentId);
        }catch (UnrecognizableException ue) {
            throw new UnauthorizedException(ue);
        }catch (InvalidIdException | MissingRequiredException | TreeException e) {
            throw new UserException(e);
        }catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (IncompatibleException ie) {
            assert(false);
        }

    }

    /**
     * It removes the comment which has comment id by the order of author which has the author id delivered.
     * @param authorId
     * @param commentId
     * @throws UserException
     * @throws ServiceNotAvailableException
     * @throws UnauthorizedException
     */
    public void removeComment(final String authorId, final String commentId) throws UserException, ServiceNotAvailableException, UnauthorizedException {
        try{
            String actualAuthorId = commentHandler.getAuthor(commentId);
            boolean isMine = authorId.compareTo(actualAuthorId) == 0;
            if(isMine)
                commentHandler.removeComment(commentId);
            else{
                throw new UnauthorizedException("Every comment can be removed by the author only");
            }
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }

    }

    /**
     *
     * @param authorId
     * @param commentId
     * @param comment
     * @throws UserException
     * @throws ServiceNotAvailableException
     * @throws UnauthorizedException
     */
    public void updateComment(final String authorId, final String commentId, final Comment comment) throws UserException, ServiceNotAvailableException, UnauthorizedException {
        try{
            String actualAuthorId = commentHandler.getAuthor(commentId);
            boolean isMine = authorId.compareTo(actualAuthorId) == 0;
            if(isMine)
                commentHandler.updateComment(commentId, comment);
            else{
                throw new UnauthorizedException("Every comment can be updated by the author only");
            }
        }  catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }
}
