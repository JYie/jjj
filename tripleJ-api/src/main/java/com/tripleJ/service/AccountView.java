package com.tripleJ.service;

import com.tripleJ.InitializeException;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;
import com.tripleJ.TripleJType;
import com.tripleJ.ds.account.Account;
import com.tripleJ.ds.account.AccountHandler;
import com.tripleJ.ds.account.AccountInformation;
import com.tripleJ.ds.account.Preference;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.group.GroupInformation;
import com.tripleJ.ds.relation.OrientationHandler;
import com.tripleJ.unique.InvalidIdException;

import java.util.List;
import java.util.Set;

/**
 * Created by kami on 2014. 7. 1..
 */
public class AccountView {

    private final AccountHandler accountHandler;
    private final GroupHandler groupHandler;
    private final OrientationHandler orientationHandler;

    AccountView(AccountHandler accountHandler, GroupHandler groupHandler, OrientationHandler orientationHandler) throws InitializeException {
        if (null == accountHandler || null == groupHandler || null == orientationHandler)
            throw new InitializeException("accountHandler, groupHander and orientationHandler must not be null");

        this.accountHandler = accountHandler;
        this.groupHandler = groupHandler;
        this.orientationHandler = orientationHandler;
    }

    public AccountView() throws InitializeException{
        this.accountHandler = Provider.getAccountHandler();
        this.groupHandler = Provider.getGroupHandler();
        this.orientationHandler = Provider.getOrientationHandler();
        if(null == this.accountHandler || null == this.groupHandler || null == this.orientationHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     * It returns the set of accounts which has a relation with the account having accountId delivered.
     * @param accountId
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public Set<AccountInformation> listFriends(final String accountId) throws UserException, ServiceNotAvailableException {
        if(null == accountId || accountId.isEmpty())
            throw new UserException("accountId must not be null");
        try{
            Set<String> relationIds = orientationHandler.getNeighbors(TripleJType.Account, accountId);
            if (null == relationIds || relationIds.size() == 0)
                return null;

            return accountHandler.recoverBySet(relationIds.toArray(new String[relationIds.size()]));
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     * It returns the list of groups which the account having accountId delivered is now joining.
     * @param accountId
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public Set<GroupInformation> listGroups(final String accountId) throws UserException, ServiceNotAvailableException {

        if(null == accountId || accountId.isEmpty())
            throw new UserException("accountId must not be null");
        try {
            Set<String> groupIds = groupHandler.listGroups(accountId);

            if (null == groupIds || groupIds.size() == 0)
                return null;

            return groupHandler.recoverBySet(groupIds.toArray(new String[groupIds.size()]));
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     * It returns the account information belongs to the account having accountId delivered.
     * @param accountId
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public Account recover(final String accountId) throws ServiceNotAvailableException, UserException {
        if(null == accountId || accountId.isEmpty())
            throw new UserException("accountId must not be null");
        try{
            List<AccountInformation> accountinfos = accountHandler.recoverByList(accountId);
            if(null == accountinfos || accountinfos.size() == 0 )
                return null;
            assert(accountinfos.size() == 1);
            AccountInformation accountInfo = accountinfos.get(0);
            assert(accountInfo.getAccountId().compareTo(accountId) == 0);
            return accountInfo.getAccount();
        }  catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     * It returns the preference that the account having accountId delivered has.
     *
     * @param accountId
     * @return
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public Preference getPreference(final String accountId) throws ServiceNotAvailableException, UserException {
        if(null == accountId || accountId.isEmpty())
            throw new UserException("accountId must not be null");
        try {
            return accountHandler.getPreference(accountId);
        }  catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }
}
