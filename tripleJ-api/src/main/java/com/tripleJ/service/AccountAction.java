package com.tripleJ.service;

import com.tripleJ.*;
import com.tripleJ.ds.account.*;
import com.tripleJ.ds.pair.*;
import com.tripleJ.ds.relation.OrientationHandler;
import com.tripleJ.security.AccessConstraint;
import com.tripleJ.unique.InvalidIdException;

/**
 * Created by kami on 2014. 6. 27..
 */
public class    AccountAction {

    private final AccountHandler accountHandler;
    private final OrientationHandler orientationHandler;
    private final RequestHandler requestHandler;

    AccountAction(AccountHandler accountHandler, OrientationHandler orientationHandler, RequestHandler requestHandler) throws InitializeException {
        if(null == accountHandler ||  null == orientationHandler || null == requestHandler)
            throw new InitializeException("accountHandler, orientationHandler and requestHandler must not be null");
        this.accountHandler = accountHandler;
        this.orientationHandler = orientationHandler;
        this.requestHandler = requestHandler;
    }

    public AccountAction() throws InitializeException{
        this.accountHandler = Provider.getAccountHandler();
        this.orientationHandler = Provider.getOrientationHandler();
        this.requestHandler = Provider.getRequestHandler();
        if(null == this.accountHandler || null == this.orientationHandler || null == this.requestHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     * Register new account
     * @param accountId
     * @param account
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     * @throws AlreadyExistException it the accountId delivered already exists in the system
     */
    public String registerAccount(final String accountId, final Account account) throws UserException, ServiceNotAvailableException, AlreadyExistException {
        if(null == accountId || null == account)
            throw new UserException("accountId and account must not be null");

        try{
            return accountHandler.registerAccount(accountId, account);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (MissingRequiredException | InvalidIdException e) {
            throw new UserException(e);
        }
    }

    /**
     * remove the account
     * @param accountId
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public boolean withdrawAccount(final String accountId) throws UserException, ServiceNotAvailableException {
        if(null == accountId)
            throw new UserException("accountId must not be null");

        try{
            return accountHandler.unregisterAccount(accountId);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException | MissingRequiredException ie) {
            throw new UserException(ie);
        }
    }

    /**
     * modifies the account
     * @param accountId
     * @param account
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public boolean modifyAccount(final String accountId, final Account account) throws UserException, ServiceNotAvailableException {
        if(null == accountId || null == account)
            throw new UserException("account and accountId must not be null");

        try{
            return accountHandler.modifyAccount(accountId, account);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        }
    }

    /**
     * It removes the relation between two accounts
     * @param account1
     * @param account2
     * @return
     * @throws ServiceNotAvailableException
     * @throws UserException
     */
    public boolean removeAccountRelation(final String account1, final String account2) throws ServiceNotAvailableException, UserException {
        if(null == account1 || null == account2)
            throw new UserException("account1 and account2 must not be null");

        try{
            return this.orientationHandler.removeRelation(TripleJType.Account, account1, account2);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (IncompatibleException ie) {
            assert(false);
            return false;
        }
    }

    /**
     It registers the request to add a relation between requestor and responder. The request can be completed by calling RequestAction.complete method
     * with parameter request id and responder id.
     * @param requestor
     * @param responder
     * @return
     * @throws UnauthorizedException
     * @throws UnrecognizableException
     * @throws InappropriateStatusException
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public String requestToAddRelation(final String requestor, final String responder) throws UnauthorizedException, UnrecognizableException, InappropriateStatusException, UserException, ServiceNotAvailableException {
        if(null == requestor || null == responder)
            throw new UserException("requestor and responder must not be null");
        try{
            StringBuffer comment = new StringBuffer();
            comment.append(requestor);
            comment.append(" request to make a relation with ");
            comment.append(responder);
            Request requestToRelate = new Request(RequestType.AddRelation, comment.toString());

            int relation = orientationHandler.getShortestPathLength(TripleJType.Account, requestor, responder);
            Preference listenerPreference = accountHandler.getPreference(responder);
            AccessConstraint constraint;
            if(null == listenerPreference) {
                constraint = new DefaultAccountConstraint();
            }
            else
                constraint = listenerPreference.getAccessConstraint();

            constraint.checkPermission(AccessConstraint.ConstrainedActions.SendInvitation, relation);

            String requestId = requestHandler.registerRequest(requestor, responder, requestToRelate);
            requestHandler.updateStatus(requestId, RequestStatus.CREATED);
            return requestId;
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }
}
