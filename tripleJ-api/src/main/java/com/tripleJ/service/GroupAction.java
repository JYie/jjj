package com.tripleJ.service;

import com.tripleJ.InitializeException;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;

import com.tripleJ.ds.account.AlreadyExistException;
import com.tripleJ.ds.group.Group;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.pair.*;
import com.tripleJ.unique.InvalidIdException;

import java.util.Set;

/**
 * Created by kami on 2014. 6. 30..
 */
public class GroupAction {

    private final GroupHandler groupHandler;
    private final RequestHandler requestHandler;

    GroupAction(GroupHandler groupHandler, RequestHandler requestHandler) throws InitializeException {
        if(null == groupHandler || null == requestHandler)
            throw new InitializeException("gruopHandler and requestHandler must not be null");
        this.groupHandler = groupHandler;
        this.requestHandler = requestHandler;
    }

    public GroupAction() throws InitializeException{
        this.groupHandler = Provider.getGroupHandler();
        this.requestHandler = Provider.getRequestHandler();
        if(null == this.groupHandler || null == this.requestHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     * It creates a group and returns a groupId generated. Also it registers creators as members of this group.
     * @param group
     * @return group id
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public String createGroup(String groupId, Group group) throws UserException, ServiceNotAvailableException, AlreadyExistException {
        try{
            this.groupHandler.createGroup(groupId, group);
            Set<String> creators = group.getCreators();
            for(String creator: creators){
                groupHandler.join(groupId, creator);
            }
            return groupId;
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (MissingRequiredException | InvalidIdException e) {
            throw new UserException(e);
        }
    }

    /**
     *
     * @param accountId
     * @param groupId
     * @return
     * @throws InappropriateStatusException
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public String registerRequestToJoin(final String accountId, final String groupId) throws InappropriateStatusException, UserException, ServiceNotAvailableException {
        try{
            StringBuffer comment = new StringBuffer();
            comment.append(accountId);
            comment.append(" request to join group ");
            comment.append(groupId);
            Request requestToJoin = new Request(RequestType.JoinGroup, comment.toString());

            String requestId = requestHandler.registerRequest(accountId, groupId, requestToJoin);
            requestHandler.updateStatus(requestId, RequestStatus.CREATED);

            return requestId;
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     *
     * @param groupId
     * @param accountId
     * @throws ServiceNotAvailableException
     * @throws UserException
     */
    public void dropOut(String groupId, String accountId) throws ServiceNotAvailableException, UserException {
        try{
            this.groupHandler.dropOut(groupId, accountId);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }
}
