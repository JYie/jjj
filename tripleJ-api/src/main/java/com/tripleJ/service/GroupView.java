package com.tripleJ.service;

import com.google.common.collect.Sets;
import com.tripleJ.InitializeException;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;
import com.tripleJ.ds.account.AccountHandler;
import com.tripleJ.ds.account.AccountInformation;
import com.tripleJ.ds.group.Group;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.group.GroupInformation;
import com.tripleJ.unique.InvalidIdException;

import java.util.List;
import java.util.Set;

/**
 * Created by kami on 2014. 8. 5..
 */
public class GroupView {

    private final GroupHandler groupHandler;
    private final AccountHandler accountHandler;

    GroupView(GroupHandler groupHandler, AccountHandler accountHandler) throws InitializeException {
        if(null == groupHandler || null == accountHandler)
            throw new InitializeException("gruopHandler and accountHandler must not be null");
        this.groupHandler = groupHandler;
        this.accountHandler = accountHandler;
    }

    public GroupView() throws InitializeException{
        this.groupHandler = Provider.getGroupHandler();
        this.accountHandler = Provider.getAccountHandler();
        if(null == this.groupHandler || null == this.accountHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     * It returns the set of member who is joining this group.
     * @param groupId
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public Set<AccountInformation> listMembers(String groupId) throws UserException, ServiceNotAvailableException {
        try{
            Set<String> memberIds = groupHandler.getMembers(groupId);
            if(null == memberIds || memberIds.isEmpty())
                return Sets.newHashSet();
            return accountHandler.recoverBySet(memberIds.toArray(new String[memberIds.size()]));
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        }
    }

    /**
     *  It returns the set of group which the account is joining.
     * @param accountId
     * @return
     * @throws ServiceNotAvailableException
     * @throws UserException
     */
    public Set<GroupInformation> listGroups(String accountId) throws ServiceNotAvailableException, UserException {
        try{
            Set<String> groupIds = groupHandler.listGroups(accountId);
            if(null == groupIds || groupIds.isEmpty())
                return Sets.newHashSet();
            return groupHandler.recoverBySet(groupIds.toArray(new String[groupIds.size()]));
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        }
    }

    /**
     * It returns the set of creators of this group. It is possible that there exists a creator who is not joining this group.
     * @param groupId
     * @return
     * @throws ServiceNotAvailableException
     * @throws UserException
     */
    public Set<AccountInformation> listCreators(String groupId) throws ServiceNotAvailableException, UserException {
        try{
            List<GroupInformation> gi = groupHandler.recoverByList(groupId);
            assert(null != gi && gi.size() == 1);
            GroupInformation groupInformation = gi.get(0);
            Group group = groupInformation.getGroup();
            assert(null != group);
            Set<String> creators = group.getCreators();
            if(null == creators || creators.isEmpty())
                return Sets.newHashSet();
            return accountHandler.recoverBySet(creators.toArray(new String[creators.size()]));
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException | MissingRequiredException e) {
            throw new UserException(e);
        }
    }
}
