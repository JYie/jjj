package com.tripleJ.service;

import com.google.common.collect.Lists;
import com.tripleJ.*;
import com.tripleJ.ds.account.AccountHandler;
import com.tripleJ.ds.account.DefaultAccountConstraint;
import com.tripleJ.ds.account.Preference;
import com.tripleJ.ds.comment.CommentHandler;
import com.tripleJ.ds.comment.CommentInformation;
import com.tripleJ.ds.group.GroupHandler;
import com.tripleJ.ds.relation.BrokenImageException;
import com.tripleJ.ds.relation.OrientationHandler;
import com.tripleJ.ds.relation.OrphanException;
import com.tripleJ.ds.relation.RootedTree;
import com.tripleJ.security.AccessConstraint;
import com.tripleJ.unique.InvalidIdException;

import java.util.List;
import java.util.Set;

/**
 *
 * Created by kami on 2014. 6. 30..
 */
public class PostingView {

    private final AccountHandler accountHandler;
    private final CommentHandler commentHandler;
    private final OrientationHandler orientationHandler;
    private final GroupHandler groupHandler;

    PostingView(AccountHandler accountHandler, CommentHandler commentHandler, OrientationHandler orientationHandler) throws InitializeException{
        this(accountHandler, commentHandler, orientationHandler, null);
    }

    protected PostingView(AccountHandler accountHandler, CommentHandler commentHandler, OrientationHandler orientationHandler, GroupHandler groupHandler) throws InitializeException {
        if(null == accountHandler || null == commentHandler || null == orientationHandler)
            throw new InitializeException("every handler must not be null");
        this.accountHandler = accountHandler;
        this.commentHandler = commentHandler;
        this.orientationHandler = orientationHandler;
        this.groupHandler = groupHandler;
    }

    public PostingView() throws InitializeException{
        this.accountHandler = Provider.getAccountHandler();
        this.commentHandler = Provider.getCommentHandler();
        this.orientationHandler = Provider.getOrientationHandler();
        this.groupHandler = Provider.getGroupHandler();
        if(null == this.accountHandler || null == this.commentHandler || null == this.orientationHandler || null == this.groupHandler)
            throw new InitializeException("Provider is not established");
    }

    /**
     It returns the list of tree of comments which the master writes or the relation of the master writes or the comment written on the wall of
     * the group the master belongs to.
     * @param masterId
     * @param offset
     * @param count
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<RootedTree<CommentInformation>> listTimelines(String masterId, int offset, int count) throws ServiceNotAvailableException, UserException {
        List<RootedTree<String>> outLines = listTimeOutlines(masterId, offset, count);
        if(null == outLines || 0 == outLines.size())
            return Lists.newLinkedList();
        else{
            try {
                return transform(outLines);
            } catch (StoreException se) {
                throw new UserException(se);
            } catch (InvalidIdException ie) {
                throw new UserException(ie);
            } catch (MissingRequiredException me) {
                throw new UserException(me);
            }
        }

    }

    /**
     It returns the list of tree of id of comments which the master writes or the relation of the master writes or the comment written on the wall of
     * the group the master belongs to
     * @param masterId
     * @param offset
     * @param count
     * @return
     * @throws UserException
     * @throws ServiceNotAvailableException
     */
    public List<RootedTree<String>> listTimeOutlines(String masterId, int offset, int count) throws UserException, ServiceNotAvailableException {
        if(null == masterId)
            throw new UserException("masterId must not be null");
        if(offset < 0 || count < 0)
            throw new UserException(new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero"));

        if(0 == count)
            return Lists.newLinkedList();

        try{
            int candidateSize = offset + count;
            List<String> commentsMasterWrites = commentHandler.listWideCommentIdsByAuthor(0, candidateSize, masterId);
            if(null == commentsMasterWrites)
                commentsMasterWrites = Lists.newLinkedList();
            List<String> commentsMasterListens = commentHandler.listWideCommentIdsByListener(0, candidateSize, masterId);
            if(null != commentsMasterListens && commentsMasterListens.size() > 0)
                commentsMasterWrites.addAll(commentsMasterListens);

            Set<String> neighbors = orientationHandler.getNeighbors(TripleJType.Account, masterId);
            if(null != neighbors && neighbors.size() > 0){
                List<String> commentsRelationWrites = commentHandler.listWideCommentIdsByAuthor(0, candidateSize, neighbors.toArray(new String[neighbors.size()]));
                if(null != commentsRelationWrites && commentsRelationWrites.size() > 0)
                    commentsMasterWrites.addAll(commentsRelationWrites);
            }

            if(commentsMasterWrites.size() == 0)
                return Lists.newLinkedList();

            boolean continues = true;
            List<RootedTree<String>> result = Lists.newLinkedList();

            while(continues){
                RootedTree<String> extract = extractTree(commentsMasterWrites);
                if(null != extract)
                    result.add(extract);
                if(commentsMasterWrites.size() == 0)
                    continues = false;
            }

            int total = result.size();

            int toIndex = SubListHelper.getToIndex(total, offset, count);
            if(-1 == toIndex)
                return Lists.newLinkedList();
            else
                return result.subList(offset, toIndex);
        }catch (StoreException se){
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException ie) {
            throw new UserException(ie);
        } catch (MissingRequiredException me) {
            throw new UserException(me);
        }
    }
    /**
     * As of now, visitor can read master's comment if the master's access constraint policy allows it.
     * @param visitorId
     * @param masterId
     * @param offset
     * @param count
     * @return
     * @throws MissingRequiredException
     */
    public List<RootedTree<CommentInformation>> listVisitingTimelines(String visitorId, String masterId, int offset, int count) throws ServiceNotAvailableException, UserException {

        List<RootedTree<String>> outlines = listVisitingOutlines(visitorId, masterId, offset, count);
        try{
            return transform(outlines);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (MissingRequiredException me) {
            throw new UserException(me);
        } catch (InvalidIdException ie) {
            throw new UserException(ie);
        }
    }

    /**
     * As of now, visitor can read master's comment if the master's access constraint policy allows it.
     * @param visitorId
     * @param masterId
     * @param offset
     * @param count
     * @return
     * @throws UserException
     */
    public List<RootedTree<String>> listVisitingOutlines(String visitorId, String masterId, int offset, int count) throws UserException, ServiceNotAvailableException {

        if(null == visitorId || null == masterId)
            throw new UserException("visitorId and masterId must not be null");
        if(offset < 0 || count < 0)
            throw new UserException(new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero"));

        if(0 == count)
            return Lists.newLinkedList();

        try{
            int relation = orientationHandler.getShortestPathLength(TripleJType.Account, visitorId, masterId);
            Preference preference = accountHandler.getPreference(masterId);
            AccessConstraint accessConstraint;
            if(null == preference)
                accessConstraint = new DefaultAccountConstraint();
            else
                accessConstraint = preference.getAccessConstraint();

            try{
                accessConstraint.checkPermission(AccessConstraint.ConstrainedActions.ReadComment, relation);
            } catch (UnrecognizableException e) {
                return Lists.newLinkedList();
            } catch (UnauthorizedException e) {
                return Lists.newLinkedList();
            }
            int candidateSize = offset + count;

            List<String> commentsMasterWrites = commentHandler.listWideCommentIdsByAuthor(0, candidateSize, masterId);
            if(null == commentsMasterWrites)
                commentsMasterWrites = Lists.newLinkedList();
            List<String> commentsMasterListens = commentHandler.listWideCommentIdsByListener(0, candidateSize, masterId);
            if(null != commentsMasterListens && commentsMasterListens.size() > 0)
                commentsMasterWrites.addAll(commentsMasterListens);

            boolean continues = true;
            List<RootedTree<String>> result = Lists.newLinkedList();

            while(continues){
                RootedTree<String> extract = extractTree(commentsMasterWrites);
                if(null != extract)
                    result.add(extract);
                if(commentsMasterWrites.size() == 0)
                    continues = false;
            }
            return result;
        } catch (InvalidIdException ie) {
            throw new UserException(ie);
        } catch (MissingRequiredException me) {
            throw new UserException(me);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        }
    }

    /**
     * It is a simple wrapper of recoverByList of CommentHandler.
     * @param commentIds
     * @return
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<CommentInformation> recoverByList(String... commentIds) throws UserException, ServiceNotAvailableException {
        if(null == commentIds || commentIds.length == 0)
            return Lists.newLinkedList();
        try {
            return commentHandler.recoverByList(commentIds);
        } catch (MissingRequiredException me) {
            throw new UserException(me);
        } catch (StoreException se) {
            throw new ServiceNotAvailableException(se);
        } catch (InvalidIdException ie) {
            throw new UserException(ie);
        }
    }

    /**
     *
     * @param resources
     * @return
     * @throws StoreException
     * @throws InvalidIdException
     * @throws OrphanException
     * @throws DuplicateIdException
     */
    RootedTree<String> extractTree(List<String> resources) throws StoreException, InvalidIdException {
        if(null == resources || resources.size() == 0)
            return null;

        String extracted = resources.get(0);

        RootedTree<String> first = generateTree(extracted);
        assert(null != first);
        List<String> treeMembers = first.getMembers();
        assert(null != treeMembers && treeMembers.size() > 0);

        resources.removeAll(treeMembers);
        return first;
    }

    /**
     *
     * @param commentId
     * @return
     * @throws StoreException
     * @throws InvalidIdException
     * @throws DuplicateIdException
     * @throws OrphanException
     */
    RootedTree<String> generateTree(final String commentId) throws StoreException, InvalidIdException{
        try {
            String root = orientationHandler.getRoot(TripleJType.Comment, commentId);
            RootedTree<String> tree = new RootedTree(root);
            boolean needExpands = true;
            while(needExpands) {
                needExpands = expandTree(tree);
            }
            return tree;
        } catch (MissingRequiredException e) {
            assert(false);
            return null;
        } catch (IncompatibleException e) {
            assert(false);
            return null;
        }
    }

    /**
     * It returns true if there are leaves which have children, otherwise it returns false, i.e every leaf has no children.
     * @param tree
     * @return
     * @throws StoreException
     * @throws InvalidIdException
     * @throws MissingRequiredException

     * @throws DuplicateIdException
     */
    boolean expandTree(RootedTree<String> tree) throws StoreException, InvalidIdException, MissingRequiredException {
        assert(null != tree);
        Set<String> currentLeaves = tree.getLeaves();
        boolean result = false;
        for(String currentLeaf:currentLeaves){
            Set<String> newLeaves = orientationHandler.getOutNeighbors(TripleJType.Comment, currentLeaf);

            if(null != newLeaves && newLeaves.size() > 0){
                try{
                    tree.addChildren(currentLeaf, newLeaves.toArray(new String[newLeaves.size()]));
                }catch(OrphanException oe){
                    continue;
                }catch(DuplicateIdException de){
                    continue;
                }
                result = true;
            }
        }
        return result;
    }

    /**
     *
     * @param origin
     * @return
     * @throws StoreException
     * @throws InvalidIdException
     * @throws MissingRequiredException
     */
    List<RootedTree<CommentInformation>> transform(final List<RootedTree<String>> origin) throws StoreException, InvalidIdException, MissingRequiredException{

        List<RootedTree<CommentInformation>> transformed = Lists.newLinkedList();
        if(null == origin || origin.size() == 0)
            return transformed;
        List<String> totalIds = Lists.newLinkedList();
        List<Integer> delimiters = Lists.newLinkedList();
        for(RootedTree<String> idTree : origin){
            List<String> members = idTree.getMembers();
            delimiters.add(members.size());
            totalIds.addAll(members);
        }

        List<CommentInformation> recoveredInfos = commentHandler.recoverByList(totalIds.toArray(new String[totalIds.size()]));
        assert(null != recoveredInfos);
        assert(recoveredInfos.size() == totalIds.size());
        assert(checkRecovery(recoveredInfos, totalIds));

        int current = 0;
        for(int i=0;i<delimiters.size();i++){
            int delimiter = delimiters.get(i);
            if(delimiter > 0){
                List<CommentInformation> elements = recoveredInfos.subList(current, current + delimiter);
                current = current + delimiter;
                try {
                    RootedTree<CommentInformation> rootedTree = new RootedTree(elements, origin.get(i).makeFootprint() );
                    transformed.add(rootedTree);
                } catch (BrokenImageException e) {
                    assert(false);
                }
            }
        }
        return transformed;
    }

    private boolean checkRecovery(List<CommentInformation> recovered, List<String> origin){
        for(int i=0;i<recovered.size();i++){
            CommentInformation commentInformation = recovered.get(i);
            if(commentInformation.getIdentifier().compareTo(origin.get(i)) != 0)
                return false;
        }
        return true;
    }


}
