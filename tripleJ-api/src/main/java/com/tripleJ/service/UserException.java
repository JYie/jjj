package com.tripleJ.service;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 8. 5..
 */
public class UserException extends TripleJException {

    public UserException(Exception exception) {
        super(exception);
    }

    public UserException(String arg, Exception exception) {
        super(arg, exception);
    }

    public UserException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public UserException(String arg0) {
        super(arg0);
    }

    public UserException(Throwable arg0) {
        super(arg0);
    }

    public UserException() {
    }
}
