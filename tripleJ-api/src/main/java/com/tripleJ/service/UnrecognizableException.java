package com.tripleJ.service;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 6. 26..
 */
public class UnrecognizableException extends TripleJException {

    public UnrecognizableException(Exception exception) {
        super(exception);
    }

    public UnrecognizableException(String arg, Exception exception) {
        super(arg, exception);
    }

    public UnrecognizableException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public UnrecognizableException(String arg0) {
        super(arg0);
    }

    public UnrecognizableException(Throwable arg0) {
        super(arg0);
    }

    public UnrecognizableException() {
    }
}
