package com.tripleJ;

/**
 * Created by kami on 2014. 6. 16..
 */
public class StoreException extends TripleJException {
    /**
     *
     */
    private static final long serialVersionUID = -1481814177263703107L;

    public StoreException() {
        super();
    }

    public StoreException(Exception exception) {
        super(exception);
    }

    public StoreException(String arg, Exception exception) {
        super(arg, exception);
    }

    public StoreException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public StoreException(String arg0) {
        super(arg0);
    }

    public StoreException(Throwable arg0) {
        super(arg0);
    }

}
