package com.tripleJ.ds.pair;

import com.google.common.collect.Maps;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.unique.IdGeneratable;

import java.util.Map;

/**
 * Created by kami on 2014. 6. 27..
 */
public class Request implements IdGeneratable{

    private final RequestType requestType;
    private final String comment;

    public Request(RequestType requestType, String comment) throws MissingRequiredException {
        if(null == requestType)
            throw new MissingRequiredException("requestType must be declared");
        this.requestType = requestType;
        this.comment = comment;
    }


    public RequestType getRequestType() {
        return requestType;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public Map<String, String> getResources() {
        Map<String, String> map = Maps.newHashMap();
        map.put("type", requestType.name());
        return map;
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestType=" + requestType +
                ", comment='" + comment + '\'' +
                '}';
    }
}
