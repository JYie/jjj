package com.tripleJ.ds.pair;

import com.tripleJ.MissingRequiredException;
import com.tripleJ.unique.Identifiable;

import java.util.Date;

/**
 * Created by kami on 2014. 7. 1..
 */
public class RequestInformation implements Identifiable{

    private final String requestor;
    private final String responder;
    private final String identifier;
    private final RequestStatus currentStatus;
    private final Request request;
    private final Date lastStatusChanged;

    public RequestInformation(String requestor, String responder, String identifier, RequestStatus currentStatus, Request request, Date lastStatusChanged) throws MissingRequiredException {
        if(null == requestor || null == responder || null == identifier || null == request)
            throw new MissingRequiredException("requestor, responder, identifier and request must not be null");

        this.requestor = requestor;
        this.responder = responder;
        this.identifier = identifier;
        this.currentStatus = currentStatus;
        this.request = request;
        this.lastStatusChanged = lastStatusChanged;
    }

    public String getRequestor() {
        return requestor;
    }

    public String getResponder() {
        return responder;
    }

    @Override
    public String getIdentifier(){
        return identifier;
    }

    public RequestStatus getCurrentStatus() {
        return currentStatus;
    }

    public Request getRequest() {
        return request;
    }

    public Date getLastStatusChanged(){
        return lastStatusChanged;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestInformation that = (RequestInformation) o;

        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;
        if (requestor != null ? !requestor.equals(that.requestor) : that.requestor != null) return false;
        if (responder != null ? !responder.equals(that.responder) : that.responder != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = requestor != null ? requestor.hashCode() : 0;
        result = 31 * result + (responder != null ? responder.hashCode() : 0);
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequestInformation{" +
                "requestor='" + requestor + '\'' +
                ", responder='" + responder + '\'' +
                ", identifier='" + identifier + '\'' +
                ", currentStatus=" + currentStatus +
                ", request=" + request +
                ", lastStatusChanged=" + lastStatusChanged +
                '}';
    }
}
