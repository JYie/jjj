package com.tripleJ.ds.pair;

/**
 * Created by kami on 2014. 6. 27..
 */
public enum RequestStatus {
    CREATED(0), NOTIFIED(1), CANCLED(2), ACCEPTED(3), DECLINED(3);

    private final int value;

    RequestStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
