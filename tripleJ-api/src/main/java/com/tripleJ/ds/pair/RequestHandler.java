package com.tripleJ.ds.pair;

import com.google.common.collect.Lists;
import com.tripleJ.*;
import com.tripleJ.service.UnrecognizableException;
import com.tripleJ.unique.IDGenerator;
import com.tripleJ.unique.InvalidIdException;

import java.util.*;

/**
 * Created by kami on 2014. 6. 27..
 */
public class RequestHandler implements Handler{

    private final RequestStore requestStore;

    public RequestHandler(RequestStore requestStore) throws InitializeException {
        if(null == requestStore)
            throw new InitializeException("RequestStore must not be null");
        this.requestStore = requestStore;
    }

    /**
     *
     * @param requestorId
     * @param responderId
     * @param request
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public String registerRequest(final String requestorId, final String responderId, final Request request) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == requestorId || null == responderId || null == request)
            throw new MissingRequiredException("request, requestId, requestorId and responderId must not be null");
        new IDGenerator(requestorId).checkValidity();
        new IDGenerator(responderId).checkValidity();

        Map<String, String> idResources = request.getResources();
        idResources.put("requestorId", requestorId);
        idResources.put("responderId", responderId);
        long time = Calendar.getInstance().getTimeInMillis();
        idResources.put("time", Long.toHexString(time));

        String requestId = generateID(idResources);
        Calendar current = generateCurrentTime();
        this.requestStore.registerRequest(requestId, requestorId, responderId, request, current.getTime());
        this.requestStore.updateStatus(requestId, RequestStatus.CREATED, current.getTime());
        return requestId;
    }

    /**
     * It cancels the request which has an id delivered.
     * @param requestId id of the request
     * @return result
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws UnrecognizableException
     * @throws InappropriateStatusException
     * @throws InvalidIdException
     */
    public boolean cancelRequest(final String requestId) throws MissingRequiredException, StoreException, UnrecognizableException, InappropriateStatusException, InvalidIdException{
        if(null == requestId)
            throw new MissingRequiredException("requestId must not be null");
        new IDGenerator(requestId).checkValidity();

        if(!requestStore.isExist(requestId))
            return false;

        RequestStatus currentStatus = requestStore.getStatus(requestId);
        if(currentStatus == RequestStatus.CREATED)
            requestStore.updateStatus(requestId, RequestStatus.CANCLED, Calendar.getInstance().getTime());
        else
            throw new InappropriateStatusException("A cancellation is allowed at created status only");
        return true;
    }

    /**
     * It modifies the request which has an id delivered to new request
     * @param requestId id of the request that we want to modify
     * @param request
     * @return result
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InappropriateStatusException
     * @throws InvalidIdException
     */
    public boolean updateRequest(final String requestId, final Request request) throws MissingRequiredException, StoreException, InappropriateStatusException, InvalidIdException{
        if(null == requestId || null == request)
            throw new MissingRequiredException("requestId and request must not be null");
        new IDGenerator(requestId).checkValidity();

        if(!requestStore.isExist(requestId))
            return false;

        RequestStatus currentStatus = requestStore.getStatus(requestId);
        Calendar current = generateCurrentTime();
        if(currentStatus == RequestStatus.CREATED)
            requestStore.updateRequest(requestId, request, current.getTime());
        else
            throw new InappropriateStatusException("An update is allowed at created status only");
        return true;
    }

    /**
     * It returns the current status of the request which has an id delivered.
     * @param requestId id of the request that we want to know the status
     * @return status
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public RequestStatus getStatus(final String requestId) throws  MissingRequiredException, StoreException, InvalidIdException{
        if(null == requestId)
            throw new MissingRequiredException("requestId must not be null");
        new IDGenerator(requestId).checkValidity();
        return requestStore.getStatus(requestId);
    }

    /**
     * It changes the status of the request having requestId delivered to status delivered. But the status of request can not be changed as its value are decreasing.
     * @param requestId
     * @param status
     * @throws InvalidIdException
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public boolean updateStatus(final String requestId, final RequestStatus status) throws InvalidIdException, MissingRequiredException, StoreException, InappropriateStatusException {
        if(null == requestId || null == status)
            throw new MissingRequiredException("requestId and status must not be null");

        new IDGenerator(requestId).checkValidity();

        if(!requestStore.isExist(requestId))
            return false;

        Calendar current = generateCurrentTime();

        RequestStatus currentStatus = requestStore.getStatus(requestId);
        if(null == currentStatus || currentStatus.getValue() < 0)
            throw new InvalidIdException("can not find current status");

        if(status.getValue() < currentStatus.getValue())
            throw new InappropriateStatusException("request can not be changed as its value are decreasing");

        try{
            requestStore.updateStatus(requestId, status, current.getTime());
            return true;
        }catch(StoreException ste){
            return false;
        }
    }

    /**
     * It returns the time when the request having the requestId delivered is accessed recently.
     * @param requestId
     * @return
     * @throws StoreException
     * @throws MissingRequiredException
     */
    public Date getLastAccessed(String requestId) throws StoreException, MissingRequiredException {
        if(null == requestId)
            throw new MissingRequiredException("requestId must not be null");
        return requestStore.getLastAccessed(requestId);
    }

    /**
     * It returns the time the request having the requestId delivered created.
     * @param requestId
     * @return
     * @throws StoreException
     */
    public Date getCreated(String requestId) throws StoreException, MissingRequiredException {
        if(null == requestId)
            throw new MissingRequiredException("requestId must not be null");
        return requestStore.getCreated(requestId);
    }

    /**
     * Basically, it is a simple wrapper of requestStore's recoverByList. It would be better to refer requestStore.
     * @param requestIds array of ids
     * @return list of request information
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<RequestInformation> recoverByList(String... requestIds) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == requestIds || requestIds.length == 0)
            throw new MissingRequiredException("requestId must not be null");

        for(String requestId:requestIds)
            new IDGenerator(requestId).checkValidity();

        return requestStore.recoverByList(requestIds);
    }

    /**
     * Basically, it is a simple wrapper of requestStore's recoverByList. It would be better to refer requestStore.
     * @param requestIds array of ids
     * @return list of request information
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public Set<RequestInformation> recoverBySet(String... requestIds) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == requestIds || requestIds.length == 0)
            throw new MissingRequiredException("requestId must not be null");
        for(String requestId:requestIds)
            new IDGenerator(requestId).checkValidity();
        return requestStore.recoverBySet(requestIds);
    }

    /**
     *
     * @param requestId
     * @return
     * @throws StoreException
     */
    public String getRequestor(String requestId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == requestId)
            throw new MissingRequiredException("requestId must not be null");
        new IDGenerator(requestId).checkValidity();
        return requestStore.getRequestor(requestId);
    }

    /**
     *
     * @param requestId
     * @return
     * @throws StoreException
     */
    public String getResponder(String requestId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == requestId)
            throw new MissingRequiredException("requestId must not be null");
        new IDGenerator(requestId).checkValidity();
        return requestStore.getResponder(requestId);
    }

    /**
     * It returns the list of RequestInformation
     * @param requestorId
     * @param offset
     * @param count
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<RequestInformation> listRequestsByRequestor(final int offset, final int count, final String... requestorId) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == requestorId)
            throw new MissingRequiredException("requestorId must not be null");
        if(offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if(0 == count)
            return Lists.newLinkedList();

        List<String> ids = requestStore.listRequestsByRequestor(offset, count, requestorId);
        if(null == ids || ids.size() == 0)
            return Lists.newLinkedList();

        List<RequestInformation> resultSet = requestStore.recoverByList(ids.toArray(new String[ids.size()]));
        assert(null != resultSet);
        assert(resultSet.size() == ids.size());
        return resultSet;
    }

    /**
     *
     * @param requestorId
     * @return
     * @throws StoreException
     * @throws MissingRequiredException
     */
    public int getSizeOfRequestsByRequestor(String... requestorId) throws StoreException, MissingRequiredException {
        if(null == requestorId)
            throw new MissingRequiredException("requestorId must not be null");

        return requestStore.getSizeOfRequestsByRequestor(requestorId);
    }

    /**
     *
     * @param responderId
     * @param offset
     * @param count
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<RequestInformation> listRequestsByResponder(final int offset, final int count, final String... responderId) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == responderId)
            throw new MissingRequiredException("responderId must not be null");
        if(offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if(0 == count)
            return Lists.newLinkedList();

        List<String> ids = requestStore.listRequestsByResponder(offset, count, responderId);
        if(null == ids || ids.size() == 0)
            return Lists.newLinkedList();

        List<RequestInformation> resultSet = requestStore.recoverByList(ids.toArray(new String[ids.size()]));
        assert(null != resultSet);
        assert(resultSet.size() == ids.size());
        return resultSet;

    }

    /**
     *
     * @param responderId
     * @return
     * @throws StoreException
     * @throws MissingRequiredException
     */
    public int getSizeOfRequestsByResponder(String... responderId) throws StoreException, MissingRequiredException {
        if(null == responderId)
            throw new MissingRequiredException("responderId must not be null");

        return requestStore.getSizeOfRequestsByResponder(responderId);
    }


    @Override
    public String generateID(Map<String, String> resources) {
        return IDGenerator.generateId(TripleJType.Request, resources);
    }
}
