package com.tripleJ.ds.pair;

/**
 * Created by kami on 2014. 6. 27..
 */
public enum RequestType {
    AddRelation, JoinGroup
}
