package com.tripleJ.ds.pair;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 6. 27..
 */
public class InappropriateStatusException extends TripleJException {
    public InappropriateStatusException(Exception exception) {
        super(exception);
    }

    public InappropriateStatusException(String arg, Exception exception) {
        super(arg, exception);
    }

    public InappropriateStatusException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public InappropriateStatusException(String arg0) {
        super(arg0);
    }

    public InappropriateStatusException(Throwable arg0) {
        super(arg0);
    }

    public InappropriateStatusException() {
    }
}
