package com.tripleJ.ds.pair;

import com.tripleJ.MissingRequiredException;
import com.tripleJ.StoreException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * RequestStore is a store interface to handle a life-cycle of request.
 * Created by kami on 2014. 6. 27..
 */
public interface RequestStore {

    /**
     *
     * @param requestId
     * @param requstorId
     * @param responderId
     * @param request
     * @param current
     * @throws StoreException
     */
    void registerRequest(String requestId, String requstorId, String responderId, Request request, Date current) throws StoreException;

    /**
     * This function changes properties of request.
     * @param requestId
     * @param request
     * @throws StoreException
     */
    void updateRequest(String requestId, Request request, Date current) throws StoreException;

    /**
     * This function changes the status of the request.
     * @param requestId
     * @throws StoreException
     */
    void updateStatus(String requestId, RequestStatus status, Date current) throws StoreException;

    /**
     * It returns true if the request having the requestId delivered exists.
     * @param requestId id of request
     * @return true if the request having the requestId delivered exists.
     * @throws StoreException
     */
    boolean isExist(String requestId) throws StoreException;

    /**
     *
     * @param requestId
     * @return
     * @throws StoreException
     */
    RequestStatus getStatus(String requestId) throws StoreException;

    /**
     * It returns the list of requestInformation. It preserves the order of requestId delivered, i.e i th element of the result list must have a requestId which is ith element of the array delivered.
     * @param requestId
     * @return
     * @throws StoreException
     */
    List<RequestInformation> recoverByList(String... requestId) throws StoreException;

    /**
     * It returns the set of requestInformation. The difference of it and recoverBySet is that it does not preserves the order. But it has to remove the duplicated element in the case that the array delivered
     *  has a duplicated one.
     * @param requestId
     * @return
     * @throws StoreException
     */
    Set<RequestInformation> recoverBySet(String... requestId) throws StoreException;

    /**
     *
     * @param requestId
     * @return
     * @throws StoreException
     */
    String getRequestor(String requestId) throws StoreException;

    /**
     *
     * @param requestId
     * @return
     * @throws StoreException
     */
    String getResponder(String requestId) throws StoreException;

    /**
     * It returns the time when the request having the requestId delivered is accessed recently.
     * @param requestId
     * @return
     * @throws StoreException
     */
    Date getLastAccessed(String requestId) throws StoreException;

    /**
     * It returns the time the request having the requestId delivered created.
     * @param requestId
     * @return
     * @throws StoreException
     */
    Date getCreated(String requestId) throws StoreException;

    /**
     *
     * @param requestorIds
     * @return
     * @throws StoreException
     */
    int getSizeOfRequestsByRequestor(String... requestorIds) throws StoreException;

    /**
     *
     * @param requestorId
     * @param offset
     * @param count
     * @return
     * @throws StoreException
     */
    List<String> listRequestsByRequestor(int offset, int count, String... requestorId) throws StoreException;

    /**
     *
     * @param responderIds
     * @return
     * @throws StoreException
     */
    int getSizeOfRequestsByResponder(String... responderIds) throws StoreException;

    /**
     *
     * @param responderId
     * @param offset
     * @param count
     * @return
     * @throws StoreException
     */
    List<String> listRequestsByResponder(int offset, int count, String... responderId) throws StoreException;
}
