package com.tripleJ.ds.account;

import com.google.common.collect.Maps;
import com.tripleJ.security.AccessConstraint;

import java.util.Map;
import java.util.Set;

/**
 * Created by kami on 2014. 6. 18..
 */
public class Preference<T extends AccessConstraint>{

    private final T accessConstraint;

    private Map<String, String> properties;

    public Preference(T accessConstraint){
        this.accessConstraint = accessConstraint;
        this.properties = Maps.newConcurrentMap();
    }

    public T getAccessConstraint(){
        return accessConstraint;
    }

    public void updateProperties(String key, String value){
        properties.put(key,value);
    }

    public Set<String> keys() {
        return properties.keySet();
    }

    public String getProperty(String key){
        if(null == key)
            return null;
        return this.properties.get(key);
    }
}
