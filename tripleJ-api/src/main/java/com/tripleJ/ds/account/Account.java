package com.tripleJ.ds.account;

import com.google.common.collect.Maps;
import com.tripleJ.MissingRequiredException;

import java.util.Map;

/**
 * Created by kami on 2014. 6. 16..
 */
public class Account{

    private final String uid;

    private final String name;

    private final String provider;

    private final String email;

    private final byte[] image;

    private Map<String, String> etc;

    public Account(String uid, String name, String provider, String email, byte[] image) throws MissingRequiredException {
        if(null == uid || uid.isEmpty() || null == name || name.isEmpty())
            throw new MissingRequiredException("uid, name must not be null");
        this.uid = uid;
        this.name = name;
        this.provider = provider;
        this.email = email;
        if(null != image){
            this.image = new byte[image.length];
            for(int i=0;i<image.length;i++)
                this.image[i] = image[i];
        }else
            this.image = null;
        this.etc = Maps.newConcurrentMap();
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getProvider() {
        return provider;
    }

    public String getEmail() {
        return email;
    }

    public void addProperty(String key, String value){
        if(null != key && null != value)
            this.etc.put(key, value);
    }

    public byte[] getImage() {
        return image;
    }

    public Map<String, String> getEtc() {
        return etc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (name != null ? !name.equals(account.name) : account.name != null) return false;
        if (provider != null ? !provider.equals(account.provider) : account.provider != null) return false;
        if (uid != null ? !uid.equals(account.uid) : account.uid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Account{" +
                "uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", provider='" + provider + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
