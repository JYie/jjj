package com.tripleJ.ds.account;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 7. 31..
 */
public class AlreadyExistException extends TripleJException {
    public AlreadyExistException(Exception exception) {
        super(exception);
    }

    public AlreadyExistException(String arg, Exception exception) {
        super(arg, exception);
    }

    public AlreadyExistException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public AlreadyExistException(String arg0) {
        super(arg0);
    }

    public AlreadyExistException(Throwable arg0) {
        super(arg0);
    }

    public AlreadyExistException() {
    }
}
