package com.tripleJ.ds.account;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 6. 19..
 */
public class InvalidPreferenceException extends TripleJException {

    public InvalidPreferenceException(Exception exception) {
        super(exception);
    }

    public InvalidPreferenceException(String arg, Exception exception) {
        super(arg, exception);
    }

    public InvalidPreferenceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public InvalidPreferenceException(String arg0) {
        super(arg0);
    }

    public InvalidPreferenceException(Throwable arg0) {
        super(arg0);
    }

    public InvalidPreferenceException() {
    }
}
