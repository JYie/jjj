package com.tripleJ.ds.account;

import com.google.common.collect.Sets;
import com.tripleJ.UnauthorizedException;
import com.tripleJ.security.AccessConstraint;
import com.tripleJ.service.UnrecognizableException;

import java.util.*;

/**
 * This class is an implementation of AccessConstraint. It referred the access constrains facebook supports.
 *
 * Created by kami on 2014. 6. 27..
 */
public class DefaultAccountConstraint implements AccessConstraint {

    public static enum Keys{
        onlyFriendsCanReadComment, FriendsOfFriendsCanReadComment,
        onlyFriendsCanWriteCommentToMe, FriendsOfFriendsCanWriteReCommentToMe,
        EveryOneCanInviteMe, OnlyFriendsOfFriendsCanInviteMe,
        EveryOneCanSendMessageToMe, OnlyFriendsOfFriendsCanSendMessageToMe
    }

    private static final Set<String> actions;
    private static final Set<String> constraints;

    static{
        constraints = Sets.newHashSet();
        constraints.add(Keys.onlyFriendsCanReadComment.toString());
        constraints.add(Keys.FriendsOfFriendsCanReadComment.toString());

        constraints.add(Keys.onlyFriendsCanWriteCommentToMe.toString());
        constraints.add(Keys.FriendsOfFriendsCanWriteReCommentToMe.toString());

        constraints.add(Keys.EveryOneCanInviteMe.toString());
        constraints.add(Keys.OnlyFriendsOfFriendsCanInviteMe.toString());

        constraints.add(Keys.EveryOneCanSendMessageToMe.toString());
        constraints.add(Keys.OnlyFriendsOfFriendsCanSendMessageToMe.toString());

        actions = Sets.newHashSet();
        actions.add(ConstrainedActions.WriteComment.toString());
        actions.add(ConstrainedActions.WriteReComment.toString());

        actions.add(ConstrainedActions.SendMessage.toString());
        actions.add(ConstrainedActions.SendInvitation.toString());

        actions.add(ConstrainedActions.ReadComment.toString());
    }

    private Set<String> authorizations;

    public DefaultAccountConstraint() {
        this.authorizations = Sets.newHashSet();
        setDefault();
    }

    private void setDefault(){
        this.authorizations.add(Keys.EveryOneCanInviteMe.name()); // DefaultConstrint allows that every one can send invitiation to me.
        this.authorizations.add(Keys.FriendsOfFriendsCanReadComment.name());
        this.authorizations.add(Keys.FriendsOfFriendsCanWriteReCommentToMe.name());
        this.authorizations.add(Keys.OnlyFriendsOfFriendsCanSendMessageToMe.name());
    }

    public void init(){
        this.authorizations = Sets.newHashSet();
    }

    @Override
    public void giveAuthorization(String key) throws UnrecognizableException {
        if(null == key)
            throw new UnrecognizableException("key is not recognizable");
        isRecognizable(key);
        if(!authorizations.contains(key))
            authorizations.add(key);
    }

    @Override
    public void removeAuthorization(String key) throws UnrecognizableException {
        if(null == key)
            throw new UnrecognizableException("key is not recognizable");
        isRecognizable(key);
        if(authorizations.contains(key))
            authorizations.remove(key);
    }

    @Override
    public void checkPermission(AccessConstraint.ConstrainedActions action, int relation) throws UnrecognizableException, UnauthorizedException {
        if(null == action)
            throw new UnrecognizableException("key is not recognizable");

        String reason;

        switch (action){
            case ReadComment:
                reason = checkReadComment(relation);
                break;
            case WriteComment:
                reason = checkWriteComment(relation);
                break;
            case WriteReComment:
                reason = checkWriteReComment(relation);
                break;
            case SendMessage:
                reason = checkSendMessage(relation);
                break;
            case SendInvitation:
                reason = checkSendInvitation(relation);
                break;
            default:
                throw new UnrecognizableException("Unknown action");
        }

        if(null != reason && !reason.isEmpty())
            throw new UnauthorizedException(reason);
    }

    void isRecognizable(String key) throws UnrecognizableException{
        if(!constraints.contains(key))
            throw new UnrecognizableException("Unrecognized Key");
    }

    String checkReadComment(int relation){
        if(relation < 0)
            return "read comment is not allowed to an unknown account";
        if(relation == 1)
            return null;

        boolean discriminant1 = this.authorizations.contains(Keys.FriendsOfFriendsCanReadComment.toString());
        if(!discriminant1)
            return "only friend can read my comment";
        if(relation > 2)
            return "relation is too far to read a comment";
        return null;
    }

    String checkWriteComment(int relation){
        if(relation < 0)
            return "write comment is not allowed to an unknown account";
        if(relation == 1)
            return null;
        else
            return "Only Friend can write comment to me";
    }

    String checkWriteReComment(int relation){
        if(relation < 0)
            return "write re-comment is not allowed to an unknown account";
        if(relation == 1)
            return null;

        boolean discriminant1 = this.authorizations.contains(Keys.FriendsOfFriendsCanWriteReCommentToMe.toString());
        if(!discriminant1)
            return "only friend can write re-comment to me";
        if(relation > 2)
            return "relation is too far to give a re-comment";
        return null;
    }

    String checkSendMessage(int relation){
        if(relation == 1)
            return null;

        boolean discriminant1 = this.authorizations.contains(Keys.EveryOneCanSendMessageToMe.toString());
        if(discriminant1)
            return null;
        else
            return "only friend can send message to me";
    }

    String checkSendInvitation(int relation){
        if(relation == 1)
            return null;

        boolean discriminant1 = this.authorizations.contains(Keys.EveryOneCanInviteMe.toString());
        if(discriminant1)
            return null;
        if(relation > 2)
            return "relation is too far to send an invitation";
        return null;
    }
}