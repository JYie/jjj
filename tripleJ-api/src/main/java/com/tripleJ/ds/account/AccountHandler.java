package com.tripleJ.ds.account;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tripleJ.*;
import com.tripleJ.unique.IDGenerator;
import com.tripleJ.unique.InvalidIdException;
import com.tripleJ.unique.UIDPolicy;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by kami on 2014. 6. 26..
 */
public class AccountHandler implements Handler {

    private static final String defaultProvider = "TripleJ";
    private static final String Provider = "Provider";
    private static final String UID = "UID";

    private final AccountStore accountStore;
    private final PreferenceStore preferenceStore;

    public AccountHandler(AccountStore accountStore,PreferenceStore preferenceStore) throws InitializeException {
        if(null == accountStore)
            throw new InitializeException("accountStore must not be null");
        this.accountStore = accountStore;
        this.preferenceStore = preferenceStore;
    }

    /**
     * It registers new account.
     * @param accountId accountID is a primary id to distinguish account
     * @param account account
     * @return uid of generated user.
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public String registerAccount(final String accountId, final Account account) throws MissingRequiredException, StoreException, AlreadyExistException, InvalidIdException {
        if(null == accountId || null == account) {
            throw new MissingRequiredException("accountId and account must not be null");
        }

        if(!UIDPolicy.canUse(accountId))
            throw new InvalidIdException(accountId + "can not be used as a accountId");

//        Map<String, String> idResources = Maps.newHashMap(); //
//        String provider = account.getProvider();
//        if(null != provider)
//            idResources.put(Provider, provider);
//        else
//            idResources.put(Provider, defaultProvider);
//
//        idResources.put(UID, account.getUid());
//
//        String accountId = generateID(idResources);
        boolean isRegistered = accountStore.isRegisteredId(accountId);
        if(isRegistered)
            throw new AlreadyExistException(accountId + "is already registered");
        this.accountStore.registerAccount(accountId, account);
        return accountId;
    }

    /**
     * It removes an existing account.
     * @param accountId id of an account will be unregistered.
     * @return true if it succeeds, false if accountId does not exist
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public boolean unregisterAccount(final String accountId) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == accountId)
            throw new MissingRequiredException("accountID must not be null");
        new IDGenerator(accountId).checkValidity();

        if(!this.accountStore.isRegisteredId(accountId))
            return false;
        this.accountStore.unregisterAccount(accountId);
        return true;
    }

    /**
     * It modifies properties of existing account.
     * @param accountId id of an account will be modified.
     * @param account has properties will be reflected.
     * @return true it modification succeeds
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public boolean modifyAccount(final String accountId, final Account account) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == accountId || null == account)
            throw new MissingRequiredException("account and accountId must not be null");
        new IDGenerator(accountId).checkValidity();

        if(!this.accountStore.isRegisteredId(accountId))
            return false;
        this.accountStore.modifyAccount(accountId, account);
        return true;

    }

    /**
     * It returns true if there is an account which has an id delivered.
     * @param accountId is an id which want to know to be existed
     * @return true if there is an account which has an id delivered.
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public boolean isRegisteredAccount(String accountId) throws StoreException, MissingRequiredException, InvalidIdException {
        if(null == accountId)
            throw new MissingRequiredException("accountId must not be null");
        new IDGenerator(accountId).checkValidity();

        this.accountStore.isRegisteredId(accountId);
        return true;

    }

    /**
     * Basically, it is a simple wrapper of accountStore's recoverByList. It would be better to refer accountStore.
     * @param accountIds an array of ids of account which want to be recovered.
     * @return List of account information
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<AccountInformation> recoverByList(final String... accountIds) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == accountIds)
            throw new MissingRequiredException("accountId must not be null");
        if(accountIds.length == 0)
            return Lists.newLinkedList();

        for(String accountId:accountIds)
            new IDGenerator(accountId).checkValidity();

        return this.accountStore.recoverByList(accountIds);
    }

    /**
     * Basically, it is a simple wrapper of accountStore's recoverBySet. It would be better to refer accountStore.
     * @param accountIds array of id will be recovered
     * @return Set of account information
     * @throws StoreException
     */
    public Set<AccountInformation> recoverBySet(final String... accountIds) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == accountIds)
            throw new MissingRequiredException("accountId must not be null");
        if(accountIds.length == 0)
            return Sets.newHashSet();

        for(String accountId:accountIds)
            new IDGenerator(accountId).checkValidity();

        return this.accountStore.recoverBySet(accountIds);
    }


    /**
     * It returns the preference of the user having an accountId as an uid
     * @param accountId id of an account
     * @return preference
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public Preference getPreference(final String accountId) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == accountId)
            throw new MissingRequiredException("accountId must not be null");
        new IDGenerator(accountId).checkValidity();
        if(null == preferenceStore)
            return new Preference(new DefaultAccountConstraint());
        return preferenceStore.getPreference(accountId);
    }

    /**
     * It updates the preference
     * @param accountId id of an account
     * @param preference preference
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public boolean updatePreference(final String accountId, final Preference preference) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == accountId || null == preference)
            throw new MissingRequiredException("accoundId and preference must not be null");
        new IDGenerator(accountId).checkValidity();
        if(null == preferenceStore)
            return false;
        preferenceStore.updatePreference(accountId,preference);
        return true;
    }

    @Override
    public String generateID(Map<String, String> resources) {
        StringBuffer sb = new StringBuffer();
        sb.append(resources.get(Provider));
        sb.append(":");
        sb.append(resources.get(UID));
        return sb.toString();
    }
}
