package com.tripleJ.ds.account;

import com.tripleJ.StoreException;

import java.util.List;
import java.util.Set;

/**
 * AccountStore is a store interface to handle a life-cycle of account.
 * Created by kami on 2014. 6. 16..
 */
public interface AccountStore {

    /**
     * It saves an account with accountId.
     * @param accountId id of account
     * @param account account
     * @throws StoreException
     */
    void registerAccount(String accountId, Account account) throws StoreException;

    /**
     * It deletes an account which has an id delivered.
     * @param accountId id of account
     * @throws StoreException
     */
    void unregisterAccount(String accountId) throws StoreException;

    /**
     * It modifies properties of account which has an id delivered.
     * @param accountId id of account
     * @param account has new properties
     * @throws StoreException
     */
    void modifyAccount(String accountId, Account account) throws StoreException;

    /**
     * It returns true if the account exists having id delivered.
     * @param accountId id of account
     * @return true if the account exists having id delivered.
     * @throws StoreException
     */
    boolean isRegisteredId(String accountId) throws StoreException;

    /**
     * It returns the list of Account which has an uid belongs to accountIds delivered as an accountId.
     * The size of result must be equal to size of array delivered and it also has to preserve the order. To satisfy it, the result list can have null.
     * @param accountIds array of id will be recovered
     * @return List of account information
     * @throws StoreException
     */
    List<AccountInformation> recoverByList(String... accountIds) throws StoreException;

    /**
     * It returns the list of Account which has an uid belongs to accountIds delivered as an accountId
     * @param accountIds array of id will be recovered
     * @return Set of account information
     * @throws StoreException
     */
    Set<AccountInformation> recoverBySet(String... accountIds) throws StoreException;
}
