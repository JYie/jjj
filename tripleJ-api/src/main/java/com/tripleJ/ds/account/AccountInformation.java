package com.tripleJ.ds.account;

/**
 * Created by kami on 2014. 7. 16..
 */
public class AccountInformation {

    private final String accountId;
    private final Account account;

    public AccountInformation(String accountId, Account account) {
        this.accountId = accountId;
        this.account = account;
    }

    public String getAccountId() {
        return accountId;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountInformation that = (AccountInformation) o;

        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = accountId != null ? accountId.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AccountInformation{" +
                "accountId='" + accountId + '\'' +
                ", account=" + account +
                '}';
    }
}
