package com.tripleJ.ds.account;


import com.tripleJ.StoreException;

/**
 * PreferenceStore is a store interface to handle a life-cycle of preference. It is used to define a personal something which can influence some other business logic.
 * So each account can have his own preference.
 * Created by kami on 2014. 6. 18..
 */
public interface PreferenceStore {

    Preference getPreference(String accountId) throws StoreException;

    void updatePreference(String accountId, Preference preference) throws StoreException;

}
