package com.tripleJ.ds.group;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tripleJ.unique.IdGeneratable;

import java.util.Map;
import java.util.Set;

/**
 * Created by kami on 2014. 6. 18..
 */
public class Group{

    private static final String GROUPNAME = "groupname";

    private Map<String, String> basicResources;

    private Set<String> creators;

    private final String name;

    private final String introduction;

    private final byte[] image;

    private Map<String, String> properties;

    public Group(String creator, String name, String introduction, byte[] image) {
        this.name = name;
        this.introduction = introduction;
        this.creators = Sets.newHashSet();
        if(null != image){
            this.image = new byte[image.length];
            for(int i=0;i<image.length;i++)
                this.image[i] = image[i];
        }else
            this.image = null;
        if(null != creator)
            this.creators.add(creator);
        this.basicResources = Maps.newHashMap();
        this.basicResources.put(GROUPNAME, name);
    }

    public Set<String> getCreators(){
        return creators;
    }

    public String getName() {
        return name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public byte[] getImage() {
        return image;
    }

    public void putCreator(String creator){
        this.creators.add(creator);
    }

    public void initCreator(String creator){
        this.creators = Sets.newHashSet();
        this.creators.add(creator);
    }

    public void addProperty(String key, String value){
        if(null != key && null != value)
            this.properties.put(key, value);
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (name != null ? !name.equals(group.name) : group.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Group{" +
                "creators=" + creators +
                ", name='" + name + '\'' +
                ", introduction='" + introduction + '\'' +
                '}';
    }
}
