package com.tripleJ.ds.group;

import com.tripleJ.StoreException;

import java.util.List;
import java.util.Set;

/**
 * GroupStore is a store interface to handle a life-cycle of group.
 * Created by kami on 2014. 6. 18..
 */
public interface GroupStore {

    /**
     * It stores the group in the store.
     * @param groupId id of the group
     * @param group context of the group
     * @throws StoreException
     */
    void createGroup(String groupId, Group group) throws StoreException;

    /**
     * It removes the group which has an id delivered in the store.
     * @param groupId
     * @throws StoreException
     */
    void removeGroup(String groupId) throws StoreException;

    /**
     * It modifies the context of the group which has an id delivered.
     * @param groupId
     * @param group
     * @throws StoreException
     */
    void updateGroup(String groupId, Group group) throws StoreException;

    /**
     * It stores the relation information between account and group.
     * @param groupId id of the group
     * @param accountId id of the account
     * @throws StoreException
     */
    void join(String groupId, String accountId) throws StoreException;

    /**
     * It removes the relation information between acount and group.
     * @param groupId id of the group
     * @param accountId id of the account
     * @throws StoreException
     */
    void dropOut(String groupId, String accountId) throws StoreException;

    /**
     * It returns true if the group which has an id delivered existed in the store.
     * @param groupId id of the group
     * @return true if the group which has an id delivered existed in the store.
     * @throws StoreException
     */
    boolean isExist(String groupId) throws StoreException;

    /**
     * It returns true if the group having an id delivered and account having an id delivered have related by the join method.
     * @param groupId id of the group
     * @param accountId id of the account
     * @return true if the group having an id delivered and account having an id delivered have related by the join method.
     * @throws StoreException
     */
    boolean isMember(String groupId, String accountId) throws StoreException;

    /**
     * It returns the set of id of account who joined the group having an id delivered.
     * @param groupId id of the group
     * @return set of the id of account
     * @throws StoreException
     */
    Set<String> getMembers(String groupId) throws StoreException;

    /**
     *It returns the set of id of group which the account having id delivered joined.
     * @param accountId id of the account
     * @return set of id of group
     * @throws StoreException
     */
    Set<String> getGroups(String accountId) throws StoreException;

    /**
     * It recovers the list of the group information which has an id belongs to groupIds delivered. It has to preserve the size and the order of the parameter.
     * Hence the result of this function can include null and duplicated information.
     * @param groupIds array of id of group
     * @return list of group information
     * @throws StoreException
     */
    List<GroupInformation> recoverByList(String... groupIds) throws StoreException;

    /**
     * It recovers the set of the group information which has an id belongs to groupIds delivered. It does not preserve the size and the order of the parameter.
     * Hence it does not include null and duplicated information.
     * @param groupIds
     * @return
     * @throws StoreException
     */
    Set<GroupInformation> recoverBySet(String... groupIds) throws StoreException;
}
