package com.tripleJ.ds.group;

import com.tripleJ.StoreException;
import com.tripleJ.ds.pair.RequestType;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by kami on 2014. 8. 6..
 */
public class DefaultCapacityStore implements CapacityStore {

    private GroupStore groupStore;

    DefaultCapacityStore(GroupStore groupStore){
        this.groupStore = groupStore;
    }

    @Override
    public String updateCapacity(String groupId, String accountId, Capacity capacity) throws StoreException {
        return null;
    }

    @Override
    public Capacity getCapacity(String groupId, String accountId) throws StoreException {
        Set<GroupInformation> gis = groupStore.recoverBySet(groupId);
        if(null == gis || gis.isEmpty())
            return null;

        Iterator<GroupInformation> iterator = gis.iterator();
        GroupInformation groupInformation = iterator.next();
        Group group = groupInformation.getGroup();
        Set<String> creators = group.getCreators();
        if(null == creators || creators.isEmpty())
            return new MemberCapacity();
        if(creators.contains(accountId))
            return new OwnerCapacity();

        Set<String> members = groupStore.getMembers(groupId);
        if(null == members || members.isEmpty())
            return new GuestCapacity();

        if(members.contains(accountId))
            return new MemberCapacity();
        else
            return new GuestCapacity();
    }

    public static class OwnerCapacity implements Capacity{

        @Override
        public boolean canDo(RequestType actionId) {
            return true;
        }
    }

    public static class MemberCapacity implements Capacity{

        @Override
        public boolean canDo(RequestType actionId) {
            return false;
        }
    }

    public static class GuestCapacity implements Capacity{

        @Override
        public boolean canDo(RequestType actionId) {
            return false;
        }
    }
}
