package com.tripleJ.ds.group;

import com.tripleJ.StoreException;


/**
 * CapacityStore is a store interface to handle a life-cycle of capacity. Capacity is used to define a kind of authority of account in a group.
 * The difference from the preference is that an account can have a capacity by joining a group. So a capacity can not simply belong to a account.
 * Created by kami on 2014. 6. 20..
 */
public interface CapacityStore {

    public String updateCapacity(String groupId, String accountId, Capacity capacity) throws StoreException;

    public Capacity getCapacity(String groupId, String accountId) throws StoreException;

}
