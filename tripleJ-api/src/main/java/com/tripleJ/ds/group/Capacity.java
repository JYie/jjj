package com.tripleJ.ds.group;

import com.tripleJ.ds.pair.RequestType;

/**
 * Created by kami on 2014. 6. 20..
 */
public interface Capacity {
    public boolean canDo(RequestType actionId);
}
