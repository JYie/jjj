package com.tripleJ.ds.group;

import com.tripleJ.*;
import com.tripleJ.ds.account.AlreadyExistException;
import com.tripleJ.ds.account.Preference;
import com.tripleJ.ds.account.PreferenceStore;
import com.tripleJ.unique.IDGenerator;
import com.tripleJ.unique.InvalidIdException;
import com.tripleJ.unique.UIDPolicy;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by kami on 2014. 6. 23..
 */
public class GroupHandler {

    private final GroupStore groupStore;
    private final PreferenceStore preferenceStore;
    private final CapacityStore capacityStore;

    public GroupHandler(GroupStore groupStore, PreferenceStore preferenceStore, CapacityStore capacityStore) throws InitializeException {
        if(null == groupStore)
            throw new InitializeException("groupStore and preferenceStore must not be null");
        this.groupStore = groupStore;
        this.preferenceStore = preferenceStore;
        if(null == capacityStore) {
            this.capacityStore = new DefaultCapacityStore(groupStore);
        }
        else
            this.capacityStore = capacityStore;

//        if(null == policyEngine) {
//            this.policyEngine = (operation, subjectId, groupId) -> {
//                if(null == subjectId || null == groupId)
//                    return new PermissionResult(false, "PolyEngine or SubjectId is null");
//                try{
//                    Group group = groupStore.recoverByList(groupId);
//                    Set<String> creators = group.getCreators();
//                    boolean result =  creators.contains(subjectId);
//                    if(result)
//                        return new PermissionResult(true,null);
//                    else
//                        return new PermissionResult(false, "subjectId is not included in a creator's set");
//                }catch (StoreException se){
//                    return new PermissionResult(false, "group store does not work");
//                }
//            };
//        }else
//            this.policyEngine = policyEngine;
    }

    /**
     * An User having subjectId as an uid generates a group. Automatically, the user would be a unique master of this group.
     * @param group
     * @return groupId
     * @throws StoreException
     */
    public String createGroup(final String groupId, final Group group) throws MissingRequiredException, StoreException, AlreadyExistException, InvalidIdException {
        if(null == groupId || null == group)
            throw new MissingRequiredException("group must not be null");
        Set<String> creators = group.getCreators();
        if(null == creators || creators.isEmpty())
            throw new MissingRequiredException("at least one creator must be needed");

        if(!UIDPolicy.canUse(groupId))
            throw new InvalidIdException(groupId + "can not be used as a groupID");
        boolean isExisted = groupStore.isExist(groupId);
        if(isExisted)
            throw new AlreadyExistException(groupId + "is already existed");

        groupStore.createGroup(groupId, group);
        for(String creator:creators){
            groupStore.join(groupId, creator);
        }
        return groupId;
    }

    public boolean removeGroup(final String groupId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupId)
            throw new MissingRequiredException("subjectID and groupId must not be null");

        new IDGenerator(groupId).checkValidity();
//        PermissionResult pr = this.policyEngine.checkPermssion(GroupPolicyEngine.Operation.REMOVE, subjectId,groupId);
//        boolean permission = pr.hasPermission();
//        if(permission)
        if(!groupStore.isExist(groupId))
            return false;

        groupStore.removeGroup(groupId);
        return true;
//        else{
//            throw new UnauthorizedException(pr.getReason());
//        }
    }

    public boolean updateGroup(final String groupId, final Group group) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupId)
            throw new MissingRequiredException("groupId must not be null");

        new IDGenerator(groupId).checkValidity();
//        PermissionResult pr = this.policyEngine.checkPermssion(GroupPolicyEngine.Operation.UPDATE, subjectId,groupId);
//        boolean permission = pr.hasPermission();
//        if(permission)

        if(!groupStore.isExist(groupId))
            return false;

        groupStore.updateGroup(groupId, group);
        return true;
//        else{
//            throw new UnauthorizedException(pr.getReason());
//        }
    }

    public boolean join(final String groupId, final String accountId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupId || null == accountId)
            throw new MissingRequiredException("groupId and accountId must not be null");
        new IDGenerator(groupId).checkValidity();
//        PermissionResult pr = this.policyEngine.checkPermssion(GroupPolicyEngine.Operation.JOIN, subjectId,groupId);
//        boolean permission = pr.hasPermission();
//        if(permission)
          groupStore.join(groupId, accountId);
          return true;
//        else{
//            throw new UnauthorizedException(pr.getReason());
//        }
    }

    public boolean dropOut(final String groupId, final String accountId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupId || null == accountId)
            throw new MissingRequiredException("groupId and accountId must not be null");
        new IDGenerator(groupId).checkValidity();
        new IDGenerator(groupId).checkValidity();
//        PermissionResult pr = this.policyEngine.checkPermssion(GroupPolicyEngine.Operation.DROPOUT, subjectId,groupId);
//        boolean permission = pr.hasPermission();
//        if(permission)
            groupStore.dropOut(groupId, accountId);
        return true;
//        else{
//            throw new UnauthorizedException(pr.getReason());
//        }
    }

    public boolean isMember(final String groupId, final String accountId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupId || null == accountId)
            throw new MissingRequiredException("groupId and accountId must not be null");
        new IDGenerator(groupId).checkValidity();
        new IDGenerator(groupId).checkValidity();
        return groupStore.isMember(groupId, accountId);
    }

    public Set<String> getMembers(final String groupId) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupId)
            throw new MissingRequiredException("groupId must not be null");
        new IDGenerator(groupId).checkValidity();
//        PermissionResult pr = this.policyEngine.checkPermssion(GroupPolicyEngine.Operation.LIST, subjectId,groupId);
//        boolean permission = pr.hasPermission();
//        if(permission)
            return groupStore.getMembers(groupId);
//        else{
//            throw new UnauthorizedException(pr.getReason());
//        }
    }

    public int getSize(final String groupId) throws StoreException, MissingRequiredException, UnauthorizedException, InvalidIdException{
        if(null == groupId)
            throw new MissingRequiredException("groupId must not be null");
        new IDGenerator(groupId).checkValidity();
        if(!groupStore.isExist(groupId))
            return 0;

        Set<String> members = getMembers(groupId);
        if(null == members)
            return 0;
        else
            return members.size();
    }

    /**
     * Basically, it is a simple wrapper of groupStore's recoverByList. It would be better to refer groupStore.
     * @param groupIds
     * @return
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public List<GroupInformation> recoverByList(final String... groupIds) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupIds || groupIds.length == 0)
            throw new MissingRequiredException("groupIds must not be null");
        for(String groupId:groupIds)
            new IDGenerator(groupId).checkValidity();

        return groupStore.recoverByList(groupIds);
    }

    /**
     * Basically, it is a simple wrapper of groupStore's recoverBySet. It would be better to refer groupStore.
     * @param groupIds
     * @return
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public Set<GroupInformation> recoverBySet(final String... groupIds) throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == groupIds || groupIds.length == 0)
            throw new MissingRequiredException("groupIds must not be null");
        for(String groupId:groupIds)
            new IDGenerator(groupId).checkValidity();

            return groupStore.recoverBySet(groupIds);
    }

    public Set<String> listGroups(final String accountId)  throws StoreException, MissingRequiredException, InvalidIdException{
        if(null == accountId)
            throw new MissingRequiredException("accountId must not be null");
        new IDGenerator(accountId).checkValidity();
        return groupStore.getGroups(accountId);
    }

    /**
     * It returns the preference of the group having an groupId as an uid
     * @param groupId
     * @return
     */
    public Preference getPreference(final String groupId) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == groupId)
            throw new MissingRequiredException("accountId must not be null");
        new IDGenerator(groupId).checkValidity();
        if(null == this.preferenceStore)
            return null;
        return preferenceStore.getPreference(groupId);
    }

    /**
     * It updates the preference
     * @param groupId
     * @param preference
     */
    public boolean updatePreference(final String groupId, final Preference preference) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == groupId || null == preference)
            throw new MissingRequiredException("accoundId and preference must not be null");
        new IDGenerator(groupId).checkValidity();

        if(null == this.preferenceStore)
            return false;
        preferenceStore.updatePreference(groupId,preference);
        return true;
    }

    /**
     *
     * @param groupId
     * @param accountId
     * @return
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public Capacity getCapacity(final String groupId, final String accountId) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == groupId || null == accountId)
            throw new MissingRequiredException("groupId and accountId must not be null");
        new IDGenerator(groupId).checkValidity();
        new IDGenerator(accountId).checkValidity();

        return capacityStore.getCapacity(groupId, accountId);
    }

    /**
     *
     * @param groupId
     * @param accountId
     * @param capacity
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public void updateCapacity(final String groupId, final String accountId, final Capacity capacity) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == groupId || null == accountId)
            throw new MissingRequiredException("groupId and accountId must not be null");
        new IDGenerator(groupId).checkValidity();
        new IDGenerator(accountId).checkValidity();

        capacityStore.updateCapacity(groupId, accountId, capacity);
    }
}
