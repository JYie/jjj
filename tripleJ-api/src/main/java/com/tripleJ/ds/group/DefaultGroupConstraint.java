package com.tripleJ.ds.group;

import com.tripleJ.UnauthorizedException;
import com.tripleJ.security.AccessConstraint;
import com.tripleJ.service.UnrecognizableException;

/**
 * Created by kami on 2014. 6. 30..
 */
public class DefaultGroupConstraint implements AccessConstraint {

    @Override
    public void giveAuthorization(String key) throws UnrecognizableException {

    }

    @Override
    public void removeAuthorization(String key) throws UnrecognizableException {

    }

    @Override
    public void checkPermission(ConstrainedActions action, int relation) throws UnrecognizableException, UnauthorizedException {

    }


}
