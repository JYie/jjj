package com.tripleJ.ds.group;

import com.tripleJ.unique.Identifiable;

/**
 * Created by kami on 2014. 7. 21..
 */
public class GroupInformation implements Identifiable {

    private final String identifier;
    private final Group group;

    public GroupInformation(String identifier, Group group) {
        this.identifier = identifier;
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupInformation that = (GroupInformation) o;

        if (group != null ? !group.equals(that.group) : that.group != null) return false;
        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = identifier != null ? identifier.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GroupInformation{" +
                "identifier='" + identifier + '\'' +
                ", group=" + group +
                '}';
    }
}
