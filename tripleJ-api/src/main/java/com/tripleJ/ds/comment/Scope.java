package com.tripleJ.ds.comment;

/**
 * Created by kami on 2014. 6. 27..
 */
public enum Scope {
    NARROW, WIDE;

    public static Scope getDefault(){
        return WIDE;
    }
}
