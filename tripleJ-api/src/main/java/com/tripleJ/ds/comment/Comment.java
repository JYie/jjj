package com.tripleJ.ds.comment;

import com.google.common.collect.Maps;
import com.tripleJ.Location;
import com.tripleJ.unique.IdGeneratable;
import com.tripleJ.unique.Identifiable;

import java.util.Map;

/**
 *
 */
public class Comment implements IdGeneratable{

    private final String content;

    private final byte[] image;

    private final Location location;

    private final Map<String, String> resources;

    public Comment(String content){
        this(content, null, null);
    }

    public Comment(String content, byte[] image, Location location) {
        this.content = content;
        if(null != image){
            this.image = new byte[image.length];
            for(int i=0;i<image.length;i++)
                this.image[i] = image[i];
        }else
            this.image = null;
        this.location = location;
        this.resources = Maps.newHashMap();
    }

    public String getContent() {
        return content;
    }

    public byte[] getImage() {
        return image;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "content='" + content + '\'' +
                ", location=" + location +
                '}';
    }

    @Override
    public Map<String, String> getResources() {
        return this.resources;
    }

}
