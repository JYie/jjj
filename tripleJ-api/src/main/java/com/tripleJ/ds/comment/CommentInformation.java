package com.tripleJ.ds.comment;

import com.tripleJ.unique.Identifiable;

/**
 * Created by kami on 2014. 6. 18..
 */
public class CommentInformation implements Identifiable, Comparable{

    private final String author;
    private final String listener;
    private final String identifier;
    private final Comment comment;
    private final Scope scope;

    public CommentInformation(String author, String listener, String identifier, Comment comment){
        this(author, listener, identifier, comment, Scope.WIDE);
    }

    public CommentInformation(String author, String identifier, Comment comment, Scope scope){
        this(author, author, identifier, comment, scope);
    }

    public CommentInformation(String author, String identifier, Comment comment){
        this(author, author, identifier, comment, Scope.WIDE);
    }

    public CommentInformation(String author, String listener, String identifier, Comment comment, Scope scope) {
        this.author = author;
        this.listener = listener;
        this.identifier = identifier;
        this.comment = comment;
        this.scope = scope;
    }

    public String getAuthor() {
        return author;
    }

    public String getListener(){
        return listener;
    }

    public Comment getComment() {
        return comment;
    }

    @Override
    public String getIdentifier() {
        if(null == comment)
            return null;
        return this.identifier;
    }

    @Override
    public int compareTo(Object o) {
        if(null == o)
            return 1;
        if(o instanceof CommentInformation){
            CommentInformation co = (CommentInformation)o;
            if(null == co.getIdentifier())
                return 1;
            if(null == this.getIdentifier())
                return -1;
            return this.getIdentifier().compareTo(co.getIdentifier());
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentInformation that = (CommentInformation) o;

        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;
        if (listener != null ? !listener.equals(that.listener) : that.listener != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (listener != null ? listener.hashCode() : 0);
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CommentInformation{" +
                "author='" + author + '\'' +
                ", listener='" + listener + '\'' +
                ", identifier='" + identifier + '\'' +
                ", comment=" + comment +
                ", scope=" + scope +
                '}';
    }
}
