package com.tripleJ.ds.comment;

import com.google.common.collect.Lists;
import com.tripleJ.*;
import com.tripleJ.unique.IDGenerator;
import com.tripleJ.unique.InvalidIdException;

import java.util.*;
import java.util.concurrent.SynchronousQueue;

/**
 *
 * Created by kami on 2014. 6. 16..
 */
public class CommentHandler implements Handler{

    private final CommentStore commentStore;

    public CommentHandler(CommentStore commentStore) throws InitializeException{
        if(null == commentStore )
            throw new InitializeException("commentStore must not be null");
        this.commentStore = commentStore;
    }

    /**
     * It adds a comment
     * @param authorId id of an author of this comment
     * @param listenerId if of a listener of this comment
     * @param comment comment body of this comment
     * @param scope defines a how open this comment is
     * @return id of this comment
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public String addComment(final String authorId, final String listenerId, final Comment comment, final Scope scope) throws MissingRequiredException, StoreException{
        if(null == authorId || null == listenerId || null == comment)
            throw new MissingRequiredException("authorId, listenerId, Comment must not be null");

        Map<String, String> idResources = comment.getResources();
        idResources.put("author", authorId);
        long time = System.currentTimeMillis();
        idResources.put("time", "t"+Long.toHexString(time)); // "t" is added to avoid being filtered by constrained string

        String commentId = generateID(idResources);
        Calendar current = generateCurrentTime();
        if(null == scope)
            commentStore.addComment(commentId, authorId, listenerId, comment, current.getTime(), Scope.WIDE);
        else
            commentStore.addComment(commentId, authorId, listenerId, comment, current.getTime(), scope);

        return commentId;
    }

    /**
     * It removes a comment which has an id delivered.
     * @param commentId id of comment want to be removed.
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public boolean removeComment(final String commentId) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == commentId)
            throw new MissingRequiredException("subjectId and commentId must not be null");

        new IDGenerator(commentId).checkValidity();
        if(!this.commentStore.isExist(commentId))
            return false;

        commentStore.removeComment(commentId);
        return true;
    }

    /**
     * It modifies the comment
     * @param commentId id of comment
     * @param comment content of comment
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public boolean updateComment(final String commentId, final Comment comment) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == commentId || null == comment)
            throw new MissingRequiredException("subjectId, commentId and comment must not be null");

        new IDGenerator(commentId).checkValidity();
        if(!this.commentStore.isExist(commentId))
            return false;

        Calendar current = generateCurrentTime();
        commentStore.updateComment(commentId, comment, current.getTime());
        return true;
    }

    /**
     * It returns the time when the comment having the commentId delivered is accessed recently.
     * @param commentId id of comment
     * @return last access date
     * @throws MissingRequiredException
     * @throws StoreException
     */
    Date getLastAccessed(String commentId) throws MissingRequiredException, StoreException{
        if(null == commentId)
            throw new MissingRequiredException("commentId must not be null");

        return commentStore.getLastAccessed(commentId);
    }

    /**
     * It returns the time the comment having the commentId delivered created.
     * @param commentId id of comment
     * @return created date
     * @throws StoreException
     * @throws MissingRequiredException
     */
    Date getCreated(String commentId) throws StoreException, MissingRequiredException {
        if(null == commentId)
            throw new MissingRequiredException("commentId must not be null");

        return commentStore.getCreated(commentId);
    }

    /**
     * Basically, it is a simple wrapper of commentStore's recoverByList. It would be better to refer commentStore.
     * @param commentIds array of id of comment
     * @return list of comment information
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public List<CommentInformation> recoverByList(final String... commentIds) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == commentIds || commentIds.length == 0)
            throw new MissingRequiredException("commentId must not be null");

        for(String commentId:commentIds)
            new IDGenerator(commentId).checkValidity();
        return commentStore.recoverByList(commentIds);
    }

    /**
     * Basically, it is a simple wrapper of commentStore's recoverBySet. It would be better to refer commentStore.
     * @param commentIds array of id
     * @return set of the comment information
     * @throws StoreException
     */
    public Set<CommentInformation> recoverBySet(final String... commentIds) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == commentIds || commentIds.length == 0)
            throw new MissingRequiredException("commentId must not be null");

        for(String commentId:commentIds)
            new IDGenerator(commentId).checkValidity();
        return commentStore.recoverBySet(commentIds);
    }

    /**
     * It returns an author of comment having id delivered.
     * @param commentId id of comment
     * @return id of author
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public String getAuthor(final String commentId) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == commentId)
            throw new MissingRequiredException("commentId must not be null");

        new IDGenerator(commentId).checkValidity();
        return commentStore.getAuthor(commentId);
    }

    /**
     * It returns a listener of comment having id delivered.
     * @param commentId id of comment
     * @return id of listener
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    String getListener(final String commentId) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == commentId)
            throw new MissingRequiredException("commentId must not be null");

        new IDGenerator(commentId).checkValidity();
        return commentStore.getListener(commentId);
    }

    /**
     * It returns the list of id of comments which belongs to the author having authorId. The size of the list has to be lesser than or equal to count and it has to be sorted by ascending order if the ascending delivered
     * is true, otherwise by descending order.
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param authorIds array of id of authors
     * @return list of comment information
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public List<CommentInformation> listCommentInfosByAuthor(final int offset, final int count, final String... authorIds) throws MissingRequiredException, InvalidIdException, StoreException {
        if(null == authorIds)
            throw new MissingRequiredException("authorId must not be null");

        if(offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if(0 == count)
            return Lists.newLinkedList();
        for(String authorId:authorIds)
            new IDGenerator(authorId).checkValidity();

        List<String> ids = commentStore.listWideCommentsByAuthor(offset, count, authorIds);
        if(null == ids || ids.size() == 0)
            return Lists.newLinkedList();
        List<CommentInformation> result = commentStore.recoverByList(ids.toArray(new String[ids.size()]));
        assert(result.size() == ids.size());

        return result;
    }

    /**
     * It returns a list of id of wide comments which the author who has an id belongs to id array delivered writes
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param authorIds array of id of authors
     * @return list of ids of wide comments
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public List<String> listWideCommentIdsByAuthor(final int offset, final int count, final String... authorIds) throws MissingRequiredException, InvalidIdException, StoreException {
        if (null == authorIds)
            throw new MissingRequiredException("authorId must not be null");

        if (offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if (0 == count)
            return Lists.newLinkedList();
        for(String authorId:authorIds)
            new IDGenerator(authorId).checkValidity();

        return commentStore.listWideCommentsByAuthor(offset, count, authorIds);
    }

    /**
     * It returns a list of id of narrow comments which the author who has an id belongs to id array delivered writes
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param authorIds array of id of authors
     * @return list of ids of narrow comments
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */

    public List<String> listNarrowCommentIdsByAuthor(final int offset, final int count, final String... authorIds) throws MissingRequiredException, InvalidIdException, StoreException {
        if (null == authorIds)
            throw new MissingRequiredException("authorId must not be null");

        if (offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if (0 == count)
            return Lists.newLinkedList();
        for(String authorId:authorIds)
            new IDGenerator(authorId).checkValidity();

        return commentStore.listNarrowCommentsByAuthor(offset, count, authorIds);
    }


    /**
     * It returns a size of id of wide comments which the author who has an id belongs to id array delivered writes
     * @param authorIds array of id of authors
     * @return size of ids of wide comments
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public int getSizeofWideCommentsByAuthor(String... authorIds) throws StoreException, MissingRequiredException, InvalidIdException {
        if(null == authorIds)
            throw new MissingRequiredException("authorId must not be null");

        for(String authorId:authorIds)
            new IDGenerator(authorId).checkValidity();

        return commentStore.getSizeofWideCommentsByAuthor(authorIds);
    }

    /**
     * It returns a size of id of narrow comments which the author who has an id belongs to id array delivered writes
     * @param authorIds array of id of authors
     * @return size of ids of narrow comments
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public int getSizeofNarrowCommentsByAuthor(String... authorIds) throws StoreException, MissingRequiredException, InvalidIdException {
        if(null == authorIds)
            throw new MissingRequiredException("authorId must not be null");

        for(String authorId:authorIds)
            new IDGenerator(authorId).checkValidity();

        return commentStore.getSizeofNarrowCommentsByAuthor(authorIds);
    }



    /**
     * It returns the list of id of comments which belongs to the listener having listenerOD. The size of the list has to be lesser than or equal to count and it has to be sorted by ascending order if the ascending delivered
     * is true, otherwise by descending order.
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param listenerIds array of id of listeners
     * @return list of comment information
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public List<CommentInformation> listCommentInfosByListener(final int offset, final int count, final String... listenerIds) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == listenerIds)
            throw new MissingRequiredException("authorId must not be null");

        if(offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if(0 == count)
            return Lists.newLinkedList();
        for(String listenerId:listenerIds)
            new IDGenerator(listenerId).checkValidity();

        List<String> ids = commentStore.listWideCommentsByListener(offset, count, listenerIds);
        if(null == ids || ids.size() == 0)
            return Lists.newLinkedList();
        List<CommentInformation> result = commentStore.recoverByList(ids.toArray(new String[ids.size()]));
        assert(result.size() == ids.size());

        return result;
    }

    /**
     * It returns a list of id of wide comments which the listener who has an id belongs to id array delivered listen
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param listenerIds array of id of authors
     * @return list of ids of wide comments
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public List<String> listWideCommentIdsByListener(final int offset, final int count, final String... listenerIds) throws MissingRequiredException, InvalidIdException, StoreException {
        if (null == listenerIds)
            throw new MissingRequiredException("authorId must not be null");

        if (offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if (0 == count)
            return Lists.newLinkedList();
        for(String listenerId:listenerIds)
            new IDGenerator(listenerId).checkValidity();

        return commentStore.listWideCommentsByListener(offset, count, listenerIds);
    }

    /**
     * It returns a list of id of narrow comments which the listener who has an id belongs to id array delivered listen
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param listenerIds array of id of authors
     * @return list of ids of narrow comments
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public List<String> listNarrowCommentIdsByListener(final int offset, final int count, final String... listenerIds) throws MissingRequiredException, InvalidIdException, StoreException {
        if (null == listenerIds)
            throw new MissingRequiredException("authorId must not be null");

        if (offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if (0 == count)
            return Lists.newLinkedList();
        for(String listenerId:listenerIds)
            new IDGenerator(listenerId).checkValidity();

        return commentStore.listNarrowCommentsByListener(offset, count, listenerIds);
    }

    /**
     * It returns a size of id of wide comments which the listener who has an id belongs to id array delivered listen
     * @param listenerIds array of id of listeners
     * @return size of ids of wide comments
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public int getSizeofWideCommentsByListener(String... listenerIds) throws StoreException, InvalidIdException, MissingRequiredException {
        if(null == listenerIds)
            throw new MissingRequiredException("listenerId must not be null");

        for(String listenerId:listenerIds)
            new IDGenerator(listenerId).checkValidity();

        return commentStore.getSizeofWideCommentsByListener(listenerIds);
    }

    /**
     * It returns a size of id of narrow comments which the listener who has an id belongs to id array delivered listen
     * @param listenerIds array of id of listeners
     * @return size of ids of narrow comments
     * @throws StoreException
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public int getSizeofNarrowCommentsByListener(String... listenerIds) throws StoreException, InvalidIdException, MissingRequiredException {
        if(null == listenerIds)
            throw new MissingRequiredException("listenerId must not be null");

        for(String listenerId:listenerIds)
            new IDGenerator(listenerId).checkValidity();

        return commentStore.getSizeofNarrowCommentsByListener(listenerIds);
    }

    /**
     * It returns a list of wide commentIds which exactly matches with author, listener and have Scope.Wide scope.
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param  authorId id of author
     * @param listenerId id of listener
     * @return list of wide comment ids
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public List<String> listWideCommentsByPair(int offset, int count, String authorId, String listenerId) throws MissingRequiredException, InvalidIdException {
        if (null == authorId || null == listenerId)
            throw new MissingRequiredException("authorId and listenerId must not be null");

        if (offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if (0 == count)
            return Lists.newLinkedList();
        new IDGenerator(authorId).checkValidity();
        new IDGenerator(listenerId).checkValidity();

        return commentStore.listWideCommentsByPair(offset, count, authorId, listenerId);
    }

    /**
     * It returns a list of narrow commentIds which exactly matches with author, listener and have Scope.Wide scope.
     * @param offset start point, 0 means most recent.
     * @param count number of size of result list
     * @param  authorId id of author
     * @param listenerId id of listener
     * @return list of narrow comment ids
     * @throws MissingRequiredException
     * @throws InvalidIdException
     */
    public List<String> listNarrowCommentsByPair(int offset, int count, String authorId, String listenerId) throws MissingRequiredException, InvalidIdException {
        if (null == authorId || null == listenerId)
            throw new MissingRequiredException("authorId and listenerId must not be null");

        if (offset < 0 || count < 0)
            throw new ArrayIndexOutOfBoundsException("offset and count must be greater than or equal to zero");
        if (0 == count)
            return Lists.newLinkedList();
        new IDGenerator(authorId).checkValidity();
        new IDGenerator(listenerId).checkValidity();

        return commentStore.listNarrowCommentsByPair(offset, count, authorId, listenerId);
    }

    private boolean checkListener(final List<CommentInformation> actual, final String expected){
        for(CommentInformation commentInformation: actual){
            String listener = commentInformation.getListener();
            if(null == listener || listener.compareTo(expected) != 0)
                return false;
        }
        return true;
    }

    @Override
    public String generateID(Map<String, String> resources) {
        return IDGenerator.generateId(TripleJType.Comment, resources);
    }

}
