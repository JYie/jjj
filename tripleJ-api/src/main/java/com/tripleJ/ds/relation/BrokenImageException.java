package com.tripleJ.ds.relation;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 7. 10..
 */
public class BrokenImageException extends TripleJException {
    public BrokenImageException(Exception exception) {
        super(exception);
    }

    public BrokenImageException(String arg, Exception exception) {
        super(arg, exception);
    }

    public BrokenImageException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public BrokenImageException(String arg0) {
        super(arg0);
    }

    public BrokenImageException(Throwable arg0) {
        super(arg0);
    }

    public BrokenImageException() {
    }
}
