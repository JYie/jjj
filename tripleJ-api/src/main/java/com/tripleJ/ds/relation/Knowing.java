package com.tripleJ.ds.relation;

/**
 *
 */
public class Knowing {

    private final String source;
    private final String destination;
    private final boolean oriented;
    private final boolean direction;
    private final int distance;

    public Knowing(String source, String destination, int distance){
        this.source = source;
        this.destination = destination;
        this.distance = distance;
        this.oriented = true;
        this.direction = true;
    }

    public Knowing(String source, String destination, boolean oriented, int distance){
        this.source = source;
        this.destination = destination;
        this.distance = distance;
        this.oriented = oriented;
        this.direction = true;
    }

    public Knowing(String source, String destination, boolean oriented, int distance, boolean direction){
        this.source = source;
        this.destination = destination;
        this.distance = distance;
        this.oriented = oriented;
        this.direction = direction;
    }

    public String getSource() {
        if(this.oriented && !this.direction)
            return destination;
        return source;
    }

    public String getDestination() {
        if(this.oriented && !this.direction)
            return source;
        return destination;
    }

    public int getDistance() {
        return distance;
    }

    public boolean isOriented() {
        return oriented;
    }

    public boolean getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Knowing other = (Knowing) obj;

        if(oriented && other.oriented) {
            if(com.google.common.base.Objects.equal(this.direction,
                    other.direction)){
                return com.google.common.base.Objects.equal(this.source, other.source)
                        && com.google.common.base.Objects.equal(this.destination,
                        other.destination);
            }else{
                return com.google.common.base.Objects.equal(this.source, other.destination)
                        && com.google.common.base.Objects.equal(this.destination,
                        other.source);
            }
        }
        else if((!oriented) && (!other.oriented))
            return com.google.common.base.Objects.equal(this.source, other.source)
                    && com.google.common.base.Objects.equal(this.destination,
                    other.destination);
        else
            return false;
    }

    @Override
    public int hashCode() {
        if(oriented){
            if(direction)
                return com.google.common.base.Objects.hashCode(this.source,
                    this.destination, this.direction);
            else
                return com.google.common.base.Objects.hashCode(this.destination,
                        this.source, !this.direction);
        }else
            return com.google.common.base.Objects.hashCode(this.source,
                    this.destination);
    }

    @Override
    public String toString() {
        if(oriented)
            return "Knowing{" +
                "source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", oriented=" + oriented +
                ", direction=" + direction +
                ", distance=" + distance +
                '}';
        else
            return "Knowing{" +
                    "source='" + source + '\'' +
                    ", destination='" + destination + '\'' +
                    ", oriented=" + oriented +
                    ", distance=" + distance +
                    '}';
    }
}
