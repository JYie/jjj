package com.tripleJ.ds.relation;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 6. 16..
 */
public class BoundaryException extends TripleJException {

    public BoundaryException() {
        super();
    }

    public BoundaryException(Exception exception) {
        super(exception);
    }

    public BoundaryException(String arg, Exception exception) {
        super(arg, exception);
    }

    public BoundaryException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public BoundaryException(String arg0) {
        super(arg0);
    }

    public BoundaryException(Throwable arg0) {
        super(arg0);
    }

}
