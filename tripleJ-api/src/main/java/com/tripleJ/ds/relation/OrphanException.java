package com.tripleJ.ds.relation;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 6. 19..
 */
public class OrphanException extends TripleJException {

    public OrphanException(Exception exception) {
        super(exception);
    }

    public OrphanException(String arg, Exception exception) {
        super(arg, exception);
    }

    public OrphanException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public OrphanException(String arg0) {
        super(arg0);
    }

    public OrphanException(Throwable arg0) {
        super(arg0);
    }

    public OrphanException() {
    }
}
