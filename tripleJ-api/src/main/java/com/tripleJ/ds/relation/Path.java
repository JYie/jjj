package com.tripleJ.ds.relation;

import com.google.common.base.Joiner;

import java.util.LinkedList;
import java.util.List;

public class Path {

	private String source;
	private String destination;
	private List<String> intermediates;

	public Path(String... elements) {
		if (null != elements && elements.length > 0) {
			this.intermediates = new LinkedList<String>();
			this.source = elements[0];
			this.destination = elements[elements.length - 1];
			for (int i = 1; i < elements.length - 1; i++)
				this.intermediates.add(elements[i]);
		}
	}

	public Path(List<String> elements) {
		if (null != elements && elements.size() > 0) {
			this.source = elements.get(0);
			this.destination = elements.get(elements.size() - 1);
			this.intermediates = new LinkedList<String>();
			for (int i = 1; i < elements.size() - 1; i++)
				this.intermediates.add(elements.get(i));
		}
	}

	public String getSource() {
		return source;
	}

	public String getDestination() {
		return destination;
	}

	public List<String> getIntermediates() {
		return this.intermediates;
	}

	public int getLength() {
		if (this.source == this.destination)
			return 0;
		else if (null == this.intermediates)
			return 1;
		else
			return this.intermediates.size() + 1;
	}

	/**
	 * Using Guava to compare provided object to me for equality.
	 * 
	 * @param obj
	 *            Object to be compared to me for equality.
	 * @return {@code true} if provided object is considered equal to me or
	 *         {@code false} if provided object is not considered equal to me.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Path other = (Path) obj;

		return com.google.common.base.Objects.equal(this.source, other.source)
				&& com.google.common.base.Objects.equal(this.destination,
						other.destination)
                && Joiner.on(',').join(this.intermediates).compareTo(Joiner.on(',').join(other.intermediates)) == 0;
	}

	/**
	 * Uses Guava to assist in providing hash code of this path` instance.
	 * 
	 * @return My hash code.
	 */
	@Override
	public int hashCode() {
		return com.google.common.base.Objects.hashCode(this.source,
				this.destination, this.intermediates);
	}

	@Override
	public String toString() {
		return "Path [source=" + source + ", destination=" + destination
				+ ", intermediates=" + intermediatesString() + "]";
	}
	
	private String intermediatesString(){
		if(null == this.intermediates)
			return "";
		
		StringBuffer sb = new StringBuffer();
		boolean isFirst = true;
		for(String intermediate:this.intermediates){
			if(!isFirst)
				sb.append(",");
			sb.append(intermediate);
			isFirst = false;
		}
		return sb.toString();
	}

}
