package com.tripleJ.ds.relation;

import com.google.common.collect.Sets;
import com.tripleJ.*;
import com.tripleJ.unique.IDGenerator;
import com.tripleJ.unique.InvalidIdException;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by kami on 2014. 6. 16..
 */
public class OrientationHandler {

    public static final int defaultMaxDepth = 6;

    public OrientationHandler(OrientationStore orientationStore) throws InitializeException {
        if (null == orientationStore) {
            throw new InitializeException("RelationHandler is not initialized");
        }
        this.orientationStore = orientationStore;
        this.maxDepth = defaultMaxDepth;
    }

    private final OrientationStore orientationStore;
    private int maxDepth;

    public int getMaxDepth() {
        return this.maxDepth;
    }

    public synchronized void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    /**
     * Remove an orientation from src to destination
     *
     * @param src
     * @param destination
     * @throws MissingRequiredException
     */
    public boolean removeOrientation(GraphType graphType, String src, String destination)
            throws MissingRequiredException, StoreException, InvalidIdException, TreeException, IncompatibleException {
        if (null == orientationStore)
            throw new MissingRequiredException("RelationStore is null");

        if (null == graphType || null == src || null == destination)
            throw new MissingRequiredException("Parameter is null");

        if(!graphType.isDirected())
            throw new IncompatibleException("removeOrientation is only possible for directedGraph Type");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        if(graphType.leafRemovalOnly()){
            boolean isLeaf = isDirectedLeaf(graphType, destination);
            if(!isLeaf)
                throw new TreeException("This type has leafRemovalOnly flag, but destination is not a leaf");
        }

        orientationStore.removeOrientation(graphType, src, destination);
        return true;
    }

    boolean isDirectedLeaf(GraphType type, String node) throws StoreException, MissingRequiredException, InvalidIdException {
        Set<String> children = getOutNeighbors(type, node);
        if(null != children && children.size() > 0)
            return false;
        else
            return true;
    }

    /**
     * Remove a bi-directional orientation between entity1 and entity2.
     * @param graphType
     * @param entity1
     * @param entity2
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public boolean removeRelation(GraphType graphType, String entity1, String entity2) throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException {
        if(null == graphType || null == entity1 || null == entity2)
            throw new MissingRequiredException("Parameter is null");

        if(graphType.isDirected())
            throw new IncompatibleException("removeRelatoin is only possible for undirectedGraph Type");

        new IDGenerator(entity1).checkValidity();
        new IDGenerator(entity2).checkValidity();

        orientationStore.removeRelation(graphType, entity1, entity2);
        return true;
    }

    /**
     * Add an orientation from src to destination.
     *
     * @param src
     * @param destination
     */
    public boolean addOrientation(GraphType graphType, String src, String destination)
            throws MissingRequiredException, StoreException, InvalidIdException, TreeException, IncompatibleException {
        if (null == orientationStore)
            throw new MissingRequiredException("RelationStore is not initialized");

        if (null == graphType ||null == src || null == destination)
            throw new MissingRequiredException("Parameter is null");

        if(!graphType.isDirected())
            throw new IncompatibleException("addOrientation is only possible for directedGraph Type");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        if(graphType.allowsSingleParentOnly()) {
            Set<String> check = getInNeighbors(graphType, destination);
            if (null != check && check.size() > 0)
                throw new TreeException("Only one parent is possible");
        }

        if(graphType.isTree()){
            int path = getShortestDirectPathLength(graphType, destination, src);
            if(path != -1)
                throw new TreeException("Tree does not allow to generate a cycle");
        }

        orientationStore.addOrientation(graphType, src, destination);
        return true;

    }

    /**
     * Add a bi-directional orientation between entity1 and entity2.
     * @param graphType
     * @param entity1
     * @param entity2
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public boolean addRelation(GraphType graphType, String entity1, String entity2) throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException, TreeException {
        if(null == graphType || null == entity1 || null == entity2)
            throw new MissingRequiredException("Parameter is null");

        if(graphType.isDirected())
            throw new IncompatibleException("addRelation is only possible for undirectedGraph Type");

        new IDGenerator(entity1).checkValidity();
        new IDGenerator(entity2).checkValidity();

        if(graphType.isTree()){
            int path = getShortestPathLength(graphType, entity1, entity2);

            if(path != -1)
                throw new TreeException("Tree does not allow to generate a cycle");
        }


        orientationStore.addRelation(graphType, entity1, entity2);
        return true;
    }

    /**
     * Return true if there is an orientation which is from src to destination.
     *
     * @param src
     * @param destination
     * @return
     */
    public boolean hasOrientation(GraphType graphType, String src, String destination)
            throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        if(!graphType.isDirected())
            throw new IncompatibleException("this method is not available for undirected graph");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Set<String> outNeighbors = getOutNeighbors(graphType, src);
        if (null == outNeighbors)
            return false;
        else
            return outNeighbors.contains(destination);
    }

    /**
     * Return true if there exists direct paths from src to destination  in fixed length(cf. default max length is 6)
     * false otherwise
     *
     * @param src
     * @param destination
     * @return
     */
    public boolean hasDirectPath(GraphType graphType, String src, String destination)
            throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        if(!graphType.isDirected())
            throw new IncompatibleException("this method is not available for undirected graph");

        Set<String> srcs = Sets.newHashSet(src);
        Set<String> destinations = Sets.newHashSet(destination);
        int result = findIntersectionPointRecursivelyAsDirection(graphType, 0, srcs, destinations);
        if (-1 == result)
            return false;
        else
            return true;
    }

    /**
     * Return true if there exists paths(not considering direction) from src to destination in fixed length.
     *
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     */
    public boolean hasPath(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException{

        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Set<String> srcs = Sets.newHashSet(src);
        Set<String> destinations = Sets.newHashSet(destination);
        int result = findIntersectionPointRecursively(graphType, 0, srcs, destinations);
        if (-1 == result)
            return false;
        else
            return true;
    }

    /**
     * It returns a set of src's out-neighbors list
     *
     * @param sources
     * @return
     */
    public Set<String> getOutNeighbors(GraphType graphType, String... sources)
            throws MissingRequiredException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == sources || sources.length == 0) {
            throw new MissingRequiredException("Parameter is null");
        }

        for(String source:sources)
            new IDGenerator(source).checkValidity();

        Set<String> result = Sets.newHashSet();
        Set<Knowing> knowings = orientationStore.getOutNeighbors(graphType, sources);
        if(null == knowings || knowings.isEmpty())
            return result;
        for(Knowing knowing: knowings){
            String destination;
            if(knowing.getDirection())
                destination= knowing.getDestination();
            else
                destination = knowing.getSource();
            assert(null != destination);
            assert(knowing.isOriented());
            assert(knowing.getDistance() == 1);
            result.add(destination);
        }
        return result;
    }

    /**
     * It returns a set of knowings which have orientations going from sources delivered.
     * @param graphType
     * @param sources
     * @return
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     * @throws IncompatibleException
     */
    public Set<Knowing> getOutKnowings(GraphType graphType, String... sources) throws MissingRequiredException, InvalidIdException, StoreException, IncompatibleException {
        if (null == sources || sources.length == 0) {
            throw new MissingRequiredException("Parameter is null");
        }

        for(String source:sources)
            new IDGenerator(source).checkValidity();

        if(!graphType.isDirected())
            throw new IncompatibleException("The term out is defined only at directed graph");

        return orientationStore.getOutNeighbors(graphType, sources);
    }

    /**
     * It returns a set of destination's in-neighbors
     *
     * @param destinations
     * @return
     * @throws MissingRequiredException
     */
    public Set<String> getInNeighbors(GraphType graphType, String... destinations) throws MissingRequiredException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == destinations || destinations.length == 0) {
            throw new MissingRequiredException("Parameter is null");
        }

        for(String destination:destinations)
            new IDGenerator(destination).checkValidity();

        Set<String> result = Sets.newHashSet();
        Set<Knowing> knowings = orientationStore.getInNeighbors(graphType, destinations);
        if(null == knowings || knowings.isEmpty())
            return result;
        for(Knowing knowing: knowings){
            String source = knowing.getSource();
            assert(null != source);
            assert(knowing.isOriented());
            assert(knowing.getDistance() == 1);
            result.add(source);
        }
        return result;
    }

    /**
     * It returns a set of knowings which have orientations going to destinations delivered.
     * @param graphType
     * @param destinations
     * @return
     * @throws InvalidIdException
     * @throws StoreException
     * @throws IncompatibleException
     * @throws MissingRequiredException
     */
    public Set<Knowing> getInKnowings(GraphType graphType, String... destinations) throws InvalidIdException, StoreException, IncompatibleException, MissingRequiredException {
        if (null == destinations || destinations.length == 0) {
            throw new MissingRequiredException("Parameter is null");
        }

        for(String destination:destinations)
            new IDGenerator(destination).checkValidity();

        if(!graphType.isDirected())
            throw new IncompatibleException("The term in is defined only at directed graph");

        return orientationStore.getInNeighbors(graphType, destinations);
    }

    /**
     * If the type is directed, it would returns the union of inNeighbors and outNeighbors. Otherwise, it returns the set of relations.
     * @param graphType
     * @param vertices
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public Set<String> getNeighbors(GraphType graphType, String... vertices) throws MissingRequiredException, StoreException, InvalidIdException{
        if(null == vertices || vertices.length == 0){
            throw new MissingRequiredException("origin must not be null");
        }

        for(String vertex:vertices)
            new IDGenerator(vertex).checkValidity();

        if(!graphType.isDirected()) {
            Set<String> result = Sets.newHashSet();
            Set<Knowing> knowings = orientationStore.getRelations(graphType, vertices);
            if(null == knowings || knowings.isEmpty())
                return result;
            for(Knowing knowing: knowings){
                String destination = knowing.getDestination();
                assert(null != destination);
                assert(!knowing.isOriented());
                assert(knowing.getDistance() == 1);
                result.add(destination);
            }
            return result;
        }

        Set<String> inNeighbors = getInNeighbors(graphType, vertices);
        Set<String> outNeighbors = getOutNeighbors(graphType, vertices);

        if(null == inNeighbors || inNeighbors.size() == 0)
            return outNeighbors;
        if(null != outNeighbors) {
            return Sets.union(inNeighbors, outNeighbors);
        }else
            return outNeighbors;
    }

    /**
     * It returns a set of knowings each of which has a vertex in vertices delivered as a source or a destination.
     * @param graphType
     * @param vertices
     * @return
     * @throws MissingRequiredException
     * @throws InvalidIdException
     * @throws StoreException
     */
    public Set<Knowing> getKnowings(GraphType graphType, String... vertices) throws MissingRequiredException, InvalidIdException, StoreException {
        if (null == vertices || vertices.length == 0) {
            throw new MissingRequiredException("Parameter is null");
        }

        for(String vertex:vertices)
            new IDGenerator(vertex).checkValidity();

        if(!graphType.isDirected())
            return orientationStore.getRelations(graphType, vertices);
        else{
            Set<Knowing> inNeighbors = orientationStore.getInNeighbors(graphType, vertices);
            Set<Knowing> outNeighbors = orientationStore.getOutNeighbors(graphType, vertices);

            if(null == inNeighbors || inNeighbors.size() == 0)
                return outNeighbors;
            if(null != outNeighbors) {
                return Sets.union(inNeighbors, outNeighbors);
            }else
                return outNeighbors;
        }
    }

    /**
     * It returns the parent's id which are actually parent of child delivered.
     * It is only supported for a directed graph, otherwise it throws an Incompatible exception.
     * @param graphType
     * @param child
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public String getParent(GraphType graphType, String child) throws MissingRequiredException, StoreException, IncompatibleException, InvalidIdException {
        if(null == graphType || null == child)
            throw new MissingRequiredException("Parameter is null");

        if(!graphType.isDirected())
            throw new IncompatibleException("The term parent is defined only at directed graph");

        new IDGenerator(child).checkValidity();

        Set<String> temp = getInNeighbors(graphType, child);
        if(null == temp || temp.size() == 0)
            return null;
        assert(temp.size() == 1);
        Iterator<String> iterator = temp.iterator();
        return iterator.next();
    }

    /**
     * It returns src's out-neighbors which has a out-going path to destination.
     *
     * @param src
     * @param destination
     * @return
     */
    public Set<String> getSrcOutNeighborsLinkingToDest(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        if(!graphType.isDirected())
            throw new IncompatibleException("getSrcOutKnowingsLinkingToDest is only possible for directedGraph Type");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Set<String> candidates = getOutNeighbors(graphType, src);
        if (null == candidates || candidates.size() == 0)
            return null;
        Set<String> total = expandByInDirectionRecursively(graphType, maxDepth - 1, Sets.newHashSet(destination));

        if(null == total || total.size() == 0)
            return null;
        Sets.SetView<String> intersected;
        if(candidates.size() <= total.size())
            intersected = Sets.intersection(candidates, total);
        else
            intersected = Sets.intersection(total, candidates);

        return intersected;
    }

    /**
     * It returns src's out-relations who has a out-going path to destination and has a distance delivered between src and destination
     *
     * @param src
     * @param destination
     * @param dist distance from src
     * @return
     * @throws MissingRequiredException
     * @throws  BoundaryException
     */
    public Set<Knowing> getSrcOutKnowingsLinkingToDest(GraphType graphType, int dist, boolean scope, String src, String destination) throws MissingRequiredException, BoundaryException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore)
            throw new MissingRequiredException("RelationStore is null");

        if (null == src || null == destination)
            throw new MissingRequiredException("Parameter is null");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        if (dist < 0 || dist > maxDepth)
            throw new BoundaryException(" Distance should be greater than 0 and lesser than maxDepth");

        if(!graphType.isDirected())
            throw new IncompatibleException("getSrcOutKnowingsLinkingToDest is only possible for directedGraph Type");

        Knowings candidates = null;
        int count = maxDepth - dist;
        if (scope)
            candidates = expandByOutDirectionRecursively(graphType, dist, 0, src, null);
        else
            candidates = goByOutDirectionRecursively(graphType, dist, 0, src, null);

        if (null == candidates)
            return null;

        candidates.removeKnowing(src);
        if (candidates.getSize() == 0)
            return null;

        Knowings total = expandByInDirectionRecursively(graphType, count, 0, destination, null);
        if(null == total || total.getSize() == 0)
            return null;

        return Knowings.getHookedKnowingsFront(candidates, total);
    }

    /**
     * It returns src's in-neighbors who has in-comming paths from destination.
     *
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     */
    public Set<String> getDestInNeighborsLinkingFromSrc(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, IncompatibleException, InvalidIdException {
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        if(!graphType.isDirected())
            throw new IncompatibleException("getDestInNeighborsLinkingFromSrc is only possible for directedGraph Type");

        Set<String> candidates = getInNeighbors(graphType, destination);
        if (null == candidates || candidates.size() == 0)
            return null;
        Set<String> total = expandByOutDirectionRecursively(graphType, maxDepth - 1, Sets.newHashSet(src));

        if(null == total || total.size() == 0)
            return null;
        Sets.SetView<String> intersected;
        if(candidates.size() <= total.size())
            intersected = Sets.intersection(candidates, total);
        else
            intersected = Sets.intersection(total, candidates);

        return intersected;
    }

    /**
     * It returns destination's in-relations who has a in-coming path from source and has distance delivered between src and dest
     *
     * @param src
     * @param destination
     * @param dist distance from src
     * @return
     * @throws MissingRequiredException
     */
    public Set<Knowing> getDestInKnowingsLinkingFromSrc(GraphType graphType, int dist, boolean scope, String src, String destination) throws MissingRequiredException, BoundaryException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore)
            throw new MissingRequiredException("RelationStore is null");

        if (null == src || null == destination)
            throw new MissingRequiredException("Parameter is null");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        if (dist < 0 || dist > maxDepth)
            throw new BoundaryException(" Distance should be greater than 0 and lesser than maxDepth");

        if(!graphType.isDirected())
            throw new IncompatibleException("getDestInNeighborsLinkingFromSrc is only possible for directedGraph Type");

        Knowings candidates = null;
        int count = maxDepth - dist;
        if (scope)
            candidates = expandByInDirectionRecursively(graphType, dist, 0, destination, null);
        else
            candidates = goByInDirectionRecursively(graphType, dist, 0, destination, null);

        if (null == candidates)
            return null;

        candidates.removeKnowing(destination);
        if (candidates.getSize() == 0)
            return null;

        Knowings total = expandByOutDirectionRecursively(graphType, count, 0, src, null);
        if(null == total || total.getSize() == 0)
            return null;

        return Knowings.getHookedKnowingsRear(total, candidates);
    }

    /**
     * It returns the set of neighbors of sources which have paths linking to destinationination. If the type is directed, we do not care the type of neighbors, i.e
     * both of inNeighbor and outNeighbor can be candidate. Otherwise, we just use the relation.
     * @param graphType
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public Set<String> getSrcBothNeighborsLinkingToDest(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Set<String> candidates = getNeighbors(graphType, src);
        if (null == candidates || candidates.size() == 0)
            return null;

        Set<String> total = expandByBothDirectionRecursively(graphType, maxDepth - 1, Sets.newHashSet(destination));

        if(null == total || total.size() == 0)
            return null;
        Sets.SetView<String> intersected;
        if(candidates.size() <= total.size())
            intersected = Sets.intersection(candidates, total);
        else
            intersected = Sets.intersection(total, candidates);

        return intersected;
    }

    /**
     *
     * @param graphType
     * @param dist
     * @param scope
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     * @throws BoundaryException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public Set<Knowing> getSrcBothKnowingsLinkingToDest(GraphType graphType, int dist, boolean scope, String src, String destination) throws MissingRequiredException, BoundaryException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }
        if (dist < 0 || dist > maxDepth)
            throw new BoundaryException(" Distance should be greater than 0 and lesser than maxDepth");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Knowings candidates = null;
        int count = maxDepth - dist;
        if (scope)
            candidates = expandByBothDirectionRecursively(graphType, dist, 0, src, null);
        else
            candidates = goByBothDirectionRecursively(graphType, dist, 0, src, null);

        if (null == candidates)
            return null;

        candidates.removeKnowing(src);
        if (candidates.getSize() == 0)
            return null;

        Knowings total = expandByBothDirectionRecursively(graphType, count, 0, destination, null);
        if(null == total || total.getSize() == 0)
            return null;

        return Knowings.getHookedKnowingsFront(candidates, total);
    }

    /**
     * It returns the set of neighbors of destinationinations which have paths linking from source. If the type is directed, we do not care the type of neighbors, i.e
     * both of inNeighbor and outNeighbor can be candidate. Otherwise, we just use the relation.
     * @param graphType
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public Set<String> getDestBothNeighborsLinkingFromSrc(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Set<String> candidates = getNeighbors(graphType, destination);
        if (null == candidates || candidates.size() == 0)
            return null;

        Set<String> total = expandByBothDirectionRecursively(graphType, maxDepth - 1, Sets.newHashSet(src));
        Sets.SetView<String> intersected = Sets.intersection(candidates, total);
        return intersected;
    }

    /**
     *
     * @param graphType
     * @param dist
     * @param scope
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     * @throws BoundaryException
     * @throws StoreException
     * @throws InvalidIdException
     */
    public Set<Knowing> getDestBothKnowingsLinkingFromSrc(GraphType graphType, final int dist, boolean scope, String src, String destination) throws MissingRequiredException, BoundaryException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }
        if (dist < 0 || dist > maxDepth)
            throw new BoundaryException(" Distance should be greater than 0 and lesser than maxDepth");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Knowings candidates = null;
        int count = maxDepth - dist;
        if (scope)
            candidates = expandByBothDirectionRecursively(graphType, dist, 0, destination, null);
        else
            candidates = goByBothDirectionRecursively(graphType, dist, 0, destination, null);

        if (null == candidates)
            return null;

        candidates.removeKnowing(destination);
        if (candidates.getSize() == 0)
            return null;

        Knowings total = expandByBothDirectionRecursively(graphType, count, 0, src, null);
        if(null == total || total.getSize() == 0)
            return null;

        return Knowings.getHookedKnowingsRear(total, candidates);
    }

    /**
     * Return all direct paths from src to destination.
     *
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     */
    public Set<Path> getAllDirectPaths(String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        return null;
    }

    /**
     * Return all paths(not considering direction) from src to destination.
     *
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     */
    public Set<Path> getAllPaths(String src, String destination)
            throws MissingRequiredException, StoreException{
        return null;
    }

    /**
     * Return independent direct paths from src to destination.
     *
     * @param src
     * @param destination
     * @return
     */
    public Set<Path> getIndependentDirectPaths(String src, String destination) throws MissingRequiredException, StoreException{
        return null;
    }

    /**
     * Return independent paths(not considering direction) paths between src and destination.
     *
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     */
    public Set<Path> getIndependentPaths(String src, String destination) throws MissingRequiredException, StoreException{
        return null;
    }

    /**
     * It returns the shortest direct path from src to destination.
     *
     * @param src
     * @param destination
     * @return
     */
    public int getShortestDirectPathLength(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        if(!graphType.isDirected())
            throw new IncompatibleException("Direct path is only defined at directed graph");

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();


        Set<String> srcs = Sets.newHashSet(src);
        Set<String> destinations = Sets.newHashSet(destination);
        int result = findIntersectionPointRecursivelyAsDirection(graphType, 0, srcs, destinations);
        return result;
    }

    /**
     * It returns the shortest paths(not considering direction) between src, destination. Therefore, if the type is not directed then we just use relation.
     *
     * @param src
     * @param destination
     * @return
     * @throws MissingRequiredException
     */
    public int getShortestPathLength(GraphType graphType, String src, String destination) throws MissingRequiredException, StoreException, InvalidIdException{
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src || null == destination) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();
        new IDGenerator(destination).checkValidity();

        Set<String> srcs = Sets.newHashSet(src);
        Set<String> destinations = Sets.newHashSet(destination);
        int result = findIntersectionPointRecursively(graphType, 0, srcs, destinations);
        assert(result <= this.getMaxDepth());
        return result;
    }

    /**
     * It returns true if src has no child element.
     * @param graphType
     * @param src
     * @return
     * @throws MissingRequiredException
     * @throws StoreException
     */
    public boolean isRoot(GraphType graphType, String src) throws MissingRequiredException, StoreException, InvalidIdException, IncompatibleException {
        if (null == orientationStore) {
            throw new MissingRequiredException("RelationStore is null");
        }
        if (null == src) {
            throw new MissingRequiredException("Parameter is null");
        }

        new IDGenerator(src).checkValidity();

        if(!graphType.isDirected())
            throw new IncompatibleException("The term root is only available for directed graph");

        Set<String> inNeighbors = getInNeighbors(graphType, src);
        if(inNeighbors == null || inNeighbors.size() == 0)
            return true;
        return false;
    }

    /**
     * It returns the root of source. If src has no parent, i.e it is a root, it would return src itself.
     * @param graphType
     * @param src
     * @return
     */
    public String getRoot(GraphType graphType, String src) throws MissingRequiredException, StoreException, IncompatibleException, InvalidIdException {
        if(null == graphType || null == src)
            throw new MissingRequiredException("Parameter is null");
        if(!graphType.isDirected())
            throw new IncompatibleException("The term root is only available for directed graph");

        String child = src;
        String parent = null;
        do{
            if(null != parent)
                child = parent;
            parent = getParent(graphType, child);
        }while(null != parent);

        return child;
    }

    int findIntersectionPointRecursivelyAsDirection(GraphType graphType, int result, Set<String> a, Set<String> b) throws StoreException, MissingRequiredException, InvalidIdException {
        if (null == a || a.size() == 0)
            return -1;
        if (null == b || b.size() == 0)
            return -1;
        if (result > maxDepth) {
            return -1;
        }

        Sets.SetView<String> intersected = Sets.intersection(a, b);
        if (null != intersected && intersected.size() > 0)
            return result;
        ++result;
        if (result % 2 == 0) {
            Set<String> a_ = getNextNeighborsByOutDirection(graphType, a.toArray(new String[a.size()]));
            return findIntersectionPointRecursivelyAsDirection(graphType, result, a_, b);
        } else {
            Set<String> b_ = getNextNeighborsByInDirection(graphType, b.toArray(new String[b.size()]));
            return findIntersectionPointRecursivelyAsDirection(graphType, result, a, b_);
        }
    }

    int findIntersectionPointRecursively(GraphType graphType, int result, Set<String> a, Set<String> b) throws StoreException, MissingRequiredException, InvalidIdException {
        if (null == a || a.size() == 0)
            return -1;
        if (null == b || b.size() == 0)
            return -1;
        if (result > maxDepth)
            return -1;
        Sets.SetView<String> intersected = Sets.intersection(a, b);
        if (null != intersected && intersected.size() > 0)
            return result;
        result++;
        if (result % 2 == 0) {
            Set<String> a_ = getNextNeighborsByBothDirection(graphType, a.toArray(new String[a.size()]));
            return findIntersectionPointRecursively(graphType, result, a_, b);
        } else {
            Set<String> b_ = getNextNeighborsByBothDirection(graphType, b.toArray(new String[b.size()]));
            return findIntersectionPointRecursively(graphType, result, a, b_);
        }
    }

    Set<String> getNextNeighborsByOutDirection(GraphType graphType, String... sources) throws StoreException, MissingRequiredException, InvalidIdException {
        assert (null != sources);
        Set<String> expanded = Sets.newHashSet();
        Set<String> secondNeighbors = getOutNeighbors(graphType, sources);
        if (null != secondNeighbors && secondNeighbors.size() > 0)
            expanded.addAll(secondNeighbors);

        return expanded;
    }

    Set<String> expandByOutDirectionRecursively(GraphType graphType, int repeat, Set<String> sources) throws StoreException, MissingRequiredException, InvalidIdException {
        assert (null != sources);
        if (0 == repeat)
            return sources;
        Set<String> expanded = Sets.newHashSet(sources);
        Set<String> secondNeighbors = getOutNeighbors(graphType, sources.toArray(new String[sources.size()]));
        if (null != secondNeighbors && secondNeighbors.size() > 0)
            expanded.addAll(secondNeighbors);
        --repeat;
        return expandByOutDirectionRecursively(graphType, repeat, expanded);
    }

    Knowings expandByOutDirectionRecursively(GraphType graphType, final int distance, int current,String source, Knowings currentKnowings) throws StoreException, MissingRequiredException, InvalidIdException {
        assert(distance >= 0);
        assert(current >= 0);

        if(distance <= current)
            return currentKnowings;

        if(0 == current){
            currentKnowings = new Knowings(source);
            currentKnowings.addKnowing(source, true, 0, true);
        }
        assert(null != currentKnowings);

        Set<String> currentKnowings_ = currentKnowings.getWings();
        Set<String> secondNeighbors = getOutNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
        if(null == secondNeighbors)
            return currentKnowings;

        ++current;
        for(String secondNeighbor: secondNeighbors){
            currentKnowings.addKnowing(secondNeighbor,true, current, true);
        }

        return expandByOutDirectionRecursively(graphType, distance, current, source, currentKnowings);
    }

    Knowings goByOutDirectionRecursively(GraphType graphType, final int distance, int current,String source, Knowings currentKnowings) throws StoreException, MissingRequiredException, InvalidIdException {
        assert(distance >= 0);
        assert(current >= 0);

        if(distance <= current)
            return currentKnowings;

        if(0 == current){
            currentKnowings = new Knowings(source);
            currentKnowings.addKnowing(source, true, 0, true);
        }
        assert(null != currentKnowings);

        Set<String> currentKnowings_ = currentKnowings.getWings();
        Set<String> secondNeighbors = getOutNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
        if(null == secondNeighbors)
            return null;
        Knowings knowings = new Knowings(source);

        ++current;
        for(String secondNeighbor: secondNeighbors){
            knowings.addKnowing(secondNeighbor, true, current, true);
        }

        return goByOutDirectionRecursively(graphType, distance, current, source, knowings);
    }

    Set<String> getNextNeighborsByInDirection(GraphType graphType, String... destinations) throws StoreException, MissingRequiredException, InvalidIdException {
        assert (null != destinations);
        Set<String> expanded = Sets.newHashSet();
        Set<String> secondNeighbors = getInNeighbors(graphType, destinations);
        if (null != secondNeighbors && secondNeighbors.size() > 0)
            expanded.addAll(secondNeighbors);

        return expanded;
    }

    Set<String> expandByInDirectionRecursively(GraphType graphType, int repeat, Set<String> destinations) throws StoreException, MissingRequiredException, InvalidIdException {
        assert (null != destinations);
        if (0 == repeat)
            return destinations;
        Set<String> expanded = Sets.newHashSet(destinations);
        Set<String> secondNeighbors = getInNeighbors(graphType, destinations.toArray(new String[destinations.size()]));
        if (null != secondNeighbors && secondNeighbors.size() > 0)
            expanded.addAll(secondNeighbors);
        --repeat;
        return expandByInDirectionRecursively(graphType, repeat, expanded);
    }


    Knowings expandByInDirectionRecursively(GraphType graphType, final int distance, int current,String source, Knowings currentKnowings) throws StoreException, MissingRequiredException, InvalidIdException {
        assert(distance >= 0);
        assert(current >= 0);

        if(distance <= current)
            return currentKnowings;

        if(0 == current){
            currentKnowings = new Knowings(source);
            currentKnowings.addKnowing(source, true, 0, false);
        }
        assert(null != currentKnowings);

        Set<String> currentKnowings_ = currentKnowings.getWings();
        Set<String> secondNeighbors = getInNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
        if(null == secondNeighbors)
            return currentKnowings;

        ++current;
        for(String secondNeighbor: secondNeighbors){
            currentKnowings.addKnowing(secondNeighbor, true, current, false);
        }

        return expandByInDirectionRecursively(graphType, distance, current, source, currentKnowings);
    }

    Knowings goByInDirectionRecursively(GraphType graphType, final int distance, int current,String source, Knowings currentKnowings) throws StoreException, MissingRequiredException, InvalidIdException {
        assert(distance >= 0);
        assert(current >= 0);

        if(distance <= current)
            return currentKnowings;

        if(0 == current){
            currentKnowings = new Knowings(source);
            currentKnowings.addKnowing(source, true, 0, false);
        }
        assert(null != currentKnowings);

        Set<String> currentKnowings_ = currentKnowings.getWings();
        Set<String> secondNeighbors = getInNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
        if(null == secondNeighbors)
            return null;
        Knowings knowings = new Knowings(source);

        ++current;
        for(String secondNeighbor: secondNeighbors){
            knowings.addKnowing(secondNeighbor, true, current, false);
        }

        return goByInDirectionRecursively(graphType, distance, current, source, knowings);
    }

    Set<String> getNextNeighborsByBothDirection(GraphType graphType, String... sources) throws StoreException, MissingRequiredException, InvalidIdException {
        assert (null != sources);
        Set<String> expanded = Sets.newHashSet();

//        if(graphType.isDirected()){
//            Set<String> secondOutNeighbors = getOutNeighbors(graphType, sources);
//            if (null != secondOutNeighbors && secondOutNeighbors.size() > 0)
//                expanded.addAll(secondOutNeighbors);
//            Set<String> secondInNeighbors = getInNeighbors(graphType, sources);
//            if (null != secondInNeighbors && secondInNeighbors.size() > 0)
//                expanded.addAll(secondInNeighbors);
//        }else{
//            expanded.addAll(orientationStore.getRelations(graphType, sources));
//        }
        expanded.addAll(getNeighbors(graphType, sources));
        return expanded;
    }

    Set<String> expandByBothDirectionRecursively(GraphType graphType, int repeat, Set<String> sources) throws StoreException, MissingRequiredException, InvalidIdException {
        assert (null != sources);
        if (0 == repeat)
            return sources;
        Set<String> expanded = Sets.newHashSet(sources);

//        if(graphType.isDirected()){
//            Set<String> secondInNeighbors = getInNeighbors(graphType, sources.toArray(new String[sources.size()]));
//            if (null != secondInNeighbors && secondInNeighbors.size() > 0)
//                expanded.addAll(secondInNeighbors);
//            Set<String> secondOutNeighbors = getOutNeighbors(graphType, sources.toArray(new String[sources.size()]));
//            if (null != secondOutNeighbors && secondOutNeighbors.size() > 0)
//                expanded.addAll(secondOutNeighbors);
//        }else{
//            Set<String> secondNeighbors = orientationStore.getRelations(graphType, sources.toArray(new String[sources.size()]));
//            expanded.addAll(secondNeighbors);
//        }
        expanded.addAll(getNeighbors(graphType, sources.toArray(new String[sources.size()])));
        --repeat;
        return expandByBothDirectionRecursively(graphType, repeat, expanded);
    }

    Knowings expandByBothDirectionRecursively(GraphType graphType, final int distance, int current,String destinationination, Knowings currentKnowings) throws StoreException, MissingRequiredException, InvalidIdException {
        assert(distance >= 0);
        assert(current >= 0);

        if(distance <= current)
            return currentKnowings;

        if(0 == current){
            currentKnowings = new Knowings(destinationination);
            currentKnowings.addKnowing(destinationination, false, 0);
        }
        assert(null != currentKnowings);

        Set<String> currentKnowings_ = currentKnowings.getWings();
        ++current;
//        if(graphType.isDirected()){
//            Set<String> secondOutNeighbors = getOutNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
//            Set<String> secondInNeighbors = getInNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
//            if(null != secondOutNeighbors && secondOutNeighbors.size() > 0){
//                for(String secondOutNeighbor: secondOutNeighbors){
//                    currentKnowings.addKnowing(secondOutNeighbor, false, current);
//                }
//            }
//            if(null != secondInNeighbors && secondInNeighbors.size() > 0){
//                for(String secondInNeighbor: secondInNeighbors){
//                    currentKnowings.addKnowing(secondInNeighbor, false, current);
//                }
//            }
//        }else{
//            Set<String> secondNeighbors = orientationStore.getRelations(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
//            if(null != secondNeighbors && secondNeighbors.size() > 0){
//                for(String secondNeighbor: secondNeighbors){
//                    currentKnowings.addKnowing(secondNeighbor, false, current);
//                }
//            }
//        }
        Set<String> secondNeighbors = getNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
        if(null != secondNeighbors && secondNeighbors.size() > 0){
            for(String secondNeighbor: secondNeighbors){
                currentKnowings.addKnowing(secondNeighbor, false, current);
            }
        }

        return expandByBothDirectionRecursively(graphType, distance, current, destinationination, currentKnowings);
    }

    Knowings goByBothDirectionRecursively(GraphType graphType, final int distance, int current,String destination, Knowings currentKnowings) throws StoreException, MissingRequiredException, InvalidIdException {
        assert(distance >= 0);
        assert(current >= 0);

        if(distance <= current)
            return currentKnowings;

        if(0 == current){
            currentKnowings = new Knowings(destination);
            currentKnowings.addKnowing(destination, false, 0);
        }
        assert(null != currentKnowings);

        Set<String> currentKnowings_ = currentKnowings.getWings();

        Knowings knowings = new Knowings(destination);
//        if(graphType.isDirected()){
//            Set<String> secondOutNeighbors = getOutNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
//            Set<String> secondInNeighbors = getInNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
//
//            ++current;
//            if(null != secondOutNeighbors && secondOutNeighbors.size() > 0){
//                for(String secondOutNeighbor: secondOutNeighbors){
//                    knowings.addKnowing(secondOutNeighbor, false, current);
//                }
//            }
//            if(null != secondInNeighbors && secondInNeighbors.size() > 0){
//                for(String secondInNeighbor: secondInNeighbors){
//                    knowings.addKnowing(secondInNeighbor, false, current);
//                }
//            }
//
//        }else{
//            Set<String> secondNeighbors = orientationStore.getRelations(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));
//
//            ++current;
//            if(null != secondNeighbors && secondNeighbors.size() > 0){
//                for(String secondNeighbor: secondNeighbors){
//                    knowings.addKnowing(secondNeighbor, false, current);
//                }
//            }
//        }
        Set<String> secondNeighbors = getNeighbors(graphType, currentKnowings_.toArray(new String[currentKnowings_.size()]));

        ++current;
        if(null != secondNeighbors && secondNeighbors.size() > 0){
            for(String secondNeighbor: secondNeighbors){
                knowings.addKnowing(secondNeighbor, false, current);
            }
        }
        return goByBothDirectionRecursively(graphType, distance, current, destination, knowings);
    }


//
//    Set<String> getNeighbors(Type type, String source) throws StoreException{
//        assert (null != source);
//        Set<String> inNeighbors = orientationStore.getInNeighbors(type, source);
//        Set<String> outNeighbors = orientationStore.getOutNeighbors(type, source);
//        if (null == inNeighbors)
//            return outNeighbors;
//        if (null == outNeighbors)
//            return inNeighbors;
//        inNeighbors.addAll(outNeighbors);
//        return inNeighbors;
//    }

    int getDepthFromSource() {
        if (this.maxDepth % 2 == 0)
            return this.maxDepth / 2;
        else
            return this.maxDepth / 2 + 1;
    }

    int getDepthFromDestination() {
        return this.maxDepth / 2;
    }
}
