package com.tripleJ.ds.relation;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.tripleJ.DuplicateIdException;
import com.tripleJ.MissingRequiredException;
import com.tripleJ.unique.InvalidIdException;

import java.util.*;

/**
 * Created by kami on 2014. 6. 18..
 */
public class RootedTree<T extends Comparable> {

    private final T root;
    private Map<T, Set<T>> treeRelation;
    private Map<T, Integer> elements;
    private Set<T> leaves;

    public RootedTree(T root) {
        this.root = root;
        init();
    }

    private void init() {
        this.treeRelation = Maps.newConcurrentMap();
        this.treeRelation.put(root, Sets.newHashSet());
        this.elements = Maps.newLinkedHashMap();
        this.elements.put(root, 0);
        this.leaves = Sets.newHashSet();
        this.leaves.add(root);
    }

    public RootedTree(List<T> elements, int[] image) throws MissingRequiredException, InvalidIdException, BrokenImageException{

        if (null == elements || null == image)
            throw new MissingRequiredException("elements and image must not be null");
        if (elements.size() != image.length)
            throw new InvalidIdException("element size and image size must be identical");
        checkFootprint(image);
        this.root = elements.get(0);
        init();
        int current = 0;
        int right = 0;
        while (current < image.length) {
            try {
                right = makeUp(current, right, image, elements);
                current++;
            }catch(DuplicateIdException de){
                throw new BrokenImageException(de);
            }catch(OrphanException oe){
                throw new BrokenImageException(oe);
            }
        }
        assert (Arrays.equals(image, makeFootprint()));
    }

    public T getRoot() {
        return root;
    }

    public Set<T> getChildren(final T parent) {
        if (null == parent)
            return null;

        if (null != treeRelation.get(parent))
            return Sets.newCopyOnWriteArraySet(treeRelation.get(parent));
        else
            return Sets.newHashSet();
    }

    public Set<T> getLeaves() {
        if (null != leaves)
            return Sets.newCopyOnWriteArraySet(leaves);
        else
            return Sets.newHashSet();
    }

    public boolean hasChildren(final T parent) {
        Set<T> children = getChildren(parent);
        if (null == children || children.size() == 0)
            return false;
        return true;
    }

    public void addChildren(final T parent, final T... children) throws DuplicateIdException, OrphanException, MissingRequiredException {
        if (null == parent || null == children || children.length == 0)
            throw new MissingRequiredException();

        if (!elements.keySet().contains(parent))
            throw new OrphanException();

        int level = elements.get(parent);

        Set<T> siblings = treeRelation.get(parent);
        if (null == siblings)
            siblings = Sets.newHashSet();
        for (T child : children) {
            if (elements.keySet().contains(child))
                throw new DuplicateIdException();
            siblings.add(child);
            elements.put(child, level + 1);
            leaves.add(child);
        }
        treeRelation.put(parent, siblings);
        leaves.remove(parent);
    }

    public List<T> getMembers() {
        List<T> sortedMembers = Lists.newLinkedList();
        sortedMembers.addAll(elements.keySet());
        Collections.sort(sortedMembers, byLevel);
        return sortedMembers;
    }

    public int[] makeFootprint() {
        int[] image = new int[elements.size()];
        int size = elements.size();
        for (int i = 0; i < size; i++) {
            Set<T> children = getChildren(getMembers().get(i));
            if (null == children)
                image[i] = 0;
            else
                image[i] = children.size();
        }
        return image;
    }

    void checkFootprint(final int[] image) throws BrokenImageException {
        int length = image.length;
        if (0 != image[length - 1])
            throw new BrokenImageException("Last member can not be non zero");
    }

    int makeUp(final int current, final int right, final int[] image, final List<T> elements) throws MissingRequiredException, DuplicateIdException, OrphanException {
        T parent = elements.get(current);

        List<T> children = elements.subList(current + right + 1, current + right + 1 + image[current]);
        for (T child : children)
            addChildren(parent, child);

        if (right > 0)
            return right - 1;
        return image[current] - 1;
    }

    Ordering<T> byLevel = new Ordering<T>() {
        @Override
        public int compare(T t, T t2) {
            int levelDiff = elements.get(t) - elements.get(t2);
            if (0 != levelDiff)
                return levelDiff;
            else
                return t.compareTo(t2);
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RootedTree that = (RootedTree) o;

        List<T> myMember = getMembers();
        List<T> yourMember = that.getMembers();

        if (null == myMember) {
            if (null == yourMember)
                return true;
            else
                return false;
        }
        if (null == yourMember)
            return false;

        if (myMember.size() != yourMember.size())
            return false;

        boolean image = Arrays.equals(this.makeFootprint(), that.makeFootprint());
        if (!image)
            return false;

        int size = myMember.size();
        for (int i = 0; i < size; i++) {
            T t1 = myMember.get(i);
            T t2 = yourMember.get(i);
            if (!t1.equals(t2))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(getMembers(), Arrays.toString(makeFootprint()));
    }

    @Override
    public String toString() {
        return "RootedTree{" +
                "root=" + root +
                ", treeRelation=" + treeRelation +
                '}';
    }
}
