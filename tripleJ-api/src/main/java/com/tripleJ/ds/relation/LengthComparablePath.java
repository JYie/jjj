package com.tripleJ.ds.relation;

import java.util.List;

public class LengthComparablePath extends Path implements Comparable<Path> {

	public LengthComparablePath(List<String> elements) {
		super(elements);
	}

	public LengthComparablePath(String... elements) {
		super(elements);
	}

	@Override
	public int compareTo(Path o) {
		return this.getLength() - o.getLength();
	}
}
