package com.tripleJ.ds.relation;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;

/**
 * @author : kami
 * Date: 13. 7. 30.
 * Time: 오후 5:58
 * To change this template use File | Settings | File Templates.
 */
public class Knowings {

    private final String pivot;

    private Set<Knowing> knowings;

    private Set<String> wings;
    private Map<String, Integer> distances;
    private Map<String, Boolean> orientations;
    private Map<String, Boolean> directions;

    public Knowings(String pivot){
        this.pivot = pivot;
        this.knowings = Sets.newHashSet();
        this.wings = Sets.newHashSet();
        this.distances = Maps.newHashMap();
        this.orientations = Maps.newHashMap();
        this.directions = Maps.newHashMap();
    }

    public String getPivot(){
        return this.pivot;
    }

    public boolean addKnowing(String destination, int distance){
        if(null == destination || distance < 0)
            return false;
        boolean duplicated = this.wings.add(destination);
        if(!duplicated)
            return false;
        this.distances.put(destination, distance);
        this.directions.put(destination, true);
        this.orientations.put(destination, true);
        return true;
//        Knowing knowing = new Knowing(pivot, destination, distance);
//        return knowings.add(knowing);
    }

    public boolean removeKnowing(String destination){
        if(null == destination)
            return false;
        boolean result = this.wings.remove(destination);
        if(!result)
            return result;
        this.distances.remove(destination);
        this.directions.remove(destination);
        this.orientations.remove(destination);
        return true;
    }

    public boolean addKnowing(String destination, boolean oriented, int distance, boolean direction){
        if(null == destination || distance < 0)
            return false;
        boolean duplicated = this.wings.add(destination);
        if(!duplicated)
            return false;
        this.distances.put(destination, distance);
        this.directions.put(destination, direction);
        this.orientations.put(destination, oriented);
        return true;
//        Knowing knowing = new Knowing(pivot, destination, oriented, distance, direction);
//        return knowings.add(knowing);
    }


    public boolean addKnowing(String destination, boolean oriented, int distance){
        if(null == destination || distance < 0)
            return false;
        boolean duplicated = this.wings.add(destination);
        if(!duplicated)
            return false;
        this.distances.put(destination, distance);
        this.orientations.put(destination, oriented);
        if(oriented)
            this.directions.put(destination, true);
        return true;
//        Knowing knowing = new Knowing(pivot, destination, oriented, distance);
//        return knowings.add(knowing);
    }

    public boolean addKnowing(Knowing knowing){
        if(null == knowing)
            return false;
        String tSource = knowing.getSource();
        if(null == tSource)
            return false;
        if(tSource.compareTo(this.pivot) != 0)
            return false;

        String destination = knowing.getDestination();
        boolean duplicated = this.wings.add(destination);
        if(!duplicated)
            return false;
        int distance = knowing.getDistance();
        boolean oriented = knowing.isOriented();
        boolean direction = knowing.getDirection();
        this.distances.put(destination, distance);
        this.orientations.put(destination, oriented);
        this.directions.put(destination, direction);
        return true;
//        return knowings.add(knowing);
    }

    public void addKnowings(Iterable<Knowing> knowings){
        if(null == knowings)
            return;
        for(Knowing knowing:knowings){
            addKnowing(knowing);
        }
    }

    public Knowing pickKnowing(String wing){
        if(null == wing)
            return null;
        Integer distance = this.distances.get(wing);
        if(null == distance)
            return null;
        boolean oriented = this.orientations.get(wing);
        if(oriented)
            return new Knowing(this.getPivot(), wing, true, this.distances.get(wing), this.directions.get(wing));
        else
            return new Knowing(this.getPivot(), wing, false, this.distances.get(wing));
    }

    /**
     * It returns immutable set of wing.
     * @return
     */
    public Set<String> getWings(){
        return ImmutableSet.copyOf(this.wings);
    }

    public int getSize(){
        if(null == this.wings)
            return 0;
        return this.wings.size();
    }

    public static Set<Knowing> getHookedKnowingsFront(Knowings from, Knowings to){
        if(null == from || null == to)
            return null;

        Set<String> fWings = from.getWings();
        Set<String> tWings = to.getWings();
        if(null == fWings || null == tWings)
            return null;
        Set<String> candidates = Sets.intersection(fWings, tWings);
        if(null == candidates || candidates.size() == 0)
            return null;

        Set<Knowing> hanging = Sets.newHashSet();
        for(String candidate : candidates){
            if(from.orientations.get(candidate)){
                if(!to.orientations.get(candidate))
                    continue;
                if(from.directions.get(candidate) && !to.directions.get(candidate))
                    hanging.add(from.pickKnowing(candidate));
                else if(!from.directions.get(candidate) && to.directions.get(candidate))
                    hanging.add(from.pickKnowing(candidate));
            }else if(!to.orientations.get(candidate)) {
                Knowing knowing = from.pickKnowing(candidate);
                hanging.add(knowing);
            }
        }
        return hanging;
    }

    public static Set<Knowing> getHookedKnowingsRear(Knowings from, Knowings to){
        if(null == from || null == to)
            return null;

        Set<String> fWings = from.getWings();
        Set<String> tWings = to.getWings();
        if(null == fWings || null == tWings)
            return null;
        Set<String> candidates = Sets.intersection(fWings, tWings);
        if(null == candidates || candidates.size() == 0)
            return null;

        Set<Knowing> hanging = Sets.newHashSet();
        for(String candidate : candidates){
            if(from.orientations.get(candidate)){
                if(!to.orientations.get(candidate))
                    continue;
                if(from.directions.get(candidate) && !to.directions.get(candidate))
                    hanging.add(to.pickKnowing(candidate));
                else if(!from.directions.get(candidate) && to.directions.get(candidate))
                    hanging.add(to.pickKnowing(candidate));
            }else if(!to.orientations.get(candidate)) {
                Knowing knowing = to.pickKnowing(candidate);
                hanging.add(knowing);
            }
        }
        return hanging;
    }
}
