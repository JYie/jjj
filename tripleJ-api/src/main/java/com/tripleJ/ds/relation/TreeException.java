package com.tripleJ.ds.relation;

import com.tripleJ.TripleJException;

/**
 * Created by kami on 2014. 7. 4..
 */
public class TreeException extends TripleJException {
    public TreeException(Exception exception) {
        super(exception);
    }

    public TreeException(String arg, Exception exception) {
        super(arg, exception);
    }

    public TreeException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TreeException(String arg0) {
        super(arg0);
    }

    public TreeException(Throwable arg0) {
        super(arg0);
    }

    public TreeException() {
    }
}
