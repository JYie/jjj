package com.tripleJ.ds.relation;

import com.tripleJ.StoreException;

import java.util.Set;

/**
 * OrientationStore is a store interface to handle a life-cycle of orientation and relation.
 * The main difference of orientation and relation is the existence of direction. First one supports the concept of source and destination, i.e it has a direction,
 * but the second one does not distinguish the direction.
 * @author kami
 *
 */
public interface OrientationStore {

    /**
     * Add a directed relation from source to destination
     * @param graphType
     * @param source uid of source entity
     * @param destination uid of destination entity
     *
     */
    void addOrientation(GraphType graphType, String source, String destination) throws StoreException;

    /**
     * Remove a directed relation from source to destination
     * @param graphType
     * @param source uid of source entity
     * @param destination uid of destination entity
     */
    void removeOrientation(GraphType graphType, String source, String destination) throws StoreException;

    /**
     * Add an undirected relation between vert1 and vert2
     * @param graphType
     * @param vert1
     * @param vert2
     * @throws StoreException
     */
    void addRelation(GraphType graphType, String vert1, String vert2) throws StoreException;

    /**
     * Remove an undirected relation between vert1 and vert2
     * @param graphType
     * @param vert1
     * @param vert2
     * @throws StoreException
     */
    void removeRelation(GraphType graphType, String vert1, String vert2) throws StoreException;

    /**
     * It returns a set of knowings which have orientations going from sources delivered.
     * @param graphType
     * @param sources
     * @return
     */
    Set<Knowing> getOutNeighbors(GraphType graphType, String... sources) throws StoreException;

    /**
     * It returns a set of knowings which have orientations going to destinations delivered.
     * @param graphType
     * @param destinations
     * @return
     * @throws StoreException
     */
    Set<Knowing> getInNeighbors(GraphType graphType, String... destinations) throws StoreException;

    /**
     * It returns a set of knowings each of which has a vertex in vertices delivered as a source or a destination.
     * @param graphType
     * @param vertices
     * @return
     * @throws StoreException
     */
    Set<Knowing> getRelations(GraphType graphType, String... vertices) throws StoreException;
}
