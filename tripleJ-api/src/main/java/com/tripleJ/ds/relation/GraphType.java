package com.tripleJ.ds.relation;

/**
 *
 * Created by kami on 2014. 7. 7..
 */
public interface GraphType {

    /**
     *
     * @return name of this type
     */
    public String getName();

    /**
     * If it returns true, this graph is a tree.
     * @return true if this graph is a tree.
     */
    public boolean isTree();

    /**
     * If it returns true if this graph allows only a single parent.
     * @return true if this graph allows only a single parent.
     */
    public boolean allowsSingleParentOnly();

    /**
     * If it returns true, it prohibits to remove the node which is not a leaf.
     * @return true if it prohibits to remove the node which is not a leaf.
     */
    public boolean leafRemovalOnly();

    /**
     * It it returns true, this graphType has a direction.
     * @return true if this graphType has a direction.
     */
    public boolean isDirected();

    /**
     * As of now, every graph returns 2
     * @return 2
     */
    public int getEdgeNumber();
}
