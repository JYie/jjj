package com.tripleJ;

import com.tripleJ.ds.relation.GraphType;

/**
 * Created by kami on 2014. 6. 17..
 */
public enum TripleJType implements GraphType{
    Account(0), Comment(1), Group(0), Request(0), Follows(2);

    private final int discriminant;

    TripleJType(int discriminant) {
        this.discriminant = discriminant;
    }

    @Override
    public String getName(){
        return this.name();
    }

    @Override
    public boolean isTree() {
        if(this.discriminant == 1)
            return true;
        return false;
    }

    @Override
    public boolean allowsSingleParentOnly(){
        if(this.discriminant == 1)
            return true;
        return false;
    }

    @Override
    public boolean leafRemovalOnly(){
        if(this.discriminant == 1)
            return true;
        return false;
    }

    @Override
    public boolean isDirected(){
        if(this.discriminant == 1 || this.discriminant == 2)
            return true;
        return false;
    }

    @Override
    public int getEdgeNumber(){
        return 2;
    }

}
