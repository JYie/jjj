package com.tripleJ.security;

import com.tripleJ.UnauthorizedException;
import com.tripleJ.service.UnrecognizableException;

/**
 * Created by kami on 2014. 6. 26..
 */
public interface AccessConstraint {

    public static enum ConstrainedActions {
        WriteComment, WriteReComment, SendMessage, SendInvitation,
        ReadComment
    }

//    public Set<String> listConstraints();

    public void giveAuthorization(String key) throws UnrecognizableException;

    public void removeAuthorization(String key) throws UnrecognizableException;

    public void checkPermission(ConstrainedActions action, int relation) throws UnrecognizableException, UnauthorizedException;
}
