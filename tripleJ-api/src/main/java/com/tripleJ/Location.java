package com.tripleJ;

import java.util.StringTokenizer;

/**
 * Created by kami on 2014. 6. 16..
 */
public class Location {

    private final String delimiter = ",";
    private final float latitude;
    private final float longitude;

    private static final String LAT = "LAT";
    private static final String LON = "LON";

    private int countryCode;
    private String providence;
    private String city;
    private String etc;

    public Location(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        transform();
    }

    public Location(String form) throws InitializeException {
        if(null == form)
            throw new InitializeException("form must not be null");

        StringTokenizer st = new StringTokenizer(form, delimiter);
        String latForm = null;
        String longForm = null;
        while(st.hasMoreTokens()){
            String token = st.nextToken();
            int index = token.indexOf(LAT);
            if(-1 != index)
                latForm = token.substring(index + 4);
            index = token.indexOf(LON);
            if(-1 != index)
                longForm = token.substring(index + 4);
        }
        if(null == latForm || null == longForm)
            throw new InitializeException("Unrecognizable Form");

        this.longitude = parseLongitude(longForm);
        this.latitude = parseLatitude(latForm);
        transform();
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public String getProvidence() {
        return providence;
    }

    public String getCity() {
        return city;
    }

    public String getEtc() {
        return etc;
    }

    /**
     * generate countryCode, providence, city, etc information from latitude and longitude;
     */
    private void transform(){

    }

    @Override
    public String toString() {
        return "Location{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", countryCode=" + countryCode +
                ", providence='" + providence + '\'' +
                ", city='" + city + '\'' +
                ", etc='" + etc + '\'' +
                '}';
    }

    public String generateForm(){
        StringBuffer sb = new StringBuffer();
        sb.append(LAT);
        sb.append("=");
        sb.append(this.latitude);
        sb.append(delimiter);
        sb.append(LON);
        sb.append("=");
        sb.append(this.longitude);
        return sb.toString();
    }


    private float parseLatitude(String form){
        return Float.parseFloat(form);
    }

    private float parseLongitude(String form){
        return Float.parseFloat(form);
    }
}
