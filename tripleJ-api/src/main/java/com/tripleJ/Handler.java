package com.tripleJ;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by kami on 2014. 7. 3..
 */
public interface Handler {

    default Calendar generateCurrentTime(){
        return Calendar.getInstance();
    }

    String generateID(Map<String, String> resources);

}
