package com.tripleJ.auth;

/**
 * Created by kami on 2014. 6. 23..
 */
public class PermissionResult {

    private final boolean permission;

    private final String reason;

    public PermissionResult(boolean result, String reason) {
        this.permission = result;
        this.reason = reason;
    }

    public boolean hasPermission() {
        return permission;
    }

    public String getReason() {
        return reason;
    }
}
