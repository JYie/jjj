package com.tripleJ.auth;

/**
 * Created by kami on 2014. 6. 16..
 */
public interface Authorizer {

    public boolean isPermissible();

}
