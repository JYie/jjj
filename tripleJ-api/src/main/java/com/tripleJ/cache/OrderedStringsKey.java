package com.tripleJ.cache;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * This class is used by the cache as a key for functions having array parameter which the order must be cared.
 * Created by kami on 2014. 7. 23..
 */
public class OrderedStringsKey {

    private final int size;
    private final List<String> origin;

    public OrderedStringsKey(String... strs) {
        this.origin = Lists.newCopyOnWriteArrayList(Lists.newArrayList(strs));
        this.size = origin.size();
    }

    public boolean isAffected(String source) {
        if (null == source)
            return false;

        return this.origin.contains(source);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderedStringsKey other = (OrderedStringsKey) obj;

        return com.google.common.base.Objects.equal(this.origin, other.origin)
                && com.google.common.base.Objects.equal(this.size, other.size);
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(this.origin, this.size);
    }

    @Override
    public String toString() {
        return "OrderedStringsKey{" +
                "origin=" + origin +
                '}';
    }
}
