package com.tripleJ.cache;

import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * This class is used by the cache as a key for functions having array parameter which the order is not cared.
 * Created by kami on 2014. 7. 23..
 */
public class UnorderedStringsKey {
    public static final String delim = ",";
    private static final Joiner engine = Joiner.on(delim).skipNulls();

    private Set<String> filtered;
    private String key;
    private int size;

    public UnorderedStringsKey(String... strs) {
        Collection<String> origin = Lists.newArrayList(strs);
        List<String> notNullAndEmpty = Lists.newArrayList(Collections2.filter(origin,
                new Predicate<String>() {
                    public boolean apply(String target) {
                        if (null == target)
                            return false;
                        return !target.isEmpty();
                    }
                }));

        this.filtered = ImmutableSortedSet.copyOf(notNullAndEmpty);
        this.size = this.filtered.size();
        generateKey();
    }

    private void generateKey() {
        this.key = engine.join(filtered);
    }

    public boolean isAffected(String source) {
        if (null == source)
            return false;
        if (null == this.filtered)
            return false;
        return this.filtered.contains(source);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnorderedStringsKey other = (UnorderedStringsKey) obj;

        return com.google.common.base.Objects.equal(this.key, other.key)
                && com.google.common.base.Objects.equal(this.size, other.size);
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(this.key, this.size);
    }

    @Override
    public String toString() {
        return Joiner.on(delim).join(this.filtered);
    }
}
