package com.tripleJ;

/**
 * Created by kami on 2014. 6. 30..
 */
public class InitializeException extends TripleJException {
    public InitializeException(Exception exception) {
        super(exception);
    }

    public InitializeException(String arg, Exception exception) {
        super(arg, exception);
    }

    public InitializeException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public InitializeException(String arg0) {
        super(arg0);
    }

    public InitializeException(Throwable arg0) {
        super(arg0);
    }

    public InitializeException() {
    }
}
