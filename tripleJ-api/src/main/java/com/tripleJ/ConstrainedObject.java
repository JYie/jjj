package com.tripleJ;

/**
 * Created by kami on 2014. 7. 18..
 */
public interface ConstrainedObject<T> {

    public boolean isConstrained(T t);
}
