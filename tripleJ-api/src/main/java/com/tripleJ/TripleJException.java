package com.tripleJ;

/**
 * This class is a basic exception class for TripleJ project. Every exception class in TripleJ inherits this class.
 * Created by kami on 2014. 6. 16..
 */
public class TripleJException extends  Exception {

    public TripleJException(Exception exception) {
        super(exception);
    }

    public TripleJException(String arg, Exception exception) {
        super(arg, exception);
    }

    public TripleJException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TripleJException(String arg0) {
        super(arg0);
    }

    public TripleJException(Throwable arg0) {
        super(arg0);
    }

    public TripleJException() {
        super();
    }
}
