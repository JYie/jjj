package com.tripleJ;

/**
 * Created by kami on 2014. 8. 1..
 */
public class SubListHelper {

    public static int getToIndex(int targetSize, int offset, int size){
        if(offset >= targetSize)
            return -1;

        int toIndex = offset + size ;

        if(toIndex <= targetSize)
            return toIndex;
        else
            return targetSize ;
    }
}
