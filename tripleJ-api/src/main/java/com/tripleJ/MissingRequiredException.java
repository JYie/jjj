package com.tripleJ;

/**
 * Created by kami on 2014. 6. 16..
 */
public class MissingRequiredException extends TripleJException{

    public MissingRequiredException(Exception exception) {

    }

    public MissingRequiredException(String arg, Exception exception) {

    }

    public MissingRequiredException(String arg0, Throwable arg1) {

    }

    public MissingRequiredException(String arg0) {

    }

    public MissingRequiredException(Throwable arg0) {

    }

    public MissingRequiredException() {

    }
}
